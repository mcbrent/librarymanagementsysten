﻿using System;
using MySql.Data.MySqlClient;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;

namespace LibraryManagementSystem.app.db
{
    class DatabaseConnection
    {
        protected MySqlConnection Connection;

        private string _server = "localhost";
        private string _database = "library-system";
        private string _user_id = "root";
        private string _password = "";
        private string _ssl_mode = "none";

        private String tableName = "";
        protected string TableName { get => tableName; set => tableName = value; }

        //Constructor
        public DatabaseConnection()
        {
            Initialize();                       
        }
        public DatabaseConnection(Boolean isInitialize)
        {
            if (isInitialize)
            {
                Initialize();
            }
        }

        //Initialize values
        private void Initialize()
        {
            String connectionString;
            _server = Properties.Settings.Default.DB_server;
            _database = Properties.Settings.Default.DB_name;
            _user_id = Properties.Settings.Default.DB_user;
            _password = Properties.Settings.Default.DB_pass;
            connectionString = "SERVER=" + _server + ";" + "DATABASE=" + _database + ";" + "UID=" + _user_id + ";" + "PASSWORD=" + _password + ";SslMode=" + _ssl_mode;

            Connection = new MySqlConnection(connectionString);
            OpenConnection();
            CloseConnection();
        }

        public Boolean TestConnection(String server, String database, String user_id, String password)
        {

            Boolean isValid = false;
            MySqlConnection testConnection = new MySqlConnection("SERVER=" + server + ";" + "DATABASE=" + database + ";" + "UID=" + user_id + ";" + "Pwd=" + password + ";SslMode=none;");
            try
            {
                testConnection.Open();
                testConnection.Close();
                isValid = true;
            }
            catch (MySqlException ex)
            {
                System.Windows.Forms.MessageBox.Show(ex.Message);
            }
            return isValid;
        }
        



        //open connection to database
        private void OpenConnection()
        {

            Connection.Open();

            
        } 

        //Close connection
        private void CloseConnection()
        {
            Connection.Close();
        }

        //Insert statement
        /// <summary>
        /// Insert Sql Statement
        /// </summary>
        /// <param name="query"></param>
        /// <param name="dic"></param>
        /// <returns>Returns last inserted ID</returns>
        public Int64 Insert(string query, Dictionary<string, string> dic)
        {
            //string query = "INSERT INTO tableinfo (name, age) VALUES('John Smith', '33')";

            //open connection
            OpenConnection();
            //create command and assign the query and connection from the constructor
            MySqlCommand cmd = new MySqlCommand(query, Connection);

            //Execute command
            loadParameters(dic, ref cmd);
            cmd.ExecuteNonQuery();

            //close connection
            CloseConnection();
            return cmd.LastInsertedId;
        }

        //Update statement
        public void Update(string query, Dictionary<string, string> dic)
        {
            //string query = "UPDATE tableinfo SET name='Joe', age='22' WHERE name='John Smith'";

            //Open connection
            OpenConnection();
            //create mysql command
            MySqlCommand cmd = new MySqlCommand();
            //Assign the query using CommandText
            cmd.CommandText = query;
            //Assign the connection using Connection
            cmd.Connection = Connection;
            loadParameters(dic, ref cmd);
            //Execute query
            cmd.ExecuteNonQuery();

            //close connection
            this.CloseConnection();
            
        }

        //Delete statement
        public void Delete(string query, Dictionary<string, string> dic)
        {
            //string query = "DELETE FROM tableinfo WHERE name='John Smith'";

            OpenConnection();
            
            MySqlCommand cmd = new MySqlCommand(query, Connection);
            loadParameters(dic, ref cmd);
            cmd.ExecuteNonQuery();
            this.CloseConnection();
            
        }

        //Select statement
        public DataTable Select(string query, Dictionary<string, string> dic = null)
        {
            DataTable dt = new DataTable();
            //Create a list to store the result


            //Open connection
            OpenConnection();
            
            //Create Command
            MySqlCommand cmd = new MySqlCommand(query, Connection);
            loadParameters(dic, ref cmd);
            dt.Load(cmd.ExecuteReader());
            CloseConnection();
            return dt;
            //Create a data reader and Execute the command
            //MySqlDataReader dataReader = cmd.ExecuteReader();
            
            ////Read the data and store them in the list
            //while (dataReader.Read())
            //{
            //    list[0].Add(dataReader["id"] + "");
            //    list[1].Add(dataReader["name"] + "");
            //    list[2].Add(dataReader["age"] + "");
            //}

            ////close Data Reader
            //dataReader.Close();

            ////close Connection
            //CloseConnection();

            ////return list to be displayed
            //return list;
        }

        //Count statement
        public int Count()
        {
            string query = "SELECT Count(*) FROM tableinfo";
            int Count = -1;

            //Open Connection
            OpenConnection();

                //Create Mysql Command
            MySqlCommand cmd = new MySqlCommand(query, Connection);

            //ExecuteScalar will return one value
            Count = int.Parse(cmd.ExecuteScalar() + "");

            //close Connection
             CloseConnection();

            return Count;

        }

        //Backup
        public void Backup()
        {
        }

        //Restore
        public void Restore()
        {
        }


        private void loadParameters(Dictionary<string, string> dic, ref MySqlCommand cmd)
        {
            if(dic != null)
            {
                foreach (KeyValuePair<string, string> data in dic)
                {
                    cmd.Parameters.AddWithValue(data.Key, data.Value);
                }
            }            
        }

    }
}
