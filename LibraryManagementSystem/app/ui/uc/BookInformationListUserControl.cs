﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using LibraryManagementSystem.app.ui.form;
using LibraryManagementSystem.app.db;

namespace LibraryManagementSystem.app.ui.uc
{
    public partial class BookInformationListUserControl : UserControl
    {
        public BookInformationListUserControl()
        {
            InitializeComponent();
        }

        public void Add()
        {
            BookInformationFormData add = new app.ui.form.BookInformationFormData();
            if (DialogResult.OK == add.ShowDialog())
            {
                DatabaseConnection dc = new DatabaseConnection();
                String authorID = "";
                if(add.BookAuthor == "0")
                {
                    String insertQuery = @"INSERT INTO tbl_author 
                                (author_name) 
                                VALUES 
                                (@author_name)";
                    Dictionary<string, string> insertDic = new Dictionary<string, string>();
                    insertDic.Add("@author_name", add.ScannedAuthor);
                    authorID = dc.Insert(insertQuery, insertDic).ToString();
                }
                else
                {
                    authorID = add.BookAuthor;
                }
                
                String query = @"INSERT INTO tbl_bookinfo 
                                (bi_isbn,
                                bi_author_id,
                                bi_title,
                                bi_bookcategory_id) 
                                VALUES 
                                (@bi_isbn,
                                @bi_author_id,
                                @bi_title,
                                @bi_bookcategory_id)";
                Dictionary<string, string> dic = new Dictionary<string, string>();
                dic.Add("@bi_isbn", add.BookISBN);
                dic.Add("@bi_author_id", authorID);
                dic.Add("@bi_title", add.BookTitle);
                dic.Add("@bi_bookcategory_id", (add.BookCategory == "-1")? null : add.BookCategory);
                dc.Insert(query, dic);
                reloadDataGridView();
            }
        }
        private void reloadDataGridView()
        {
            DatabaseConnection dc = new DatabaseConnection();
            String filter = textBoxFilterValue.Text.Trim();

            DataTable dt;
            String query = @"SELECT bi_id, bi_isbn, bi_title, category_name, author_name
                                FROM `tbl_bookinfo` 
                                LEFT JOIN `tbl_author` 
                                ON bi_author_id = author_id
                                LEFT JOIN `tbl_category` 
                                ON bi_bookcategory_id = category_id";
            if (filter == "")
            {
                dt = dc.Select(query);
            }
            else
            {
                query += @" WHERE bi_isbn LIKE @bi_isbn 
                            OR bi_title LIKE @bi_title 
                            OR category_name LIKE @category_name 
                            OR author_name LIKE @author_name 
                             ";
                Dictionary<string, string> dic = new Dictionary<string, string>();
                dic.Add("@bi_isbn", '%' + filter + '%');
                dic.Add("@bi_title", '%' + filter + '%');
                dic.Add("@category_name", '%' + filter + '%');
                dic.Add("@author_name", '%' + filter + '%');
                dt = dc.Select(query, dic);
            }

            dataGridViewRegisteredBookList.DataSource = dt;
            dataGridViewRegisteredBookList.Columns["bi_id"].Visible = false;
                        
        }
        private void buttonNew_Click(object sender, EventArgs e)
        {
            this.Add();
        }

        private void BookInformationListUserControl_Load(object sender, EventArgs e)
        {
            reloadDataGridView();
        }

        private void buttonEdit_Click(object sender, EventArgs e)
        {
            if (dataGridViewRegisteredBookList.SelectedRows.Count > 0)
            {
                DatabaseConnection dc = new DatabaseConnection();
                Dictionary<string, string> dic = new Dictionary<string, string>();
                String bi_id = dataGridViewRegisteredBookList["bi_id", dataGridViewRegisteredBookList.CurrentRow.Index].Value.ToString();
                dic.Add("bi_id", bi_id);
                String query = @"SELECT * FROM tbl_bookinfo WHERE bi_id = @bi_id";
                DataTable dt = dc.Select(query, dic);
                BookInformationFormData add = new BookInformationFormData();
                add.BookISBN = dt.Rows[0]["bi_isbn"].ToString();
                add.BookAuthor = dt.Rows[0]["bi_author_id"].ToString();
                add.BookTitle = dt.Rows[0]["bi_title"].ToString();
                add.BookCategory = dt.Rows[0]["bi_bookcategory_id"].ToString();

                if (DialogResult.OK == add.ShowDialog())
                {
                    String authorID = "";
                    if (add.BookAuthor == "0")
                    {
                        String insertQuery = @"INSERT INTO tbl_author 
                                (author_name) 
                                VALUES 
                                (@author_name)";
                        Dictionary<string, string> insertDic = new Dictionary<string, string>();
                        insertDic.Add("@author_name", add.ScannedAuthor);
                        authorID = dc.Insert(insertQuery, insertDic).ToString();
                    }
                    else
                    {
                        authorID = add.BookAuthor;
                    }
                    Dictionary<string, string> update = new Dictionary<string, string>();
                    String updateQuery = @"UPDATE tbl_bookinfo 
                                        SET bi_isbn = @bi_isbn,
                                        bi_title = @bi_title,
                                        bi_author_id = @bi_author_id,
                                        bi_bookcategory_id = @bi_bookcategory_id
                                        WHERE bi_id = @bi_id";
                    update.Add("@bi_isbn", add.BookISBN);
                    update.Add("@bi_title", add.BookTitle);
                    update.Add("@bi_author_id", authorID);
                    update.Add("@bi_bookcategory_id", add.BookCategory);
                    update.Add("@bi_id", bi_id);

                    dc.Update(updateQuery, update);
                    reloadDataGridView();
                }
            }
        }

        private void buttonDelete_Click(object sender, EventArgs e)
        {
            if (dataGridViewRegisteredBookList.SelectedRows.Count > 0)
            {
                DatabaseConnection dc = new DatabaseConnection();
                Dictionary<string, string> dic = new Dictionary<string, string>();
                String bi_id = dataGridViewRegisteredBookList["bi_id", dataGridViewRegisteredBookList.CurrentRow.Index].Value.ToString();
                dic.Add("bi_id", bi_id);
                DialogResult dr = MessageBox.Show(this, "Are you sure you want to Delete this book information? All Books associated with this book information will also be unusable. (Borrowed, Return, etc.)", "Deleting Book Information", MessageBoxButtons.YesNo, MessageBoxIcon.Stop, MessageBoxDefaultButton.Button1);
                if(dr == DialogResult.Yes)
                {
                    Dictionary<string, string> dicDelete = new Dictionary<string, string>();
                    String queryDelete = @"DELETE FROM tbl_bookinfo WHERE bi_id = @bi_id";
                    dicDelete.Add("@bi_id", bi_id);
                    dc.Delete(queryDelete, dicDelete);
                    Dictionary<string, string> dicBook = new Dictionary<string, string>();
                    String queryDeleteBook = @"DELETE FROM tbl_book WHERE book_bi_id = @book_bi_id";
                    dicBook.Add("@book_bi_id", bi_id);
                    dc.Delete(queryDeleteBook, dicBook);
                    reloadDataGridView();
                }
                
            }
        }

        private void textBoxFilterValue_TextChanged(object sender, EventArgs e)
        {
            reloadDataGridView();
        }
    }
}
