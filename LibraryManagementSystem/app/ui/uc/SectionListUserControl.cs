﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using LibraryManagementSystem.app.db;
using LibraryManagementSystem.app.ui.form;

namespace LibraryManagementSystem.app.ui.uc
{
    public partial class SectionListUserControl : UserControl
    {
        public SectionListUserControl()
        {
            InitializeComponent();
        }

        public void Add()
        {
            SectionFormData add = new SectionFormData();
            if (DialogResult.OK == add.ShowDialog())
            {
                DatabaseConnection dc = new DatabaseConnection();
                String query = "INSERT INTO tbl_section (section_name) VALUES (@section_name)";
                Dictionary<string, string> dic = new Dictionary<string, string>();
                dic.Add("@section_name", add.Section);
                dc.Insert(query, dic);
                reloadDataGridView();
            }
        }

        private void buttonNew_Click(object sender, EventArgs e)
        {
            this.Add();
        }

        private void SectionListUserControl_Load(object sender, EventArgs e)
        {
            reloadDataGridView();
        }

        private void reloadDataGridView()
        {
            DatabaseConnection dc = new DatabaseConnection();
            String filter = textBoxFilterValue.Text.Trim();

            DataTable dt;
            if (filter == "")
            {
                dt = dc.Select("SELECT section_id, section_name FROM tbl_section");
            }
            else
            {
                Dictionary<string, string> dic = new Dictionary<string, string>();
                dic.Add("@section_name", '%' + filter + '%');
                dt = dc.Select("SELECT section_id, section_name FROM tbl_section WHERE section_name LIKE @section_name", dic);
            }

            dataGridViewSectionList.DataSource = dt;
            dataGridViewSectionList.Columns["section_id"].Visible = false;
            dataGridViewSectionList.Columns["section_name"].HeaderText = "Section Name";
            dataGridViewSectionList.Columns["section_name"].HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter;

        }

        private void buttonEdit_Click(object sender, EventArgs e)
        {
            if (dataGridViewSectionList.SelectedRows.Count > 0)
            {
                DatabaseConnection dc = new DatabaseConnection();
                Dictionary<string, string> dic = new Dictionary<string, string>();
                String section_id = dataGridViewSectionList["section_id", dataGridViewSectionList.CurrentRow.Index].Value.ToString();
                dic.Add("section_id", section_id);
                DataTable dt = dc.Select("SELECT * FROM tbl_section WHERE section_id = @section_id", dic);
                SectionFormData add = new SectionFormData();
                add.Section = dt.Rows[0]["section_name"].ToString();
                
                if (DialogResult.OK == add.ShowDialog())
                {
                    String query = "UPDATE tbl_section SET section_name = @section_name WHERE section_id = @section_id";
                    Dictionary<string, string> update = new Dictionary<string, string>();
                    update.Add("@section_id", section_id);
                    update.Add("@section_name", add.Section);
                    dc.Update(query, update);
                    reloadDataGridView();
                }
            }
        }

        private void buttonDelete_Click(object sender, EventArgs e)
        {
            if (dataGridViewSectionList.SelectedRows.Count > 0)
            {
                DialogResult dr = MessageBox.Show(this, "Are you sure you would like to delete this?", "Delete", MessageBoxButtons.YesNo, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button2);
                if (dr == DialogResult.Yes)
                {
                    DatabaseConnection dc = new DatabaseConnection();
                    String section_id = dataGridViewSectionList["section_id", dataGridViewSectionList.CurrentRow.Index].Value.ToString();
                    String query = "DELETE FROM tbl_section WHERE section_id = @section_id";
                    Dictionary<string, string> update = new Dictionary<string, string>();
                    update.Add("@section_id", section_id);
                    dc.Delete(query, update);
                    reloadDataGridView();
                }                
            }
            
        }

        private void buttonSearch_Click(object sender, EventArgs e)
        {
            reloadDataGridView();
        }

        private void textBoxFilterValue_TextChanged(object sender, EventArgs e)
        {

        }
    }
}
