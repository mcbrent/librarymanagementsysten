﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using LibraryManagementSystem.app.ui.form;
using LibraryManagementSystem.app.db;
using System.IO;
using System.Reflection;

namespace LibraryManagementSystem.app.ui.uc
{
    public partial class StudentListUserControl : UserControl
    {
        String defaultImage = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location) + "\\res\\user.png";
        public StudentListUserControl()
        {
            InitializeComponent();
        }

        public void Add()
        {
            StudentFormData add = new app.ui.form.StudentFormData();
            add.IDPicture = defaultImage;
            add.IsEdit = false;
            if (DialogResult.OK == add.ShowDialog())
            {
                MessageBox.Show(add.IDPicture);
                DatabaseConnection dc = new DatabaseConnection();
                String queryFileManager = @"INSERT INTO tbl_file_manager 
                                (fm_file_name) 
                                VALUES 
                                (@fm_file_name)";
                Dictionary<string, string> dicFileManager = new Dictionary<string, string>();
                String imageName = SaveImage(add.IDPicture);
                dicFileManager.Add("@fm_file_name", imageName);
                String fm_id = dc.Insert(queryFileManager, dicFileManager).ToString();

                String query = @"INSERT INTO tbl_student 
                                (student_id,
                                student_lastname,
                                student_firstname,
                                student_middlename,
                                student_birthdate,
                                student_gender,
                                student_fatherfullname,
                                student_motherfullname,
                                student_cnumber1,
                                student_cnumber2,
                                student_address,
                                student_section_id,
                                student_emergencycname,
                                student_emergencycnumber,
                                student_emergencycaddress,
                                student_emergencycrelation,
                                student_adviser_id,
                                student_fm_id) 
                                VALUES 
                                (@student_id,
                                @student_lastname,
                                @student_firstname,
                                @student_middlename,
                                @student_birthdate,
                                @student_gender,
                                @student_fatherfullname,
                                @student_motherfullname,
                                @student_cnumber1,
                                @student_cnumber2,
                                @student_address,
                                @student_section_id,
                                @student_emergencycname,
                                @student_emergencycnumber,
                                @student_emergencycaddress,
                                @student_emergencycrelation,
                                @student_adviser_id,
                                @student_fm_id)";
                Dictionary<string, string> dic = new Dictionary<string, string>();
                dic.Add("@student_id", add.StudentNo);
                dic.Add("@student_lastname", add.LastName);
                dic.Add("@student_firstname", add.FirstName);
                dic.Add("@student_middlename", add.MiddleName);
                dic.Add("@student_birthdate", add.Birthdate.ToString("yyyy-MM-dd"));
                dic.Add("@student_gender", add.Gender);
                dic.Add("@student_fatherfullname", add.FatherName);
                dic.Add("@student_motherfullname", add.MotherName);
                dic.Add("@student_cnumber1", add.MobileNumber);
                dic.Add("@student_cnumber2", add.TelephoneNumber);
                dic.Add("@student_address", add.Address);
                dic.Add("@student_section_id", add.Section);
                dic.Add("@student_emergencycname", add.EContactName);
                dic.Add("@student_emergencycnumber", add.EContactNumber);
                dic.Add("@student_emergencycaddress", add.EAddress);
                dic.Add("@student_emergencycrelation", add.ERelation);
                dic.Add("@student_adviser_id", add.Adviser);
                dic.Add("@student_fm_id", fm_id);
                dc.Insert(query, dic);
                

                reloadDataGridView();
            }
        }
        private void reloadDataGridView()
        {
            DatabaseConnection dc = new DatabaseConnection();
            String filter = textBoxFilterValue.Text.Trim();
            String filterQuery = "WHERE (student_id LIKE @student_id OR " +
                                "CONCAT(student_lastname, ', ', student_firstname, ' ', student_middlename) LIKE @student OR " +
                                "section_name LIKE @section_name OR adviser_name LIKE @adviser_name  OR student_gender LIKE @student_gender OR " +
                                "student_address = @student_address)";

            DataTable dt;
            String query = @"SELECT student_id as `ID`, 
                                CONCAT(student_lastname, ', ', student_firstname, ' ', student_middlename) as student,
                                section_name as `Section`,
                                adviser_name as `Adviser`,
                                student_gender as `Gender`,
                                TIMESTAMPDIFF(YEAR, student_birthdate, CURDATE()) AS Age,
                                student_address as `Address`
                                FROM `tbl_student` 
                                LEFT JOIN `tbl_section` 
                                ON student_section_id = section_id
                                LEFT JOIN `tbl_adviser` 
                                ON student_adviser_id = adviser_id ";
            if (filter == "")
            {
                dt = dc.Select(query);
            }
            else
            {
                Dictionary<string, string> dic = new Dictionary<string, string>();
                dic.Add("@student_id", '%' + filter + '%');
                dic.Add("@student", '%' + filter + '%');
                dic.Add("@section_name", '%' + filter + '%');
                dic.Add("@adviser_name", '%' + filter + '%');
                dic.Add("@student_gender", '%' + filter + '%');
                dic.Add("@student_address", '%' + filter + '%');
                dt = dc.Select(query + filterQuery, dic);
            }

            dataGridViewStudentList.DataSource = dt;
            
        }
        private void buttonNew_Click(object sender, EventArgs e)
        {
            this.Add();
        }

        private void StudentListUserControl_Load(object sender, EventArgs e)
        {
            reloadDataGridView();
        }

        private void buttonEdit_Click(object sender, EventArgs e)
        {
            if (dataGridViewStudentList.SelectedRows.Count > 0)
            {
                DatabaseConnection dc = new DatabaseConnection();
                Dictionary<string, string> dic = new Dictionary<string, string>();
                String student_id = dataGridViewStudentList["ID", dataGridViewStudentList.CurrentRow.Index].Value.ToString();
                dic.Add("student_id", student_id);
                String query = @"SELECT * FROM tbl_student WHERE student_id = @student_id";
                DataTable dt = dc.Select(query, dic);
                Dictionary<string, string> dicImage = new Dictionary<string, string>();
                dicImage.Add("fm_id", dt.Rows[0]["student_fm_id"].ToString());
                String queryImage = @"SELECT * FROM tbl_file_manager WHERE fm_id = @fm_id";
                DataTable dtImage = dc.Select(queryImage, dicImage);
                StudentFormData add = new StudentFormData();
                add.StudentNo = dt.Rows[0]["student_id"].ToString();
                add.FirstName = dt.Rows[0]["student_firstname"].ToString();
                add.MiddleName = dt.Rows[0]["student_middlename"].ToString();
                add.LastName = dt.Rows[0]["student_lastname"].ToString();
                add.Birthdate = DateTime.Parse(dt.Rows[0]["student_birthdate"].ToString());
                add.Gender = dt.Rows[0]["student_gender"].ToString();
                add.FatherName = dt.Rows[0]["student_fatherfullname"].ToString();
                add.MotherName = dt.Rows[0]["student_motherfullname"].ToString();
                add.MobileNumber = dt.Rows[0]["student_cnumber1"].ToString();
                add.TelephoneNumber = dt.Rows[0]["student_cnumber2"].ToString();
                add.TelephoneNumber = dt.Rows[0]["student_cnumber2"].ToString();
                add.EContactName = dt.Rows[0]["student_emergencycname"].ToString();
                add.ERelation = dt.Rows[0]["student_emergencycrelation"].ToString();
                add.EContactNumber = dt.Rows[0]["student_emergencycnumber"].ToString();
                add.EAddress = dt.Rows[0]["student_emergencycaddress"].ToString();
                add.IDPicture = loadImage(dtImage.Rows[0]["fm_file_name"].ToString());
                add.IsEdit = true;
                //DialogResult dr = add.ShowDialog();
                if (DialogResult.OK == add.ShowDialog())
                {
                    if(dtImage.Rows[0]["fm_file_name"].ToString() != Path.GetFileName(add.IDPicture))
                    {
                        updateImage(add.IDPicture, dtImage.Rows[0]["fm_file_name"].ToString());
                    }
                    Dictionary<string, string> update = new Dictionary<string, string>();
                    update.Add("@student_id", add.StudentNo);
                    update.Add("@student_lastname", add.LastName);
                    update.Add("@student_firstname", add.FirstName);
                    update.Add("@student_middlename", add.MiddleName);
                    update.Add("@student_birthdate", add.Birthdate.ToString("yyyy-MM-dd"));
                    update.Add("@student_gender", add.Gender);
                    update.Add("@student_fatherfullname", add.FatherName);
                    update.Add("@student_motherfullname", add.MotherName);
                    update.Add("@student_cnumber1", add.MobileNumber);
                    update.Add("@student_cnumber2", add.TelephoneNumber);
                    update.Add("@student_address", add.Address);
                    update.Add("@student_section_id", add.Section);
                    update.Add("@student_emergencycname", add.EContactName);
                    update.Add("@student_emergencycnumber", add.EContactNumber);
                    update.Add("@student_emergencycaddress", add.EAddress);
                    update.Add("@student_emergencycrelation", add.ERelation);
                    update.Add("@student_adviser_id", add.Adviser);
                    update.Add("@id", student_id);
                    String updateQuery = @"UPDATE tbl_student 
                                            SET student_id = @student_id,
                                            student_lastname = @student_lastname,
                                            student_firstname = @student_firstname,
                                            student_middlename = @student_middlename,
                                            student_birthdate = @student_birthdate,
                                            student_gender = @student_gender,
                                            student_fatherfullname = @student_fatherfullname,
                                            student_motherfullname = @student_motherfullname,
                                            student_cnumber1 = @student_cnumber1,
                                            student_cnumber2 = @student_cnumber2,
                                            student_address = @student_address,
                                            student_section_id = @student_section_id,
                                            student_emergencycname = @student_emergencycname,
                                            student_emergencycnumber = @student_emergencycnumber,
                                            student_emergencycaddress = @student_emergencycaddress,                                            
                                            student_emergencycrelation = @student_emergencycrelation,
                                            student_adviser_id = @student_adviser_id
                                            WHERE student_id = @id";
                    //Dictionary<string, string> update = new Dictionary<string, string>();
                    //update.Add("@section_id", section_id);
                    //update.Add("@section_name", add.Section);
                    //dc.Update(query, update);
                    dc.Update(updateQuery, update);
                    reloadDataGridView();
                }
            }
        }

        private String SaveImage(String image_name)
        {
            String profileDir = "\\student";
            DatabaseConnection dc = new DatabaseConnection();
            Dictionary<string, string> dic = new Dictionary<string, string>();
            dic.Add("settings_key", "FILE_PATH");
            String query = @"SELECT * FROM tbl_settings WHERE 	settings_key = @settings_key";
            DataTable dt = dc.Select(query, dic);

            string fileName = Path.GetFileName(image_name);
            String newImageName = Guid.NewGuid().ToString("N") + Path.GetExtension(fileName);
            
            string sourcePath = image_name;
            string targetPath = dt.Rows[0]["settings_value"].ToString() + profileDir;

           
            if (!Directory.Exists(targetPath))
            {
                Directory.CreateDirectory(targetPath);
            }
            string sourceFile = sourcePath;
            string destFile = Path.Combine(targetPath, newImageName);
            File.Copy(sourceFile, destFile, true);

            return newImageName;
        }

        private String loadImage(String imageName)
        {
            String profileDir = "\\student";
            DatabaseConnection dc = new DatabaseConnection();
            Dictionary<string, string> dic = new Dictionary<string, string>();
            dic.Add("settings_key", "FILE_PATH");
            String query = @"SELECT * FROM tbl_settings WHERE 	settings_key = @settings_key";
            DataTable dt = dc.Select(query, dic);
            string targetPath = dt.Rows[0]["settings_value"].ToString() + profileDir;
            return Path.Combine(targetPath, imageName);
        }
        private String updateImage(String sourceImage, String imageName)
        {
            String profileDir = "\\student";
            DatabaseConnection dc = new DatabaseConnection();
            Dictionary<string, string> dic = new Dictionary<string, string>();
            dic.Add("settings_key", "FILE_PATH");
            String query = @"SELECT * FROM tbl_settings WHERE 	settings_key = @settings_key";
            DataTable dt = dc.Select(query, dic);

            String newImageName = imageName;
            string sourcePath = sourceImage;
            string targetPath = dt.Rows[0]["settings_value"].ToString() + profileDir;
            
            if (!Directory.Exists(targetPath))
            {
                Directory.CreateDirectory(targetPath);
            }
            string sourceFile = sourcePath;
            string destFile = Path.Combine(targetPath, newImageName);
            File.Copy(sourceFile, destFile, true);

            return newImageName;
        }

        private void buttonDelete_Click(object sender, EventArgs e)
        {
            if (dataGridViewStudentList.SelectedRows.Count > 0)
            {
                DialogResult dr = MessageBox.Show(this, "Are you sure you want to delete? There's no way to undo this action", "Deleting Student Record", MessageBoxButtons.YesNo, MessageBoxIcon.Stop, MessageBoxDefaultButton.Button2);
                if(dr == DialogResult.Yes)
                {
                    DatabaseConnection dc = new DatabaseConnection();
                    Dictionary<string, string> dic = new Dictionary<string, string>();
                    String student_id = dataGridViewStudentList["ID", dataGridViewStudentList.CurrentRow.Index].Value.ToString();
                    dic.Add("student_id", student_id);
                    String query = @"DELETE FROM tbl_student WHERE student_id = @student_id";
                    dc.Delete(query, dic);
                    reloadDataGridView();
                }
            }
        }

        private void textBoxFilterValue_TextChanged(object sender, EventArgs e)
        {
            reloadDataGridView();
        }

        private void buttonView_Click(object sender, EventArgs e)
        {
            if (dataGridViewStudentList.SelectedRows.Count > 0)
            {
                DatabaseConnection dc = new DatabaseConnection();
                Dictionary<string, string> dic = new Dictionary<string, string>();
                String student_id = dataGridViewStudentList["ID", dataGridViewStudentList.CurrentRow.Index].Value.ToString();
                dic.Add("student_id", student_id);
                String query = @"SELECT * FROM tbl_student WHERE student_id = @student_id";
                DataTable dt = dc.Select(query, dic);
                Dictionary<string, string> dicImage = new Dictionary<string, string>();
                dicImage.Add("fm_id", dt.Rows[0]["student_fm_id"].ToString());
                String queryImage = @"SELECT * FROM tbl_file_manager WHERE fm_id = @fm_id";
                DataTable dtImage = dc.Select(queryImage, dicImage);
                StudentViewFormData view = new StudentViewFormData();
                view.StudentNo = dt.Rows[0]["student_id"].ToString();
                view.FirstName = dt.Rows[0]["student_firstname"].ToString();
                view.MiddleName = dt.Rows[0]["student_middlename"].ToString();
                view.LastName = dt.Rows[0]["student_lastname"].ToString();
                view.Birthdate = DateTime.Parse(dt.Rows[0]["student_birthdate"].ToString());
                view.Gender = dt.Rows[0]["student_gender"].ToString();
                view.FatherName = dt.Rows[0]["student_fatherfullname"].ToString();
                view.MotherName = dt.Rows[0]["student_motherfullname"].ToString();
                view.MobileNumber = dt.Rows[0]["student_cnumber1"].ToString();
                view.TelephoneNumber = dt.Rows[0]["student_cnumber2"].ToString();
                view.TelephoneNumber = dt.Rows[0]["student_cnumber2"].ToString();
                view.EContactName = dt.Rows[0]["student_emergencycname"].ToString();
                view.ERelation = dt.Rows[0]["student_emergencycrelation"].ToString();
                view.EContactNumber = dt.Rows[0]["student_emergencycnumber"].ToString();
                view.EAddress = dt.Rows[0]["student_emergencycaddress"].ToString();
                view.IDPicture = loadImage(dtImage.Rows[0]["fm_file_name"].ToString());
                //DialogResult dr = add.ShowDialog();
                if (DialogResult.OK == view.ShowDialog())
                {

                }
            }
        }
    }
}
