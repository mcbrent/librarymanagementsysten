﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using LibraryManagementSystem.app.ui.form;
using LibraryManagementSystem.app.db;
using LibraryManagementSystem.app.lib;

namespace LibraryManagementSystem.app.ui.uc
{
    public partial class TransactionLogUserControl : UserControl
    {
        public event EventHandler ReturnButton;
        public event EventHandler BorrowButton;
        private Reference refLib;
        DatabaseConnection dc;
        private RFIDController rfidC;
        public TransactionLogUserControl()
        {
            InitializeComponent();
            dc = new DatabaseConnection();
            rfidC = new RFIDController();

            refLib = new Reference();
            Initial();
        }

        public void Initial()
        {
            
            
        }       

        public void reloadLog()
        {
            String query = @"SELECT ts_date as 'Date', ts_type as 'Type', borrow_tcode as 'Transaction Code', (CASE 
                            WHEN `borrow_type` = 1 THEN CONCAT(student_lastname, ', ', student_firstname, ' ', student_middlename) 
                            WHEN `borrow_type` = 2 THEN adviser_name
                            WHEN `borrow_type` = 3 THEN borrow_other
                            ELSE ''
                            END) 'Name', SUM(1) as 'No. Of Books', 
                            (CASE
                                WHEN ts_type = 'Borrow' THEN borrow_notes
                                WHEN ts_type = 'Return' THEN borrow_return_notes
                                ELSE ''
                            END
                            ) as 'Notes'
                            FROM `tbl_transaction_summary` 
                            INNER JOIN `tbl_borrow`
                            ON `ts_borrow_id` = `borrow_id`
                            INNER JOIN `tbl_borrow_detail`
                            ON `bd_borrow_id` = `borrow_id`
                            LEFT JOIN `tbl_student`
                            ON `borrow_student_id` = `student_id`
                            LEFT JOIN `tbl_adviser`
                            ON `borrow_adviser_id` = `adviser_id`
                            GROUP BY ts_date, ts_type, borrow_tcode, Name
                            ORDER BY ts_date DESC";
            DataTable dt = dc.Select(query);
            dataGridViewTransactionLog.DataSource = dt;
        }
        

        private void BurrowBookUserControl_Load(object sender, EventArgs e)
        {

            reloadLog();
        }

        private void buttonReturnBook_Click(object sender, EventArgs e)
        {
            ReturnButton(this, EventArgs.Empty);
        }

        private void buttonBorrowBook_Click(object sender, EventArgs e)
        {
            BorrowButton(this, EventArgs.Empty);
        }
    }
}
