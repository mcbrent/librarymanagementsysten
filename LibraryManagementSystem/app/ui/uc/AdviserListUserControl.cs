﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using LibraryManagementSystem.app.db;
using LibraryManagementSystem.app.ui.form;

namespace LibraryManagementSystem.app.ui.uc
{
    public partial class AdviserListUserControl : UserControl
    {
        public AdviserListUserControl()
        {
            InitializeComponent();
        }

        public void Add()
        {
            AdviserFormData add = new AdviserFormData();
            if (DialogResult.OK == add.ShowDialog())
            {
                DatabaseConnection dc = new DatabaseConnection();
                String query = "INSERT INTO tbl_adviser (adviser_name) VALUES (@adviser_name)";
                Dictionary<string, string> dic = new Dictionary<string, string>();
                dic.Add("@adviser_name", add.Adviser);
                dc.Insert(query, dic);
                reloadDataGridView();
            }
        }

        private void buttonNew_Click(object sender, EventArgs e)
        {
            this.Add();
        }

        private void reloadDataGridView()
        {
            DatabaseConnection dc = new DatabaseConnection();
            String filter = textBoxFilterValue.Text.Trim();

            DataTable dt;
            if (filter == "")
            {
                dt = dc.Select("SELECT adviser_id, adviser_name FROM tbl_adviser");
            }
            else
            {
                Dictionary<string, string> dic = new Dictionary<string, string>();
                dic.Add("@adviser_name", '%' + filter + '%');
                dt = dc.Select("SELECT adviser_id, adviser_name FROM tbl_adviser WHERE adviser_name LIKE @adviser_name", dic);
            }

            dataGridViewAdviserList.DataSource = dt;
            dataGridViewAdviserList.Columns["adviser_id"].Visible = false;
            dataGridViewAdviserList.Columns["adviser_name"].HeaderText = "Adviser Name";
            dataGridViewAdviserList.Columns["adviser_name"].HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter;

        }

        private void buttonEdit_Click(object sender, EventArgs e)
        {
            if (dataGridViewAdviserList.SelectedRows.Count > 0)
            {
                DatabaseConnection dc = new DatabaseConnection();
                Dictionary<string, string> dic = new Dictionary<string, string>();
                String adviser_id = dataGridViewAdviserList["adviser_id", dataGridViewAdviserList.CurrentRow.Index].Value.ToString();
                dic.Add("adviser_id", adviser_id);
                DataTable dt = dc.Select("SELECT * FROM tbl_adviser WHERE adviser_id = @adviser_id", dic);
                AdviserFormData add = new AdviserFormData();
                add.Adviser = dt.Rows[0]["adviser_name"].ToString();
                
                if (DialogResult.OK == add.ShowDialog())
                {
                    String query = "UPDATE tbl_adviser SET adviser_name = @adviser_name WHERE adviser_id = @adviser_id";
                    Dictionary<string, string> update = new Dictionary<string, string>();
                    update.Add("@adviser_id", adviser_id);
                    update.Add("@adviser_name", add.Adviser);
                    dc.Update(query, update);
                    reloadDataGridView();
                }
            }
        }

        private void buttonDelete_Click(object sender, EventArgs e)
        {
            if (dataGridViewAdviserList.SelectedRows.Count > 0)
            {
                DialogResult dr = MessageBox.Show(this, "Are you sure you would like to delete this?", "Delete", MessageBoxButtons.YesNo, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button2);
                if (dr == DialogResult.Yes)
                {
                    DatabaseConnection dc = new DatabaseConnection();
                    String adviser_id = dataGridViewAdviserList["adviser_id", dataGridViewAdviserList.CurrentRow.Index].Value.ToString();
                    String query = "DELETE FROM tbl_adviser WHERE adviser_id = @adviser_id";
                    Dictionary<string, string> update = new Dictionary<string, string>();
                    update.Add("@adviser_id", adviser_id);
                    dc.Delete(query, update);
                    reloadDataGridView();
                }                
            }
            
        }

        private void buttonSearch_Click(object sender, EventArgs e)
        {
            reloadDataGridView();
        }

        private void AdviserListUserControl_Load(object sender, EventArgs e)
        {
            reloadDataGridView();
        }
    }
}
