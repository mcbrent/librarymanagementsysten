﻿namespace LibraryManagementSystem.app.ui.uc
{
    partial class ReturnBookUserControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panelBody = new System.Windows.Forms.Panel();
            this.groupBoxBookInformation = new System.Windows.Forms.GroupBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.label11 = new System.Windows.Forms.Label();
            this.labelBookStatus = new System.Windows.Forms.Label();
            this.buttonScanBook = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.buttonAddToList = new System.Windows.Forms.Button();
            this.labelBookTitle = new System.Windows.Forms.Label();
            this.labelScannedBook = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.buttonScanAndAddToList = new System.Windows.Forms.Button();
            this.labelBookAuthor = new System.Windows.Forms.Label();
            this.textBoxBookNotes = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.labelBookCategory = new System.Windows.Forms.Label();
            this.labelBookISBN = new System.Windows.Forms.Label();
            this.groupBoxReturnBook = new System.Windows.Forms.GroupBox();
            this.dataGridViewBookList = new System.Windows.Forms.DataGridView();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.panelBorrowerFaculty = new System.Windows.Forms.Panel();
            this.comboBoxFaculty = new System.Windows.Forms.ComboBox();
            this.label4 = new System.Windows.Forms.Label();
            this.panelBorrowerOther = new System.Windows.Forms.Panel();
            this.comboBoxOther = new System.Windows.Forms.ComboBox();
            this.labelOther = new System.Windows.Forms.Label();
            this.panelBorrowerStudent = new System.Windows.Forms.Panel();
            this.label5 = new System.Windows.Forms.Label();
            this.textBoxStudentID = new System.Windows.Forms.TextBox();
            this.comboBoxStudentName = new System.Windows.Forms.ComboBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.labelSection = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.comboBoxBorrowerType = new System.Windows.Forms.ComboBox();
            this.panelBorrowerType = new System.Windows.Forms.Panel();
            this.buttonViewTransaction = new System.Windows.Forms.Button();
            this.comboBoxTransactions = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.textBoxReturnNotes = new System.Windows.Forms.TextBox();
            this.labelNotes = new System.Windows.Forms.Label();
            this.labeltitle = new System.Windows.Forms.Label();
            this.buttonReset = new System.Windows.Forms.Button();
            this.buttonCompleteTransaction = new System.Windows.Forms.Button();
            this.panelHeader = new System.Windows.Forms.Panel();
            this.label6 = new System.Windows.Forms.Label();
            this.labelBookCardNo = new System.Windows.Forms.Label();
            this.panelBody.SuspendLayout();
            this.groupBoxBookInformation.SuspendLayout();
            this.panel1.SuspendLayout();
            this.groupBoxReturnBook.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewBookList)).BeginInit();
            this.groupBox2.SuspendLayout();
            this.panelBorrowerFaculty.SuspendLayout();
            this.panelBorrowerOther.SuspendLayout();
            this.panelBorrowerStudent.SuspendLayout();
            this.panelHeader.SuspendLayout();
            this.SuspendLayout();
            // 
            // panelBody
            // 
            this.panelBody.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panelBody.Controls.Add(this.groupBoxBookInformation);
            this.panelBody.Controls.Add(this.groupBoxReturnBook);
            this.panelBody.Controls.Add(this.groupBox2);
            this.panelBody.Controls.Add(this.labeltitle);
            this.panelBody.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelBody.Location = new System.Drawing.Point(0, 52);
            this.panelBody.Name = "panelBody";
            this.panelBody.Size = new System.Drawing.Size(1374, 606);
            this.panelBody.TabIndex = 4;
            // 
            // groupBoxBookInformation
            // 
            this.groupBoxBookInformation.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBoxBookInformation.Controls.Add(this.panel1);
            this.groupBoxBookInformation.Enabled = false;
            this.groupBoxBookInformation.ForeColor = System.Drawing.Color.White;
            this.groupBoxBookInformation.Location = new System.Drawing.Point(770, 32);
            this.groupBoxBookInformation.Name = "groupBoxBookInformation";
            this.groupBoxBookInformation.Size = new System.Drawing.Size(589, 549);
            this.groupBoxBookInformation.TabIndex = 11;
            this.groupBoxBookInformation.TabStop = false;
            this.groupBoxBookInformation.Text = "Book Information";
            // 
            // panel1
            // 
            this.panel1.AutoScroll = true;
            this.panel1.Controls.Add(this.labelBookCardNo);
            this.panel1.Controls.Add(this.label6);
            this.panel1.Controls.Add(this.label11);
            this.panel1.Controls.Add(this.labelBookStatus);
            this.panel1.Controls.Add(this.buttonScanBook);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.label17);
            this.panel1.Controls.Add(this.buttonAddToList);
            this.panel1.Controls.Add(this.labelBookTitle);
            this.panel1.Controls.Add(this.labelScannedBook);
            this.panel1.Controls.Add(this.label15);
            this.panel1.Controls.Add(this.buttonScanAndAddToList);
            this.panel1.Controls.Add(this.labelBookAuthor);
            this.panel1.Controls.Add(this.textBoxBookNotes);
            this.panel1.Controls.Add(this.label13);
            this.panel1.Controls.Add(this.label9);
            this.panel1.Controls.Add(this.labelBookCategory);
            this.panel1.Controls.Add(this.labelBookISBN);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(3, 26);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(583, 520);
            this.panel1.TabIndex = 25;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(61, 53);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(90, 23);
            this.label11.TabIndex = 16;
            this.label11.Text = "Book ISBN";
            // 
            // labelBookStatus
            // 
            this.labelBookStatus.AutoSize = true;
            this.labelBookStatus.Location = new System.Drawing.Point(155, 194);
            this.labelBookStatus.Name = "labelBookStatus";
            this.labelBookStatus.Size = new System.Drawing.Size(157, 23);
            this.labelBookStatus.TabIndex = 24;
            this.labelBookStatus.Tag = "0";
            this.labelBookStatus.Text = "<< Book Status >>";
            // 
            // buttonScanBook
            // 
            this.buttonScanBook.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonScanBook.BackColor = System.Drawing.Color.RoyalBlue;
            this.buttonScanBook.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonScanBook.Image = global::LibraryManagementSystem.Properties.Resources.RFID_Tag_white_26px;
            this.buttonScanBook.Location = new System.Drawing.Point(15, 353);
            this.buttonScanBook.Name = "buttonScanBook";
            this.buttonScanBook.Size = new System.Drawing.Size(549, 50);
            this.buttonScanBook.TabIndex = 4;
            this.buttonScanBook.Tag = "0";
            this.buttonScanBook.Text = "Scan Book";
            this.buttonScanBook.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.buttonScanBook.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.buttonScanBook.UseVisualStyleBackColor = false;
            this.buttonScanBook.Click += new System.EventHandler(this.buttonScanBook_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(43, 194);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(99, 23);
            this.label1.TabIndex = 23;
            this.label1.Text = "Book Status";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(67, 86);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(85, 23);
            this.label17.TabIndex = 10;
            this.label17.Text = "Book Title";
            // 
            // buttonAddToList
            // 
            this.buttonAddToList.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonAddToList.BackColor = System.Drawing.Color.RoyalBlue;
            this.buttonAddToList.Enabled = false;
            this.buttonAddToList.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonAddToList.Image = global::LibraryManagementSystem.Properties.Resources.Plus_white_26px;
            this.buttonAddToList.Location = new System.Drawing.Point(15, 407);
            this.buttonAddToList.Name = "buttonAddToList";
            this.buttonAddToList.Size = new System.Drawing.Size(549, 50);
            this.buttonAddToList.TabIndex = 22;
            this.buttonAddToList.Tag = "0";
            this.buttonAddToList.Text = "Add To Return List";
            this.buttonAddToList.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.buttonAddToList.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.buttonAddToList.UseVisualStyleBackColor = false;
            this.buttonAddToList.Click += new System.EventHandler(this.buttonAddToList_Click);
            // 
            // labelBookTitle
            // 
            this.labelBookTitle.AutoSize = true;
            this.labelBookTitle.Location = new System.Drawing.Point(164, 86);
            this.labelBookTitle.Name = "labelBookTitle";
            this.labelBookTitle.Size = new System.Drawing.Size(143, 23);
            this.labelBookTitle.TabIndex = 11;
            this.labelBookTitle.Text = "<< Book Title >>";
            // 
            // labelScannedBook
            // 
            this.labelScannedBook.AutoSize = true;
            this.labelScannedBook.Location = new System.Drawing.Point(29, 327);
            this.labelScannedBook.Name = "labelScannedBook";
            this.labelScannedBook.Size = new System.Drawing.Size(68, 23);
            this.labelScannedBook.TabIndex = 21;
            this.labelScannedBook.Tag = "0";
            this.labelScannedBook.Text = "ID Here";
            this.labelScannedBook.Visible = false;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(45, 121);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(106, 23);
            this.label15.TabIndex = 12;
            this.label15.Text = "Book Author";
            // 
            // buttonScanAndAddToList
            // 
            this.buttonScanAndAddToList.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonScanAndAddToList.BackColor = System.Drawing.Color.RoyalBlue;
            this.buttonScanAndAddToList.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonScanAndAddToList.Image = global::LibraryManagementSystem.Properties.Resources.Add_List_white_26px;
            this.buttonScanAndAddToList.Location = new System.Drawing.Point(15, 463);
            this.buttonScanAndAddToList.Name = "buttonScanAndAddToList";
            this.buttonScanAndAddToList.Size = new System.Drawing.Size(549, 50);
            this.buttonScanAndAddToList.TabIndex = 20;
            this.buttonScanAndAddToList.Tag = "0";
            this.buttonScanAndAddToList.Text = "Scan && Add To Return List";
            this.buttonScanAndAddToList.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.buttonScanAndAddToList.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.buttonScanAndAddToList.UseVisualStyleBackColor = false;
            this.buttonScanAndAddToList.Click += new System.EventHandler(this.buttonScanAndAddToList_Click);
            // 
            // labelBookAuthor
            // 
            this.labelBookAuthor.AutoSize = true;
            this.labelBookAuthor.Location = new System.Drawing.Point(164, 121);
            this.labelBookAuthor.Name = "labelBookAuthor";
            this.labelBookAuthor.Size = new System.Drawing.Size(164, 23);
            this.labelBookAuthor.TabIndex = 13;
            this.labelBookAuthor.Text = "<< Book Author >>";
            // 
            // textBoxBookNotes
            // 
            this.textBoxBookNotes.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textBoxBookNotes.BackColor = System.Drawing.Color.White;
            this.textBoxBookNotes.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBoxBookNotes.Location = new System.Drawing.Point(160, 230);
            this.textBoxBookNotes.Multiline = true;
            this.textBoxBookNotes.Name = "textBoxBookNotes";
            this.textBoxBookNotes.ReadOnly = true;
            this.textBoxBookNotes.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.textBoxBookNotes.Size = new System.Drawing.Size(394, 84);
            this.textBoxBookNotes.TabIndex = 19;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(29, 156);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(122, 23);
            this.label13.TabIndex = 14;
            this.label13.Text = "Book Category";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(41, 228);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(98, 23);
            this.label9.TabIndex = 18;
            this.label9.Text = "Book Notes";
            // 
            // labelBookCategory
            // 
            this.labelBookCategory.AutoSize = true;
            this.labelBookCategory.Location = new System.Drawing.Point(164, 156);
            this.labelBookCategory.Name = "labelBookCategory";
            this.labelBookCategory.Size = new System.Drawing.Size(180, 23);
            this.labelBookCategory.TabIndex = 15;
            this.labelBookCategory.Text = "<< Book Category >>";
            // 
            // labelBookISBN
            // 
            this.labelBookISBN.AutoSize = true;
            this.labelBookISBN.Location = new System.Drawing.Point(164, 53);
            this.labelBookISBN.Name = "labelBookISBN";
            this.labelBookISBN.Size = new System.Drawing.Size(148, 23);
            this.labelBookISBN.TabIndex = 17;
            this.labelBookISBN.Text = "<< Book ISBN >>";
            // 
            // groupBoxReturnBook
            // 
            this.groupBoxReturnBook.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.groupBoxReturnBook.Controls.Add(this.dataGridViewBookList);
            this.groupBoxReturnBook.Enabled = false;
            this.groupBoxReturnBook.ForeColor = System.Drawing.Color.White;
            this.groupBoxReturnBook.Location = new System.Drawing.Point(12, 365);
            this.groupBoxReturnBook.Name = "groupBoxReturnBook";
            this.groupBoxReturnBook.Size = new System.Drawing.Size(741, 217);
            this.groupBoxReturnBook.TabIndex = 10;
            this.groupBoxReturnBook.TabStop = false;
            this.groupBoxReturnBook.Text = "Return Book";
            // 
            // dataGridViewBookList
            // 
            this.dataGridViewBookList.AllowUserToAddRows = false;
            this.dataGridViewBookList.AllowUserToDeleteRows = false;
            this.dataGridViewBookList.AllowUserToResizeColumns = false;
            this.dataGridViewBookList.AllowUserToResizeRows = false;
            this.dataGridViewBookList.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dataGridViewBookList.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.dataGridViewBookList.BackgroundColor = System.Drawing.Color.White;
            this.dataGridViewBookList.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewBookList.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridViewBookList.Location = new System.Drawing.Point(3, 26);
            this.dataGridViewBookList.MultiSelect = false;
            this.dataGridViewBookList.Name = "dataGridViewBookList";
            this.dataGridViewBookList.ReadOnly = true;
            this.dataGridViewBookList.RowHeadersVisible = false;
            this.dataGridViewBookList.RowTemplate.Height = 24;
            this.dataGridViewBookList.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridViewBookList.Size = new System.Drawing.Size(735, 188);
            this.dataGridViewBookList.TabIndex = 1;
            this.dataGridViewBookList.DataSourceChanged += new System.EventHandler(this.dataGridViewBookList_DataSourceChanged);
            this.dataGridViewBookList.RowsAdded += new System.Windows.Forms.DataGridViewRowsAddedEventHandler(this.dataGridViewBookList_RowsAdded);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.panelBorrowerFaculty);
            this.groupBox2.Controls.Add(this.panelBorrowerOther);
            this.groupBox2.Controls.Add(this.panelBorrowerStudent);
            this.groupBox2.Controls.Add(this.label3);
            this.groupBox2.Controls.Add(this.comboBoxBorrowerType);
            this.groupBox2.Controls.Add(this.panelBorrowerType);
            this.groupBox2.Controls.Add(this.buttonViewTransaction);
            this.groupBox2.Controls.Add(this.comboBoxTransactions);
            this.groupBox2.Controls.Add(this.label2);
            this.groupBox2.Controls.Add(this.textBoxReturnNotes);
            this.groupBox2.Controls.Add(this.labelNotes);
            this.groupBox2.ForeColor = System.Drawing.Color.White;
            this.groupBox2.Location = new System.Drawing.Point(12, 32);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(741, 329);
            this.groupBox2.TabIndex = 9;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Returnee Information";
            // 
            // panelBorrowerFaculty
            // 
            this.panelBorrowerFaculty.Controls.Add(this.comboBoxFaculty);
            this.panelBorrowerFaculty.Controls.Add(this.label4);
            this.panelBorrowerFaculty.Location = new System.Drawing.Point(397, 0);
            this.panelBorrowerFaculty.Name = "panelBorrowerFaculty";
            this.panelBorrowerFaculty.Size = new System.Drawing.Size(31, 18);
            this.panelBorrowerFaculty.TabIndex = 20;
            this.panelBorrowerFaculty.Visible = false;
            this.panelBorrowerFaculty.DockChanged += new System.EventHandler(this.panelBorrowerFaculty_DockChanged);
            // 
            // comboBoxFaculty
            // 
            this.comboBoxFaculty.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxFaculty.FormattingEnabled = true;
            this.comboBoxFaculty.Location = new System.Drawing.Point(98, 11);
            this.comboBoxFaculty.Name = "comboBoxFaculty";
            this.comboBoxFaculty.Size = new System.Drawing.Size(665, 31);
            this.comboBoxFaculty.TabIndex = 19;
            this.comboBoxFaculty.SelectionChangeCommitted += new System.EventHandler(this.comboBoxFaculty_SelectionChangeCommitted);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(20, 12);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(62, 23);
            this.label4.TabIndex = 18;
            this.label4.Text = "Faculty";
            // 
            // panelBorrowerOther
            // 
            this.panelBorrowerOther.Controls.Add(this.comboBoxOther);
            this.panelBorrowerOther.Controls.Add(this.labelOther);
            this.panelBorrowerOther.Location = new System.Drawing.Point(441, 1);
            this.panelBorrowerOther.Name = "panelBorrowerOther";
            this.panelBorrowerOther.Size = new System.Drawing.Size(26, 33);
            this.panelBorrowerOther.TabIndex = 21;
            this.panelBorrowerOther.Visible = false;
            this.panelBorrowerOther.DockChanged += new System.EventHandler(this.panelBorrowerOther_DockChanged);
            // 
            // comboBoxOther
            // 
            this.comboBoxOther.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxOther.FormattingEnabled = true;
            this.comboBoxOther.Location = new System.Drawing.Point(88, 21);
            this.comboBoxOther.Name = "comboBoxOther";
            this.comboBoxOther.Size = new System.Drawing.Size(585, 31);
            this.comboBoxOther.TabIndex = 20;
            this.comboBoxOther.SelectionChangeCommitted += new System.EventHandler(this.comboBoxOther_SelectionChangeCommitted);
            // 
            // labelOther
            // 
            this.labelOther.AutoSize = true;
            this.labelOther.Location = new System.Drawing.Point(25, 25);
            this.labelOther.Name = "labelOther";
            this.labelOther.Size = new System.Drawing.Size(54, 23);
            this.labelOther.TabIndex = 18;
            this.labelOther.Text = "Other";
            // 
            // panelBorrowerStudent
            // 
            this.panelBorrowerStudent.Controls.Add(this.label5);
            this.panelBorrowerStudent.Controls.Add(this.textBoxStudentID);
            this.panelBorrowerStudent.Controls.Add(this.comboBoxStudentName);
            this.panelBorrowerStudent.Controls.Add(this.label7);
            this.panelBorrowerStudent.Controls.Add(this.label8);
            this.panelBorrowerStudent.Controls.Add(this.labelSection);
            this.panelBorrowerStudent.Location = new System.Drawing.Point(362, 6);
            this.panelBorrowerStudent.Name = "panelBorrowerStudent";
            this.panelBorrowerStudent.Size = new System.Drawing.Size(28, 43);
            this.panelBorrowerStudent.TabIndex = 19;
            this.panelBorrowerStudent.Visible = false;
            this.panelBorrowerStudent.DockChanged += new System.EventHandler(this.panelBorrowerStudent_DockChanged);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(35, 22);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(91, 23);
            this.label5.TabIndex = 1;
            this.label5.Text = "Student ID";
            // 
            // textBoxStudentID
            // 
            this.textBoxStudentID.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textBoxStudentID.Location = new System.Drawing.Point(133, 19);
            this.textBoxStudentID.Name = "textBoxStudentID";
            this.textBoxStudentID.Size = new System.Drawing.Size(0, 30);
            this.textBoxStudentID.TabIndex = 0;
            this.textBoxStudentID.KeyDown += new System.Windows.Forms.KeyEventHandler(this.textBoxStudentID_KeyDown);
            this.textBoxStudentID.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textBoxStudentID_KeyPress);
            // 
            // comboBoxStudentName
            // 
            this.comboBoxStudentName.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.comboBoxStudentName.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxStudentName.FormattingEnabled = true;
            this.comboBoxStudentName.Location = new System.Drawing.Point(133, 55);
            this.comboBoxStudentName.Name = "comboBoxStudentName";
            this.comboBoxStudentName.Size = new System.Drawing.Size(0, 24);
            this.comboBoxStudentName.TabIndex = 8;
            this.comboBoxStudentName.SelectionChangeCommitted += new System.EventHandler(this.comboBoxStudentName_SelectionChangeCommitted);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(70, 58);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(56, 23);
            this.label7.TabIndex = 9;
            this.label7.Text = "Name";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(58, 94);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(66, 23);
            this.label8.TabIndex = 10;
            this.label8.Text = "Section";
            // 
            // labelSection
            // 
            this.labelSection.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.labelSection.AutoSize = true;
            this.labelSection.Location = new System.Drawing.Point(136, 94);
            this.labelSection.Name = "labelSection";
            this.labelSection.Size = new System.Drawing.Size(0, 23);
            this.labelSection.TabIndex = 11;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(12, 77);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(119, 23);
            this.label3.TabIndex = 20;
            this.label3.Text = "Borrower Type";
            // 
            // comboBoxBorrowerType
            // 
            this.comboBoxBorrowerType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxBorrowerType.FormattingEnabled = true;
            this.comboBoxBorrowerType.Items.AddRange(new object[] {
            "Student",
            "Faculty",
            "Other"});
            this.comboBoxBorrowerType.Location = new System.Drawing.Point(137, 72);
            this.comboBoxBorrowerType.Name = "comboBoxBorrowerType";
            this.comboBoxBorrowerType.Size = new System.Drawing.Size(286, 31);
            this.comboBoxBorrowerType.TabIndex = 19;
            this.comboBoxBorrowerType.SelectedIndexChanged += new System.EventHandler(this.comboBoxBorrowerType_SelectedIndexChanged);
            // 
            // panelBorrowerType
            // 
            this.panelBorrowerType.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panelBorrowerType.Location = new System.Drawing.Point(5, 121);
            this.panelBorrowerType.Name = "panelBorrowerType";
            this.panelBorrowerType.Size = new System.Drawing.Size(730, 112);
            this.panelBorrowerType.TabIndex = 18;
            // 
            // buttonViewTransaction
            // 
            this.buttonViewTransaction.BackgroundImage = global::LibraryManagementSystem.Properties.Resources.Search_26px;
            this.buttonViewTransaction.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.buttonViewTransaction.Location = new System.Drawing.Point(680, 29);
            this.buttonViewTransaction.Name = "buttonViewTransaction";
            this.buttonViewTransaction.Size = new System.Drawing.Size(35, 34);
            this.buttonViewTransaction.TabIndex = 16;
            this.buttonViewTransaction.UseVisualStyleBackColor = true;
            this.buttonViewTransaction.Click += new System.EventHandler(this.buttonViewTransaction_Click);
            // 
            // comboBoxTransactions
            // 
            this.comboBoxTransactions.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxTransactions.FormattingEnabled = true;
            this.comboBoxTransactions.Location = new System.Drawing.Point(136, 32);
            this.comboBoxTransactions.Name = "comboBoxTransactions";
            this.comboBoxTransactions.Size = new System.Drawing.Size(538, 31);
            this.comboBoxTransactions.TabIndex = 15;
            this.comboBoxTransactions.SelectionChangeCommitted += new System.EventHandler(this.comboBoxTransactions_SelectionChangeCommitted);
            this.comboBoxTransactions.DataSourceChanged += new System.EventHandler(this.comboBoxTransactions_DataSourceChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(11, 35);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(119, 23);
            this.label2.TabIndex = 14;
            this.label2.Text = "Transaction ID";
            // 
            // textBoxReturnNotes
            // 
            this.textBoxReturnNotes.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textBoxReturnNotes.Location = new System.Drawing.Point(136, 245);
            this.textBoxReturnNotes.MaxLength = 255;
            this.textBoxReturnNotes.Multiline = true;
            this.textBoxReturnNotes.Name = "textBoxReturnNotes";
            this.textBoxReturnNotes.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.textBoxReturnNotes.Size = new System.Drawing.Size(596, 71);
            this.textBoxReturnNotes.TabIndex = 13;
            // 
            // labelNotes
            // 
            this.labelNotes.AutoSize = true;
            this.labelNotes.Location = new System.Drawing.Point(71, 245);
            this.labelNotes.Name = "labelNotes";
            this.labelNotes.Size = new System.Drawing.Size(55, 23);
            this.labelNotes.TabIndex = 12;
            this.labelNotes.Text = "Notes";
            // 
            // labeltitle
            // 
            this.labeltitle.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.labeltitle.Location = new System.Drawing.Point(8, 6);
            this.labeltitle.Name = "labeltitle";
            this.labeltitle.Size = new System.Drawing.Size(1347, 23);
            this.labeltitle.TabIndex = 2;
            this.labeltitle.Text = "Return Books";
            this.labeltitle.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // buttonReset
            // 
            this.buttonReset.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonReset.BackColor = System.Drawing.Color.RoyalBlue;
            this.buttonReset.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonReset.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.buttonReset.Location = new System.Drawing.Point(1155, 5);
            this.buttonReset.Name = "buttonReset";
            this.buttonReset.Size = new System.Drawing.Size(210, 43);
            this.buttonReset.TabIndex = 13;
            this.buttonReset.Text = "Reset";
            this.buttonReset.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.buttonReset.UseVisualStyleBackColor = false;
            this.buttonReset.Click += new System.EventHandler(this.buttonReset_Click);
            // 
            // buttonCompleteTransaction
            // 
            this.buttonCompleteTransaction.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonCompleteTransaction.BackColor = System.Drawing.Color.RoyalBlue;
            this.buttonCompleteTransaction.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonCompleteTransaction.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.buttonCompleteTransaction.Location = new System.Drawing.Point(939, 5);
            this.buttonCompleteTransaction.Name = "buttonCompleteTransaction";
            this.buttonCompleteTransaction.Size = new System.Drawing.Size(210, 43);
            this.buttonCompleteTransaction.TabIndex = 12;
            this.buttonCompleteTransaction.Text = "Complete Transaction";
            this.buttonCompleteTransaction.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.buttonCompleteTransaction.UseVisualStyleBackColor = false;
            this.buttonCompleteTransaction.Click += new System.EventHandler(this.buttonCompleteTransaction_Click);
            // 
            // panelHeader
            // 
            this.panelHeader.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panelHeader.Controls.Add(this.buttonCompleteTransaction);
            this.panelHeader.Controls.Add(this.buttonReset);
            this.panelHeader.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelHeader.Location = new System.Drawing.Point(0, 0);
            this.panelHeader.Name = "panelHeader";
            this.panelHeader.Size = new System.Drawing.Size(1374, 52);
            this.panelHeader.TabIndex = 14;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(37, 22);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(121, 23);
            this.label6.TabIndex = 16;
            this.label6.Text = "Book Card No.";
            // 
            // labelBookCardNo
            // 
            this.labelBookCardNo.AutoSize = true;
            this.labelBookCardNo.Location = new System.Drawing.Point(164, 23);
            this.labelBookCardNo.Name = "labelBookCardNo";
            this.labelBookCardNo.Size = new System.Drawing.Size(179, 23);
            this.labelBookCardNo.TabIndex = 25;
            this.labelBookCardNo.Text = "<< Book Card No. >>";
            // 
            // ReturnBookUserControl
            // 
            this.AutoScroll = true;
            this.BackColor = System.Drawing.Color.Navy;
            this.Controls.Add(this.panelBody);
            this.Controls.Add(this.panelHeader);
            this.Font = new System.Drawing.Font("Segoe UI", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ForeColor = System.Drawing.Color.White;
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.Name = "ReturnBookUserControl";
            this.Size = new System.Drawing.Size(1374, 658);
            this.Load += new System.EventHandler(this.ReturnBookUserControl_Load);
            this.panelBody.ResumeLayout(false);
            this.groupBoxBookInformation.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.groupBoxReturnBook.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewBookList)).EndInit();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.panelBorrowerFaculty.ResumeLayout(false);
            this.panelBorrowerFaculty.PerformLayout();
            this.panelBorrowerOther.ResumeLayout(false);
            this.panelBorrowerOther.PerformLayout();
            this.panelBorrowerStudent.ResumeLayout(false);
            this.panelBorrowerStudent.PerformLayout();
            this.panelHeader.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Panel panelBody;
        private System.Windows.Forms.Button buttonScanBook;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox textBoxStudentID;
        private System.Windows.Forms.ComboBox comboBoxStudentName;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label labelSection;
        private System.Windows.Forms.GroupBox groupBoxReturnBook;
        private System.Windows.Forms.DataGridView dataGridViewBookList;
        private System.Windows.Forms.GroupBox groupBoxBookInformation;
        private System.Windows.Forms.TextBox textBoxBookNotes;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label labelBookISBN;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label labelBookCategory;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label labelBookAuthor;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label labelBookTitle;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Button buttonScanAndAddToList;
        private System.Windows.Forms.Label labelScannedBook;
        private System.Windows.Forms.Button buttonAddToList;
        private System.Windows.Forms.Button buttonReset;
        private System.Windows.Forms.Button buttonCompleteTransaction;
        private System.Windows.Forms.Panel panelHeader;
        private System.Windows.Forms.Label labelBookStatus;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label labeltitle;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.TextBox textBoxReturnNotes;
        private System.Windows.Forms.Label labelNotes;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button buttonViewTransaction;
        private System.Windows.Forms.ComboBox comboBoxTransactions;
        private System.Windows.Forms.Panel panelBorrowerType;
        private System.Windows.Forms.Panel panelBorrowerStudent;
        private System.Windows.Forms.Panel panelBorrowerOther;
        private System.Windows.Forms.Label labelOther;
        private System.Windows.Forms.Panel panelBorrowerFaculty;
        private System.Windows.Forms.ComboBox comboBoxFaculty;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ComboBox comboBoxBorrowerType;
        private System.Windows.Forms.ComboBox comboBoxOther;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label labelBookCardNo;
    }
}
