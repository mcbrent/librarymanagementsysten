﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using LibraryManagementSystem.app.ui.form;
using LibraryManagementSystem.app.db;

namespace LibraryManagementSystem.app.ui.uc
{
    public partial class BookCategoryListUserControl : UserControl
    {
        public BookCategoryListUserControl()
        {
            InitializeComponent();
        }

        public void Add()
        {
            BookCategoryFormData add = new BookCategoryFormData();
            if (DialogResult.OK == add.ShowDialog())
            {
                DatabaseConnection dc = new DatabaseConnection();
                String query = @"INSERT INTO tbl_category 
                                (category_name) 
                                VALUES 
                                (@category_name)";
                Dictionary<string, string> dic = new Dictionary<string, string>();
                dic.Add("@category_name", add.CategoryName);
                dc.Insert(query, dic);
                reloadDataGridView();
            }
        }
        private void reloadDataGridView()
        {
            DatabaseConnection dc = new DatabaseConnection();
            String filter = textBoxFilterValue.Text.Trim();

            DataTable dt;
            String query = @"SELECT *
                                FROM `tbl_category`";
            if (filter == "")
            {
                dt = dc.Select(query);
            }
            else
            {
                Dictionary<string, string> dic = new Dictionary<string, string>();
                dic.Add("@category_name", '%' + filter + '%');
                dt = dc.Select(query, dic);
            }

            dataGridViewCategoryList.DataSource = dt;
            dataGridViewCategoryList.Columns["category_id"].Visible = false;
            dataGridViewCategoryList.Columns["category_name"].HeaderText = "Category";
            dataGridViewCategoryList.Columns["category_name"].HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter;

        }
        private void buttonNew_Click(object sender, EventArgs e)
        {
            this.Add();
        }

        private void BookCategoryListUserControl_Load(object sender, EventArgs e)
        {
            reloadDataGridView();
        }

        private void buttonEdit_Click(object sender, EventArgs e)
        {
            if (dataGridViewCategoryList.SelectedRows.Count > 0)
            {
                DatabaseConnection dc = new DatabaseConnection();
                Dictionary<string, string> dic = new Dictionary<string, string>();
                String category_id = dataGridViewCategoryList["category_id", dataGridViewCategoryList.CurrentRow.Index].Value.ToString();
                dic.Add("category_id", category_id);
                String query = @"SELECT * FROM tbl_category WHERE category_id = @category_id";
                DataTable dt = dc.Select(query, dic);
                BookCategoryFormData add = new BookCategoryFormData();
                add.CategoryName = dt.Rows[0]["category_name"].ToString();

                if (DialogResult.OK == add.ShowDialog())
                {
                    Dictionary<string, string> update = new Dictionary<string, string>();
                    update.Add("@category_name", add.CategoryName);
                    dc.Update(query, dic);
                    //String query = "UPDATE tbl_section SET section_name = @section_name WHERE section_id = @section_id";
                    //Dictionary<string, string> update = new Dictionary<string, string>();
                    //update.Add("@section_id", section_id);
                    //update.Add("@section_name", add.Section);
                    //dc.Update(query, update);
                    reloadDataGridView();
                }
            }
        }

        private void buttonDelete_Click(object sender, EventArgs e)
        {
            if (dataGridViewCategoryList.SelectedRows.Count > 0)
            {
                DatabaseConnection dc = new DatabaseConnection();
                Dictionary<string, string> dic = new Dictionary<string, string>();
                String category_id = dataGridViewCategoryList["category_id", dataGridViewCategoryList.CurrentRow.Index].Value.ToString();
                dic.Add("category_id", category_id);
                DialogResult dr = MessageBox.Show(this, "Are you sure you want to delete this category?", "Deleting Category", MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button2);

                //add.CategoryName = dt.Rows[0]["category_name"].ToString();

                if (dr == DialogResult.Yes)
                {
                    Dictionary<string, string> update = new Dictionary<string, string>();
                    String query = "DELETE FROM tbl_category WHERE category_id = @category_id";
                    dc.Delete(query, dic);
                    reloadDataGridView();
                }
            }
        }
    }
}
