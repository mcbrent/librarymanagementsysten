﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using LibraryManagementSystem.app.ui.form;
using LibraryManagementSystem.app.db;
using LibraryManagementSystem.app.lib;

namespace LibraryManagementSystem.app.ui.uc
{
    public partial class BurrowBookUserControl : UserControl
    {
        public event EventHandler ResetButton;
        private Reference refLib;
        DatabaseConnection dc;
        private RFIDController rfidC;
        public BurrowBookUserControl()
        {
            InitializeComponent();
            dc = new DatabaseConnection();
            rfidC = new RFIDController();

            refLib = new Reference();
            Initial();
        }

        public void Initial()
        {
            String query = @"SELECT student_id, CONCAT(student_lastname, ', ', student_firstname, ' ', student_middlename) as `student_name`
                                FROM tbl_student ";
            DataTable dt = dc.Select(query);
            DataRow dr = dt.NewRow();
            dr["student_id"] = "0";     
            dt.Rows.InsertAt(dr, 0);
            comboBoxStudentName.DataSource = dt;
            comboBoxStudentName.DisplayMember = "student_name";
            comboBoxStudentName.ValueMember = "student_id";


            DataTable dtBorrow = new DataTable();
            dtBorrow.Columns.Add("Book_ID");
            dtBorrow.Columns.Add("Book Name");
            dtBorrow.Columns.Add("Book Card No.");
            
            dataGridViewBookList.DataSource = dtBorrow;
            dataGridViewBookList.Columns["Book_ID"].Visible = false;

            labelTransactionID.Text = refLib.getTransactionSeries("transaction", false);


            DataTable dtAdviser = dc.Select("Select * FROM tbl_adviser");
            DataRow drAdviser = dtAdviser.NewRow();
            drAdviser["adviser_id"] = "0";
            drAdviser["adviser_name"] = "";
            dtAdviser.Rows.InsertAt(drAdviser, 0);
            comboBoxFaculty.DataSource = dtAdviser;
            comboBoxFaculty.DisplayMember = "adviser_name";
            comboBoxFaculty.ValueMember = "adviser_id";
            
            dateTimePickerReturn.Value = DateTime.Now.AddDays(3);
        }

        private void textBoxStudentID_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                Dictionary<string, string> dic = new Dictionary<string, string>();
                dic.Add("@student_id", textBoxStudentID.Text.Trim());
                String query = @"SELECT * 
                                FROM tbl_student 
                                WHERE student_id = @student_id ";
                DataTable dt = dc.Select(query, dic);
                if (dt.Rows.Count > 0)
                {
                    setBorrower(dt.Rows[0]["student_id"].ToString());
                }
                    
                
                //   processSearchedISBN();
                //enter key is down
            }
        }

        private void setBorrower(String StudentID)
        {
            comboBoxStudentName.SelectedValue = StudentID;
            Dictionary<string, string> dicStudent = new Dictionary<string, string>();
            dicStudent.Add("@student_id", comboBoxStudentName.SelectedValue.ToString());
            String queryStudent = @"SELECT * 
                                FROM tbl_student 
                                WHERE student_id = @student_id ";
            DataTable dtStudent = dc.Select(queryStudent, dicStudent);

            Dictionary<string, string> dic = new Dictionary<string, string>();
            dic.Add("@section_id", dtStudent.Rows[0]["student_section_id"].ToString());
            String query = @"SELECT *
                                FROM tbl_section 
                                WHERE section_id = @section_id ";
            DataTable dtSection = dc.Select(query, dic);
            labelSection.Text = dtSection.Rows[0]["section_name"].ToString();
        }

        private void comboBoxStudentName_SelectionChangeCommitted(object sender, EventArgs e)
        {
            if (comboBoxStudentName.SelectedValue.ToString() != "0")
            {
                setBorrower(comboBoxStudentName.SelectedValue.ToString());
                textBoxStudentID.Text = "";
            }
            else
            {
                labelSection.Text = "";
            }
        }

        private void buttonScanBook_Click(object sender, EventArgs e)
        {
            DataTable dtBook = scanBook();
            if(dtBook != null)
            {
                labelScannedBook.Tag = dtBook.Rows[0]["book_id"].ToString();
                labelBookCardNo.Text = dtBook.Rows[0]["book_call_no"].ToString();
                labelBookISBN.Text = dtBook.Rows[0]["bi_isbn"].ToString();
                labelBookTitle.Text = dtBook.Rows[0]["bi_title"].ToString();
                labelBookAuthor.Text = dtBook.Rows[0]["author_name"].ToString();
                labelBookCategory.Text = dtBook.Rows[0]["category_name"].ToString();
                textBoxBookNotes.Text = dtBook.Rows[0]["book_notes"].ToString();
                labelBookStatus.Tag = dtBook.Rows[0]["book_status"].ToString();
                if (labelBookStatus.Tag.ToString() == "0")
                {
                    labelBookStatus.Text = "Available for Borrowing";
                    labelBookStatus.ForeColor = Color.Green;
                    buttonAddToList.Enabled = true;
                }
                else
                {
                    labelBookStatus.Text = "Not Available for Borrowing";
                    labelBookStatus.ForeColor = Color.Red;
                    buttonAddToList.Enabled = false;
                }


                
            }
        }



        private DataTable scanBook()
        {
            DataTable dtBook = null;
            if (rfidC.Connect())
            {
                if (rfidC.CheckCard())
                {
                    String Rfid = rfidC.Read();
                    Dictionary<string, string> dic = new Dictionary<string, string>();
                    dic.Add("@book_rfid", Rfid);
                    String query = @"SELECT * 
                                FROM tbl_book
                                LEFT JOIN tbl_bookinfo 
                                ON bi_id = book_bi_id
                                LEFT JOIN tbl_author
                                ON bi_author_id = author_id
                                LEFT JOIN tbl_category
                                ON bi_bookcategory_id = category_id
                                WHERE book_rfid = @book_rfid";
                    DataTable dt = dc.Select(query, dic);
                    if (dt.Rows.Count > 0)
                    {
                        dtBook = dt;
                    }
                    else
                    {
                        MessageBox.Show(this, "This book isn't Registered");
                    }

                }
                else
                {
                    MessageBox.Show(this, "Make sure Book has an RFID Sticker", "Book Tag undetected", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }           
            return dtBook;
        }

        private Boolean AddToBookList(DataTable dtBook)
        {

            DataTable dtBookList = (DataTable)dataGridViewBookList.DataSource;
            //DataRow [] find_row = dtBookList.Select("{Book ID} like '%" + dtBook.Rows[0]["book_id"].ToString() + "%'");
            DataRow[] find_row = dtBookList.Select(string.Format("{0} LIKE '%{1}%'", "Book_ID", dtBook.Rows[0]["book_id"].ToString()));

            if (find_row.Length == 0)
            {
                DataRow dr = dtBookList.NewRow();
                dr["Book_ID"] = dtBook.Rows[0]["book_id"].ToString();
                dr["Book Name"] = dtBook.Rows[0]["bi_title"].ToString();
                dr["Book Card No."] = dtBook.Rows[0]["book_call_no"].ToString();
                dtBookList.Rows.Add(dr);
            }
            else
            {
                MessageBox.Show("Book Already Scanned");
            }

            


            return true;
        }

        private void buttonScanAndAddToList_Click(object sender, EventArgs e)
        {
            buttonScanBook.PerformClick();
            buttonAddToList.PerformClick();

        }

        private void buttonAddToList_Click(object sender, EventArgs e)
        {
            if(labelScannedBook.Tag.ToString() != "0")
            {
                String bookId = labelScannedBook.Tag.ToString();
                Dictionary<string, string> dic = new Dictionary<string, string>();
                dic.Add("@book_id", bookId);
                String query = @"SELECT * 
                                FROM tbl_book
                                LEFT JOIN tbl_bookinfo 
                                ON bi_id = book_bi_id
                                WHERE book_id = @book_id ";
                DataTable dt = dc.Select(query, dic);
                if(dt.Rows.Count > 0)
                {
                    AddToBookList(dt);
                    buttonAddToList.Enabled = false;
                }
                else
                {
                    MessageBox.Show(this, "This book isn't Registered");
                }
                labelScannedBook.Tag = "0";
            }
            else
            {
                MessageBox.Show(this, "No Book Selected");
            }
        }

        private void buttonCompleteTransaction_Click(object sender, EventArgs e)
        {
            Boolean valid = true;
            if(dataGridViewBookList.Rows.Count <= 0)
            {
                valid = false;
            }
            if (comboBoxBorrowerType.SelectedIndex == 0)
            {
                if(comboBoxStudentName.SelectedValue.ToString() == "0")
                {
                    valid = false;
                }
            }
            if (comboBoxBorrowerType.SelectedIndex == 1)
            {
                if (comboBoxFaculty.SelectedValue.ToString() == "0")
                {
                    valid = false;
                }
            }
            if (comboBoxBorrowerType.SelectedIndex == 2)
            {
                if(textBoxOther.Text.Trim().Length == 0)
                {
                    valid = false;
                }
            }
            if (valid)
            {
                Dictionary<string, string> insertBorrowDic = new Dictionary<string, string>();
                String borrower_column = "";
                int borrow_type = 0;
                if (comboBoxBorrowerType.SelectedIndex == 0)
                {
                    borrower_column = "borrow_student_id";
                    insertBorrowDic.Add("@"+ borrower_column, comboBoxStudentName.SelectedValue.ToString());
                    borrow_type = 1;
                }
                if (comboBoxBorrowerType.SelectedIndex == 1)
                {
                    borrower_column = "borrow_adviser_id";
                    insertBorrowDic.Add("@" + borrower_column, comboBoxFaculty.SelectedValue.ToString());
                    borrow_type = 2;
                }
                if (comboBoxBorrowerType.SelectedIndex == 2)
                {
                    borrower_column = "borrow_other";
                    insertBorrowDic.Add("@" + borrower_column, textBoxOther.Text.Trim());
                    borrow_type = 3;
                }


                String insertBorrowQuery = @"INSERT INTO tbl_borrow 
                                (borrow_notes,
                                borrow_tcode,
                                "+ borrower_column + @",
                                borrow_type,
                                borrow_librarian_id,
                                borrow_date,
                                borrow_expected_date,
                                borrow_status) 
                                VALUES 
                                (@borrow_notes,
                                @borrow_tcode,
                                @" + borrower_column + @",
                                @borrow_type,
                                @borrow_librarian_id,
                                @borrow_date,
                                @borrow_expected_date,
                                @borrow_status)";
                
                insertBorrowDic.Add("@borrow_notes", textBoxBorrowNotes.Text);
                insertBorrowDic.Add("@borrow_tcode", refLib.getTransactionSeries("transaction", true));                
                insertBorrowDic.Add("@borrow_librarian_id", Singleton.Current_user.ToString());
                insertBorrowDic.Add("@borrow_type", borrow_type.ToString());

                insertBorrowDic.Add("@borrow_date", DateTime.Now.ToString("yyyy-MM-dd H:mm:ss"));
                insertBorrowDic.Add("@borrow_expected_date", dateTimePickerReturn.Value.ToString("yyyy-MM-dd"));
                insertBorrowDic.Add("@borrow_status", "1");
                String borrowID = dc.Insert(insertBorrowQuery, insertBorrowDic).ToString();
                createTransactionLog(borrowID);
                DataTable dtBookList = (DataTable) dataGridViewBookList.DataSource;
                for (int i = 0; i< dtBookList.Rows.Count; i++)
                {
                    String insertBorrowDetailQuery = @"INSERT INTO tbl_borrow_detail 
                                    (bd_borrow_id,
                                    bd_book_id,
                                    bd_is_returned,
                                    bd_borrow_date) 
                                    VALUES 
                                    (@bd_borrow_id,
                                    @bd_book_id,
                                    @bd_is_returned,
                                    @bd_borrow_date)";
                    String book_id = dtBookList.Rows[i]["book_id"].ToString();
                    Dictionary<string, string> insertBorrowDetailDic = new Dictionary<string, string>();
                    insertBorrowDetailDic.Add("@bd_borrow_id", borrowID);
                    insertBorrowDetailDic.Add("@bd_book_id", book_id);
                    insertBorrowDetailDic.Add("@bd_is_returned", (0).ToString());
                    insertBorrowDetailDic.Add("@bd_borrow_date", DateTime.Now.ToString("yyyy-MM-dd H:mm:ss"));

                    String borrowDetailID = dc.Insert(insertBorrowDetailQuery, insertBorrowDetailDic).ToString();


                    String updateBookQuery = @"UPDATE tbl_book
                                               SET book_status = '1'
                                               WHERE book_id = @book_id ";
                    Dictionary<string, string> updateBookQueryDic = new Dictionary<string, string>();
                    updateBookQueryDic.Add("@book_id", book_id);
                    dc.Update(updateBookQuery, updateBookQueryDic);
                    MessageBox.Show(this, "Borrowing Book Successful!", "Transcation Completed", MessageBoxButtons.OK, MessageBoxIcon.None);
                    buttonReset.PerformClick();
                }
            }
            else
            {
                MessageBox.Show("Transaction Can't be completed");
            }
        }

        private void panelBorrowerStudent_DockChanged(object sender, EventArgs e)
        {
            panelBorrowerStudent.Visible = (panelBorrowerStudent.Dock == DockStyle.Fill);
        }

        

        private void comboBoxBorrowerType_SelectedIndexChanged(object sender, EventArgs e)
        {
            if(comboBoxBorrowerType.SelectedIndex == 0)
            {
                panelBorrowerType.Controls.Add(this.panelBorrowerStudent);
                panelBorrowerType.Controls.Remove(this.panelBorrowerFaculty);
                panelBorrowerType.Controls.Remove(this.panelBorrowerOther);
                panelBorrowerStudent.Dock = DockStyle.Fill;
                panelBorrowerFaculty.Dock = DockStyle.None;
                panelBorrowerOther.Dock = DockStyle.None;
            }
            if (comboBoxBorrowerType.SelectedIndex == 1)
            {
                panelBorrowerType.Controls.Add(this.panelBorrowerFaculty);
                panelBorrowerType.Controls.Remove(this.panelBorrowerStudent);
                panelBorrowerType.Controls.Remove(this.panelBorrowerOther);
                panelBorrowerFaculty.Dock = DockStyle.Fill;
                panelBorrowerStudent.Dock = DockStyle.None;
                panelBorrowerOther.Dock = DockStyle.None;
            }
            if (comboBoxBorrowerType.SelectedIndex == 2)
            {
                panelBorrowerType.Controls.Add(this.panelBorrowerOther);
                panelBorrowerType.Controls.Remove(this.panelBorrowerStudent);
                panelBorrowerType.Controls.Remove(this.panelBorrowerFaculty);
                panelBorrowerOther.Dock = DockStyle.Fill;
                panelBorrowerFaculty.Dock = DockStyle.None;
                panelBorrowerStudent.Dock = DockStyle.None;
            }
        }

        private void panelBorrowerFaculty_DockChanged(object sender, EventArgs e)
        {
            panelBorrowerFaculty.Visible = (panelBorrowerFaculty.Dock == DockStyle.Fill);
        }

        private void panelBorrowerOther_DockChanged(object sender, EventArgs e)
        {
            panelBorrowerOther.Visible = (panelBorrowerOther.Dock == DockStyle.Fill);
        }

        private void BurrowBookUserControl_Load(object sender, EventArgs e)
        {
            comboBoxBorrowerType.SelectedIndex = 0;            
        }
        private void createTransactionLog(String id)
        {
            String insert = @"INSERT INTO tbl_transaction_summary 
                                (ts_borrow_id,
                                ts_type,
                                ts_date,
                                ts_notes) 
                                VALUES 
                                (@ts_borrow_id,
                                @ts_type,
                                @ts_date,
                                @ts_notes)";
            Dictionary<string, string> insertTransactionLog = new Dictionary<string, string>();
            insertTransactionLog.Add("@ts_borrow_id", id);
            insertTransactionLog.Add("@ts_type", "Borrow");
            insertTransactionLog.Add("@ts_notes", "");
            insertTransactionLog.Add("@ts_date", DateTime.Now.ToString("yyyy-MM-dd H:mm:ss"));
            dc.Insert(insert, insertTransactionLog);            
        }
        private void buttonReset_Click(object sender, EventArgs e)
        {
            Initiator.AdminForm.PreventShiftingUC = "";
            Initiator.LibrarianForm.PreventShiftingUC = "";
            ResetButton(this, EventArgs.Empty);
        }

        private void dataGridViewBookList_RowsAdded(object sender, DataGridViewRowsAddedEventArgs e)
        {
            if(dataGridViewBookList.Rows.Count > 0)
            {
                Initiator.AdminForm.PreventShiftingUC = "Current Borrow Transaction Ongoing!";
                Initiator.LibrarianForm.PreventShiftingUC = "Current Borrow Transaction Ongoing!";
            }
        }

        private void textBoxStudentID_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = !char.IsDigit(e.KeyChar) && !char.IsControl(e.KeyChar);
        }
    }
}
