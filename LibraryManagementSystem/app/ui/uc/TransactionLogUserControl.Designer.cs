﻿namespace LibraryManagementSystem.app.ui.uc
{
    partial class TransactionLogUserControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panelBody = new System.Windows.Forms.Panel();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.buttonReturnBook = new System.Windows.Forms.Button();
            this.buttonBorrowBook = new System.Windows.Forms.Button();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.panel2 = new System.Windows.Forms.Panel();
            this.dataGridViewTransactionLog = new System.Windows.Forms.DataGridView();
            this.panelBody.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.panel1.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewTransactionLog)).BeginInit();
            this.SuspendLayout();
            // 
            // panelBody
            // 
            this.panelBody.AutoScroll = true;
            this.panelBody.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panelBody.Controls.Add(this.groupBox4);
            this.panelBody.Controls.Add(this.groupBox3);
            this.panelBody.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelBody.Location = new System.Drawing.Point(0, 0);
            this.panelBody.Name = "panelBody";
            this.panelBody.Size = new System.Drawing.Size(1374, 658);
            this.panelBody.TabIndex = 4;
            // 
            // groupBox4
            // 
            this.groupBox4.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox4.Controls.Add(this.panel1);
            this.groupBox4.ForeColor = System.Drawing.Color.White;
            this.groupBox4.Location = new System.Drawing.Point(1103, 22);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(256, 611);
            this.groupBox4.TabIndex = 11;
            this.groupBox4.TabStop = false;
            // 
            // panel1
            // 
            this.panel1.AutoScroll = true;
            this.panel1.Controls.Add(this.buttonReturnBook);
            this.panel1.Controls.Add(this.buttonBorrowBook);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(3, 26);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(250, 582);
            this.panel1.TabIndex = 25;
            // 
            // buttonReturnBook
            // 
            this.buttonReturnBook.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonReturnBook.BackColor = System.Drawing.Color.RoyalBlue;
            this.buttonReturnBook.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonReturnBook.Image = global::LibraryManagementSystem.Properties.Resources.Book_Stack_white_26px;
            this.buttonReturnBook.Location = new System.Drawing.Point(18, 13);
            this.buttonReturnBook.Name = "buttonReturnBook";
            this.buttonReturnBook.Size = new System.Drawing.Size(216, 50);
            this.buttonReturnBook.TabIndex = 22;
            this.buttonReturnBook.Tag = "0";
            this.buttonReturnBook.Text = "Return Book";
            this.buttonReturnBook.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.buttonReturnBook.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.buttonReturnBook.UseVisualStyleBackColor = false;
            this.buttonReturnBook.Click += new System.EventHandler(this.buttonReturnBook_Click);
            // 
            // buttonBorrowBook
            // 
            this.buttonBorrowBook.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonBorrowBook.BackColor = System.Drawing.Color.RoyalBlue;
            this.buttonBorrowBook.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonBorrowBook.Image = global::LibraryManagementSystem.Properties.Resources.Add_List_white_26px;
            this.buttonBorrowBook.Location = new System.Drawing.Point(18, 69);
            this.buttonBorrowBook.Name = "buttonBorrowBook";
            this.buttonBorrowBook.Size = new System.Drawing.Size(216, 50);
            this.buttonBorrowBook.TabIndex = 20;
            this.buttonBorrowBook.Tag = "0";
            this.buttonBorrowBook.Text = "Borrow Book";
            this.buttonBorrowBook.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.buttonBorrowBook.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.buttonBorrowBook.UseVisualStyleBackColor = false;
            this.buttonBorrowBook.Click += new System.EventHandler(this.buttonBorrowBook_Click);
            // 
            // groupBox3
            // 
            this.groupBox3.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox3.Controls.Add(this.panel2);
            this.groupBox3.ForeColor = System.Drawing.Color.White;
            this.groupBox3.Location = new System.Drawing.Point(12, 16);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(1079, 617);
            this.groupBox3.TabIndex = 10;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "All Transaction";
            // 
            // panel2
            // 
            this.panel2.AutoScroll = true;
            this.panel2.Controls.Add(this.dataGridViewTransactionLog);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel2.ForeColor = System.Drawing.Color.Black;
            this.panel2.Location = new System.Drawing.Point(3, 26);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(1073, 588);
            this.panel2.TabIndex = 2;
            // 
            // dataGridViewTransactionLog
            // 
            this.dataGridViewTransactionLog.AllowUserToAddRows = false;
            this.dataGridViewTransactionLog.AllowUserToDeleteRows = false;
            this.dataGridViewTransactionLog.AllowUserToResizeColumns = false;
            this.dataGridViewTransactionLog.AllowUserToResizeRows = false;
            this.dataGridViewTransactionLog.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dataGridViewTransactionLog.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.dataGridViewTransactionLog.BackgroundColor = System.Drawing.Color.White;
            this.dataGridViewTransactionLog.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewTransactionLog.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridViewTransactionLog.Location = new System.Drawing.Point(0, 0);
            this.dataGridViewTransactionLog.MultiSelect = false;
            this.dataGridViewTransactionLog.Name = "dataGridViewTransactionLog";
            this.dataGridViewTransactionLog.ReadOnly = true;
            this.dataGridViewTransactionLog.RowHeadersVisible = false;
            this.dataGridViewTransactionLog.RowTemplate.Height = 24;
            this.dataGridViewTransactionLog.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridViewTransactionLog.Size = new System.Drawing.Size(1073, 588);
            this.dataGridViewTransactionLog.TabIndex = 1;
            // 
            // TransactionLogUserControl
            // 
            this.AutoScroll = true;
            this.BackColor = System.Drawing.Color.Navy;
            this.Controls.Add(this.panelBody);
            this.Font = new System.Drawing.Font("Segoe UI", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ForeColor = System.Drawing.Color.White;
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.Name = "TransactionLogUserControl";
            this.Size = new System.Drawing.Size(1374, 658);
            this.Load += new System.EventHandler(this.BurrowBookUserControl_Load);
            this.panelBody.ResumeLayout(false);
            this.groupBox4.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.groupBox3.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewTransactionLog)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Panel panelBody;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.Button buttonBorrowBook;
        private System.Windows.Forms.Button buttonReturnBook;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.DataGridView dataGridViewTransactionLog;
    }
}
