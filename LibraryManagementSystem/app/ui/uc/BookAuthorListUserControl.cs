﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using LibraryManagementSystem.app.ui.form;
using LibraryManagementSystem.app.db;

namespace LibraryManagementSystem.app.ui.uc
{
    public partial class BookAuthorListUserControl : UserControl
    {
        public BookAuthorListUserControl()
        {
            InitializeComponent();
        }

        public void Add()
        {
            BookAuthorFormData add = new BookAuthorFormData();
            if (DialogResult.OK == add.ShowDialog())
            {
                DatabaseConnection dc = new DatabaseConnection();
                String query = @"INSERT INTO tbl_author 
                                (author_name) 
                                VALUES 
                                (@author_name)";
                Dictionary<string, string> dic = new Dictionary<string, string>();
                dic.Add("@author_name", add.AuthorName);
                dc.Insert(query, dic);
                reloadDataGridView();
            }
        }
        private void reloadDataGridView()
        {
            DatabaseConnection dc = new DatabaseConnection();
            String filter = textBoxFilterValue.Text.Trim();

            DataTable dt;
            String query = @"SELECT *
                                FROM `tbl_author`";
            if (filter == "")
            {
                dt = dc.Select(query);
            }
            else
            {
                query += " WHERE author_name LIKE @author_name ";
                Dictionary<string, string> dic = new Dictionary<string, string>();
                dic.Add("@author_name", '%' + filter + '%');
                dt = dc.Select(query, dic);
            }

            dataGridViewBookAuthorList.DataSource = dt;
            dataGridViewBookAuthorList.Columns["author_id"].Visible = false;
            dataGridViewBookAuthorList.Columns["author_name"].HeaderText = "Author";
            dataGridViewBookAuthorList.Columns["author_name"].HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter;

        }
        private void buttonNew_Click(object sender, EventArgs e)
        {
            this.Add();
        }

        private void BookAuthorListUserControl_Load(object sender, EventArgs e)
        {
            reloadDataGridView();
        }

        private void buttonEdit_Click(object sender, EventArgs e)
        {
            if (dataGridViewBookAuthorList.SelectedRows.Count > 0)
            {
                DatabaseConnection dc = new DatabaseConnection();
                Dictionary<string, string> dic = new Dictionary<string, string>();
                String author_id = dataGridViewBookAuthorList["author_id", dataGridViewBookAuthorList.CurrentRow.Index].Value.ToString();
                dic.Add("author_id", author_id);
                String query = @"SELECT * FROM tbl_author WHERE author_id = @author_id";
                DataTable dt = dc.Select(query, dic);
                BookAuthorFormData add = new BookAuthorFormData();
                add.AuthorName = dt.Rows[0]["author_name"].ToString();

                if (DialogResult.OK == add.ShowDialog())
                {
                    Dictionary<string, string> update = new Dictionary<string, string>();
                    update.Add("@author_name", add.AuthorName);
                    update.Add("@author_id", author_id);
                    String updateQuery = "UPDATE tbl_author SET author_name = @author_name WHERE author_id = @author_id";
                    dc.Update(updateQuery, update);
                    //
                    //Dictionary<string, string> update = new Dictionary<string, string>();
                    //update.Add("@section_id", section_id);
                    //update.Add("@section_name", add.Section);
                    //dc.Update(query, update);
                    reloadDataGridView();
                }
            }
        }

        private void buttonDelete_Click(object sender, EventArgs e)
        {
            if (dataGridViewBookAuthorList.SelectedRows.Count > 0)
            {
                DatabaseConnection dc = new DatabaseConnection();
                Dictionary<string, string> dic = new Dictionary<string, string>();
                String author_id = dataGridViewBookAuthorList["author_id", dataGridViewBookAuthorList.CurrentRow.Index].Value.ToString();
                dic.Add("author_id", author_id);
                DialogResult dr = MessageBox.Show(this, "Are you sure you want to delete this author?", "Deleting Author", MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button2);

                //add.CategoryName = dt.Rows[0]["category_name"].ToString();

                if (dr == DialogResult.Yes)
                {
                    Dictionary<string, string> update = new Dictionary<string, string>();
                    String query = "DELETE FROM tbl_author WHERE author_id = @author_id";
                    dc.Delete(query, dic);
                    reloadDataGridView();
                }
            }
        }

        private void textBoxFilterValue_TextChanged(object sender, EventArgs e)
        {
            reloadDataGridView();
        }
    }
}
