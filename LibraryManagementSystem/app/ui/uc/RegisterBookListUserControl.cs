﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using LibraryManagementSystem.app.ui.form;
using LibraryManagementSystem.app.db;
using LibraryManagementSystem.app.lib;

namespace LibraryManagementSystem.app.ui.uc
{
    public partial class RegisterBookListUserControl : UserControl
    {
        DatabaseConnection dc;
        private RFIDController rfidC;
        public RegisterBookListUserControl()
        {
            InitializeComponent();
            dc = new DatabaseConnection();
            rfidC = new RFIDController();
        }

        private void buttonScanBook_Click(object sender, EventArgs e)
        {
            if (rfidC.Connect())
            {
                if (rfidC.CheckCard())
                {
                    String Rfid = rfidC.Read();
                    Dictionary<string, string> dic = new Dictionary<string, string>();
                    dic.Add("@book_rfid", Rfid);
                    String query = @"SELECT * 
                                FROM tbl_book
                                LEFT JOIN tbl_bookinfo 
                                ON bi_id = book_bi_id
                                LEFT JOIN tbl_author
                                ON bi_author_id = author_id
                                LEFT JOIN tbl_category
                                ON bi_bookcategory_id = category_id
                                WHERE book_rfid = @book_rfid";
                    DataTable dt = dc.Select(query, dic);
                    if (dt.Rows.Count > 0)
                    {
                        buttonSelectBook.Tag = dt.Rows[0]["book_id"].ToString();
                        labelBookISBN.Text = dt.Rows[0]["bi_isbn"].ToString();
                        labelBookTitle.Text = dt.Rows[0]["bi_title"].ToString();
                        labelBookAuthor.Text = dt.Rows[0]["author_name"].ToString();
                        labelBookCategory.Text = dt.Rows[0]["category_name"].ToString();
                        textBoxBookCardNumber.Text = dt.Rows[0]["book_call_no"].ToString();
                        textBoxBookNotes.Text = dt.Rows[0]["book_notes"].ToString();
                    }
                    else
                    {

                    }


                }
                else
                {
                    MessageBox.Show(this, "Make sure Book has an RFID Sticker", "Book Tag undetected", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            
        }

        private void clearSelection()
        {
            buttonSelectBook.Tag = "0";
            labelBookISBN.Text = "";
            labelBookTitle.Text = "";
            labelBookAuthor.Text = "";
            labelBookCategory.Text = "";
            textBoxBookNotes.Text = "";
            textBoxBookCardNumber.Text = "";
        }

        private void buttonRegisterBook_Click(object sender, EventArgs e)
        {            
            if (buttonSelectBook.Tag.ToString() != "0" && textBoxBookCardNumber.Text.Trim() != "")
            {
                if (rfidC.Connect())
                {
                    if (rfidC.CheckCard())
                    {
                        String Rfid = rfidC.Read();
                        Dictionary<string, string> dic = new Dictionary<string, string>();
                        dic.Add("@book_rfid", Rfid);
                        String query = @"SELECT * 
                                FROM tbl_book
                                LEFT JOIN tbl_bookinfo 
                                ON bi_id = book_bi_id
                                LEFT JOIN tbl_author
                                ON bi_author_id = author_id
                                LEFT JOIN tbl_category
                                ON bi_bookcategory_id = category_id
                                WHERE book_rfid = @book_rfid";
                        DataTable dt = dc.Select(query, dic);
                        if (dt.Rows.Count > 0)
                        {
                            DialogResult dr = MessageBox.Show(this, "This book is already registered. Would you like change to reassign the book? Any Transaction made with this book will be retained", "Book Registration", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
                            if (dr == DialogResult.Yes)
                            {
                                String updateQuery = @"UPDATE tbl_book SET
                            book_bi_id = @book_bi_id, book_notes = @book_notes, book_call_no = @book_call_no
                            WHERE book_rfid = @book_rfid ";
                                Dictionary<string, string> updateDic = new Dictionary<string, string>();

                                updateDic.Add("@book_bi_id", buttonSelectBook.Tag.ToString());
                                updateDic.Add("@book_notes", textBoxBookNotes.Text);
                                updateDic.Add("@book_call_no", textBoxBookCardNumber.Text.Trim());
                                updateDic.Add("@book_rfid", dt.Rows[0]["book_rfid"].ToString());
                                dc.Update(updateQuery, updateDic);
                                MessageBox.Show(this, "Book has been reassigned to new information. Transaction and history is retained", "Book Registration", MessageBoxButtons.OK, MessageBoxIcon.Information);
                                clearSelection();
                            }
                        }
                        else
                        {
                            String RFIDCode = rfidC.generateKey();
                            rfidC.Write(RFIDCode);
                            String insertQuery = @"INSERT INTO tbl_book 
                            (book_rfid,
                            book_bi_id,
                            book_call_no,
                            book_notes) 
                            VALUES 
                            (@book_rfid,
                            @book_bi_id,
                            @book_call_no,
                            @book_notes)";
                            Dictionary<string, string> insertDic = new Dictionary<string, string>();
                            insertDic.Add("@book_rfid", RFIDCode);
                            insertDic.Add("@book_bi_id", buttonSelectBook.Tag.ToString());
                            insertDic.Add("@book_call_no", textBoxBookCardNumber.Text.Trim());
                            insertDic.Add("@book_notes", textBoxBookNotes.Text);
                            dc.Insert(insertQuery, insertDic);

                            MessageBox.Show(this, "Book has been registered to the system and ready for transaction", "Book Registration", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            clearSelection();
                        }


                    }
                    else
                    {
                        MessageBox.Show(this, "Make sure Book has an RFID Sticker", "Book Tag undetected", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
            }
            else
            {
                if(buttonSelectBook.Tag.ToString() != "0")
                {
                    MessageBox.Show(this, "Select Book Information", "No Book Information", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                else
                {
                    MessageBox.Show(this, "Enter Book Card Number", "Book Card Number must not be empty", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }

        private void buttonSelectBook_Click(object sender, EventArgs e)
        {
            BookInformationFormLookUp add = new BookInformationFormLookUp();
            if (DialogResult.OK == add.ShowDialog())
            {
                buttonSelectBook.Tag = add.BookInfoID;
                Dictionary<string, string> dic = new Dictionary<string, string>();
                dic.Add("bi_id", add.BookInfoID);
                String query = @"SELECT * 
                                FROM tbl_bookinfo 
                                LEFT JOIN tbl_author
                                ON bi_author_id = author_id
                                LEFT JOIN tbl_category
                                ON bi_bookcategory_id = category_id
                                WHERE bi_id = @bi_id";
                DataTable dt = dc.Select(query, dic);
                labelBookISBN.Text = dt.Rows[0]["bi_isbn"].ToString();
                labelBookTitle.Text = dt.Rows[0]["bi_title"].ToString();
                labelBookAuthor.Text = dt.Rows[0]["author_name"].ToString();
                labelBookCategory.Text = dt.Rows[0]["category_name"].ToString();
                textBoxBookNotes.Text = "";
            }
        }
        private Boolean IsWrite()
        {
            return false;
        }

        private void buttonClearBook_Click(object sender, EventArgs e)
        {
            if (rfidC.Connect())
            {
                if (rfidC.CheckCard())
                {
                    String Rfid = rfidC.Read();
                    Dictionary<string, string> dic = new Dictionary<string, string>();
                    dic.Add("@book_rfid", Rfid);
                    String query = @"SELECT * 
                                FROM tbl_book
                                LEFT JOIN tbl_bookinfo 
                                ON bi_id = book_bi_id
                                LEFT JOIN tbl_author
                                ON bi_author_id = author_id
                                LEFT JOIN tbl_category
                                ON bi_bookcategory_id = category_id
                                WHERE book_rfid = @book_rfid";
                    DataTable dt = dc.Select(query, dic);
                    if (dt.Rows.Count > 0)
                    {
                        DialogResult dr = MessageBox.Show(this, "This book is currently registered. Would you like change to clear records of this book? Any Transaction made with this book will be forfeited", "Book Clear", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
                        if (dr == DialogResult.Yes)
                        {
                            String deleteQuery = @"DELETE FROM tbl_book 
                            WHERE book_rfid = @book_rfid ";
                            Dictionary<string, string> deleteDic = new Dictionary<string, string>();
                            deleteDic.Add("@book_rfid", dt.Rows[0]["book_rfid"].ToString());
                            dc.Delete(deleteQuery, deleteDic);
                            rfidC.Write("");
                            MessageBox.Show(this, "All information associated with this book has been cleared", "Book Clear", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            clearSelection();
                        }
                    }
                    else
                    {
                        rfidC.Write("");
                        MessageBox.Show(this, "All information associated with this book has been cleared", "Book Clear", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        clearSelection();
                    }


                }
                else
                {
                    MessageBox.Show(this, "Make sure Book has an RFID Sticker", "Book Tag undetected", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            
        }
    }
}
