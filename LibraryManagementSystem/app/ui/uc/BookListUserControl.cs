﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Web.Script.Serialization;
using System.Collections;
using LibraryManagementSystem.app.db;

namespace LibraryManagementSystem.app.ui.uc
{
    public partial class BookListUserControl : UserControl
    {
        public BookListUserControl()
        {
            InitializeComponent();
        }

        private void buttonSearch_Click(object sender, EventArgs e)
        {
            search(checkBoxSearchLocal.Checked);  
        }

        private void search(Boolean searchLocal)
        {
            String filter = textBoxFilterValue.Text.Trim();
            String searchType = comboBoxFilterType.Text.ToLower();

            if(searchLocal)
            {
                DatabaseConnection dc = new DatabaseConnection();
                Dictionary<string, string> dic = new Dictionary<string, string>();
                String whereQuery = "";
                String query = "";
                //1 = With Grouping
                //2 = Without Groupings
                int query_type = 1;
                
                if(searchType == "isbn")
                {
                    dic.Add("@bi_isbn", '%' + filter + '%');
                    whereQuery = " WHERE bi_isbn LIKE @bi_isbn ";
                }
                if (searchType == "title")
                {
                    dic.Add("@bi_title", '%' + filter + '%');
                    whereQuery = " WHERE bi_title LIKE @bi_title ";
                }
                if (searchType == "author")
                {
                    dic.Add("@author_name", '%' + filter + '%');
                    whereQuery = " WHERE author_name LIKE @author_name ";
                }
                if (searchType == "card #")
                {
                    dic.Add("@book_call_no", '%' + filter + '%');
                    whereQuery = " WHERE book_call_no LIKE @book_call_no ";
                    query_type = 2;
                }
                if(query_type == 1)
                {
                    query = @"SELECT bi_isbn as 'ISBN', bi_title as 'Title', 
                            category_name as 'Category', author_name as 'Author', COUNT(`book_id`) as 'No. Of Books',
							SUM((CASE
                             WHEN book_status = '0' THEN 1
                             ELSE 0
                            END)) as 'Books Available' 
                            FROM `tbl_bookinfo` 
                            INNER JOIN `tbl_category`
                            ON `bi_bookcategory_id` = `category_id`
                            INNER JOIN `tbl_author`
                            ON `bi_author_id` = `author_id`
                            LEFT JOIN `tbl_book`
                            ON `book_bi_id` = `bi_id`
                            " + whereQuery + @"
                            GROUP BY bi_isbn, bi_title, category_name, author_name ";
                }
                else
                {
                    query = @"SELECT book_call_no as 'CardNo', bi_isbn as 'ISBN', bi_title as 'Title', book_notes as 'Notes',
                            category_name as 'Category', author_name as 'Author', 
                            (
                                CASE 
                                    WHEN book_status = 1 THEN 'Available'
                                    ELSE 'Not Available'
                                END
                            ) as 'Status',
                            (
                                CASE 
    	                            WHEN book_status = 1 THEN
                                    (
                                    CASE
                                        WHEN `borrow_type` = 1 THEN CONCAT(student_lastname, ', ', student_firstname, ' ', student_middlename)
                                        WHEN `borrow_type` = 2 THEN adviser_name
                                        WHEN `borrow_type` = 3 THEN borrow_other
                                        ELSE ''
                                    END
                                    )
    	                            WHEN book_status = 0 THEN 'Library'
                                    ELSE 'On Hold'
                                END
                            ) as 'Possesion', MAX(bd_borrow_date) as 'Borrowed Date'
                            FROM `tbl_bookinfo` 
                            INNER JOIN `tbl_category`
                            ON `bi_bookcategory_id` = `category_id`
                            INNER JOIN `tbl_author`
                            ON `bi_author_id` = `author_id`
                            LEFT JOIN `tbl_book`
                            ON `book_bi_id` = `bi_id`
                            LEFT JOIN `tbl_borrow_detail`
                            ON `bd_book_id` = `book_id`
                            LEFT JOIN `tbl_borrow`
                            ON `borrow_id` = `bd_borrow_id`
                            LEFT JOIN `tbl_student`
                            ON `borrow_student_id` = `student_id`
                            LEFT JOIN `tbl_adviser`
                            ON `borrow_adviser_id` = `adviser_id`" +
                            whereQuery + @"
                            GROUP BY book_id";
                }
                dataGridViewBookList.DataSource = null;
                dataGridViewBookList.DataSource = dc.Select(query, dic);
            }
            else
            {
                if (lib.BookLookUp.CheckForInternetConnection())
                {
                    lib.BookLookUp bl = new lib.BookLookUp();
                    String data = bl.search(filter, searchType);
                    DataTable dt = null;

                    dt = this.convertFromJSON(data, searchType);

                    DataTable dtCleaned = removeNoISBN(dt);
                    DataTable dtMatched = searchMatchOnDatabase(dtCleaned);
                    dataGridViewBookList.DataSource = dtMatched;
                    dataGridViewBookList.Columns["bi_id"].Visible = false;
                }
                else
                {
                    MessageBox.Show(this, "No Internet Connection", "Failed to retieve information online", MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1);
                }                
            }            
        }

        private DataTable removeNoISBN(DataTable dt)
        {
            for (int i = dt.Rows.Count - 1; i >= 0; i--)
            {
                DataRow dr = dt.Rows[i];
                if (dr["ISBN"].ToString() == "")
                {
                    dr.Delete();
                }
                    
            }
            return dt;
        }

        private DataTable searchMatchOnDatabase(DataTable dt)
        {
            DatabaseConnection dc = new DatabaseConnection();
            DataTable newDataTable = new DataTable();
            newDataTable.Columns.Add("bi_id");
            newDataTable.Columns.Add("Title");
            newDataTable.Columns.Add("Author");
            newDataTable.Columns.Add("ISBN");
            newDataTable.Columns.Add("Publish Year");
            newDataTable.Columns.Add("Registered");
            newDataTable.Columns.Add("Books Registered");
            newDataTable.Columns.Add("Books Available");
            for (int i = dt.Rows.Count - 1; i >= 0; i--)
            {
                String []ISBNList = dt.Rows[i]["ISBN"].ToString().Split(new string[] { Environment.NewLine }, StringSplitOptions.None);
                if(ISBNList.Length == 1)
                { // Meaning only 1 ISBN
                    String currentISBN = dt.Rows[i]["ISBN"].ToString();

                    Dictionary<string, string> dic = new Dictionary<string, string>();
                    dic.Add("@bi_isbn", currentISBN);
                    String query = @"SELECT bi_id, bi_isbn, bi_title, author_name, COUNT(`book_id`) as 'numOfBooks', SUM(
                            CASE
                                WHEN book_status = '0' THEN 1
                                ELSE 0
                            END) as 'booksAvail'
                            FROM `tbl_bookinfo` 
                            INNER JOIN `tbl_author`
                            ON `bi_author_id` = `author_id`
                            LEFT JOIN `tbl_book`
                            ON `book_bi_id` = `bi_id`
                            WHERE bi_isbn = @bi_isbn
                            GROUP BY bi_isbn, bi_title, author_name";

                    DataTable dtCurrent = dc.Select(query, dic);

                    DataRow drNew = newDataTable.NewRow();
                    if (dtCurrent.Rows.Count > 0)
                    {
                        drNew["bi_id"] = dtCurrent.Rows[0]["bi_id"].ToString();
                        drNew["Title"] = dtCurrent.Rows[0]["bi_title"].ToString();
                        drNew["Author"] = dtCurrent.Rows[0]["author_name"].ToString();
                        drNew["ISBN"] = dtCurrent.Rows[0]["bi_isbn"].ToString();

                        drNew["Publish Year"] = dt.Rows[i]["Publish Year"].ToString();

                        drNew["Registered"] = "Yes";
                        drNew["Books Registered"] = dtCurrent.Rows[0]["numOfBooks"].ToString();
                        drNew["Books Available"] = dtCurrent.Rows[0]["booksAvail"].ToString();
                    }
                    else
                    {
                        drNew["bi_id"] = "0";
                        drNew["Title"] = dt.Rows[i]["Title"].ToString();
                        drNew["Author"] = dt.Rows[i]["Author"].ToString();
                        drNew["ISBN"] = dt.Rows[i]["ISBN"].ToString();

                        drNew["Publish Year"] = dt.Rows[i]["Publish Year"].ToString();

                        drNew["Registered"] = "No";
                        drNew["Books Registered"] = "";
                        drNew["Books Available"] = "";
                    }

                    newDataTable.Rows.Add(drNew);
                }
                else
                {
                    //Only one row but counts all book occurance
                    
                    String isbnParam = "'" + string.Join("', '", ISBNList) + "'";
                    String query = @"SELECT bi_id, bi_isbn, bi_title, author_name, COUNT(`book_id`) as 'numOfBooks', COUNT(`book_id`) as 'numOfBooks', SUM(
                            CASE
                                WHEN book_status = '0' THEN 1
                                ELSE 0
                            END) as 'booksAvail'
                        FROM `tbl_bookinfo` 
                        INNER JOIN `tbl_author`
                        ON `bi_author_id` = `author_id`
                        LEFT JOIN `tbl_book`
                        ON `book_bi_id` = `bi_id`
                        WHERE bi_isbn IN (" + isbnParam  + @")
                        GROUP BY bi_isbn, bi_title, author_name";

                    DataTable dtCurrent = dc.Select(query);

                    DataRow drNew = newDataTable.NewRow();
                    if (dtCurrent.Rows.Count > 0)
                    {
                        drNew["bi_id"] = dtCurrent.Rows[0]["bi_id"].ToString();
                        drNew["Title"] = dtCurrent.Rows[0]["bi_title"].ToString();
                        drNew["Author"] = dtCurrent.Rows[0]["author_name"].ToString();
                        drNew["ISBN"] = dt.Rows[i]["ISBN"].ToString();

                        drNew["Publish Year"] = dt.Rows[i]["Publish Year"].ToString();

                        drNew["Registered"] = "Yes";
                        drNew["Books Registered"] = dtCurrent.Rows[0]["numOfBooks"].ToString();
                        drNew["Books Available"] = dtCurrent.Rows[0]["booksAvail"].ToString();
                    }
                    else
                    {
                        drNew["bi_id"] = "0";
                        drNew["Title"] = dt.Rows[i]["Title"].ToString();
                        drNew["Author"] = dt.Rows[i]["Author"].ToString();
                        drNew["ISBN"] = dt.Rows[i]["ISBN"].ToString();

                        drNew["Publish Year"] = dt.Rows[i]["Publish Year"].ToString();

                        drNew["Registered"] = "No";
                        drNew["Books Available"] = "";
                        drNew["Books Registered"] = "";
                    }

                    newDataTable.Rows.Add(drNew);
                    
                }
                

            }

            return newDataTable;
        }

        private DataTable convertFromJSON(String jsonResult, String searchType)
        {

            JavaScriptSerializer serializer = new JavaScriptSerializer();
            
            
            DataTable dt = new DataTable();

            if(searchType == "isbn")
            {
                var formattedData = serializer.Deserialize<Dictionary<string, object>>(jsonResult);
                dt.Columns.Add("Title");
                dt.Columns.Add("Author");
                dt.Columns.Add("ISBN");
                dt.Columns.Add("Publish Year");

                for (int i = 0; i < formattedData.Count; i++)
                {
                    DataRow dr = dt.NewRow();
                    //formattedData[i + ""];
                    Dictionary<string, object> row = ((Dictionary<string, object>)formattedData.ElementAt(i).Value);
                    Dictionary<string, object> layer2 = ((Dictionary<string, object>)row["details"]);
                    //Console.WriteLine((Dictionary<string, object>)layer1.ElementAt(4).Value);
                    //var layer1 = serializer.Deserialize<Dictionary<string, object>>(formattedData.ElementAt(i).Value.ToString());
                    dr["Title"] = layer2["title"];
                    dr["Author"] = (layer2.ContainsKey("by_statement")) ? layer2["by_statement"] : "";
                    dr["ISBN"] = row["bib_key"].ToString().Replace("ISBN:", "");
                    dr["Publish Year"] = layer2["publish_date"];
                    dt.Rows.Add(dr);
                }
            }

            else if (searchType == "title" || searchType == "author")
            {
                var formattedData = serializer.Deserialize<Dictionary<string, object>>(jsonResult);
                dt.Columns.Add("Title");
                dt.Columns.Add("Author");
                dt.Columns.Add("ISBN");
                dt.Columns.Add("Publish Year");

                if(formattedData != null)
                {
                    ArrayList rowList = (ArrayList)formattedData["docs"];
                    for (int i = 0; i < rowList.Count; i++)
                    {
                        string author_name = "";
                        string isbn_desc = "";

                        DataRow dr = dt.NewRow();
                        ////formattedData[i + ""];
                        Dictionary<string, object> row = ((Dictionary<string, object>)rowList[i]);
                        if (row.ContainsKey("author_name"))
                        {
                            ArrayList author = (ArrayList)row["author_name"];
                            for (int j = 0; j < author.Count; j++)
                            {
                                author_name += author[j].ToString() + Environment.NewLine;
                            }
                        }

                        if (row.ContainsKey("isbn"))
                        {
                            ArrayList isbn = (ArrayList)row["isbn"];
                            for (int j = 0; j < isbn.Count; j++)
                            {
                                isbn_desc += isbn[j].ToString() + Environment.NewLine;
                            }
                        }


                        ////Console.WriteLine((Dictionary<string, object>)layer1.ElementAt(4).Value);
                        ////var layer1 = serializer.Deserialize<Dictionary<string, object>>(formattedData.ElementAt(i).Value.ToString());
                        dr["Title"] = row["title"];

                        dr["Author"] = author_name;// layer2["by_statement"];
                        dr["ISBN"] = isbn_desc;
                        dr["Publish Year"] = row.ContainsKey("first_publish_year") ? row["first_publish_year"] : "";
                        dt.Rows.Add(dr);
                    }
                }
                
            }


            return dt;
            
        }

        private void textBoxFilterValue_KeyDown(object sender, KeyEventArgs e)
        {
            if(e.KeyCode == Keys.Enter)
            {
                search(checkBoxSearchLocal.Checked);
            }
        }

        private void BookListUserControl_Load(object sender, EventArgs e)
        {
            comboBoxFilterType.SelectedIndex = 0;
        }
    }
}
