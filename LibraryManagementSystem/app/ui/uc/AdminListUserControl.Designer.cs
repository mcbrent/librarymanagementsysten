﻿namespace LibraryManagementSystem.app.ui.uc
{
    partial class AdminListUserControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.textBoxFilterValue = new System.Windows.Forms.TextBox();
            this.panelBody = new System.Windows.Forms.Panel();
            this.panelContent = new System.Windows.Forms.Panel();
            this.dataGridViewFilteredData = new System.Windows.Forms.DataGridView();
            this.labeltitle = new System.Windows.Forms.Label();
            this.panelHeading = new System.Windows.Forms.Panel();
            this.label1 = new System.Windows.Forms.Label();
            this.buttonDelete = new System.Windows.Forms.Button();
            this.buttonEdit = new System.Windows.Forms.Button();
            this.buttonNew = new System.Windows.Forms.Button();
            this.panelBody.SuspendLayout();
            this.panelContent.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewFilteredData)).BeginInit();
            this.panelHeading.SuspendLayout();
            this.SuspendLayout();
            // 
            // textBoxFilterValue
            // 
            this.textBoxFilterValue.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.textBoxFilterValue.Location = new System.Drawing.Point(1154, 24);
            this.textBoxFilterValue.Name = "textBoxFilterValue";
            this.textBoxFilterValue.Size = new System.Drawing.Size(192, 30);
            this.textBoxFilterValue.TabIndex = 3;
            this.textBoxFilterValue.TextChanged += new System.EventHandler(this.textBoxFilterValue_TextChanged);
            // 
            // panelBody
            // 
            this.panelBody.Controls.Add(this.panelContent);
            this.panelBody.Controls.Add(this.labeltitle);
            this.panelBody.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelBody.Location = new System.Drawing.Point(0, 76);
            this.panelBody.Name = "panelBody";
            this.panelBody.Size = new System.Drawing.Size(1374, 507);
            this.panelBody.TabIndex = 4;
            // 
            // panelContent
            // 
            this.panelContent.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panelContent.Controls.Add(this.dataGridViewFilteredData);
            this.panelContent.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelContent.ForeColor = System.Drawing.Color.Black;
            this.panelContent.Location = new System.Drawing.Point(0, 23);
            this.panelContent.Name = "panelContent";
            this.panelContent.Size = new System.Drawing.Size(1374, 484);
            this.panelContent.TabIndex = 2;
            // 
            // dataGridViewFilteredData
            // 
            this.dataGridViewFilteredData.AllowUserToAddRows = false;
            this.dataGridViewFilteredData.AllowUserToDeleteRows = false;
            this.dataGridViewFilteredData.AllowUserToResizeColumns = false;
            this.dataGridViewFilteredData.AllowUserToResizeRows = false;
            this.dataGridViewFilteredData.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dataGridViewFilteredData.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.dataGridViewFilteredData.BackgroundColor = System.Drawing.Color.White;
            this.dataGridViewFilteredData.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.dataGridViewFilteredData.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewFilteredData.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridViewFilteredData.Location = new System.Drawing.Point(0, 0);
            this.dataGridViewFilteredData.MultiSelect = false;
            this.dataGridViewFilteredData.Name = "dataGridViewFilteredData";
            this.dataGridViewFilteredData.ReadOnly = true;
            this.dataGridViewFilteredData.RowHeadersVisible = false;
            this.dataGridViewFilteredData.RowTemplate.Height = 24;
            this.dataGridViewFilteredData.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridViewFilteredData.Size = new System.Drawing.Size(1370, 480);
            this.dataGridViewFilteredData.TabIndex = 3;
            // 
            // labeltitle
            // 
            this.labeltitle.Dock = System.Windows.Forms.DockStyle.Top;
            this.labeltitle.Location = new System.Drawing.Point(0, 0);
            this.labeltitle.Name = "labeltitle";
            this.labeltitle.Size = new System.Drawing.Size(1374, 23);
            this.labeltitle.TabIndex = 1;
            this.labeltitle.Text = "Admininstrator List";
            this.labeltitle.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // panelHeading
            // 
            this.panelHeading.AutoScroll = true;
            this.panelHeading.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panelHeading.Controls.Add(this.label1);
            this.panelHeading.Controls.Add(this.buttonDelete);
            this.panelHeading.Controls.Add(this.buttonEdit);
            this.panelHeading.Controls.Add(this.buttonNew);
            this.panelHeading.Controls.Add(this.textBoxFilterValue);
            this.panelHeading.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelHeading.Location = new System.Drawing.Point(0, 0);
            this.panelHeading.Name = "panelHeading";
            this.panelHeading.Size = new System.Drawing.Size(1374, 76);
            this.panelHeading.TabIndex = 5;
            // 
            // label1
            // 
            this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(1085, 27);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(61, 23);
            this.label1.TabIndex = 8;
            this.label1.Text = "Search";
            // 
            // buttonDelete
            // 
            this.buttonDelete.BackColor = System.Drawing.Color.RoyalBlue;
            this.buttonDelete.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonDelete.Image = global::LibraryManagementSystem.Properties.Resources.Denied_white_26px;
            this.buttonDelete.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.buttonDelete.Location = new System.Drawing.Point(322, 10);
            this.buttonDelete.Name = "buttonDelete";
            this.buttonDelete.Size = new System.Drawing.Size(150, 49);
            this.buttonDelete.TabIndex = 6;
            this.buttonDelete.Text = "Delete";
            this.buttonDelete.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.buttonDelete.UseVisualStyleBackColor = false;
            this.buttonDelete.Click += new System.EventHandler(this.buttonDelete_Click);
            // 
            // buttonEdit
            // 
            this.buttonEdit.BackColor = System.Drawing.Color.RoyalBlue;
            this.buttonEdit.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonEdit.Image = global::LibraryManagementSystem.Properties.Resources.Registration_white_26px;
            this.buttonEdit.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.buttonEdit.Location = new System.Drawing.Point(166, 10);
            this.buttonEdit.Name = "buttonEdit";
            this.buttonEdit.Size = new System.Drawing.Size(150, 49);
            this.buttonEdit.TabIndex = 5;
            this.buttonEdit.Text = "Edit";
            this.buttonEdit.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.buttonEdit.UseVisualStyleBackColor = false;
            this.buttonEdit.Click += new System.EventHandler(this.buttonEdit_Click);
            // 
            // buttonNew
            // 
            this.buttonNew.BackColor = System.Drawing.Color.RoyalBlue;
            this.buttonNew.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonNew.Image = global::LibraryManagementSystem.Properties.Resources.Add_User_Male_white_26px;
            this.buttonNew.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.buttonNew.Location = new System.Drawing.Point(10, 10);
            this.buttonNew.Name = "buttonNew";
            this.buttonNew.Size = new System.Drawing.Size(150, 49);
            this.buttonNew.TabIndex = 4;
            this.buttonNew.Text = "New";
            this.buttonNew.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.buttonNew.UseVisualStyleBackColor = false;
            this.buttonNew.Click += new System.EventHandler(this.buttonNew_Click);
            // 
            // AdminListUserControl
            // 
            this.BackColor = System.Drawing.Color.Navy;
            this.Controls.Add(this.panelBody);
            this.Controls.Add(this.panelHeading);
            this.Font = new System.Drawing.Font("Segoe UI", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ForeColor = System.Drawing.Color.White;
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.Name = "AdminListUserControl";
            this.Size = new System.Drawing.Size(1374, 583);
            this.Load += new System.EventHandler(this.AdminListUserControl_Load);
            this.panelBody.ResumeLayout(false);
            this.panelContent.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewFilteredData)).EndInit();
            this.panelHeading.ResumeLayout(false);
            this.panelHeading.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.TextBox textBoxFilterValue;
        private System.Windows.Forms.Panel panelBody;
        private System.Windows.Forms.Panel panelHeading;
        private System.Windows.Forms.Button buttonNew;
        private System.Windows.Forms.Button buttonEdit;
        private System.Windows.Forms.Button buttonDelete;
        private System.Windows.Forms.Label labeltitle;
        private System.Windows.Forms.Panel panelContent;
        private System.Windows.Forms.DataGridView dataGridViewFilteredData;
        private System.Windows.Forms.Label label1;
    }
}
