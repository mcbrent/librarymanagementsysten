﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using LibraryManagementSystem.app.ui.form;
using LibraryManagementSystem.app.db;
using LibraryManagementSystem.app.lib;

namespace LibraryManagementSystem.app.ui.uc
{
    public partial class OverdueBookReportUserControl : UserControl
    {
        public OverdueBookReportUserControl()
        {
            InitializeComponent();
           
        }

        private void BorrowReportUserControl_Load(object sender, EventArgs e)
        {
            DatabaseConnection dc = new DatabaseConnection();

        }

        private void comboBoxFilterType_SelectedIndexChanged(object sender, EventArgs e)
        {
            
        }

        private void buttonFilter_Click(object sender, EventArgs e)
        {
            dataGridViewFilteredData.DataSource = filterData();
        }

        private void buttonPrint_Click(object sender, EventArgs e)
        {
            String filterDescription = "";
            filterDescription += "Over Due Books";

            ReportViewerForm rvf = new ReportViewerForm();

            Dictionary<string, string> parameters = new Dictionary<string, string>();
            parameters.Add("ReportParameterReportType", "OVERDUE BOOK LIST");
            parameters.Add("ReportParameterFilterDescription", filterDescription);
            parameters.Add("ReportParameterDummyColumn1", "Book Card No.");
            parameters.Add("ReportParameterDummyColumn2", "Borrower");
            parameters.Add("ReportParameterDummyColumn3", "Title");
            parameters.Add("ReportParameterDummyColumn4", "Author");
            parameters.Add("ReportParameterDummyColumn5", "Category");
            parameters.Add("ReportParameterDummyColumn6", "Expected Date");


            DataTable buffer = filterData();
            buffer.Columns["Book Card No."].ColumnName = "DataColumnDummy1";
            buffer.Columns["Borrower"].ColumnName = "DataColumnDummy2";
            buffer.Columns["Title"].ColumnName = "DataColumnDummy3";
            buffer.Columns["Author"].ColumnName = "DataColumnDummy4";
            buffer.Columns["Category"].ColumnName = "DataColumnDummy5";
            buffer.Columns["Expected Date"].ColumnName = "DataColumnDummy6";

            rvf.ReportFile = "LibraryManagementSystem.reports.ReportBookPossessionList.rdlc";
            rvf.ReportDataTable = buffer;
            rvf.DicReportParameters = parameters;
            
                    
            
            rvf.Show();

        }



        private DataTable filterData()
        {
            DatabaseConnection dc = new DatabaseConnection();

            DataTable dt;

            Dictionary<string, string> dic = new Dictionary<string, string>();
            String query = "";
            

            query = @"SELECT book_call_no as 'Book Card No.', (
	            CASE 
		            WHEN book_status = 1 THEN
		            (
		            CASE
			            WHEN `borrow_type` = 1 THEN CONCAT(student_lastname, ', ', student_firstname, ' ', student_middlename)
			            WHEN `borrow_type` = 2 THEN adviser_name
			            WHEN `borrow_type` = 3 THEN borrow_other
			            ELSE ''
		            END
		            )
		            WHEN book_status = 0 THEN 'Library'
		            ELSE ''
	            END
            ) as 'Borrower', `bi_title` as 'Title', `author_name` as 'Author', category_name as 'Category', CONCAT(borrow_expected_date, ' - ', datediff(CURRENT_DATE(), borrow_expected_date), ' Days') as 'Expected Date'
            FROM `tbl_borrow`
            INNER JOIN `tbl_borrow_detail`
            ON `bd_borrow_id` = `borrow_id`
            INNER JOIN `tbl_book` 
            ON `book_id` = `bd_book_id`
            INNER JOIN `tbl_bookinfo`
            ON `bi_id` = `book_bi_id`
            INNER JOIN `tbl_category`
            ON `bi_bookcategory_id` = `category_id`
            INNER JOIN `tbl_author`
            ON `bi_author_id` = `author_id`
            LEFT JOIN `tbl_student`
            ON `borrow_student_id` = `student_id`
            LEFT JOIN `tbl_adviser`
            ON `borrow_adviser_id` = `adviser_id`
            WHERE `borrow_status` != '3' AND DATE(`borrow_expected_date`) <= CURRENT_DATE()
            ORDER BY borrow_expected_date ASC";
            


            dt = dc.Select(query, dic);
            
            return dt;
        }
    }
}
