﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using LibraryManagementSystem.app.ui.form;
using LibraryManagementSystem.app.db;
using LibraryManagementSystem.app.lib;

namespace LibraryManagementSystem.app.ui.uc
{
    public partial class BorrowReportUserControl : UserControl
    {
        public BorrowReportUserControl()
        {
            InitializeComponent();
           
        }

        private void BorrowReportUserControl_Load(object sender, EventArgs e)
        {
            comboBoxFilterType.SelectedIndex = 0;
        }

        private void comboBoxFilterType_SelectedIndexChanged(object sender, EventArgs e)
        {
            switch(comboBoxFilterType.SelectedIndex)
            {
                case 0:
                    panelDateFilter.Visible = false;
                    dateTimePickerFrom.Value = DateTime.Now;
                    dateTimePickerTo.Value = DateTime.Now;
                    break;
                case 1:
                    panelDateFilter.Visible = false;
                    dateTimePickerFrom.Value = DateTime.Today.AddDays(-(int)DateTime.Today.DayOfWeek + (int)DayOfWeek.Sunday);
                    dateTimePickerTo.Value = DateTime.Today.AddDays(-(int)DateTime.Today.DayOfWeek + (int)DayOfWeek.Saturday);
                    break;
                case 2:
                    panelDateFilter.Visible = false;
                    dateTimePickerFrom.Value = DateTime.Today.AddDays(-30);
                    dateTimePickerTo.Value = DateTime.Now;
                    break;
                case 3:
                    panelDateFilter.Visible = true;
                    dateTimePickerFrom.Value = DateTime.Now;
                    dateTimePickerTo.Value = DateTime.Now;
                    break;
                case 4:
                    panelDateFilter.Visible = false;
                    dateTimePickerFrom.Value = DateTime.Now;
                    dateTimePickerTo.Value = DateTime.Now;
                    break;

            }
        }

        private void buttonFilter_Click(object sender, EventArgs e)
        {
            dataGridViewFilteredData.DataSource = filterByDate();
        }

        private void buttonPrint_Click(object sender, EventArgs e)
        {
            String filterDescription = "";
            switch (comboBoxFilterType.SelectedIndex)
            {
                case 0:
                    filterDescription = "All current borrowed books today (" + DateTime.Now.ToString("MM-dd-yyyy") + ").";
                    break;
                case 1:
                    filterDescription = "All current borrowed books within this week.";
                    break;
                case 2:
                    filterDescription = "All current borrowed books for the past 30 Days";
                    break;
                case 3:
                    filterDescription = "Specific Date Range from " + dateTimePickerFrom.Value.ToString("MM-dd-yyyy") + " to " + dateTimePickerTo.Value.ToString("MM-dd-yyyy") + ".";
                    break;
                case 4:
                    filterDescription = "All current borrowed books.";
                    break;

            }

            Dictionary<string, string> parameters = new Dictionary<string, string>();
            parameters.Add("ReportParameterReportType", "Borrow Book");
            parameters.Add("ReportParameterFilterDescription", filterDescription);
            parameters.Add("ReportParameterDummyColumn1", "Date");
            parameters.Add("ReportParameterDummyColumn2", "Transaction Code");
            parameters.Add("ReportParameterDummyColumn3", "Title");
            parameters.Add("ReportParameterDummyColumn4", "Category");
            parameters.Add("ReportParameterDummyColumn5", "Borrower");
            ReportViewerForm rvf = new ReportViewerForm();
            DataTable buffer = filterByDate();
            buffer.Columns["Date"].ColumnName = "DataColumnDummy1";
            buffer.Columns["Transaction Code"].ColumnName = "DataColumnDummy2";
            buffer.Columns["Title"].ColumnName = "DataColumnDummy3";
            buffer.Columns["Category"].ColumnName = "DataColumnDummy4";
            buffer.Columns["Borrower"].ColumnName = "DataColumnDummy5";
            rvf.ReportDataTable = buffer;
            rvf.DicReportParameters = parameters;
            rvf.ReportFile = "LibraryManagementSystem.reports.ReportBorrowBook.rdlc";
            rvf.Show();

        }



        private DataTable filterByDate()
        {
            DatabaseConnection dc = new DatabaseConnection();
            DateTime dateTimeFrom = dateTimePickerFrom.Value;
            DateTime dateTimeTo = dateTimePickerTo.Value;

            DataTable dt;

            Dictionary<string, string> dic = new Dictionary<string, string>();
            String additional = "";
            if(comboBoxFilterType.SelectedIndex != 4)
            {
                dic.Add("@fromdate", dateTimeFrom.ToString("yyyy-MM-dd"));
                dic.Add("@todate", dateTimeTo.ToString("yyyy-MM-dd"));
                additional = " AND DATE(`borrow_date`) BETWEEN @fromdate AND @todate";
            }
            
            String query = @"SELECT bd_borrow_date as 'Date', borrow_tcode as 'Transaction Code', bi_title as 'Title', 
                            category_name as 'Category', 
                            (CASE
                            WHEN `borrow_type` = 1 THEN CONCAT(student_lastname, ', ', student_firstname, ' ', student_middlename)
                            WHEN `borrow_type` = 2 THEN adviser_name
                            WHEN `borrow_type` = 3 THEN borrow_other
                            ELSE ''
                            END) 'Borrower'
                            FROM `tbl_borrow_detail`
                            INNER JOIN `tbl_book`
                            ON `bd_book_id` = `book_id`
                            INNER JOIN `tbl_bookinfo`
                            ON `bi_id` = `book_bi_id`
                            INNER JOIN `tbl_category`
                            ON `bi_bookcategory_id` = `category_id`
                            INNER JOIN `tbl_borrow`
                            ON `bd_borrow_id` = `borrow_id`
                            LEFT JOIN `tbl_student`
                            ON `borrow_student_id` = `student_id`
                            LEFT JOIN `tbl_adviser`
                            ON `borrow_adviser_id` = `adviser_id`
                            WHERE bd_return_date IS NULL  " + additional;
            dt = dc.Select(query, dic);
            
            return dt;
        }
    }
}
