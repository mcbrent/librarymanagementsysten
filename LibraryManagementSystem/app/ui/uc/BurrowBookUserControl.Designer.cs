﻿namespace LibraryManagementSystem.app.ui.uc
{
    partial class BurrowBookUserControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panelBody = new System.Windows.Forms.Panel();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.label6 = new System.Windows.Forms.Label();
            this.labelBookCardNo = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.labelBookStatus = new System.Windows.Forms.Label();
            this.buttonScanBook = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.buttonAddToList = new System.Windows.Forms.Button();
            this.labelBookTitle = new System.Windows.Forms.Label();
            this.labelScannedBook = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.buttonScanAndAddToList = new System.Windows.Forms.Button();
            this.labelBookAuthor = new System.Windows.Forms.Label();
            this.textBoxBookNotes = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.labelBookCategory = new System.Windows.Forms.Label();
            this.labelBookISBN = new System.Windows.Forms.Label();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.panel2 = new System.Windows.Forms.Panel();
            this.dataGridViewBookList = new System.Windows.Forms.DataGridView();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.panelBorrowerOther = new System.Windows.Forms.Panel();
            this.textBoxOther = new System.Windows.Forms.TextBox();
            this.labelOther = new System.Windows.Forms.Label();
            this.panelBorrowerStudent = new System.Windows.Forms.Panel();
            this.label5 = new System.Windows.Forms.Label();
            this.textBoxStudentID = new System.Windows.Forms.TextBox();
            this.comboBoxStudentName = new System.Windows.Forms.ComboBox();
            this.labelSection = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.panelBorrowerFaculty = new System.Windows.Forms.Panel();
            this.comboBoxFaculty = new System.Windows.Forms.ComboBox();
            this.label4 = new System.Windows.Forms.Label();
            this.textBoxBorrowNotes = new System.Windows.Forms.TextBox();
            this.panelBorrowerType = new System.Windows.Forms.Panel();
            this.label3 = new System.Windows.Forms.Label();
            this.labelNotes = new System.Windows.Forms.Label();
            this.comboBoxBorrowerType = new System.Windows.Forms.ComboBox();
            this.labeltitle = new System.Windows.Forms.Label();
            this.buttonReset = new System.Windows.Forms.Button();
            this.buttonCompleteTransaction = new System.Windows.Forms.Button();
            this.panelHeader = new System.Windows.Forms.Panel();
            this.labelTransactionID = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.dateTimePickerReturn = new System.Windows.Forms.DateTimePicker();
            this.panelBody.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.panel1.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewBookList)).BeginInit();
            this.groupBox2.SuspendLayout();
            this.panelBorrowerOther.SuspendLayout();
            this.panelBorrowerStudent.SuspendLayout();
            this.panelBorrowerFaculty.SuspendLayout();
            this.panelHeader.SuspendLayout();
            this.SuspendLayout();
            // 
            // panelBody
            // 
            this.panelBody.AutoScroll = true;
            this.panelBody.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panelBody.Controls.Add(this.groupBox4);
            this.panelBody.Controls.Add(this.groupBox3);
            this.panelBody.Controls.Add(this.groupBox2);
            this.panelBody.Controls.Add(this.labeltitle);
            this.panelBody.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelBody.Location = new System.Drawing.Point(0, 52);
            this.panelBody.Name = "panelBody";
            this.panelBody.Size = new System.Drawing.Size(1374, 606);
            this.panelBody.TabIndex = 4;
            // 
            // groupBox4
            // 
            this.groupBox4.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox4.Controls.Add(this.panel1);
            this.groupBox4.ForeColor = System.Drawing.Color.White;
            this.groupBox4.Location = new System.Drawing.Point(829, 30);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(530, 551);
            this.groupBox4.TabIndex = 11;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Book Information";
            // 
            // panel1
            // 
            this.panel1.AutoScroll = true;
            this.panel1.Controls.Add(this.label6);
            this.panel1.Controls.Add(this.labelBookCardNo);
            this.panel1.Controls.Add(this.label11);
            this.panel1.Controls.Add(this.labelBookStatus);
            this.panel1.Controls.Add(this.buttonScanBook);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.label17);
            this.panel1.Controls.Add(this.buttonAddToList);
            this.panel1.Controls.Add(this.labelBookTitle);
            this.panel1.Controls.Add(this.labelScannedBook);
            this.panel1.Controls.Add(this.label15);
            this.panel1.Controls.Add(this.buttonScanAndAddToList);
            this.panel1.Controls.Add(this.labelBookAuthor);
            this.panel1.Controls.Add(this.textBoxBookNotes);
            this.panel1.Controls.Add(this.label13);
            this.panel1.Controls.Add(this.label9);
            this.panel1.Controls.Add(this.labelBookCategory);
            this.panel1.Controls.Add(this.labelBookISBN);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(3, 26);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(524, 522);
            this.panel1.TabIndex = 25;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(31, 22);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(121, 23);
            this.label6.TabIndex = 25;
            this.label6.Text = "Book Card No.";
            // 
            // labelBookCardNo
            // 
            this.labelBookCardNo.AutoSize = true;
            this.labelBookCardNo.Location = new System.Drawing.Point(164, 23);
            this.labelBookCardNo.Name = "labelBookCardNo";
            this.labelBookCardNo.Size = new System.Drawing.Size(179, 23);
            this.labelBookCardNo.TabIndex = 26;
            this.labelBookCardNo.Text = "<< Book Card No. >>";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(61, 54);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(90, 23);
            this.label11.TabIndex = 16;
            this.label11.Text = "Book ISBN";
            // 
            // labelBookStatus
            // 
            this.labelBookStatus.AutoSize = true;
            this.labelBookStatus.Location = new System.Drawing.Point(155, 195);
            this.labelBookStatus.Name = "labelBookStatus";
            this.labelBookStatus.Size = new System.Drawing.Size(157, 23);
            this.labelBookStatus.TabIndex = 24;
            this.labelBookStatus.Tag = "0";
            this.labelBookStatus.Text = "<< Book Status >>";
            // 
            // buttonScanBook
            // 
            this.buttonScanBook.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonScanBook.BackColor = System.Drawing.Color.RoyalBlue;
            this.buttonScanBook.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonScanBook.Image = global::LibraryManagementSystem.Properties.Resources.RFID_Tag_white_26px;
            this.buttonScanBook.Location = new System.Drawing.Point(15, 355);
            this.buttonScanBook.Name = "buttonScanBook";
            this.buttonScanBook.Size = new System.Drawing.Size(490, 50);
            this.buttonScanBook.TabIndex = 4;
            this.buttonScanBook.Tag = "0";
            this.buttonScanBook.Text = "Scan Book";
            this.buttonScanBook.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.buttonScanBook.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.buttonScanBook.UseVisualStyleBackColor = false;
            this.buttonScanBook.Click += new System.EventHandler(this.buttonScanBook_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(43, 195);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(99, 23);
            this.label1.TabIndex = 23;
            this.label1.Text = "Book Status";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(67, 87);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(85, 23);
            this.label17.TabIndex = 10;
            this.label17.Text = "Book Title";
            // 
            // buttonAddToList
            // 
            this.buttonAddToList.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonAddToList.BackColor = System.Drawing.Color.RoyalBlue;
            this.buttonAddToList.Enabled = false;
            this.buttonAddToList.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonAddToList.Image = global::LibraryManagementSystem.Properties.Resources.Plus_white_26px;
            this.buttonAddToList.Location = new System.Drawing.Point(15, 409);
            this.buttonAddToList.Name = "buttonAddToList";
            this.buttonAddToList.Size = new System.Drawing.Size(490, 50);
            this.buttonAddToList.TabIndex = 22;
            this.buttonAddToList.Tag = "0";
            this.buttonAddToList.Text = "Add To List";
            this.buttonAddToList.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.buttonAddToList.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.buttonAddToList.UseVisualStyleBackColor = false;
            this.buttonAddToList.Click += new System.EventHandler(this.buttonAddToList_Click);
            // 
            // labelBookTitle
            // 
            this.labelBookTitle.AutoSize = true;
            this.labelBookTitle.Location = new System.Drawing.Point(164, 87);
            this.labelBookTitle.Name = "labelBookTitle";
            this.labelBookTitle.Size = new System.Drawing.Size(143, 23);
            this.labelBookTitle.TabIndex = 11;
            this.labelBookTitle.Text = "<< Book Title >>";
            // 
            // labelScannedBook
            // 
            this.labelScannedBook.AutoSize = true;
            this.labelScannedBook.Location = new System.Drawing.Point(29, 327);
            this.labelScannedBook.Name = "labelScannedBook";
            this.labelScannedBook.Size = new System.Drawing.Size(68, 23);
            this.labelScannedBook.TabIndex = 21;
            this.labelScannedBook.Tag = "0";
            this.labelScannedBook.Text = "ID Here";
            this.labelScannedBook.Visible = false;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(45, 122);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(106, 23);
            this.label15.TabIndex = 12;
            this.label15.Text = "Book Author";
            // 
            // buttonScanAndAddToList
            // 
            this.buttonScanAndAddToList.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonScanAndAddToList.BackColor = System.Drawing.Color.RoyalBlue;
            this.buttonScanAndAddToList.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonScanAndAddToList.Image = global::LibraryManagementSystem.Properties.Resources.Add_List_white_26px;
            this.buttonScanAndAddToList.Location = new System.Drawing.Point(15, 465);
            this.buttonScanAndAddToList.Name = "buttonScanAndAddToList";
            this.buttonScanAndAddToList.Size = new System.Drawing.Size(490, 50);
            this.buttonScanAndAddToList.TabIndex = 20;
            this.buttonScanAndAddToList.Tag = "0";
            this.buttonScanAndAddToList.Text = "Scan && Add To List";
            this.buttonScanAndAddToList.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.buttonScanAndAddToList.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.buttonScanAndAddToList.UseVisualStyleBackColor = false;
            this.buttonScanAndAddToList.Click += new System.EventHandler(this.buttonScanAndAddToList_Click);
            // 
            // labelBookAuthor
            // 
            this.labelBookAuthor.AutoSize = true;
            this.labelBookAuthor.Location = new System.Drawing.Point(164, 122);
            this.labelBookAuthor.Name = "labelBookAuthor";
            this.labelBookAuthor.Size = new System.Drawing.Size(164, 23);
            this.labelBookAuthor.TabIndex = 13;
            this.labelBookAuthor.Text = "<< Book Author >>";
            // 
            // textBoxBookNotes
            // 
            this.textBoxBookNotes.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textBoxBookNotes.BackColor = System.Drawing.Color.White;
            this.textBoxBookNotes.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBoxBookNotes.Location = new System.Drawing.Point(160, 231);
            this.textBoxBookNotes.Multiline = true;
            this.textBoxBookNotes.Name = "textBoxBookNotes";
            this.textBoxBookNotes.ReadOnly = true;
            this.textBoxBookNotes.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.textBoxBookNotes.Size = new System.Drawing.Size(335, 85);
            this.textBoxBookNotes.TabIndex = 19;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(29, 157);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(122, 23);
            this.label13.TabIndex = 14;
            this.label13.Text = "Book Category";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(43, 234);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(98, 23);
            this.label9.TabIndex = 18;
            this.label9.Text = "Book Notes";
            // 
            // labelBookCategory
            // 
            this.labelBookCategory.AutoSize = true;
            this.labelBookCategory.Location = new System.Drawing.Point(164, 157);
            this.labelBookCategory.Name = "labelBookCategory";
            this.labelBookCategory.Size = new System.Drawing.Size(180, 23);
            this.labelBookCategory.TabIndex = 15;
            this.labelBookCategory.Text = "<< Book Category >>";
            // 
            // labelBookISBN
            // 
            this.labelBookISBN.AutoSize = true;
            this.labelBookISBN.Location = new System.Drawing.Point(164, 54);
            this.labelBookISBN.Name = "labelBookISBN";
            this.labelBookISBN.Size = new System.Drawing.Size(148, 23);
            this.labelBookISBN.TabIndex = 17;
            this.labelBookISBN.Text = "<< Book ISBN >>";
            // 
            // groupBox3
            // 
            this.groupBox3.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.groupBox3.Controls.Add(this.panel2);
            this.groupBox3.ForeColor = System.Drawing.Color.White;
            this.groupBox3.Location = new System.Drawing.Point(12, 339);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(797, 242);
            this.groupBox3.TabIndex = 10;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Borrowed Book";
            // 
            // panel2
            // 
            this.panel2.AutoScroll = true;
            this.panel2.Controls.Add(this.dataGridViewBookList);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel2.ForeColor = System.Drawing.Color.Black;
            this.panel2.Location = new System.Drawing.Point(3, 26);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(791, 213);
            this.panel2.TabIndex = 2;
            // 
            // dataGridViewBookList
            // 
            this.dataGridViewBookList.AllowUserToAddRows = false;
            this.dataGridViewBookList.AllowUserToDeleteRows = false;
            this.dataGridViewBookList.AllowUserToResizeColumns = false;
            this.dataGridViewBookList.AllowUserToResizeRows = false;
            this.dataGridViewBookList.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dataGridViewBookList.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.dataGridViewBookList.BackgroundColor = System.Drawing.Color.White;
            this.dataGridViewBookList.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewBookList.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridViewBookList.Location = new System.Drawing.Point(0, 0);
            this.dataGridViewBookList.MultiSelect = false;
            this.dataGridViewBookList.Name = "dataGridViewBookList";
            this.dataGridViewBookList.ReadOnly = true;
            this.dataGridViewBookList.RowHeadersVisible = false;
            this.dataGridViewBookList.RowTemplate.Height = 24;
            this.dataGridViewBookList.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridViewBookList.Size = new System.Drawing.Size(791, 213);
            this.dataGridViewBookList.TabIndex = 1;
            this.dataGridViewBookList.RowsAdded += new System.Windows.Forms.DataGridViewRowsAddedEventHandler(this.dataGridViewBookList_RowsAdded);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.dateTimePickerReturn);
            this.groupBox2.Controls.Add(this.label10);
            this.groupBox2.Controls.Add(this.panelBorrowerOther);
            this.groupBox2.Controls.Add(this.panelBorrowerStudent);
            this.groupBox2.Controls.Add(this.panelBorrowerFaculty);
            this.groupBox2.Controls.Add(this.textBoxBorrowNotes);
            this.groupBox2.Controls.Add(this.panelBorrowerType);
            this.groupBox2.Controls.Add(this.label3);
            this.groupBox2.Controls.Add(this.labelNotes);
            this.groupBox2.Controls.Add(this.comboBoxBorrowerType);
            this.groupBox2.ForeColor = System.Drawing.Color.White;
            this.groupBox2.Location = new System.Drawing.Point(12, 30);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(797, 303);
            this.groupBox2.TabIndex = 9;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Borrower Information";
            // 
            // panelBorrowerOther
            // 
            this.panelBorrowerOther.Controls.Add(this.textBoxOther);
            this.panelBorrowerOther.Controls.Add(this.labelOther);
            this.panelBorrowerOther.Location = new System.Drawing.Point(710, 12);
            this.panelBorrowerOther.Name = "panelBorrowerOther";
            this.panelBorrowerOther.Size = new System.Drawing.Size(72, 54);
            this.panelBorrowerOther.TabIndex = 1;
            this.panelBorrowerOther.Visible = false;
            this.panelBorrowerOther.DockChanged += new System.EventHandler(this.panelBorrowerOther_DockChanged);
            // 
            // textBoxOther
            // 
            this.textBoxOther.Location = new System.Drawing.Point(96, 16);
            this.textBoxOther.Name = "textBoxOther";
            this.textBoxOther.Size = new System.Drawing.Size(658, 30);
            this.textBoxOther.TabIndex = 19;
            // 
            // labelOther
            // 
            this.labelOther.AutoSize = true;
            this.labelOther.Location = new System.Drawing.Point(25, 25);
            this.labelOther.Name = "labelOther";
            this.labelOther.Size = new System.Drawing.Size(54, 23);
            this.labelOther.TabIndex = 18;
            this.labelOther.Text = "Other";
            // 
            // panelBorrowerStudent
            // 
            this.panelBorrowerStudent.Controls.Add(this.label5);
            this.panelBorrowerStudent.Controls.Add(this.textBoxStudentID);
            this.panelBorrowerStudent.Controls.Add(this.comboBoxStudentName);
            this.panelBorrowerStudent.Controls.Add(this.labelSection);
            this.panelBorrowerStudent.Controls.Add(this.label7);
            this.panelBorrowerStudent.Controls.Add(this.label8);
            this.panelBorrowerStudent.Location = new System.Drawing.Point(577, 14);
            this.panelBorrowerStudent.Name = "panelBorrowerStudent";
            this.panelBorrowerStudent.Size = new System.Drawing.Size(23, 27);
            this.panelBorrowerStudent.TabIndex = 14;
            this.panelBorrowerStudent.Visible = false;
            this.panelBorrowerStudent.DockChanged += new System.EventHandler(this.panelBorrowerStudent_DockChanged);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(8, 11);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(91, 23);
            this.label5.TabIndex = 1;
            this.label5.Text = "Student ID";
            // 
            // textBoxStudentID
            // 
            this.textBoxStudentID.Location = new System.Drawing.Point(105, 8);
            this.textBoxStudentID.Name = "textBoxStudentID";
            this.textBoxStudentID.Size = new System.Drawing.Size(658, 30);
            this.textBoxStudentID.TabIndex = 0;
            this.textBoxStudentID.KeyDown += new System.Windows.Forms.KeyEventHandler(this.textBoxStudentID_KeyDown);
            this.textBoxStudentID.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textBoxStudentID_KeyPress);
            // 
            // comboBoxStudentName
            // 
            this.comboBoxStudentName.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxStudentName.FormattingEnabled = true;
            this.comboBoxStudentName.Location = new System.Drawing.Point(105, 44);
            this.comboBoxStudentName.Name = "comboBoxStudentName";
            this.comboBoxStudentName.Size = new System.Drawing.Size(658, 31);
            this.comboBoxStudentName.TabIndex = 8;
            this.comboBoxStudentName.SelectionChangeCommitted += new System.EventHandler(this.comboBoxStudentName_SelectionChangeCommitted);
            // 
            // labelSection
            // 
            this.labelSection.AutoSize = true;
            this.labelSection.Location = new System.Drawing.Point(108, 83);
            this.labelSection.Name = "labelSection";
            this.labelSection.Size = new System.Drawing.Size(0, 23);
            this.labelSection.TabIndex = 11;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(43, 47);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(56, 23);
            this.label7.TabIndex = 9;
            this.label7.Text = "Name";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(32, 83);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(66, 23);
            this.label8.TabIndex = 10;
            this.label8.Text = "Section";
            // 
            // panelBorrowerFaculty
            // 
            this.panelBorrowerFaculty.Controls.Add(this.comboBoxFaculty);
            this.panelBorrowerFaculty.Controls.Add(this.label4);
            this.panelBorrowerFaculty.Location = new System.Drawing.Point(630, 19);
            this.panelBorrowerFaculty.Name = "panelBorrowerFaculty";
            this.panelBorrowerFaculty.Size = new System.Drawing.Size(64, 39);
            this.panelBorrowerFaculty.TabIndex = 0;
            this.panelBorrowerFaculty.Visible = false;
            this.panelBorrowerFaculty.DockChanged += new System.EventHandler(this.panelBorrowerFaculty_DockChanged);
            // 
            // comboBoxFaculty
            // 
            this.comboBoxFaculty.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxFaculty.FormattingEnabled = true;
            this.comboBoxFaculty.Location = new System.Drawing.Point(98, 11);
            this.comboBoxFaculty.Name = "comboBoxFaculty";
            this.comboBoxFaculty.Size = new System.Drawing.Size(665, 31);
            this.comboBoxFaculty.TabIndex = 19;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(20, 12);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(62, 23);
            this.label4.TabIndex = 18;
            this.label4.Text = "Faculty";
            // 
            // textBoxBorrowNotes
            // 
            this.textBoxBorrowNotes.Location = new System.Drawing.Point(118, 232);
            this.textBoxBorrowNotes.MaxLength = 255;
            this.textBoxBorrowNotes.Multiline = true;
            this.textBoxBorrowNotes.Name = "textBoxBorrowNotes";
            this.textBoxBorrowNotes.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.textBoxBorrowNotes.Size = new System.Drawing.Size(643, 60);
            this.textBoxBorrowNotes.TabIndex = 13;
            // 
            // panelBorrowerType
            // 
            this.panelBorrowerType.Location = new System.Drawing.Point(5, 72);
            this.panelBorrowerType.Name = "panelBorrowerType";
            this.panelBorrowerType.Size = new System.Drawing.Size(790, 116);
            this.panelBorrowerType.TabIndex = 17;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(167, 39);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(119, 23);
            this.label3.TabIndex = 16;
            this.label3.Text = "Borrower Type";
            // 
            // labelNotes
            // 
            this.labelNotes.AutoSize = true;
            this.labelNotes.Location = new System.Drawing.Point(54, 231);
            this.labelNotes.Name = "labelNotes";
            this.labelNotes.Size = new System.Drawing.Size(55, 23);
            this.labelNotes.TabIndex = 12;
            this.labelNotes.Text = "Notes";
            // 
            // comboBoxBorrowerType
            // 
            this.comboBoxBorrowerType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxBorrowerType.FormattingEnabled = true;
            this.comboBoxBorrowerType.Items.AddRange(new object[] {
            "Student",
            "Faculty",
            "Other"});
            this.comboBoxBorrowerType.Location = new System.Drawing.Point(292, 34);
            this.comboBoxBorrowerType.Name = "comboBoxBorrowerType";
            this.comboBoxBorrowerType.Size = new System.Drawing.Size(286, 31);
            this.comboBoxBorrowerType.TabIndex = 15;
            this.comboBoxBorrowerType.SelectedIndexChanged += new System.EventHandler(this.comboBoxBorrowerType_SelectedIndexChanged);
            // 
            // labeltitle
            // 
            this.labeltitle.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.labeltitle.Location = new System.Drawing.Point(8, 2);
            this.labeltitle.Name = "labeltitle";
            this.labeltitle.Size = new System.Drawing.Size(1347, 23);
            this.labeltitle.TabIndex = 2;
            this.labeltitle.Text = "Borrow Books";
            this.labeltitle.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // buttonReset
            // 
            this.buttonReset.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonReset.BackColor = System.Drawing.Color.RoyalBlue;
            this.buttonReset.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonReset.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.buttonReset.Location = new System.Drawing.Point(1155, 5);
            this.buttonReset.Name = "buttonReset";
            this.buttonReset.Size = new System.Drawing.Size(210, 43);
            this.buttonReset.TabIndex = 13;
            this.buttonReset.Text = "Reset";
            this.buttonReset.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.buttonReset.UseVisualStyleBackColor = false;
            this.buttonReset.Click += new System.EventHandler(this.buttonReset_Click);
            // 
            // buttonCompleteTransaction
            // 
            this.buttonCompleteTransaction.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonCompleteTransaction.BackColor = System.Drawing.Color.RoyalBlue;
            this.buttonCompleteTransaction.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonCompleteTransaction.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.buttonCompleteTransaction.Location = new System.Drawing.Point(939, 5);
            this.buttonCompleteTransaction.Name = "buttonCompleteTransaction";
            this.buttonCompleteTransaction.Size = new System.Drawing.Size(210, 43);
            this.buttonCompleteTransaction.TabIndex = 12;
            this.buttonCompleteTransaction.Text = "Complete Transaction";
            this.buttonCompleteTransaction.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.buttonCompleteTransaction.UseVisualStyleBackColor = false;
            this.buttonCompleteTransaction.Click += new System.EventHandler(this.buttonCompleteTransaction_Click);
            // 
            // panelHeader
            // 
            this.panelHeader.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panelHeader.Controls.Add(this.labelTransactionID);
            this.panelHeader.Controls.Add(this.label2);
            this.panelHeader.Controls.Add(this.buttonCompleteTransaction);
            this.panelHeader.Controls.Add(this.buttonReset);
            this.panelHeader.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelHeader.Location = new System.Drawing.Point(0, 0);
            this.panelHeader.Name = "panelHeader";
            this.panelHeader.Size = new System.Drawing.Size(1374, 52);
            this.panelHeader.TabIndex = 14;
            // 
            // labelTransactionID
            // 
            this.labelTransactionID.AutoSize = true;
            this.labelTransactionID.BackColor = System.Drawing.Color.White;
            this.labelTransactionID.ForeColor = System.Drawing.Color.Red;
            this.labelTransactionID.Location = new System.Drawing.Point(155, 15);
            this.labelTransactionID.Name = "labelTransactionID";
            this.labelTransactionID.Size = new System.Drawing.Size(0, 23);
            this.labelTransactionID.TabIndex = 15;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(21, 15);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(128, 23);
            this.label2.TabIndex = 14;
            this.label2.Text = "Transaction ID: ";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(10, 199);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(102, 23);
            this.label10.TabIndex = 18;
            this.label10.Text = "Return Date";
            // 
            // dateTimePickerReturn
            // 
            this.dateTimePickerReturn.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dateTimePickerReturn.Location = new System.Drawing.Point(119, 196);
            this.dateTimePickerReturn.Name = "dateTimePickerReturn";
            this.dateTimePickerReturn.Size = new System.Drawing.Size(643, 30);
            this.dateTimePickerReturn.TabIndex = 19;
            this.dateTimePickerReturn.Value = new System.DateTime(2018, 11, 30, 11, 2, 20, 0);
            // 
            // BurrowBookUserControl
            // 
            this.AutoScroll = true;
            this.BackColor = System.Drawing.Color.Navy;
            this.Controls.Add(this.panelBody);
            this.Controls.Add(this.panelHeader);
            this.Font = new System.Drawing.Font("Segoe UI", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ForeColor = System.Drawing.Color.White;
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.Name = "BurrowBookUserControl";
            this.Size = new System.Drawing.Size(1374, 658);
            this.Load += new System.EventHandler(this.BurrowBookUserControl_Load);
            this.panelBody.ResumeLayout(false);
            this.groupBox4.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewBookList)).EndInit();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.panelBorrowerOther.ResumeLayout(false);
            this.panelBorrowerOther.PerformLayout();
            this.panelBorrowerStudent.ResumeLayout(false);
            this.panelBorrowerStudent.PerformLayout();
            this.panelBorrowerFaculty.ResumeLayout(false);
            this.panelBorrowerFaculty.PerformLayout();
            this.panelHeader.ResumeLayout(false);
            this.panelHeader.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Panel panelBody;
        private System.Windows.Forms.Button buttonScanBook;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox textBoxStudentID;
        private System.Windows.Forms.ComboBox comboBoxStudentName;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label labelSection;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.DataGridView dataGridViewBookList;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.TextBox textBoxBookNotes;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label labelBookISBN;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label labelBookCategory;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label labelBookAuthor;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label labelBookTitle;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Button buttonScanAndAddToList;
        private System.Windows.Forms.Label labelScannedBook;
        private System.Windows.Forms.Button buttonAddToList;
        private System.Windows.Forms.Button buttonReset;
        private System.Windows.Forms.Button buttonCompleteTransaction;
        private System.Windows.Forms.Panel panelHeader;
        private System.Windows.Forms.Label labelBookStatus;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label labeltitle;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.TextBox textBoxBorrowNotes;
        private System.Windows.Forms.Label labelNotes;
        private System.Windows.Forms.Label labelTransactionID;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Panel panelBorrowerStudent;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ComboBox comboBoxBorrowerType;
        private System.Windows.Forms.Panel panelBorrowerType;
        private System.Windows.Forms.Panel panelBorrowerFaculty;
        private System.Windows.Forms.ComboBox comboBoxFaculty;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Panel panelBorrowerOther;
        private System.Windows.Forms.Label labelOther;
        private System.Windows.Forms.TextBox textBoxOther;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label labelBookCardNo;
        private System.Windows.Forms.DateTimePicker dateTimePickerReturn;
        private System.Windows.Forms.Label label10;
    }
}
