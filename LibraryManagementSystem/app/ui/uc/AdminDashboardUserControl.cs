﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Net;
using System.Net.Sockets;
using System.IO;
using LibraryManagementSystem.app.db;
using LibraryManagementSystem.app.lib;

namespace LibraryManagementSystem.app.ui.uc
{
    public partial class AdminDashboardUserControl : UserControl
    {
        private RFIDController rfidC;
        private DatabaseConnection dc;
        private int timerUpdate = 5;
        public AdminDashboardUserControl()
        {
            InitializeComponent();
            dc = new DatabaseConnection();
            rfidC = new RFIDController();
        }

        private void timerClock_Tick(object sender, EventArgs e)
        {
            labelClock.Text = DateTime.Now.ToString("dddd - MM/dd/yyyy hh:mm:ss tt");
            if(timerUpdate-- == 0)
            {
                updateDashboard();
                timerUpdate = 5;
            }
        }

        private void buttonScanBook_Click(object sender, EventArgs e)
        {
            DataTable dtBook = scanBook();
            if (dtBook != null)
            {
                panelScanned.Visible = true;
                labelBookISBN.Text = dtBook.Rows[0]["bi_isbn"].ToString();
                labelBookTitle.Text = dtBook.Rows[0]["bi_title"].ToString();
                labelCurrentPossesion.Text = dtBook.Rows[0]["curr_pos"].ToString();
                if (dtBook.Rows[0]["book_status"].ToString() == "0")
                {
                    labelCurrentPossesion.ForeColor = Color.Lime;
                }
                else
                {
                    labelCurrentPossesion.ForeColor = Color.Red;
                }
            }
            else
            {
                panelScanned.Visible = false;
            }
        }

        private void updateDashboard()
        {
            
            String query = @"SELECT COUNT('al_id') as 'log_stud'
                            FROM `tbl_attendance_log`
                            WHERE DATE(`al_entry_date`) = CURDATE() AND al_exit_date IS NULL
                                ";
            DataTable dtPresent = dc.Select(query);
            if(dtPresent.Rows.Count > 0)
            {
                labelStudentPresent.Text = dtPresent.Rows[0]["log_stud"].ToString();
            }
            else
            {
                labelStudentPresent.Text = "0";
            }

        }

        private DataTable scanBook()
        {
            DataTable dtBook = null;
            if (rfidC.Connect())
            {
                if (rfidC.CheckCard())
                {
                    String Rfid = rfidC.Read();
                    Dictionary<string, string> dic = new Dictionary<string, string>();
                    dic.Add("@book_rfid", Rfid);
                    String query = @"SELECT bi_isbn, bi_title, book_notes,
                            category_name, author_name, book_status,
                            (
                                CASE 
    	                            WHEN book_status = 1 THEN
                                    (
                                    CASE
                                        WHEN `borrow_type` = 1 THEN CONCAT(student_lastname, ', ', student_firstname, ' ', student_middlename)
                                        WHEN `borrow_type` = 2 THEN adviser_name
                                        WHEN `borrow_type` = 3 THEN borrow_other
                                        ELSE ''
                                    END
                                    )
    	                            WHEN book_status = 0 THEN 'Library'
                                    ELSE 'On Hold'
                                END
                            ) as 'curr_pos', MAX(bd_borrow_date)
                            FROM `tbl_bookinfo` 
                            INNER JOIN `tbl_category`
                            ON `bi_bookcategory_id` = `category_id`
                            INNER JOIN `tbl_author`
                            ON `bi_author_id` = `author_id`
                            LEFT JOIN `tbl_book`
                            ON `book_bi_id` = `bi_id`
                            LEFT JOIN `tbl_borrow_detail`
                            ON `bd_book_id` = `book_id`
                            LEFT JOIN `tbl_borrow`
                            ON `borrow_id` = `bd_borrow_id`
                            LEFT JOIN `tbl_student`
                            ON `borrow_student_id` = `student_id`
                            LEFT JOIN `tbl_adviser`
                            ON `borrow_adviser_id` = `adviser_id`
                            WHERE book_rfid = @book_rfid
                            GROUP BY book_id
                                ";
                    DataTable dt = dc.Select(query, dic);
                    if (dt.Rows.Count > 0)
                    {
                        dtBook = dt;
                    }
                    else
                    {
                        MessageBox.Show(this, "This book isn't Registered");
                    }

                }
                else
                {
                    MessageBox.Show(this, "Make sure Book has an RFID Sticker", "Book Tag undetected", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            
            return dtBook;
        }
    }
}
