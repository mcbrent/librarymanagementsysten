﻿namespace LibraryManagementSystem.app.ui.uc
{
    partial class BookAuthorListUserControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.textBoxFilterValue = new System.Windows.Forms.TextBox();
            this.panelBody = new System.Windows.Forms.Panel();
            this.dataGridViewBookAuthorList = new System.Windows.Forms.DataGridView();
            this.labeltitle = new System.Windows.Forms.Label();
            this.panelHeading = new System.Windows.Forms.Panel();
            this.buttonDelete = new System.Windows.Forms.Button();
            this.buttonEdit = new System.Windows.Forms.Button();
            this.buttonNew = new System.Windows.Forms.Button();
            this.panelBody.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewBookAuthorList)).BeginInit();
            this.panelHeading.SuspendLayout();
            this.SuspendLayout();
            // 
            // textBoxFilterValue
            // 
            this.textBoxFilterValue.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.textBoxFilterValue.Location = new System.Drawing.Point(1019, 19);
            this.textBoxFilterValue.Name = "textBoxFilterValue";
            this.textBoxFilterValue.Size = new System.Drawing.Size(336, 30);
            this.textBoxFilterValue.TabIndex = 3;
            this.textBoxFilterValue.TextChanged += new System.EventHandler(this.textBoxFilterValue_TextChanged);
            // 
            // panelBody
            // 
            this.panelBody.Controls.Add(this.dataGridViewBookAuthorList);
            this.panelBody.Controls.Add(this.labeltitle);
            this.panelBody.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelBody.Location = new System.Drawing.Point(0, 76);
            this.panelBody.Name = "panelBody";
            this.panelBody.Size = new System.Drawing.Size(1374, 516);
            this.panelBody.TabIndex = 4;
            // 
            // dataGridViewBookAuthorList
            // 
            this.dataGridViewBookAuthorList.AllowUserToAddRows = false;
            this.dataGridViewBookAuthorList.AllowUserToDeleteRows = false;
            this.dataGridViewBookAuthorList.AllowUserToResizeColumns = false;
            this.dataGridViewBookAuthorList.AllowUserToResizeRows = false;
            this.dataGridViewBookAuthorList.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dataGridViewBookAuthorList.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dataGridViewBookAuthorList.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.dataGridViewBookAuthorList.BackgroundColor = System.Drawing.Color.White;
            this.dataGridViewBookAuthorList.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.dataGridViewBookAuthorList.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewBookAuthorList.Location = new System.Drawing.Point(5, 32);
            this.dataGridViewBookAuthorList.MultiSelect = false;
            this.dataGridViewBookAuthorList.Name = "dataGridViewBookAuthorList";
            this.dataGridViewBookAuthorList.ReadOnly = true;
            this.dataGridViewBookAuthorList.RowHeadersVisible = false;
            this.dataGridViewBookAuthorList.RowTemplate.Height = 24;
            this.dataGridViewBookAuthorList.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridViewBookAuthorList.Size = new System.Drawing.Size(1354, 477);
            this.dataGridViewBookAuthorList.TabIndex = 3;
            // 
            // labeltitle
            // 
            this.labeltitle.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.labeltitle.Location = new System.Drawing.Point(8, 6);
            this.labeltitle.Name = "labeltitle";
            this.labeltitle.Size = new System.Drawing.Size(1351, 23);
            this.labeltitle.TabIndex = 2;
            this.labeltitle.Text = "Book Author List";
            this.labeltitle.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panelHeading
            // 
            this.panelHeading.AutoScroll = true;
            this.panelHeading.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panelHeading.Controls.Add(this.buttonDelete);
            this.panelHeading.Controls.Add(this.buttonEdit);
            this.panelHeading.Controls.Add(this.buttonNew);
            this.panelHeading.Controls.Add(this.textBoxFilterValue);
            this.panelHeading.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelHeading.Location = new System.Drawing.Point(0, 0);
            this.panelHeading.Name = "panelHeading";
            this.panelHeading.Size = new System.Drawing.Size(1374, 76);
            this.panelHeading.TabIndex = 5;
            // 
            // buttonDelete
            // 
            this.buttonDelete.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.buttonDelete.Location = new System.Drawing.Point(322, 10);
            this.buttonDelete.Name = "buttonDelete";
            this.buttonDelete.Size = new System.Drawing.Size(150, 49);
            this.buttonDelete.TabIndex = 6;
            this.buttonDelete.Text = "Delete";
            this.buttonDelete.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.buttonDelete.UseVisualStyleBackColor = true;
            this.buttonDelete.Click += new System.EventHandler(this.buttonDelete_Click);
            // 
            // buttonEdit
            // 
            this.buttonEdit.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.buttonEdit.Location = new System.Drawing.Point(166, 10);
            this.buttonEdit.Name = "buttonEdit";
            this.buttonEdit.Size = new System.Drawing.Size(150, 49);
            this.buttonEdit.TabIndex = 5;
            this.buttonEdit.Text = "Edit";
            this.buttonEdit.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.buttonEdit.UseVisualStyleBackColor = true;
            this.buttonEdit.Click += new System.EventHandler(this.buttonEdit_Click);
            // 
            // buttonNew
            // 
            this.buttonNew.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.buttonNew.Location = new System.Drawing.Point(10, 10);
            this.buttonNew.Name = "buttonNew";
            this.buttonNew.Size = new System.Drawing.Size(150, 49);
            this.buttonNew.TabIndex = 4;
            this.buttonNew.Text = "New";
            this.buttonNew.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.buttonNew.UseVisualStyleBackColor = true;
            this.buttonNew.Click += new System.EventHandler(this.buttonNew_Click);
            // 
            // BookAuthorListUserControl
            // 
            this.BackColor = System.Drawing.Color.White;
            this.Controls.Add(this.panelBody);
            this.Controls.Add(this.panelHeading);
            this.Font = new System.Drawing.Font("Segoe UI", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.Name = "BookAuthorListUserControl";
            this.Size = new System.Drawing.Size(1374, 592);
            this.Load += new System.EventHandler(this.BookAuthorListUserControl_Load);
            this.panelBody.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewBookAuthorList)).EndInit();
            this.panelHeading.ResumeLayout(false);
            this.panelHeading.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.TextBox textBoxFilterValue;
        private System.Windows.Forms.Panel panelBody;
        private System.Windows.Forms.Panel panelHeading;
        private System.Windows.Forms.Button buttonNew;
        private System.Windows.Forms.Button buttonEdit;
        private System.Windows.Forms.Button buttonDelete;
        private System.Windows.Forms.Label labeltitle;
        private System.Windows.Forms.DataGridView dataGridViewBookAuthorList;
    }
}
