﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using LibraryManagementSystem.app.ui.form;
using LibraryManagementSystem.app.db;
using LibraryManagementSystem.app.lib;

namespace LibraryManagementSystem.app.ui.uc
{
    public partial class ReturnBookUserControl : UserControl
    {
        public event EventHandler ResetButton;
        DatabaseConnection dc;
        private RFIDController rfidC;
        public ReturnBookUserControl()
        {
            InitializeComponent();
            dc = new DatabaseConnection();
            rfidC = new RFIDController();
            Initial();
        }

        public void Initial()
        {
            String query = @"SELECT student_id, CONCAT(student_lastname, ', ', student_firstname, ' ', student_middlename) as `student_name`
                                FROM tbl_student ";
            DataTable dt = dc.Select(query);
            DataRow dr = dt.NewRow();
            dr["student_id"] = "0";     
            dt.Rows.InsertAt(dr, 0);
            comboBoxStudentName.DataSource = dt;
            comboBoxStudentName.DisplayMember = "student_name";
            comboBoxStudentName.ValueMember = "student_id";

            DataTable dtAdviser = dc.Select("Select * FROM tbl_adviser");
            DataRow drAdviser = dtAdviser.NewRow();
            drAdviser["adviser_id"] = "0";
            drAdviser["adviser_name"] = "";
            dtAdviser.Rows.InsertAt(drAdviser, 0);
            comboBoxFaculty.DataSource = dtAdviser;
            comboBoxFaculty.DisplayMember = "adviser_name";
            comboBoxFaculty.ValueMember = "adviser_id";

            DataTable dtOther = dc.Select("SELECT borrow_id, borrow_other FROM `tbl_borrow` WHERE `borrow_type` = '3' AND `borrow_status` IN(1, 2)");
            DataRow drOther = dtOther.NewRow();
            drOther["borrow_id"] = "0";
            drOther["borrow_other"] = "";
            dtOther.Rows.InsertAt(drOther, 0);
            comboBoxOther.DataSource = dtOther;
            comboBoxOther.DisplayMember = "borrow_other";
            comboBoxOther.ValueMember = "borrow_id";
        }

        private void textBoxStudentID_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                Dictionary<string, string> dic = new Dictionary<string, string>();
                dic.Add("@student_id", textBoxStudentID.Text.Trim());
                String query = @"SELECT * 
                                FROM tbl_student 
                                WHERE student_id = @student_id ";
                DataTable dt = dc.Select(query, dic);
                if (dt.Rows.Count > 0)
                {
                    setReturnee(dt.Rows[0]["student_id"].ToString(), 1);
                    
                    
                }
                    
                
                //   processSearchedISBN();
                //enter key is down
            }
        }

        private void setReturnee(String borrowerID, int type)
        {
            Dictionary<string, string> dicBorrow = new Dictionary<string, string>();
            String filterQuery = "";
            if (type == 1)
            {
                comboBoxStudentName.SelectedValue = borrowerID;
                Dictionary<string, string> dicStudent = new Dictionary<string, string>();
                dicStudent.Add("@student_id", comboBoxStudentName.SelectedValue.ToString());
                String queryStudent = @"SELECT * 
                                FROM tbl_student 
                                WHERE student_id = @student_id ";
                DataTable dtStudent = dc.Select(queryStudent, dicStudent);

                Dictionary<string, string> dic = new Dictionary<string, string>();
                dic.Add("@section_id", dtStudent.Rows[0]["student_section_id"].ToString());
                String query = @"SELECT *
                                FROM tbl_section 
                                WHERE section_id = @section_id ";
                DataTable dtSection = dc.Select(query, dic);
                labelSection.Text = dtSection.Rows[0]["section_name"].ToString();

                dicBorrow.Add("@borrow_student_id", borrowerID);
                filterQuery = "borrow_student_id = @borrow_student_id";
            }
            if (type == 2)
            {
                dicBorrow.Add("@borrow_adviser_id", borrowerID);
                filterQuery = "	borrow_adviser_id = @borrow_adviser_id";
            }
            if (type == 3)
            {
                dicBorrow.Add("@borrow_id", borrowerID);
                filterQuery = "	borrow_id = @borrow_id";
            }




            String queryBorrow = @"SELECT borrow_id, borrow_tcode, borrow_return_notes
                                FROM tbl_borrow 
                                WHERE " + filterQuery  + @" AND `borrow_status` IN (1, 2) ";
            DataTable dtBorrow = dc.Select(queryBorrow, dicBorrow);

            //textBoxTransactionDesc.Tag = dtBorrow.Rows.Count;
            if (dtBorrow.Rows.Count > 0)
            {
                comboBoxTransactions.DataSource = dtBorrow;
                comboBoxTransactions.ValueMember = "borrow_id";
                comboBoxTransactions.DisplayMember = "borrow_tcode";
                setTransaction(comboBoxTransactions.SelectedValue.ToString());
                textBoxReturnNotes.Text = dtBorrow.Rows[0]["borrow_return_notes"].ToString();
                //comboBoxStudentName_SelectionChangeCommitted.
            }
            else
            {
                comboBoxTransactions.DataSource = null;
                textBoxReturnNotes.Text = "";
                //No Borrowed Transaction
            }

        }

        private void comboBoxStudentName_SelectionChangeCommitted(object sender, EventArgs e)
        {
            if (comboBoxStudentName.SelectedValue.ToString() != "0")
            {
                setReturnee(comboBoxStudentName.SelectedValue.ToString(), 1);
                textBoxStudentID.Text = "";
            }
            else
            {
                labelSection.Text = "";
            }
        }

        private void buttonScanBook_Click(object sender, EventArgs e)
        {
            DataTable dtBook = scanBook();
            if(dtBook != null)
            {
                labelScannedBook.Tag = dtBook.Rows[0]["book_id"].ToString();
                labelBookCardNo.Text = dtBook.Rows[0]["book_call_no"].ToString();
                labelBookISBN.Text = dtBook.Rows[0]["bi_isbn"].ToString();
                labelBookTitle.Text = dtBook.Rows[0]["bi_title"].ToString();
                labelBookAuthor.Text = dtBook.Rows[0]["author_name"].ToString();
                labelBookCategory.Text = dtBook.Rows[0]["category_name"].ToString();
                textBoxBookNotes.Text = dtBook.Rows[0]["book_notes"].ToString();
                labelBookStatus.Tag = dtBook.Rows[0]["book_status"].ToString();

                Dictionary<string, string> dicCheckIfBorrowed = new Dictionary<string, string>();
                dicCheckIfBorrowed.Add("@bd_borrow_id", comboBoxTransactions.SelectedValue.ToString());
                dicCheckIfBorrowed.Add("@bd_book_id", dtBook.Rows[0]["book_id"].ToString());
                String queryCheckIfBorrowed = @"SELECT bd_id, book_id, bi_id, bi_isbn, bi_title
                            FROM `tbl_borrow_detail`
                            INNER JOIN `tbl_book`
                            ON `bd_book_id` = `book_id`
                            INNER JOIN `tbl_bookinfo`
                            ON `bi_id` = `book_bi_id`
                            WHERE `bd_borrow_id` = @bd_borrow_id AND bd_book_id = @bd_book_id";
                DataTable dtCheckIfBorrowed = dc.Select(queryCheckIfBorrowed, dicCheckIfBorrowed);
                if(dtCheckIfBorrowed.Rows.Count > 0)
                {
                    labelBookStatus.Text = "Borrowed Book";
                    labelBookStatus.ForeColor = Color.Green;
                    buttonAddToList.Enabled = true;

                }
                else
                {
                    labelBookStatus.Text = "This book is not the book being borrowed on the selected Transaction";
                    labelBookStatus.ForeColor = Color.Red;
                    buttonAddToList.Enabled = false;
                }
            }
        }



        private DataTable scanBook()
        {
            DataTable dtBook = null;
            if (rfidC.Connect())
            {
                if (rfidC.CheckCard())
                {
                    String Rfid = rfidC.Read();
                    Dictionary<string, string> dic = new Dictionary<string, string>();
                    dic.Add("@book_rfid", Rfid);
                    String query = @"SELECT * 
                                FROM tbl_book
                                LEFT JOIN tbl_bookinfo 
                                ON bi_id = book_bi_id
                                LEFT JOIN tbl_author
                                ON bi_author_id = author_id
                                LEFT JOIN tbl_category
                                ON bi_bookcategory_id = category_id
                                WHERE book_rfid = @book_rfid";
                    DataTable dt = dc.Select(query, dic);
                    if (dt.Rows.Count > 0)
                    {
                        dtBook = dt;
                    }
                    else
                    {
                        MessageBox.Show(this, "This book isn't Registered");
                    }
                }
                else
                {
                    MessageBox.Show(this, "Make sure Book has an RFID Sticker", "Book Tag undetected", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }            
            return dtBook;
        }

        private Boolean AddToBookList(DataTable dtBook)
        {

            DataTable dtBookList = (DataTable)dataGridViewBookList.DataSource;
            //DataRow [] find_row = dtBookList.Select("{Book ID} like '%" + dtBook.Rows[0]["book_id"].ToString() + "%'");
            DataRow[] find_row = dtBookList.Select(string.Format("{0} = {1}", "Book_ID", dtBook.Rows[0]["book_id"].ToString()));

            if (find_row.Length > 0)
            {
                int index = dtBookList.Rows.IndexOf(find_row[0]);
                dtBookList.Rows[index]["Return"] = true;
            }
            else
            {
                MessageBox.Show("Book Already Scanned");
            }

            


            return true;
        }
        private void setTransaction(String borrow_id)
        {
            Dictionary<string, string> dicBorrow = new Dictionary<string, string>();
            dicBorrow.Add("@borrow_id", borrow_id);
            String queryBorrow = @"SELECT borrow_return_notes
                            FROM `tbl_borrow`
                            WHERE `borrow_id` = @borrow_id";
            DataTable dtBorrow = dc.Select(queryBorrow, dicBorrow);

            textBoxReturnNotes.Text = dtBorrow.Rows[0]["borrow_return_notes"].ToString();

            Dictionary<string, string> dic = new Dictionary<string, string>();
            dic.Add("@bd_borrow_id", borrow_id);
            String query = @"SELECT bd_id, book_id, bi_id, book_call_no as 'Book Call No.', bi_title as 'Book Name', bd_is_returned as 'Return'
                            FROM `tbl_borrow_detail`
                            INNER JOIN `tbl_book`
                            ON `bd_book_id` = `book_id`
                            INNER JOIN `tbl_bookinfo`
                            ON `bi_id` = `book_bi_id`
                            WHERE `bd_borrow_id` = @bd_borrow_id";
            DataTable dt = dc.Select(query, dic);

            dataGridViewBookList.DataSource = dt;

        }

        private void buttonScanAndAddToList_Click(object sender, EventArgs e)
        {
            buttonScanBook.PerformClick();
            buttonAddToList.PerformClick();
        }

        private void buttonAddToList_Click(object sender, EventArgs e)
        {
            if(labelScannedBook.Tag.ToString() != "0")
            {
                String bookId = labelScannedBook.Tag.ToString();
                Dictionary<string, string> dic = new Dictionary<string, string>();
                dic.Add("@book_id", bookId);
                String query = @"SELECT * 
                                FROM tbl_book
                                LEFT JOIN tbl_bookinfo 
                                ON bi_id = book_bi_id
                                WHERE book_id = @book_id ";
                DataTable dt = dc.Select(query, dic);
                if(dt.Rows.Count > 0)
                {
                    AddToBookList(dt);
                    buttonAddToList.Enabled = false;
                }
                else
                {
                    MessageBox.Show(this, "This book isn't Registered");
                }
                labelScannedBook.Tag = "0";
            }
            else
            {
                MessageBox.Show(this, "No Book Selected");
            }
        }

        private void buttonCompleteTransaction_Click(object sender, EventArgs e)
        {
            Boolean valid = true;
            if (dataGridViewBookList.Rows.Count <= 0)
            {
                valid = false;
            }
            if (comboBoxBorrowerType.SelectedIndex == 0)
            {
                if (comboBoxStudentName.SelectedValue.ToString() == "0")
                {
                    valid = false;
                }
            }
            if (comboBoxBorrowerType.SelectedIndex == 1)
            {
                if (comboBoxFaculty.SelectedValue.ToString() == "0")
                {
                    valid = false;
                }
            }
            if (comboBoxBorrowerType.SelectedIndex == 2)
            {
                if (comboBoxOther.SelectedValue.ToString() == "0")
                {
                    valid = false;
                }
            }
            if (valid)
            {   
                DataTable dtBookList = (DataTable) dataGridViewBookList.DataSource;
                int numOfReturn = 0;
                for (int i = 0; i< dtBookList.Rows.Count; i++)
                {
                    if(Boolean.Parse(dtBookList.Rows[i]["Return"].ToString()))
                    {
                        String updateBorrowDetailQuery = @"UPDATE tbl_borrow_detail SET
                                        bd_is_returned = @bd_is_returned,
                                        bd_return_date = @bd_return_date
                                        WHERE bd_id = @bd_id";
                        Dictionary<string, string> updateBorrowDetailDic = new Dictionary<string, string>();
                        updateBorrowDetailDic.Add("@bd_is_returned", (1).ToString());
                        updateBorrowDetailDic.Add("@bd_return_date", DateTime.Now.ToString("yyyy-MM-dd H:mm:ss"));
                        updateBorrowDetailDic.Add("@bd_id", dtBookList.Rows[i]["bd_id"].ToString());
                        dc.Update(updateBorrowDetailQuery, updateBorrowDetailDic);

                        String updateBookQuery = @"UPDATE tbl_book SET
                                        book_status = @book_status
                                        WHERE book_id = @book_id";
                        Dictionary<string, string> updateBookDic = new Dictionary<string, string>();
                        updateBookDic.Add("@book_status", (0).ToString());
                        updateBookDic.Add("@book_id", dtBookList.Rows[i]["book_id"].ToString());
                        dc.Update(updateBookQuery, updateBookDic);
                        numOfReturn++;
                    }
                }

                if(dtBookList.Rows.Count == numOfReturn)
                {
                    //Means Full
                    String updateBorrowDetailQuery = @"UPDATE tbl_borrow SET
                                        borrow_status = @borrow_status,
                                        borrow_return_notes = @borrow_return_notes,
                                        borrow_return_date = @borrow_return_date
                                        WHERE borrow_id = @borrow_id";
                    Dictionary<string, string> updateBorrowDetailDic = new Dictionary<string, string>();
                    updateBorrowDetailDic.Add("@borrow_status", (3).ToString());
                    updateBorrowDetailDic.Add("@borrow_return_notes", textBoxReturnNotes.Text);
                    updateBorrowDetailDic.Add("@borrow_return_date", DateTime.Now.ToString("yyyy-MM-dd H:mm:ss"));
                    updateBorrowDetailDic.Add("@borrow_id", comboBoxTransactions.SelectedValue.ToString());
                    dc.Update(updateBorrowDetailQuery, updateBorrowDetailDic);
                    createTransactionLog(comboBoxTransactions.SelectedValue.ToString());
                }
                else
                {
                    //Mean Partial
                    String updateBorrowDetailQuery = @"UPDATE tbl_borrow SET
                                        borrow_status = @borrow_status,
                                        borrow_return_notes = @borrow_return_notes
                                        WHERE borrow_id = @borrow_id";
                    Dictionary<string, string> updateBorrowDetailDic = new Dictionary<string, string>();
                    updateBorrowDetailDic.Add("@borrow_status", (2).ToString());
                    updateBorrowDetailDic.Add("@borrow_return_notes", textBoxReturnNotes.Text);
                    updateBorrowDetailDic.Add("@borrow_id", comboBoxTransactions.SelectedValue.ToString());
                    dc.Update(updateBorrowDetailQuery, updateBorrowDetailDic);
                    createTransactionLog(comboBoxTransactions.SelectedValue.ToString());
                }
                MessageBox.Show(this, "Returning Book Successful!", "Transaction Completed", MessageBoxButtons.OK, MessageBoxIcon.None);
                buttonReset.PerformClick();
            }
            else
            {
                MessageBox.Show("Transaction Can't be completed");
            }
        }

        private void buttonViewTransaction_Click(object sender, EventArgs e)
        {
            MessageBox.Show(comboBoxTransactions.Text);
            
        }

        private void comboBoxTransactions_SelectionChangeCommitted(object sender, EventArgs e)
        {
            setTransaction(comboBoxTransactions.SelectedValue.ToString());
        }

        private void comboBoxTransactions_DataSourceChanged(object sender, EventArgs e)
        {
            DataTable dt = (DataTable)comboBoxTransactions.DataSource;
            if(dt == null)
            {
                groupBoxReturnBook.Enabled = false;
                groupBoxBookInformation.Enabled = false;
                dataGridViewBookList.DataSource = null;
            }
            else
            {
                groupBoxBookInformation.Enabled = true;
                groupBoxReturnBook.Enabled = true;
            }
        }

        private void dataGridViewBookList_DataSourceChanged(object sender, EventArgs e)
        {
            if(dataGridViewBookList.DataSource != null)
            {
                dataGridViewBookList.Columns["bd_id"].Visible = false;
                dataGridViewBookList.Columns["book_id"].Visible = false;
                dataGridViewBookList.Columns["bi_id"].Visible = false;

                //bd_id, book_id, bi_id
            }
        }

        private void comboBoxBorrowerType_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (comboBoxBorrowerType.SelectedIndex == 0)
            {
                panelBorrowerType.Controls.Add(this.panelBorrowerStudent);
                panelBorrowerType.Controls.Remove(this.panelBorrowerFaculty);
                panelBorrowerType.Controls.Remove(this.panelBorrowerOther);
                panelBorrowerStudent.Dock = DockStyle.Fill;
                panelBorrowerFaculty.Dock = DockStyle.None;
                panelBorrowerOther.Dock = DockStyle.None;
            }
            if (comboBoxBorrowerType.SelectedIndex == 1)
            {
                panelBorrowerType.Controls.Add(this.panelBorrowerFaculty);
                panelBorrowerType.Controls.Remove(this.panelBorrowerStudent);
                panelBorrowerType.Controls.Remove(this.panelBorrowerOther);
                panelBorrowerFaculty.Dock = DockStyle.Fill;
                panelBorrowerStudent.Dock = DockStyle.None;
                panelBorrowerOther.Dock = DockStyle.None;
            }
            if (comboBoxBorrowerType.SelectedIndex == 2)
            {
                panelBorrowerType.Controls.Add(this.panelBorrowerOther);
                panelBorrowerType.Controls.Remove(this.panelBorrowerStudent);
                panelBorrowerType.Controls.Remove(this.panelBorrowerFaculty);
                panelBorrowerOther.Dock = DockStyle.Fill;
                panelBorrowerFaculty.Dock = DockStyle.None;
                panelBorrowerStudent.Dock = DockStyle.None;
            }
        }

        private void panelBorrowerStudent_DockChanged(object sender, EventArgs e)
        {
            panelBorrowerStudent.Visible = (panelBorrowerStudent.Dock == DockStyle.Fill);
        }

        private void panelBorrowerFaculty_DockChanged(object sender, EventArgs e)
        {
            panelBorrowerFaculty.Visible = (panelBorrowerFaculty.Dock == DockStyle.Fill);
        }

        private void panelBorrowerOther_DockChanged(object sender, EventArgs e)
        {
            panelBorrowerOther.Visible = (panelBorrowerOther.Dock == DockStyle.Fill);
        }

        private void ReturnBookUserControl_Load(object sender, EventArgs e)
        {
            comboBoxBorrowerType.SelectedIndex = 0;
        }

        private void comboBoxFaculty_SelectionChangeCommitted(object sender, EventArgs e)
        {
            if (comboBoxFaculty.SelectedValue.ToString() != "0")
            {
                setReturnee(comboBoxFaculty.SelectedValue.ToString(), 2);
            }
        }

        private void buttonReset_Click(object sender, EventArgs e)
        {
            Initiator.AdminForm.PreventShiftingUC = "";
            Initiator.LibrarianForm.PreventShiftingUC = "";
            ResetButton(this, EventArgs.Empty);
        }
        private void createTransactionLog(String id)
        {
            String insert = @"INSERT INTO tbl_transaction_summary 
                                (ts_borrow_id,
                                ts_type,
                                ts_date,
                                ts_notes) 
                                VALUES 
                                (@ts_borrow_id,
                                @ts_type,
                                @ts_date,
                                @ts_notes)";
            Dictionary<string, string> insertTransactionLog = new Dictionary<string, string>();
            insertTransactionLog.Add("@ts_borrow_id", id);
            insertTransactionLog.Add("@ts_type", "Return");
            insertTransactionLog.Add("@ts_notes", "");
            insertTransactionLog.Add("@ts_date", DateTime.Now.ToString("yyyy-MM-dd H:mm:ss"));
            dc.Insert(insert, insertTransactionLog);
        }

        private void comboBoxOther_SelectionChangeCommitted(object sender, EventArgs e)
        {
            if (comboBoxOther.SelectedValue.ToString() != "0")
            {
                setReturnee(comboBoxOther.SelectedValue.ToString(), 3);
            }
        }

        private void dataGridViewBookList_RowsAdded(object sender, DataGridViewRowsAddedEventArgs e)
        {
            if (dataGridViewBookList.Rows.Count > 0)
            {
                Initiator.AdminForm.PreventShiftingUC = "Current Return Transaction Ongoing!";
                Initiator.LibrarianForm.PreventShiftingUC = "Current Return Transaction Ongoing!";
            }
        }

        private void textBoxStudentID_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = !char.IsDigit(e.KeyChar) && !char.IsControl(e.KeyChar);
        }
    }
}
