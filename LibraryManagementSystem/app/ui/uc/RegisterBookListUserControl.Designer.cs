﻿namespace LibraryManagementSystem.app.ui.uc
{
    partial class RegisterBookListUserControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panelBody = new System.Windows.Forms.Panel();
            this.buttonClearBook = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.textBoxBookNotes = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.labelBookISBN = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.labelBookCategory = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.buttonSelectBook = new System.Windows.Forms.Button();
            this.labelBookAuthor = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.labelBookTitle = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.buttonRegisterBook = new System.Windows.Forms.Button();
            this.buttonScanBook = new System.Windows.Forms.Button();
            this.labeltitle = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.textBoxBookCardNumber = new System.Windows.Forms.TextBox();
            this.panelBody.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // panelBody
            // 
            this.panelBody.Controls.Add(this.buttonClearBook);
            this.panelBody.Controls.Add(this.groupBox1);
            this.panelBody.Controls.Add(this.buttonRegisterBook);
            this.panelBody.Controls.Add(this.buttonScanBook);
            this.panelBody.Controls.Add(this.labeltitle);
            this.panelBody.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelBody.ForeColor = System.Drawing.Color.White;
            this.panelBody.Location = new System.Drawing.Point(0, 0);
            this.panelBody.Name = "panelBody";
            this.panelBody.Size = new System.Drawing.Size(1374, 592);
            this.panelBody.TabIndex = 4;
            // 
            // buttonClearBook
            // 
            this.buttonClearBook.BackColor = System.Drawing.Color.RoyalBlue;
            this.buttonClearBook.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonClearBook.Image = global::LibraryManagementSystem.Properties.Resources.Delete_Document_white_26px;
            this.buttonClearBook.Location = new System.Drawing.Point(38, 214);
            this.buttonClearBook.Name = "buttonClearBook";
            this.buttonClearBook.Size = new System.Drawing.Size(198, 59);
            this.buttonClearBook.TabIndex = 8;
            this.buttonClearBook.Text = "Clear Book";
            this.buttonClearBook.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.buttonClearBook.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.buttonClearBook.UseVisualStyleBackColor = false;
            this.buttonClearBook.Click += new System.EventHandler(this.buttonClearBook_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox1.Controls.Add(this.textBoxBookCardNumber);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Controls.Add(this.textBoxBookNotes);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.labelBookISBN);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.labelBookCategory);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.buttonSelectBook);
            this.groupBox1.Controls.Add(this.labelBookAuthor);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.labelBookTitle);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.ForeColor = System.Drawing.Color.White;
            this.groupBox1.Location = new System.Drawing.Point(267, 55);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(1080, 470);
            this.groupBox1.TabIndex = 7;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Book Information";
            // 
            // textBoxBookNotes
            // 
            this.textBoxBookNotes.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textBoxBookNotes.Location = new System.Drawing.Point(29, 306);
            this.textBoxBookNotes.MaxLength = 255;
            this.textBoxBookNotes.Multiline = true;
            this.textBoxBookNotes.Name = "textBoxBookNotes";
            this.textBoxBookNotes.Size = new System.Drawing.Size(1018, 146);
            this.textBoxBookNotes.TabIndex = 10;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(34, 280);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(98, 23);
            this.label2.TabIndex = 9;
            this.label2.Text = "Book Notes";
            // 
            // labelBookISBN
            // 
            this.labelBookISBN.AutoSize = true;
            this.labelBookISBN.Location = new System.Drawing.Point(242, 135);
            this.labelBookISBN.Name = "labelBookISBN";
            this.labelBookISBN.Size = new System.Drawing.Size(148, 23);
            this.labelBookISBN.TabIndex = 8;
            this.labelBookISBN.Text = "<< Book ISBN >>";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(139, 135);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(90, 23);
            this.label6.TabIndex = 7;
            this.label6.Text = "Book ISBN";
            // 
            // labelBookCategory
            // 
            this.labelBookCategory.AutoSize = true;
            this.labelBookCategory.Location = new System.Drawing.Point(242, 238);
            this.labelBookCategory.Name = "labelBookCategory";
            this.labelBookCategory.Size = new System.Drawing.Size(180, 23);
            this.labelBookCategory.TabIndex = 6;
            this.labelBookCategory.Text = "<< Book Category >>";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(107, 238);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(122, 23);
            this.label4.TabIndex = 5;
            this.label4.Text = "Book Category";
            // 
            // buttonSelectBook
            // 
            this.buttonSelectBook.BackColor = System.Drawing.Color.RoyalBlue;
            this.buttonSelectBook.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonSelectBook.Image = global::LibraryManagementSystem.Properties.Resources.Book_Stack_white_26px;
            this.buttonSelectBook.Location = new System.Drawing.Point(247, 47);
            this.buttonSelectBook.Name = "buttonSelectBook";
            this.buttonSelectBook.Size = new System.Drawing.Size(174, 38);
            this.buttonSelectBook.TabIndex = 4;
            this.buttonSelectBook.Tag = "0";
            this.buttonSelectBook.Text = "Select Book";
            this.buttonSelectBook.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.buttonSelectBook.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.buttonSelectBook.UseVisualStyleBackColor = false;
            this.buttonSelectBook.Click += new System.EventHandler(this.buttonSelectBook_Click);
            // 
            // labelBookAuthor
            // 
            this.labelBookAuthor.AutoSize = true;
            this.labelBookAuthor.Location = new System.Drawing.Point(242, 203);
            this.labelBookAuthor.Name = "labelBookAuthor";
            this.labelBookAuthor.Size = new System.Drawing.Size(164, 23);
            this.labelBookAuthor.TabIndex = 3;
            this.labelBookAuthor.Text = "<< Book Author >>";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(123, 203);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(106, 23);
            this.label3.TabIndex = 2;
            this.label3.Text = "Book Author";
            // 
            // labelBookTitle
            // 
            this.labelBookTitle.AutoSize = true;
            this.labelBookTitle.Location = new System.Drawing.Point(242, 168);
            this.labelBookTitle.Name = "labelBookTitle";
            this.labelBookTitle.Size = new System.Drawing.Size(143, 23);
            this.labelBookTitle.TabIndex = 1;
            this.labelBookTitle.Text = "<< Book Title >>";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(145, 168);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(85, 23);
            this.label1.TabIndex = 0;
            this.label1.Text = "Book Title";
            // 
            // buttonRegisterBook
            // 
            this.buttonRegisterBook.BackColor = System.Drawing.Color.RoyalBlue;
            this.buttonRegisterBook.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonRegisterBook.Image = global::LibraryManagementSystem.Properties.Resources.downloading_updates_white_26px;
            this.buttonRegisterBook.Location = new System.Drawing.Point(39, 136);
            this.buttonRegisterBook.Name = "buttonRegisterBook";
            this.buttonRegisterBook.Size = new System.Drawing.Size(198, 59);
            this.buttonRegisterBook.TabIndex = 6;
            this.buttonRegisterBook.Text = "Register Book";
            this.buttonRegisterBook.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.buttonRegisterBook.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.buttonRegisterBook.UseVisualStyleBackColor = false;
            this.buttonRegisterBook.Click += new System.EventHandler(this.buttonRegisterBook_Click);
            // 
            // buttonScanBook
            // 
            this.buttonScanBook.BackColor = System.Drawing.Color.RoyalBlue;
            this.buttonScanBook.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonScanBook.Image = global::LibraryManagementSystem.Properties.Resources.RFID_Tag_white_26px;
            this.buttonScanBook.Location = new System.Drawing.Point(39, 58);
            this.buttonScanBook.Name = "buttonScanBook";
            this.buttonScanBook.Size = new System.Drawing.Size(198, 59);
            this.buttonScanBook.TabIndex = 3;
            this.buttonScanBook.Text = "Scan Book";
            this.buttonScanBook.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.buttonScanBook.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.buttonScanBook.UseVisualStyleBackColor = false;
            this.buttonScanBook.Click += new System.EventHandler(this.buttonScanBook_Click);
            // 
            // labeltitle
            // 
            this.labeltitle.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.labeltitle.Location = new System.Drawing.Point(8, 6);
            this.labeltitle.Name = "labeltitle";
            this.labeltitle.Size = new System.Drawing.Size(1351, 23);
            this.labeltitle.TabIndex = 2;
            this.labeltitle.Text = "Register Books";
            this.labeltitle.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(72, 101);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(157, 23);
            this.label7.TabIndex = 11;
            this.label7.Text = "Book Card Number";
            // 
            // textBoxBookCardNumber
            // 
            this.textBoxBookCardNumber.Location = new System.Drawing.Point(249, 99);
            this.textBoxBookCardNumber.MaxLength = 20;
            this.textBoxBookCardNumber.Name = "textBoxBookCardNumber";
            this.textBoxBookCardNumber.Size = new System.Drawing.Size(310, 30);
            this.textBoxBookCardNumber.TabIndex = 12;
            // 
            // RegisterBookListUserControl
            // 
            this.AutoScroll = true;
            this.BackColor = System.Drawing.Color.Navy;
            this.Controls.Add(this.panelBody);
            this.Font = new System.Drawing.Font("Segoe UI", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.Name = "RegisterBookListUserControl";
            this.Size = new System.Drawing.Size(1374, 592);
            this.panelBody.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Panel panelBody;
        private System.Windows.Forms.Label labeltitle;
        private System.Windows.Forms.Button buttonScanBook;
        private System.Windows.Forms.Button buttonRegisterBook;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label labelBookAuthor;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label labelBookTitle;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button buttonSelectBook;
        private System.Windows.Forms.Label labelBookISBN;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label labelBookCategory;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox textBoxBookNotes;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button buttonClearBook;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox textBoxBookCardNumber;
    }
}
