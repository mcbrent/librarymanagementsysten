﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using LibraryManagementSystem.app.db;
using LibraryManagementSystem.app.ui.form;

namespace LibraryManagementSystem.app.ui.uc
{
    public partial class AdminListUserControl : UserControl
    {
        public AdminListUserControl()
        {
            InitializeComponent();
        }
        public void Add()
        {
            AdminFormData add = new AdminFormData();
            if (DialogResult.OK == add.ShowDialog())
            {
                DatabaseConnection dc = new DatabaseConnection();

                String query = @"INSERT INTO tbl_user 
                                (user_name,
                                user_password,
                                user_type) 
                                VALUES 
                                (@user_name,
                                @user_password,
                                @user_type)";
                Dictionary<string, string> dic = new Dictionary<string, string>();
                dic.Add("@user_name", add.Username);
                dic.Add("@user_password", lib.Encryption.CreateSHA256(add.Password));
                dic.Add("@user_type", "1");
                dc.Insert(query, dic);


                reloadDataGridView();
            }
        }
        private void buttonNew_Click(object sender, EventArgs e)
        {
            this.Add();
        }

        private void reloadDataGridView()
        {
            DatabaseConnection dc = new DatabaseConnection();
            String filter = textBoxFilterValue.Text.Trim();
            String filterQuery = " AND user_name LIKE @user_name";

            DataTable dt;
            String query = @"SELECT user_id, user_name as 'Username'
                            FROM `tbl_user` 
                            WHERE user_type = '1' ";
            if (filter == "")
            {
                dt = dc.Select(query);
            }
            else
            {
                Dictionary<string, string> dic = new Dictionary<string, string>();
                dic.Add("@user_name", '%' + filter + '%');
                dt = dc.Select(query + filterQuery, dic);
            }

            dataGridViewFilteredData.DataSource = dt;
            dataGridViewFilteredData.Columns["user_id"].Visible = false;

        }

        private void AdminListUserControl_Load(object sender, EventArgs e)
        {
            reloadDataGridView();
        }

        private void buttonEdit_Click(object sender, EventArgs e)
        {
            if (dataGridViewFilteredData.SelectedRows.Count > 0)
            {
                DatabaseConnection dc = new DatabaseConnection();
                Dictionary<string, string> dic = new Dictionary<string, string>();
                String user_id = dataGridViewFilteredData["user_id", dataGridViewFilteredData.CurrentRow.Index].Value.ToString();
                dic.Add("user_id", user_id);
                String query = @"SELECT * FROM tbl_user WHERE user_id = @user_id";
                DataTable dt = dc.Select(query, dic);
                
                AdminFormData add = new AdminFormData();
                add.Username = dt.Rows[0]["user_name"].ToString();
                //DialogResult dr = add.ShowDialog();
                if (DialogResult.OK == add.ShowDialog())
                {
                    String updatePassword = "";
                    Dictionary<string, string> update = new Dictionary<string, string>();
                    if(add.Password.Length > 0)
                    {
                        updatePassword += ", user_password = @user_password ";
                        update.Add("@user_password", lib.Encryption.CreateSHA256(add.Password));
                    }
                    update.Add("@user_name", add.Username);
                    update.Add("@user_id", user_id);
                    String updateQuery = @"UPDATE tbl_user 
                                            SET user_name = @user_name" + updatePassword + @"
                                            WHERE user_id = @user_id";
                    dc.Update(updateQuery, update);
                    reloadDataGridView();
                }
            }
        }

        private void buttonDelete_Click(object sender, EventArgs e)
        {
            if (dataGridViewFilteredData.SelectedRows.Count > 0)
            {
                
                DatabaseConnection dc = new DatabaseConnection();
                Dictionary<string, string> dic = new Dictionary<string, string>();
                String user_id = dataGridViewFilteredData["user_id", dataGridViewFilteredData.CurrentRow.Index].Value.ToString();
                if (lib.Singleton.Current_user == user_id)
                {
                    MessageBox.Show(this, "Current User can't be deleted", "Invalid Operation!", MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1);
                }
                else
                {            
                    DialogResult dr = MessageBox.Show(this, "Are you sure you like to delete an admin?", "Deleting admin user", MessageBoxButtons.YesNo, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1);
                    if (dr == DialogResult.Yes)
                    {
                        dic.Add("user_id", user_id);
                        String query = @"DELETE FROM tbl_user WHERE user_id = @user_id";
                        dc.Delete(query, dic);
                       
                        reloadDataGridView();
                    }
                }
                
            }
        }

        private void textBoxFilterValue_TextChanged(object sender, EventArgs e)
        {
            reloadDataGridView();
        }
    }
}
