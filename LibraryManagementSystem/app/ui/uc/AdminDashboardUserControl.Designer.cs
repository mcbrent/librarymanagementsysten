﻿namespace LibraryManagementSystem.app.ui.uc
{
    partial class AdminDashboardUserControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.panelBody = new System.Windows.Forms.Panel();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.panel2 = new System.Windows.Forms.Panel();
            this.labelStudentPresent = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.panelScanned = new System.Windows.Forms.Panel();
            this.label3 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.labelBookTitle = new System.Windows.Forms.Label();
            this.labelBookISBN = new System.Windows.Forms.Label();
            this.labelCurrentPossesion = new System.Windows.Forms.Label();
            this.buttonScanBook = new System.Windows.Forms.Button();
            this.panelHeading = new System.Windows.Forms.Panel();
            this.labelSchoolName = new System.Windows.Forms.Label();
            this.labelClock = new System.Windows.Forms.Label();
            this.timerClock = new System.Windows.Forms.Timer(this.components);
            this.panelBody.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.panel2.SuspendLayout();
            this.panel1.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.panelScanned.SuspendLayout();
            this.panelHeading.SuspendLayout();
            this.SuspendLayout();
            // 
            // panelBody
            // 
            this.panelBody.BackColor = System.Drawing.Color.White;
            this.panelBody.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panelBody.Controls.Add(this.pictureBox1);
            this.panelBody.Controls.Add(this.panel2);
            this.panelBody.Controls.Add(this.panel1);
            this.panelBody.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelBody.Location = new System.Drawing.Point(0, 58);
            this.panelBody.Name = "panelBody";
            this.panelBody.Size = new System.Drawing.Size(1374, 525);
            this.panelBody.TabIndex = 4;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.pictureBox1.Image = global::LibraryManagementSystem.Properties.Resources.logo;
            this.pictureBox1.Location = new System.Drawing.Point(1120, 271);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(240, 240);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 3;
            this.pictureBox1.TabStop = false;
            // 
            // panel2
            // 
            this.panel2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel2.Controls.Add(this.labelStudentPresent);
            this.panel2.Controls.Add(this.label4);
            this.panel2.Location = new System.Drawing.Point(370, 21);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(345, 198);
            this.panel2.TabIndex = 2;
            // 
            // labelStudentPresent
            // 
            this.labelStudentPresent.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.labelStudentPresent.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelStudentPresent.Font = new System.Drawing.Font("Segoe UI", 48F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelStudentPresent.Location = new System.Drawing.Point(0, 72);
            this.labelStudentPresent.Name = "labelStudentPresent";
            this.labelStudentPresent.Size = new System.Drawing.Size(341, 122);
            this.labelStudentPresent.TabIndex = 2;
            this.labelStudentPresent.Text = "0";
            this.labelStudentPresent.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // label4
            // 
            this.label4.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label4.Dock = System.Windows.Forms.DockStyle.Top;
            this.label4.Font = new System.Drawing.Font("Segoe UI", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(0, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(341, 72);
            this.label4.TabIndex = 1;
            this.label4.Text = "Student Present\r\nIn the Library";
            this.label4.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // panel1
            // 
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel1.Controls.Add(this.groupBox1);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(351, 521);
            this.panel1.TabIndex = 0;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.panelScanned);
            this.groupBox1.Controls.Add(this.buttonScanBook);
            this.groupBox1.ForeColor = System.Drawing.Color.Black;
            this.groupBox1.Location = new System.Drawing.Point(0, 0);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(347, 322);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Quick Book Scan";
            // 
            // panelScanned
            // 
            this.panelScanned.Controls.Add(this.label3);
            this.panelScanned.Controls.Add(this.label1);
            this.panelScanned.Controls.Add(this.label2);
            this.panelScanned.Controls.Add(this.labelBookTitle);
            this.panelScanned.Controls.Add(this.labelBookISBN);
            this.panelScanned.Controls.Add(this.labelCurrentPossesion);
            this.panelScanned.Location = new System.Drawing.Point(1, 99);
            this.panelScanned.Name = "panelScanned";
            this.panelScanned.Size = new System.Drawing.Size(345, 212);
            this.panelScanned.TabIndex = 3;
            this.panelScanned.Visible = false;
            // 
            // label3
            // 
            this.label3.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label3.Font = new System.Drawing.Font("Segoe UI", 7F);
            this.label3.ForeColor = System.Drawing.Color.DimGray;
            this.label3.Location = new System.Drawing.Point(5, 117);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(333, 23);
            this.label3.TabIndex = 37;
            this.label3.Text = "Book Title";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label1
            // 
            this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label1.Font = new System.Drawing.Font("Segoe UI", 7F);
            this.label1.ForeColor = System.Drawing.Color.DimGray;
            this.label1.Location = new System.Drawing.Point(5, 21);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(333, 23);
            this.label1.TabIndex = 35;
            this.label1.Text = "In Taking Care Of";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label2
            // 
            this.label2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label2.Font = new System.Drawing.Font("Segoe UI", 7F);
            this.label2.ForeColor = System.Drawing.Color.DimGray;
            this.label2.Location = new System.Drawing.Point(7, 68);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(333, 23);
            this.label2.TabIndex = 36;
            this.label2.Text = "ISBN";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // labelBookTitle
            // 
            this.labelBookTitle.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.labelBookTitle.Location = new System.Drawing.Point(2, 140);
            this.labelBookTitle.Name = "labelBookTitle";
            this.labelBookTitle.Size = new System.Drawing.Size(338, 58);
            this.labelBookTitle.TabIndex = 27;
            this.labelBookTitle.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // labelBookISBN
            // 
            this.labelBookISBN.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.labelBookISBN.Location = new System.Drawing.Point(5, 92);
            this.labelBookISBN.Name = "labelBookISBN";
            this.labelBookISBN.Size = new System.Drawing.Size(333, 23);
            this.labelBookISBN.TabIndex = 33;
            this.labelBookISBN.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // labelCurrentPossesion
            // 
            this.labelCurrentPossesion.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.labelCurrentPossesion.Location = new System.Drawing.Point(5, 43);
            this.labelCurrentPossesion.Name = "labelCurrentPossesion";
            this.labelCurrentPossesion.Size = new System.Drawing.Size(333, 23);
            this.labelCurrentPossesion.TabIndex = 34;
            this.labelCurrentPossesion.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // buttonScanBook
            // 
            this.buttonScanBook.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonScanBook.BackColor = System.Drawing.Color.RoyalBlue;
            this.buttonScanBook.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonScanBook.Image = global::LibraryManagementSystem.Properties.Resources.RFID_Tag_white_26px;
            this.buttonScanBook.Location = new System.Drawing.Point(19, 37);
            this.buttonScanBook.Name = "buttonScanBook";
            this.buttonScanBook.Size = new System.Drawing.Size(313, 50);
            this.buttonScanBook.TabIndex = 25;
            this.buttonScanBook.Tag = "0";
            this.buttonScanBook.Text = "Scan Book";
            this.buttonScanBook.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.buttonScanBook.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.buttonScanBook.UseVisualStyleBackColor = false;
            this.buttonScanBook.Click += new System.EventHandler(this.buttonScanBook_Click);
            // 
            // panelHeading
            // 
            this.panelHeading.AutoScroll = true;
            this.panelHeading.BackColor = System.Drawing.Color.White;
            this.panelHeading.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panelHeading.Controls.Add(this.labelSchoolName);
            this.panelHeading.Controls.Add(this.labelClock);
            this.panelHeading.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelHeading.Location = new System.Drawing.Point(0, 0);
            this.panelHeading.Name = "panelHeading";
            this.panelHeading.Size = new System.Drawing.Size(1374, 58);
            this.panelHeading.TabIndex = 5;
            // 
            // labelSchoolName
            // 
            this.labelSchoolName.AutoSize = true;
            this.labelSchoolName.Font = new System.Drawing.Font("Segoe UI", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelSchoolName.Location = new System.Drawing.Point(9, 11);
            this.labelSchoolName.Name = "labelSchoolName";
            this.labelSchoolName.Size = new System.Drawing.Size(970, 32);
            this.labelSchoolName.TabIndex = 1;
            this.labelSchoolName.Text = "DACANLAO G. AGONCILLO NATIONAL HIGH SCHOOL - Library Management System";
            // 
            // labelClock
            // 
            this.labelClock.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.labelClock.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelClock.Location = new System.Drawing.Point(876, 12);
            this.labelClock.Name = "labelClock";
            this.labelClock.Size = new System.Drawing.Size(486, 30);
            this.labelClock.TabIndex = 0;
            this.labelClock.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // timerClock
            // 
            this.timerClock.Enabled = true;
            this.timerClock.Interval = 1000;
            this.timerClock.Tick += new System.EventHandler(this.timerClock_Tick);
            // 
            // AdminDashboardUserControl
            // 
            this.BackColor = System.Drawing.Color.Navy;
            this.Controls.Add(this.panelBody);
            this.Controls.Add(this.panelHeading);
            this.Font = new System.Drawing.Font("Segoe UI", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ForeColor = System.Drawing.Color.Black;
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.Name = "AdminDashboardUserControl";
            this.Size = new System.Drawing.Size(1374, 583);
            this.panelBody.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.panel2.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.panelScanned.ResumeLayout(false);
            this.panelHeading.ResumeLayout(false);
            this.panelHeading.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Panel panelBody;
        private System.Windows.Forms.Panel panelHeading;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label labelClock;
        private System.Windows.Forms.Timer timerClock;
        private System.Windows.Forms.Label labelSchoolName;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button buttonScanBook;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label labelStudentPresent;
        private System.Windows.Forms.Panel panelScanned;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label labelCurrentPossesion;
        private System.Windows.Forms.Label labelBookISBN;
        private System.Windows.Forms.Label labelBookTitle;
        private System.Windows.Forms.PictureBox pictureBox1;
    }
}
