﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using LibraryManagementSystem.app.ui.form;
using LibraryManagementSystem.app.db;
using LibraryManagementSystem.app.lib;

namespace LibraryManagementSystem.app.ui.uc
{
    public partial class AttendanceLogReportUserControl : UserControl
    {
        public AttendanceLogReportUserControl()
        {
            InitializeComponent();
           
        }

        private void BorrowReportUserControl_Load(object sender, EventArgs e)
        {
            comboBoxFilterType.SelectedIndex = 0;

            DatabaseConnection dc = new DatabaseConnection();
            String query = @"SELECT student_id, CONCAT(student_firstname, ' ', student_middlename, ' ', student_lastname) as 'student' 
                            FROM tbl_student";
            DataTable dtStudent = dc.Select(query);

            DataRow drDefault = dtStudent.NewRow();
            drDefault["student_id"] = "";
            drDefault["student"] = "All Student";
            dtStudent.Rows.InsertAt(drDefault, 0);


            comboBoxStudentName.DataSource = dtStudent;
            comboBoxStudentName.DisplayMember = "student";
            comboBoxStudentName.ValueMember = "student_id";

            comboBoxStudentID.DataSource = dtStudent;
            comboBoxStudentID.DisplayMember = "student_id";
            comboBoxStudentID.ValueMember = "student_id";
        }

        private void comboBoxFilterType_SelectedIndexChanged(object sender, EventArgs e)
        {
            switch(comboBoxFilterType.SelectedIndex)
            {
                case 0:
                    panelDateFilter.Visible = false;
                    dateTimePickerFrom.Value = DateTime.Now;
                    dateTimePickerTo.Value = DateTime.Now;
                    break;
                case 1:
                    panelDateFilter.Visible = false;
                    dateTimePickerFrom.Value = DateTime.Today.AddDays(-(int)DateTime.Today.DayOfWeek + (int)DayOfWeek.Sunday);
                    dateTimePickerTo.Value = DateTime.Today.AddDays(-(int)DateTime.Today.DayOfWeek + (int)DayOfWeek.Saturday);
                    break;
                case 2:
                    panelDateFilter.Visible = false;
                    dateTimePickerFrom.Value = DateTime.Today.AddDays(-30);
                    dateTimePickerTo.Value = DateTime.Now;
                    break;
                case 3:
                    panelDateFilter.Visible = true;
                    dateTimePickerFrom.Value = DateTime.Now;
                    dateTimePickerTo.Value = DateTime.Now;
                    break;
                case 4:
                    panelDateFilter.Visible = false;
                    dateTimePickerFrom.Value = DateTime.Now;
                    dateTimePickerTo.Value = DateTime.Now;
                    break;

            }
        }

        private void buttonFilter_Click(object sender, EventArgs e)
        {
            dataGridViewFilteredData.DataSource = filterByDate();
        }

        private void buttonPrint_Click(object sender, EventArgs e)
        {
            String filterDescription = "";
            if (comboBoxStudentID.SelectedIndex > 0)
            {
                filterDescription += comboBoxStudentName.SelectedValue + "'s ";
            }
            else
            {
                filterDescription += "All ";
            }
            switch (comboBoxFilterType.SelectedIndex)
            {
                case 0:
                    filterDescription += "Attendance Log for today (" + DateTime.Now.ToString("MM-dd-yyyy") + ").";
                    break;
                case 1:
                    filterDescription += "Attendance Log for week.";
                    break;
                case 2:
                    filterDescription += "Attendance Log for last 30 Days";
                    break;
                case 3:
                    filterDescription += "Attendance Log for Date Range from " + dateTimePickerFrom.Value.ToString("MM-dd-yyyy") + " to " + dateTimePickerTo.Value.ToString("MM-dd-yyyy") + ".";
                    break;
                case 4:
                    filterDescription += "All Attendance";
                    break;
            }

            Dictionary<string, string> parameters = new Dictionary<string, string>();
            parameters.Add("ReportParameterReportType", "Attendance Log");
            parameters.Add("ReportParameterFilterDescription", filterDescription);
            parameters.Add("ReportParameterDummyColumn1", "Date");
            parameters.Add("ReportParameterDummyColumn2", "Time In");
            parameters.Add("ReportParameterDummyColumn3", "Time Out");
            parameters.Add("ReportParameterDummyColumn4", "ID");
            parameters.Add("ReportParameterDummyColumn5", "Student");
            parameters.Add("ReportParameterDummyColumn6", "Section");
            
            ReportViewerForm rvf = new ReportViewerForm();
            DataTable buffer = filterByDate();
            buffer.Columns["Date"].ColumnName = "DataColumnDummy1";
            buffer.Columns["Time In"].ColumnName = "DataColumnDummy2";
            buffer.Columns["Time Out"].ColumnName = "DataColumnDummy3";
            buffer.Columns["ID"].ColumnName = "DataColumnDummy4";
            buffer.Columns["Student"].ColumnName = "DataColumnDummy5";
            buffer.Columns["Section"].ColumnName = "DataColumnDummy6";
            
            rvf.ReportDataTable = buffer;
            rvf.DicReportParameters = parameters;
            rvf.ReportFile = "LibraryManagementSystem.reports.ReportAttendanceLog.rdlc";
            rvf.Show();

        }



        private DataTable filterByDate()
        {
            DatabaseConnection dc = new DatabaseConnection();
            DateTime dateTimeFrom = dateTimePickerFrom.Value;
            DateTime dateTimeTo = dateTimePickerTo.Value;

            DataTable dt;

            Dictionary<string, string> dic = new Dictionary<string, string>();
            String additional = "";
            if(comboBoxFilterType.SelectedIndex != 4)
            {
                dic.Add("@fromdate", dateTimeFrom.ToString("yyyy-MM-dd"));
                dic.Add("@todate", dateTimeTo.ToString("yyyy-MM-dd"));
                additional += " AND DATE(`al_entry_date`) BETWEEN @fromdate AND @todate";
            }
            if (comboBoxStudentID.SelectedIndex > 0 )
            {
                dic.Add("@student_id", comboBoxStudentID.SelectedValue.ToString());
                additional += " AND student_id = @student_id";
            }

            String query = @"SELECT DATE_FORMAT(al_entry_date, '%m/%d/%Y') as 'Date',
                            DATE_FORMAT(al_entry_date, '%h:%i %p') as 'Time In', DATE_FORMAT(al_exit_date, '%h:%i %p') as 'Time Out',
                            al_student_id as 'ID', 
                            CONCAT(student_firstname, ' ', student_middlename, ' ', student_lastname) as 'Student', section_name as 'Section'
                            FROM `tbl_attendance_log`
                            INNER JOIN `tbl_student`
                            ON `student_id` = `al_student_id`
                            INNER JOIN `tbl_section` 
                            ON `section_id` = `student_section_id`" + additional +
                            " ORDER BY al_entry_date ASC";
            dt = dc.Select(query, dic);
            
            return dt;
        }
    }
}
