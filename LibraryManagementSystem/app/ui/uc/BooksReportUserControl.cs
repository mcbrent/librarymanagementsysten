﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using LibraryManagementSystem.app.ui.form;
using LibraryManagementSystem.app.db;
using LibraryManagementSystem.app.lib;

namespace LibraryManagementSystem.app.ui.uc
{
    public partial class BooksReportUserControl : UserControl
    {
        public BooksReportUserControl()
        {
            InitializeComponent();
           
        }

        private void BorrowReportUserControl_Load(object sender, EventArgs e)
        {
            comboBoxFilterType.SelectedIndex = 0;
            DatabaseConnection dc = new DatabaseConnection();
            String query = @"SELECT category_id, category_name 
                            FROM tbl_category";
            DataTable dtStudent = dc.Select(query);

            DataRow drDefault = dtStudent.NewRow();
            drDefault["category_id"] = "0";
            drDefault["category_name"] = "All Category";
            dtStudent.Rows.InsertAt(drDefault, 0);


            comboBoxCategory.DataSource = dtStudent;
            comboBoxCategory.DisplayMember = "category_name";
            comboBoxCategory.ValueMember = "category_id";
        }

        private void comboBoxFilterType_SelectedIndexChanged(object sender, EventArgs e)
        {
            
        }

        private void buttonFilter_Click(object sender, EventArgs e)
        {
            dataGridViewFilteredData.DataSource = filterData();
        }

        private void buttonPrint_Click(object sender, EventArgs e)
        {
            String filterDescription = "";
            switch (comboBoxFilterType.SelectedIndex)
            {
                case 0:
                    filterDescription += "All Registered Books";
                    break;
                case 1:
                    filterDescription += "All Available Registered Books";
                    break;
                case 2:
                    filterDescription += "All Borrowed Registered Books";
                    break;
                case 3:
                    filterDescription += "All Registered Books and Who/Where are it in care of";
                    break;
            }
            if (comboBoxCategory.SelectedValue.ToString() != "0")
            {
                filterDescription += " with Category of \"" + ((DataRowView)comboBoxCategory.SelectedItem)[1].ToString() + "\"";
            }

            ReportViewerForm rvf = new ReportViewerForm();
            if (comboBoxFilterType.SelectedIndex == 3)
            {
                Dictionary<string, string> parameters = new Dictionary<string, string>();
                parameters.Add("ReportParameterReportType", "BOOK LIST");
                parameters.Add("ReportParameterFilterDescription", filterDescription);
                parameters.Add("ReportParameterDummyColumn1", "Book Card No.");
                parameters.Add("ReportParameterDummyColumn2", "Title");
                parameters.Add("ReportParameterDummyColumn3", "Category");
                parameters.Add("ReportParameterDummyColumn4", "Author");
                parameters.Add("ReportParameterDummyColumn5", "Current Possession");
                parameters.Add("ReportParameterDummyColumn6", "Book Notes");


                DataTable buffer = filterData();
                buffer.Columns["Book Card No."].ColumnName = "DataColumnDummy1";
                buffer.Columns["Title"].ColumnName = "DataColumnDummy2";
                buffer.Columns["Category"].ColumnName = "DataColumnDummy3";
                buffer.Columns["Author"].ColumnName = "DataColumnDummy4";
                buffer.Columns["Current Possession"].ColumnName = "DataColumnDummy5";
                buffer.Columns["Book Notes"].ColumnName = "DataColumnDummy6";

                rvf.ReportFile = "LibraryManagementSystem.reports.ReportBookPossessionList.rdlc";
                rvf.ReportDataTable = buffer;
                rvf.DicReportParameters = parameters;
            }
            else
            {
                Dictionary<string, string> parameters = new Dictionary<string, string>();
                parameters.Add("ReportParameterReportType", "BOOK LIST");
                parameters.Add("ReportParameterFilterDescription", filterDescription);
                parameters.Add("ReportParameterDummyColumn1", "ISBN");
                parameters.Add("ReportParameterDummyColumn2", "Title");
                parameters.Add("ReportParameterDummyColumn3", "Category");
                parameters.Add("ReportParameterDummyColumn4", "Author");               
                parameters.Add("ReportParameterDummyColumn5", "No. Of Books");
                

                DataTable buffer = filterData();
                buffer.Columns["ISBN"].ColumnName = "DataColumnDummy1";
                buffer.Columns["Title"].ColumnName = "DataColumnDummy2";
                buffer.Columns["Category"].ColumnName = "DataColumnDummy3";
                buffer.Columns["Author"].ColumnName = "DataColumnDummy4";                
                buffer.Columns["No. Of Books"].ColumnName = "DataColumnDummy5";

                rvf.ReportFile = "LibraryManagementSystem.reports.ReportBookList.rdlc";
                rvf.ReportDataTable = buffer;
                rvf.DicReportParameters = parameters;
            }            
            
            rvf.Show();

        }



        private DataTable filterData()
        {
            DatabaseConnection dc = new DatabaseConnection();

            DataTable dt;

            Dictionary<string, string> dic = new Dictionary<string, string>();
            String whereQuery = "";
            String query = "";
            if (comboBoxCategory.SelectedValue.ToString() != "0")
            {
                whereQuery = "category_id = @category_id";
                dic.Add("@category_id", comboBoxCategory.SelectedValue.ToString());
            }

            if(comboBoxFilterType.SelectedIndex == 0)
            {
                if(whereQuery.Length > 0)
                {
                    whereQuery = "WHERE " + whereQuery;
                }
                query = @"SELECT bi_isbn as 'ISBN', bi_title as 'Title', 
                            category_name as 'Category', author_name as 'Author', COUNT(`book_id`) as 'No. Of Books'
                            FROM `tbl_bookinfo` 
                            INNER JOIN `tbl_category`
                            ON `bi_bookcategory_id` = `category_id`
                            INNER JOIN `tbl_author`
                            ON `bi_author_id` = `author_id`
                            LEFT JOIN `tbl_book`
                            ON `book_bi_id` = `bi_id`
                            " + whereQuery + @"
                            GROUP BY bi_isbn, bi_title, category_name, author_name ";
            }
            else if(comboBoxFilterType.SelectedIndex == 1)
            {
                if (whereQuery.Length > 0)
                {
                    whereQuery = " AND " + whereQuery;
                }
                whereQuery = "WHERE book_status = '0'" + whereQuery;
                query = @"SELECT bi_isbn as 'ISBN', bi_title as 'Title', 
                            category_name as 'Category', author_name as 'Author', COUNT(`book_id`) as 'No. Of Books'
                            FROM `tbl_bookinfo` 
                            INNER JOIN `tbl_category`
                            ON `bi_bookcategory_id` = `category_id`
                            INNER JOIN `tbl_author`
                            ON `bi_author_id` = `author_id`
                            LEFT JOIN `tbl_book`
                            ON `book_bi_id` = `bi_id`
                            " + whereQuery + @"
                            GROUP BY bi_isbn, bi_title, category_name, author_name ";
            }
            else if (comboBoxFilterType.SelectedIndex == 2)
            {
                if (whereQuery.Length > 0)
                {
                    whereQuery = " AND " + whereQuery;
                }
                whereQuery = "WHERE book_status = '1'" + whereQuery;
                query = @"SELECT bi_isbn as 'ISBN', bi_title as 'Title', 
                            category_name as 'Category', author_name as 'Author', COUNT(`book_id`) as 'No. Of Books'
                            FROM `tbl_bookinfo` 
                            INNER JOIN `tbl_category`
                            ON `bi_bookcategory_id` = `category_id`
                            INNER JOIN `tbl_author`
                            ON `bi_author_id` = `author_id`
                            LEFT JOIN `tbl_book`
                            ON `book_bi_id` = `bi_id`
                            " + whereQuery + @"
                            GROUP BY bi_isbn, bi_title, category_name, author_name ";
            }
            else if (comboBoxFilterType.SelectedIndex == 3)
            {
                if (whereQuery.Length > 0)
                {
                    whereQuery = " AND " + whereQuery;
                }
                query = @"SELECT book_call_no as 'Book Card No.', bi_title as 'Title', category_name as 'Category',
                        author_name as 'Author', book_notes as 'Book Notes', curr_pos as 'Current Possession'
                        FROM
                        (SELECT book_call_no, bi_title, book_notes,
                            category_name, author_name,
                            (
                                CASE 
    	                            WHEN book_status = 1 THEN
                                    (
                                    CASE
                                        WHEN `borrow_type` = 1 THEN CONCAT(student_lastname, ', ', student_firstname, ' ', student_middlename)
                                        WHEN `borrow_type` = 2 THEN adviser_name
                                        WHEN `borrow_type` = 3 THEN borrow_other
                                        ELSE ''
                                    END
                                    )
    	                            WHEN book_status = 0 THEN 'Library'
                                    ELSE ''
                                END
                            ) as 'curr_pos', MAX(bd_borrow_date)
                            FROM `tbl_bookinfo` 
                            INNER JOIN `tbl_category`
                            ON `bi_bookcategory_id` = `category_id`
                            INNER JOIN `tbl_author`
                            ON `bi_author_id` = `author_id`
                            LEFT JOIN `tbl_book`
                            ON `book_bi_id` = `bi_id`
                            LEFT JOIN `tbl_borrow_detail`
                            ON `bd_book_id` = `book_id` AND `bd_is_returned` = '0'
                            LEFT JOIN `tbl_borrow`
                            ON `borrow_id` = `bd_borrow_id`
                            LEFT JOIN `tbl_student`
                            ON `borrow_student_id` = `student_id`
                            LEFT JOIN `tbl_adviser`
                            ON `borrow_adviser_id` = `adviser_id`
                            " + whereQuery + @"
                            GROUP BY book_id) as main
                            ORDER BY bi_title";
            }


            dt = dc.Select(query, dic);
            
            return dt;
        }
    }
}
