﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using LibraryManagementSystem.app.db;
using LibraryManagementSystem.app.ui.form;

namespace LibraryManagementSystem.app.ui.uc
{
    public partial class LibrarianListUserControl : UserControl
    {
        public LibrarianListUserControl()
        {
            InitializeComponent();
        }
        public void Add()
        {
            LibrarianFormData add = new LibrarianFormData();
            if (DialogResult.OK == add.ShowDialog())
            {
                DatabaseConnection dc = new DatabaseConnection();

                String query = @"INSERT INTO tbl_user 
                                (user_name,
                                user_password,
                                user_type) 
                                VALUES 
                                (@user_name,
                                @user_password,
                                @user_type)";
                Dictionary<string, string> dic = new Dictionary<string, string>();
                dic.Add("@user_name", add.Username);
                dic.Add("@user_password", lib.Encryption.CreateSHA256(add.Password));
                dic.Add("@user_type", "2");
                String user_id = dc.Insert(query, dic).ToString();
                String queryLibrarian = @"INSERT INTO tbl_librarian 
                                (librarian_user_id,
                                librarian_name) 
                                VALUES 
                                (@librarian_user_id,
                                @librarian_name)";
                Dictionary<string, string> dicLibrarian = new Dictionary<string, string>();
                dicLibrarian.Add("@librarian_user_id", user_id);
                dicLibrarian.Add("@librarian_name", add.Librarian);
                dc.Insert(queryLibrarian, dicLibrarian);
                reloadDataGridView();
            }
        }
        private void buttonNew_Click(object sender, EventArgs e)
        {
            this.Add();
        }

        private void reloadDataGridView()
        {
            DatabaseConnection dc = new DatabaseConnection();
            String filter = textBoxFilterValue.Text.Trim();
            String filterQuery = " AND user_name LIKE @user_name AND librarian_name LIKE @librarian_name ";

            DataTable dt;
            String query = @"SELECT librarian_id, librarian_name as 'Librarian', user_name as 'Username'
                            FROM `tbl_librarian`
                            INNER JOIN `tbl_user`
                            ON `user_id` = `librarian_user_id`";
            if (filter == "")
            {
                dt = dc.Select(query);
            }
            else
            {
                Dictionary<string, string> dic = new Dictionary<string, string>();
                dic.Add("@user_name", '%' + filter + '%');
                dic.Add("@librarian_name", '%' + filter + '%');
                dt = dc.Select(query + filterQuery, dic);
            }

            dataGridViewFilteredData.DataSource = dt;
            dataGridViewFilteredData.Columns["librarian_id"].Visible = false;

        }

        private void AdminListUserControl_Load(object sender, EventArgs e)
        {
            reloadDataGridView();
        }

        private void buttonEdit_Click(object sender, EventArgs e)
        {
            if (dataGridViewFilteredData.SelectedRows.Count > 0)
            {
                DatabaseConnection dc = new DatabaseConnection();
                Dictionary<string, string> dicLibrarian = new Dictionary<string, string>();
                String librarian_id = dataGridViewFilteredData["librarian_id", dataGridViewFilteredData.CurrentRow.Index].Value.ToString();
                dicLibrarian.Add("librarian_id", librarian_id);
                String queryLibrarian = @"SELECT * FROM tbl_librarian WHERE librarian_id = @librarian_id";
                DataTable dtLibrarian = dc.Select(queryLibrarian, dicLibrarian);
                Dictionary<string, string> dicUser = new Dictionary<string, string>();
                String queryUser = @"SELECT * FROM tbl_user WHERE user_id = @user_id";
                dicUser.Add("@user_id", dtLibrarian.Rows[0]["librarian_user_id"].ToString());
                DataTable dtUser = dc.Select(queryUser, dicUser);
                LibrarianFormData add = new LibrarianFormData();
                add.Librarian = dtLibrarian.Rows[0]["librarian_name"].ToString();
                add.Username = dtUser.Rows[0]["user_name"].ToString();
                //DialogResult dr = add.ShowDialog();
                if (DialogResult.OK == add.ShowDialog())
                {
                    String updatePassword = "";
                    Dictionary<string, string> update = new Dictionary<string, string>();
                    if (add.Password.Length > 0)
                    {
                        updatePassword += ", user_password = @user_password ";
                        update.Add("@user_password", lib.Encryption.CreateSHA256(add.Password));
                    }
                    update.Add("@user_name", add.Username);
                    update.Add("@user_id", dtLibrarian.Rows[0]["librarian_user_id"].ToString());
                    String updateQuery = @"UPDATE tbl_user 
                                            SET user_name = @user_name" + updatePassword + @"
                                            WHERE user_id = @user_id";
                    dc.Update(updateQuery, update);

                    Dictionary<string, string> updateLibrarian = new Dictionary<string, string>();
                    updateLibrarian.Add("@librarian_name", add.Librarian);
                    updateLibrarian.Add("@librarian_id", librarian_id);
                    String updateLibrarianQuery = @"UPDATE tbl_librarian 
                                            SET librarian_name = @librarian_name
                                            WHERE librarian_id = @librarian_id";
                    dc.Update(updateLibrarianQuery, updateLibrarian);
                    reloadDataGridView();
                }
            }
        }

        private void buttonDelete_Click(object sender, EventArgs e)
        {
            if (dataGridViewFilteredData.SelectedRows.Count > 0)
            {

                DatabaseConnection dc = new DatabaseConnection();
                
                Dictionary<string, string> dicLibrarian = new Dictionary<string, string>();
                String librarian_id = dataGridViewFilteredData["librarian_id", dataGridViewFilteredData.CurrentRow.Index].Value.ToString();
                dicLibrarian.Add("librarian_id", librarian_id);
                String queryLibrarian = @"SELECT * FROM tbl_librarian WHERE librarian_id = @librarian_id";
                DataTable dtLibrarian = dc.Select(queryLibrarian, dicLibrarian);
                String user_id = dtLibrarian.Rows[0]["librarian_user_id"].ToString();
                if (lib.Singleton.Current_user == user_id)
                {
                    MessageBox.Show(this, "Current User can't be deleted", "Invalid Operation!", MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1);
                }
                else
                {
                    DialogResult dr = MessageBox.Show(this, "Are you sure you like to delete an admin?", "Deleting admin user", MessageBoxButtons.YesNo, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1);
                    if (dr == DialogResult.Yes)
                    {
                        Dictionary<string, string> dic = new Dictionary<string, string>();
                        dic.Add("user_id", user_id);
                        String query = @"DELETE FROM tbl_user WHERE user_id = @user_id";
                        dc.Delete(query, dic);
                        
                        String deleteLibrarianQuery = @"DELETE FROM tbl_librarian WHERE librarian_id = @librarian_id";
                        dc.Delete(deleteLibrarianQuery, dicLibrarian);

                        reloadDataGridView();
                    }
                }

            }
        }

        private void textBoxFilterValue_TextChanged(object sender, EventArgs e)
        {
            reloadDataGridView();
        }
    }
}
