﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Net;
using System.Net.Sockets;
using System.IO;
using LibraryManagementSystem.app.db;
using LibraryManagementSystem.app.lib;

namespace LibraryManagementSystem.app.ui.uc
{
    public partial class StudentDashboardUserControl : UserControl
    {
        private RFIDController rfidC;
        private DatabaseConnection dc;
        private int timerUpdate = 5;
        public StudentDashboardUserControl()
        {
            InitializeComponent();
            dc = new DatabaseConnection();
            rfidC = new RFIDController();
        }

        private void timerClock_Tick(object sender, EventArgs e)
        {
            labelClock.Text = DateTime.Now.ToString("dddd - MM/dd/yyyy hh:mm:ss tt");
            if(timerUpdate-- == 0)
            {
                updateDashboard();
                timerUpdate = 5;
            }
        }

        

        private void updateDashboard()
        {
            
            

        }

        
    }
}
