﻿using LibraryManagementSystem.app.db;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LibraryManagementSystem.app.ui.form
{
    public partial class StudentViewFormData : Form
    {
        public string FirstName { get => textBoxFirstName.Text.ToString(); set => textBoxFirstName.Text = value; }
        public string MiddleName { get => textBoxMiddleName.Text.ToString(); set => textBoxMiddleName.Text = value; }
        public string LastName { get => textBoxLastName.Text.ToString(); set => textBoxLastName.Text = value; }
        public DateTime Birthdate { get => dateTimePickerBirthdate.Value; set => dateTimePickerBirthdate.Value = value; }
        public string Gender {
            get
            {
                String gender = "";
                if(radioButtonMale.Checked)
                {
                    gender = "male";
                }
                if (radioButtonFemale.Checked)
                {
                    gender = "female";
                }
                return gender;
            }                
            set
            {
                if(value == "male")
                {
                    radioButtonMale.Checked = true;
                }
                if (value == "female")
                {
                    radioButtonFemale.Checked = true;
                }
            }
        }
        public string StudentNo { get => textBoxStudentNumber.Text.ToString(); set => textBoxStudentNumber.Text = value; }
        public string Address { get => textBoxAddress.Text.ToString(); set => textBoxAddress.Text = value; }
        public string Section { get => comboBoxSection.SelectedValue.ToString(); set => comboBoxSection.SelectedValue = value; }
        public string Adviser { get => comboBoxAdviser.SelectedValue.ToString(); set => comboBoxAdviser.SelectedValue = value; }
        public string FatherName { get => textBoxFatherName.Text.ToString(); set => textBoxFatherName.Text = value; }
        public string MotherName { get => textBoxMotherName.Text.ToString(); set => textBoxMotherName.Text = value; }
        public string MobileNumber { get => textBoxMobileNumber.Text.ToString(); set => textBoxMobileNumber.Text = value; }
        public string TelephoneNumber { get => textBoxTelephoneNumber.Text.ToString(); set => textBoxTelephoneNumber.Text = value; }
        public string EContactName { get => textBoxEContactName.Text.ToString(); set => textBoxEContactName.Text = value; }
        public string ERelation { get => textBoxERelation.Text.ToString(); set => textBoxERelation.Text = value; }
        public string EContactNumber { get => textBoxEContactNumber.Text.ToString(); set => textBoxEContactNumber.Text = value; }
        public string EAddress { get => textBoxEAddress.Text.ToString(); set => textBoxEAddress.Text = value; }
        public string IDPicture { get => pictureBoxIDPicture.ImageLocation; set => pictureBoxIDPicture.Load(value); }
        

        public StudentViewFormData()
        {
            InitializeComponent();
            DatabaseConnection dc = new DatabaseConnection();
            comboBoxSection.DataSource = dc.Select("Select * FROM tbl_section");
            comboBoxSection.DisplayMember = "section_name";
            comboBoxSection.ValueMember = "section_id";

            comboBoxAdviser.DataSource = dc.Select("Select * FROM tbl_adviser");
            comboBoxAdviser.DisplayMember = "adviser_name";
            comboBoxAdviser.ValueMember = "adviser_id";

            
        }

        private void StudentFormData_FormClosing(object sender, FormClosingEventArgs e)
        {
            
        }
        private Boolean validateForm()
        {
            Boolean IsValid = true;
            String errorMessage = "";
            if(textBoxStudentNumber.Text.Trim() == "")
            {
                errorMessage = "Student Number is Empty";
                IsValid = false;
            }
            if (textBoxFirstName.Text.Trim() == "")
            {
                errorMessage = "First Name must not be Empty";
                IsValid = false;
            }
            if (textBoxLastName.Text.Trim() == "")
            {
                errorMessage = "Last Name must not be Empty";
                IsValid = false;
            }

            MessageBox.Show(this, errorMessage, "Failed to Save", MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1);

            return IsValid;
        }


        private void buttonClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void StudentViewFormData_Load(object sender, EventArgs e)
        {
            DatabaseConnection dc = new DatabaseConnection();
            Dictionary<string, string> dicAttendance = new Dictionary<string, string>();
            dicAttendance.Add("@al_student_id", StudentNo);
            String queryAttendance = @"SELECT DATE_FORMAT(al_entry_date, '%m/%d/%Y') as 'Date',
                            DATE_FORMAT(al_entry_date, '%h:%i %p') as 'Time In', DATE_FORMAT(al_exit_date, '%h:%i %p') as 'Time Out',
                            al_student_id as 'ID'
                            FROM `tbl_attendance_log`
                            WHERE al_student_id = @al_student_id
                            ORDER BY al_entry_date DESC";
            DataTable dtAttendace = dc.Select(queryAttendance, dicAttendance);
            dataGridViewAttendance.DataSource = dtAttendace;
        }

        private void buttonPrint_Click(object sender, EventArgs e)
        {
            // Calculate the age.
            int age = DateTime.Today.Year - Birthdate.Year;
            // Go back to the year the person was born in case of a leap year
            if (Birthdate > DateTime.Today.AddYears(-age)) age--;

            Dictionary<string, string> parameters = new Dictionary<string, string>();
            parameters.Add("ReportParameterReportType", "Student Data Sheet");
            parameters.Add("ReportParameterFullName", (FirstName + " " + MiddleName + " " + LastName).ToUpper());
            parameters.Add("ReportParameterImagePath", new Uri(IDPicture).AbsoluteUri );
            parameters.Add("ReportParameterGender", Gender.ToUpper());
            parameters.Add("ReportParameterBirthdate", Birthdate.ToString("MM/dd/yyyy"));

            parameters.Add("ReportParameterAge", age.ToString());
            parameters.Add("ReportParameterAddress", Address);
            parameters.Add("ReportParameterStudentNumber", StudentNo);
            parameters.Add("ReportParameterMotherFullName", MotherName);
            parameters.Add("ReportParameterFatherFullName", FatherName);

            parameters.Add("ReportParameterMobileNumber", MobileNumber);
            parameters.Add("ReportParameterTelephoneNumber", TelephoneNumber);
            comboBoxSection.Enabled = true;
            comboBoxAdviser.Enabled = true;
            parameters.Add("ReportParameterSection", comboBoxSection.SelectedText);
            parameters.Add("ReportParameterAdviser", comboBoxAdviser.SelectedText);
            comboBoxSection.Enabled = false;
            comboBoxAdviser.Enabled = false;
            parameters.Add("ReportParameterEContactName", EContactName);
            parameters.Add("ReportParameterERelation", ERelation);
            parameters.Add("ReportParameterEContactNumber", EContactNumber);
            parameters.Add("ReportParameterEFullAddress", EAddress);
            ReportViewerForm rvf = new ReportViewerForm();
            rvf.ReportDataTable = null;
            rvf.DicReportParameters = parameters;
            rvf.ReportFile = "LibraryManagementSystem.reports.ReportStudentInformation.rdlc";
            rvf.Show();
        }
    }
}
