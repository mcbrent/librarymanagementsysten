﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LibraryManagementSystem.app.ui.form
{
    public partial class SectionFormData : Form
    {

        public SectionFormData()
        {
            InitializeComponent();
        }

        public string Section { get => textBoxSectionName.Text.ToString(); set => textBoxSectionName.Text = value; }

        private void StudentFormData_Load(object sender, EventArgs e)
        {
           
        }
    }
}
