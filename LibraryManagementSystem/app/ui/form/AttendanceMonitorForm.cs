﻿using LibraryManagementSystem.app.db;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Net;
using System.Net.Sockets;
using System.IO;
using LibraryManagementSystem.app.lib;

namespace LibraryManagementSystem.app.ui.form
{
    public partial class AttendanceMonitorForm : Form
    {
        private TcpClient client;
        private TcpListener listener;
        public StreamReader STR;
        public string recieve = "";
        public String TextToSend;


        
        public AttendanceMonitorForm()
        {
            InitializeComponent();   
        }

        private void backgroundWorkerServer_DoWork(object sender, DoWorkEventArgs e)
        {
            MethodInvoker mi = new MethodInvoker(delegate ()
            {
                if (recieve.Length > 0)
                {
                    labelNotification.Text = "true";

                    SetLogin(recieve);
                    recieve = "";

                }
            });
            client = listener.AcceptTcpClient();
            STR = new StreamReader(client.GetStream());
            while (client.Connected)
            {
                try
                {
                    recieve = STR.ReadLine();

                    //labelNotification.Invoke(new MethodInvoker(delegate ()
                    //{
                    //    if (recieve.Length > 0)
                    //    {
                    //        this.Text = "true";

                    //        SetLogin(recieve);
                    //        recieve = "";

                    //    }

                    //    //reloadAttendanceLog();

                    //}));
                    labelNotification.Invoke(mi);
                    recieve = "";
                }
                catch (Exception ex)
                {
                    //MessageBox.Show(ex.Message.ToString());
                    if (recieve.Length > 0)
                    {
                        labelNotification.Text = "true";

                        SetLogin(recieve);
                        recieve = "";

                    }
                }
            }
        }



        private void reloadAttendanceLog()
        {
            DatabaseConnection dc = new DatabaseConnection();

            DataTable dt;
            String query = @"SELECT al_id, student_id as `ID`, CONCAT(student_lastname, ', ', student_firstname, ' ', student_middlename) 'student', section_name as 'Section', al_entry_date as 'Time In', al_exit_date as 'Time Out'
                            FROM `tbl_attendance_log`
                            INNER JOIN `tbl_student`
                            ON `al_student_id` = `student_id`
                            INNER JOIN `tbl_section`
                            ON `student_section_id` = `section_id`
                            WHERE DATE(`al_entry_date`) = CURDATE() 
                            ORDER BY al_entry_date DESC";

            dt = dc.Select(query);
            dataGridViewAttendanceLog.DataSource = dt;
            dataGridViewAttendanceLog.Columns["al_id"].Visible = false;

        }





        private void AttendanceMonitorForm_Load(object sender, EventArgs e)
        {
            listener = MonitorServer.Listener;
            
            backgroundWorkerServer.RunWorkerAsync();
            reloadAttendanceLog();
        }

        private void labelNotification_TextChanged(object sender, EventArgs e)
        {
            if (Boolean.Parse(labelNotification.Text))
            {
                reloadAttendanceLog();
                labelNotification.Text = "false";
            }
        }

        private void SetLogin(String al_id)
        {
            DatabaseConnection db = new DatabaseConnection();

            Dictionary<string, string> dic = new Dictionary<string, string>();
            dic.Add("@al_id", al_id);
            String query = @"SELECT * 
                                FROM `tbl_attendance_log`
                                INNER JOIN `tbl_student`
                                ON `al_student_id` = `student_id`
                                INNER JOIN `tbl_section`
                                ON `student_section_id` = `section_id`
                                WHERE al_id = @al_id ";
            DataTable dtStudent = db.Select(query, dic);
            groupBoxUserInformation.Visible = true;

            
            labelTimeIn.Text = DateTime.Parse(dtStudent.Rows[0]["al_entry_date"].ToString()).ToString("MM/dd/yyyy hh:mm:ss tt");

            if (dtStudent.Rows[0]["al_exit_date"].ToString() == "")
            {
                labelTimeOut.Text = "";
            }
            else
            {
                labelTimeOut.Text = DateTime.Parse(dtStudent.Rows[0]["al_exit_date"].ToString()).ToString("MM/dd/yyyy hh:mm:ss tt");
            }

            Dictionary<string, string> dicFile = new Dictionary<string, string>();
            dicFile.Add("@fm_id", dtStudent.Rows[0]["student_fm_id"].ToString());
            String queryFile = @"SELECT * 
                                FROM `tbl_file_manager`
                                WHERE fm_id = @fm_id ";
            DataTable dtFile = db.Select(queryFile, dicFile);

            String queryImgDir = @"SELECT * 
                                FROM `tbl_settings`
                                WHERE settings_key = 'FILE_PATH' ";
            DataTable dtImgDir = db.Select(queryImgDir);


            labelStudentFullName.Text = dtStudent.Rows[0]["student_lastname"].ToString()
                                        + ", " + dtStudent.Rows[0]["student_firstname"].ToString()
                                        + " " + dtStudent.Rows[0]["student_middlename"].ToString();
            labelSection.Text = dtStudent.Rows[0]["section_name"].ToString();
            pictureBoxIDPicture.Load(dtImgDir.Rows[0]["settings_value"].ToString() + "\\student\\" + dtFile.Rows[0]["fm_file_name"].ToString());

            groupBoxUserInformation.Visible = true;
        }

        private void AttendanceMonitorForm_FormClosing(object sender, FormClosingEventArgs e)
        {

        }

        private void buttonReloadLast_Click(object sender, EventArgs e)
        {
            DatabaseConnection db = new DatabaseConnection();

            Dictionary<string, string> dic = new Dictionary<string, string>();
            String query = @"SELECT * 
                            FROM `tbl_attendance_log` 
                            ORDER BY al_id DESC
                            LIMIT 1 ";
            DataTable dtLast = db.Select(query);
            if(dtLast.Rows.Count > 0)
            {
                labelNotification.Text = "true";
                recieve = dtLast.Rows[0]["aL_id"].ToString();
                SetLogin(recieve);
                recieve = "";
            }
            
        }
    }
}
