﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using LibraryManagementSystem.app.db;
using LibraryManagementSystem.app.lib;

namespace LibraryManagementSystem.app.ui.form
{
    public partial class StudentForm : DevExpress.XtraEditors.XtraForm
    {
        private Boolean _overpassFormClose = false;
        public bool OverpassFormClose { get => _overpassFormClose; set => _overpassFormClose = value; }

        private UserControl _current = null;

        

        public StudentForm()
        {
            InitializeComponent();
        }


        public void setContent(UserControl userControl)
        {
            panelMainArea.Controls.Clear();
            _current = userControl;
            _current.Dock = DockStyle.Fill;
            panelMainArea.Controls.Add(userControl);
        }

        private void bookSearchToolStripMenuItem_Click(object sender, EventArgs e)
        {
            setContent(new app.ui.uc.BookListUserControl());
        }

        private void viewAllToolStripMenuItem_Click(object sender, EventArgs e)
        {
            setContent(new app.ui.uc.AdminListUserControl());
        }

        private void AdminForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            DialogResult dr = MessageBox.Show(this, "Are you sure you like to exit the application", "Library Management System", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if (dr == DialogResult.Yes)
            {
                e.Cancel = false;
            }
            else
            {
                e.Cancel = true;
            }
        }

        private void AdminForm_FormClosed(object sender, FormClosedEventArgs e)
        {
            Application.Exit();
        }

        private void createToolStripMenuItem_Click(object sender, EventArgs e)
        {
            uc.AdminListUserControl al = new uc.AdminListUserControl();
            setContent(al);
            al.Add();
        }

        private void createToolStripMenuItem2_Click(object sender, EventArgs e)
        {
            Form add = new app.ui.form.SectionFormData();
            add.ShowDialog();
        }

        private void allToolStripMenuItem_Click(object sender, EventArgs e)
        {
            setContent(new app.ui.uc.SectionListUserControl());
        }

        private void viewAllToolStripMenuItem2_Click(object sender, EventArgs e)
        {
            setContent(new app.ui.uc.StudentListUserControl());
        }

        private void addToolStripMenuItem_Click(object sender, EventArgs e)
        {
            uc.SectionListUserControl sluc = new uc.SectionListUserControl();
            setContent(sluc);
            sluc.Add();
        }

        private void allAdviserToolStripMenuItem_Click(object sender, EventArgs e)
        {
            setContent(new app.ui.uc.AdviserListUserControl());
        }

        private void createAdviserToolStripMenuItem_Click(object sender, EventArgs e)
        {
            uc.AdviserListUserControl aluc = new uc.AdviserListUserControl();
            setContent(aluc);
            aluc.Add();
        }

        private void allStudentToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void allBookToolStripMenuItem_Click(object sender, EventArgs e)
        {
            setContent(new app.ui.uc.BookInformationListUserControl());
        }

        private void bookCategoryToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            setContent(new app.ui.uc.BookCategoryListUserControl());

        }

        private void allBookAuthorToolStripMenuItem_Click(object sender, EventArgs e)
        {
            setContent(new app.ui.uc.BookAuthorListUserControl());
        }

        private void addBookToolStripMenuItem_Click(object sender, EventArgs e)
        {
            uc.BookInformationListUserControl biluc = new uc.BookInformationListUserControl();
            setContent(biluc);
            biluc.Add();
        }

        private void bookRegistrationToolStripMenuItem_Click(object sender, EventArgs e)
        {
            setContent(new app.ui.uc.RegisterBookListUserControl());
        }

        

        private void fileDirectoryToolStripMenuItem_Click(object sender, EventArgs e)
        {
            DatabaseConnection dc = new DatabaseConnection();
            FileDirectoryFormData fdfd = new FileDirectoryFormData();
            DataTable dt = dc.Select("SELECT * FROM tbl_settings WHERE settings_key = 'FILE_PATH' ");
            fdfd.Directory = dt.Rows[0]["settings_value"].ToString();
            if (DialogResult.OK == fdfd.ShowDialog())
            {
                String newDir = fdfd.Directory;
                String idPictureDir = "/student";
                System.IO.Directory.CreateDirectory(newDir + idPictureDir);
                String updateQuery = @"UPDATE tbl_settings 
                                SET settings_value = @settings_value
                                WHERE settings_key = @settings_key ";
                Dictionary<string, string> dic = new Dictionary<string, string>();
                dic.Add("@settings_value", newDir );
                dic.Add("@settings_key", "FILE_PATH");
                dc.Update(updateQuery, dic);
            }

        }

        private void databaseConnectionToolStripMenuItem_Click(object sender, EventArgs e)
        {
            DatabaseSettingsFormData dsfd = new DatabaseSettingsFormData();
            dsfd.ShowDialog();
        }

        

        private void logoutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Hide();
            Initiator i = new Initiator();
            i.ShowLogin();
        }

        private void calculatorToolStripMenuItem_Click(object sender, EventArgs e)
        {
            System.Diagnostics.Process.Start("Calc");
        }

        private void dashboardToolStripMenuItem_Click(object sender, EventArgs e)
        {
            setContent(new uc.StudentDashboardUserControl());
        }

        private void AdminForm_Load(object sender, EventArgs e)
        {
            setContent(new uc.StudentDashboardUserControl());
        }

        private void attendanceToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //this.Hide();
            Initiator i = new Initiator();
            i.ShowAttendance();
        }

        private void borrowReportToolStripMenuItem_Click(object sender, EventArgs e)
        {
            setContent(new uc.BorrowReportUserControl());
        }

        private void returnReportToolStripMenuItem_Click(object sender, EventArgs e)
        {
            setContent(new uc.ReturnReportUserControl());
        }

        private void attendanceLogReportToolStripMenuItem_Click(object sender, EventArgs e)
        {
            setContent(new uc.AttendanceLogReportUserControl());
        }

        private void allBookListToolStripMenuItem_Click(object sender, EventArgs e)
        {
            setContent(new uc.BooksReportUserControl());
        }

        private void allTransactionToolStripMenuItem_Click(object sender, EventArgs e)
        {
            uc.TransactionLogUserControl tlu = new uc.TransactionLogUserControl();
            tlu.ReturnButton += new EventHandler(TransactionLogUserControl_ReturnClicked);
            tlu.BorrowButton += new EventHandler(TransactionLogUserControl_BorrowClicked);
            setContent(tlu);
        }
        private void returnToolStripMenuItem_Click(object sender, EventArgs e)
        {
            uc.ReturnBookUserControl rbu = new uc.ReturnBookUserControl();
            rbu.ResetButton += new
                    EventHandler(ReturnBookUserControl_ResetClicked);
            setContent(rbu);
        }
        private void borrowToolStripMenuItem_Click(object sender, EventArgs e)
        {
            uc.BurrowBookUserControl bbu = new uc.BurrowBookUserControl();
            bbu.ResetButton += new
                    EventHandler(BurrowBookUserControl_ResetClicked);
            setContent(bbu);
        }
        private void TransactionLogUserControl_BorrowClicked(object sender, EventArgs e)
        {
            
        }
        private void TransactionLogUserControl_ReturnClicked(object sender, EventArgs e)
        {
            
        }
        private void BurrowBookUserControl_ResetClicked(object sender, EventArgs e)
        {
            
        }
        private void ReturnBookUserControl_ResetClicked(object sender, EventArgs e)
        {
           
        }

        private void viewAllToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            setContent(new app.ui.uc.LibrarianListUserControl());
        }

        private void createToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            uc.LibrarianListUserControl ll = new uc.LibrarianListUserControl();
            setContent(ll);
            ll.Add();
        }

        private void attendanceMonitorToolStripMenuItem_Click(object sender, EventArgs e)
        {
            form.AttendanceMonitorForm amf = new form.AttendanceMonitorForm();
            amf.Show();
        }
    }
}