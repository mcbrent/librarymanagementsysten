﻿using LibraryManagementSystem.app.db;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Script.Serialization;
using System.Windows.Forms;

namespace LibraryManagementSystem.app.ui.form
{
    public partial class BookInformationFormLookUp : Form
    {
        private string _scannedAuthor = "";
        public String ScannedAuthor { get => _scannedAuthor; }
        public String BookInfoID {
            get
            {
                String bi_id = "";
                if (dataGridViewBookInfoList.SelectedRows.Count > 0)
                {
                    DatabaseConnection dc = new DatabaseConnection();
                    bi_id = dataGridViewBookInfoList["bi_id", dataGridViewBookInfoList.CurrentRow.Index].Value.ToString();
                }
                else
                {
                    bi_id = "0";
                }
                return bi_id;
            }
        }
        private DatabaseConnection dc;
        public BookInformationFormLookUp()
        {
            InitializeComponent();
            dc = new DatabaseConnection();
            String filter = textBoxFilter.Text.Trim();

            DataTable dt;
            String query = @"SELECT bi_id, bi_isbn, bi_title, category_name, author_name
                                FROM `tbl_bookinfo` 
                                LEFT JOIN `tbl_author` 
                                ON bi_author_id = author_id
                                LEFT JOIN `tbl_category` 
                                ON bi_bookcategory_id = category_id";
            if (filter == "")
            {
                dt = dc.Select(query);
            }
            else
            {
                Dictionary<string, string> dic = new Dictionary<string, string>();
                dic.Add("@section_name", '%' + filter + '%');
                dt = dc.Select(query, dic);
            }

            dataGridViewBookInfoList.DataSource = dt;
            dataGridViewBookInfoList.Columns["bi_id"].Visible = false;

        }

        

        
    }
}
