﻿using LibraryManagementSystem.app.db;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LibraryManagementSystem.app.ui.form
{
    public partial class FileDirectoryFormData : Form
    {
        public string Directory { get => textBoxDirectory.Text.ToString(); set => textBoxDirectory.Text = value; }
        public FileDirectoryFormData()
        {
            InitializeComponent();
        }

        private void buttonSearch_Click(object sender, EventArgs e)
        {
            FolderBrowserDialog fbd = new FolderBrowserDialog();
            fbd.SelectedPath = textBoxDirectory.Text;
            fbd.ShowDialog();
            textBoxDirectory.Text = fbd.SelectedPath;
        }
    }
}
