﻿namespace LibraryManagementSystem.app.ui.form
{
    partial class AdminForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(AdminForm));
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.systemToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.dashboardToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.usersToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.administratorToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.createToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.viewAllToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.librarianToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.createToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.viewAllToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.studentToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.createToolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.viewAllToolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.systemConfigurationToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.databaseConnectionToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.fileDirectoryToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.utilitiesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.calculatorToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.attendanceToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.attendanceMonitorToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.logoutToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.listToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.sectionToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.addToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.allToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.adviserToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.createAdviserToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.allAdviserToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.bookToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.addBookToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.allBookToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.bookCategoryToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.addBookCategoryToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.bookCategoryToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.bookAuthorToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.addBookAuthorToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.allBookAuthorToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.reportsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.borrowReportToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.returnReportToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.attendanceLogReportToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.allBookListToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.bookToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.bookSearchToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.bookRegistrationToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.transactionToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.borrowToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.returnToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.allTransactionToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.panelMainArea = new System.Windows.Forms.Panel();
            this.overDueBookReportToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.systemToolStripMenuItem,
            this.listToolStripMenuItem,
            this.reportsToolStripMenuItem,
            this.bookToolStripMenuItem,
            this.transactionToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(864, 28);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // systemToolStripMenuItem
            // 
            this.systemToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.dashboardToolStripMenuItem,
            this.usersToolStripMenuItem,
            this.systemConfigurationToolStripMenuItem,
            this.utilitiesToolStripMenuItem,
            this.logoutToolStripMenuItem});
            this.systemToolStripMenuItem.Name = "systemToolStripMenuItem";
            this.systemToolStripMenuItem.Size = new System.Drawing.Size(68, 24);
            this.systemToolStripMenuItem.Text = "System";
            // 
            // dashboardToolStripMenuItem
            // 
            this.dashboardToolStripMenuItem.Name = "dashboardToolStripMenuItem";
            this.dashboardToolStripMenuItem.Size = new System.Drawing.Size(226, 26);
            this.dashboardToolStripMenuItem.Text = "Dashboard";
            this.dashboardToolStripMenuItem.Click += new System.EventHandler(this.dashboardToolStripMenuItem_Click);
            // 
            // usersToolStripMenuItem
            // 
            this.usersToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.administratorToolStripMenuItem,
            this.librarianToolStripMenuItem,
            this.studentToolStripMenuItem});
            this.usersToolStripMenuItem.Name = "usersToolStripMenuItem";
            this.usersToolStripMenuItem.Size = new System.Drawing.Size(226, 26);
            this.usersToolStripMenuItem.Text = "Users";
            // 
            // administratorToolStripMenuItem
            // 
            this.administratorToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.createToolStripMenuItem,
            this.viewAllToolStripMenuItem});
            this.administratorToolStripMenuItem.Name = "administratorToolStripMenuItem";
            this.administratorToolStripMenuItem.Size = new System.Drawing.Size(175, 26);
            this.administratorToolStripMenuItem.Text = "Administrator";
            // 
            // createToolStripMenuItem
            // 
            this.createToolStripMenuItem.Name = "createToolStripMenuItem";
            this.createToolStripMenuItem.Size = new System.Drawing.Size(138, 26);
            this.createToolStripMenuItem.Text = "Create";
            this.createToolStripMenuItem.Click += new System.EventHandler(this.createToolStripMenuItem_Click);
            // 
            // viewAllToolStripMenuItem
            // 
            this.viewAllToolStripMenuItem.Name = "viewAllToolStripMenuItem";
            this.viewAllToolStripMenuItem.Size = new System.Drawing.Size(138, 26);
            this.viewAllToolStripMenuItem.Text = "View All";
            this.viewAllToolStripMenuItem.Click += new System.EventHandler(this.viewAllToolStripMenuItem_Click);
            // 
            // librarianToolStripMenuItem
            // 
            this.librarianToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.createToolStripMenuItem1,
            this.viewAllToolStripMenuItem1});
            this.librarianToolStripMenuItem.Name = "librarianToolStripMenuItem";
            this.librarianToolStripMenuItem.Size = new System.Drawing.Size(175, 26);
            this.librarianToolStripMenuItem.Text = "Librarian";
            // 
            // createToolStripMenuItem1
            // 
            this.createToolStripMenuItem1.Name = "createToolStripMenuItem1";
            this.createToolStripMenuItem1.Size = new System.Drawing.Size(138, 26);
            this.createToolStripMenuItem1.Text = "Create";
            this.createToolStripMenuItem1.Click += new System.EventHandler(this.createToolStripMenuItem1_Click);
            // 
            // viewAllToolStripMenuItem1
            // 
            this.viewAllToolStripMenuItem1.Name = "viewAllToolStripMenuItem1";
            this.viewAllToolStripMenuItem1.Size = new System.Drawing.Size(138, 26);
            this.viewAllToolStripMenuItem1.Text = "View All";
            this.viewAllToolStripMenuItem1.Click += new System.EventHandler(this.viewAllToolStripMenuItem1_Click);
            // 
            // studentToolStripMenuItem
            // 
            this.studentToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.createToolStripMenuItem2,
            this.viewAllToolStripMenuItem2});
            this.studentToolStripMenuItem.Name = "studentToolStripMenuItem";
            this.studentToolStripMenuItem.Size = new System.Drawing.Size(175, 26);
            this.studentToolStripMenuItem.Text = "Student";
            // 
            // createToolStripMenuItem2
            // 
            this.createToolStripMenuItem2.Name = "createToolStripMenuItem2";
            this.createToolStripMenuItem2.Size = new System.Drawing.Size(138, 26);
            this.createToolStripMenuItem2.Text = "Create";
            this.createToolStripMenuItem2.Click += new System.EventHandler(this.createToolStripMenuItem2_Click);
            // 
            // viewAllToolStripMenuItem2
            // 
            this.viewAllToolStripMenuItem2.Name = "viewAllToolStripMenuItem2";
            this.viewAllToolStripMenuItem2.Size = new System.Drawing.Size(138, 26);
            this.viewAllToolStripMenuItem2.Text = "View All";
            this.viewAllToolStripMenuItem2.Click += new System.EventHandler(this.viewAllToolStripMenuItem2_Click);
            // 
            // systemConfigurationToolStripMenuItem
            // 
            this.systemConfigurationToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.databaseConnectionToolStripMenuItem,
            this.fileDirectoryToolStripMenuItem});
            this.systemConfigurationToolStripMenuItem.Name = "systemConfigurationToolStripMenuItem";
            this.systemConfigurationToolStripMenuItem.Size = new System.Drawing.Size(226, 26);
            this.systemConfigurationToolStripMenuItem.Text = "System Configuration";
            // 
            // databaseConnectionToolStripMenuItem
            // 
            this.databaseConnectionToolStripMenuItem.Name = "databaseConnectionToolStripMenuItem";
            this.databaseConnectionToolStripMenuItem.Size = new System.Drawing.Size(226, 26);
            this.databaseConnectionToolStripMenuItem.Text = "Database Connection";
            this.databaseConnectionToolStripMenuItem.Click += new System.EventHandler(this.databaseConnectionToolStripMenuItem_Click);
            // 
            // fileDirectoryToolStripMenuItem
            // 
            this.fileDirectoryToolStripMenuItem.Name = "fileDirectoryToolStripMenuItem";
            this.fileDirectoryToolStripMenuItem.Size = new System.Drawing.Size(226, 26);
            this.fileDirectoryToolStripMenuItem.Text = "File Directory";
            this.fileDirectoryToolStripMenuItem.Click += new System.EventHandler(this.fileDirectoryToolStripMenuItem_Click);
            // 
            // utilitiesToolStripMenuItem
            // 
            this.utilitiesToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.calculatorToolStripMenuItem,
            this.attendanceToolStripMenuItem,
            this.attendanceMonitorToolStripMenuItem});
            this.utilitiesToolStripMenuItem.Name = "utilitiesToolStripMenuItem";
            this.utilitiesToolStripMenuItem.Size = new System.Drawing.Size(226, 26);
            this.utilitiesToolStripMenuItem.Text = "Utilities";
            // 
            // calculatorToolStripMenuItem
            // 
            this.calculatorToolStripMenuItem.Name = "calculatorToolStripMenuItem";
            this.calculatorToolStripMenuItem.Size = new System.Drawing.Size(217, 26);
            this.calculatorToolStripMenuItem.Text = "Calculator";
            this.calculatorToolStripMenuItem.Click += new System.EventHandler(this.calculatorToolStripMenuItem_Click);
            // 
            // attendanceToolStripMenuItem
            // 
            this.attendanceToolStripMenuItem.Name = "attendanceToolStripMenuItem";
            this.attendanceToolStripMenuItem.Size = new System.Drawing.Size(217, 26);
            this.attendanceToolStripMenuItem.Text = "Attendance";
            this.attendanceToolStripMenuItem.Click += new System.EventHandler(this.attendanceToolStripMenuItem_Click);
            // 
            // attendanceMonitorToolStripMenuItem
            // 
            this.attendanceMonitorToolStripMenuItem.Name = "attendanceMonitorToolStripMenuItem";
            this.attendanceMonitorToolStripMenuItem.Size = new System.Drawing.Size(217, 26);
            this.attendanceMonitorToolStripMenuItem.Text = "Attendance Monitor";
            this.attendanceMonitorToolStripMenuItem.Click += new System.EventHandler(this.attendanceMonitorToolStripMenuItem_Click);
            // 
            // logoutToolStripMenuItem
            // 
            this.logoutToolStripMenuItem.Name = "logoutToolStripMenuItem";
            this.logoutToolStripMenuItem.Size = new System.Drawing.Size(226, 26);
            this.logoutToolStripMenuItem.Text = "Logout";
            this.logoutToolStripMenuItem.Click += new System.EventHandler(this.logoutToolStripMenuItem_Click);
            // 
            // listToolStripMenuItem
            // 
            this.listToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.sectionToolStripMenuItem,
            this.adviserToolStripMenuItem,
            this.bookToolStripMenuItem1,
            this.bookCategoryToolStripMenuItem,
            this.bookAuthorToolStripMenuItem});
            this.listToolStripMenuItem.Name = "listToolStripMenuItem";
            this.listToolStripMenuItem.Size = new System.Drawing.Size(43, 24);
            this.listToolStripMenuItem.Text = "List";
            // 
            // sectionToolStripMenuItem
            // 
            this.sectionToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.addToolStripMenuItem,
            this.allToolStripMenuItem});
            this.sectionToolStripMenuItem.Name = "sectionToolStripMenuItem";
            this.sectionToolStripMenuItem.Size = new System.Drawing.Size(182, 26);
            this.sectionToolStripMenuItem.Text = "Section";
            // 
            // addToolStripMenuItem
            // 
            this.addToolStripMenuItem.Name = "addToolStripMenuItem";
            this.addToolStripMenuItem.Size = new System.Drawing.Size(180, 26);
            this.addToolStripMenuItem.Text = "Create Section";
            this.addToolStripMenuItem.Click += new System.EventHandler(this.addToolStripMenuItem_Click);
            // 
            // allToolStripMenuItem
            // 
            this.allToolStripMenuItem.Name = "allToolStripMenuItem";
            this.allToolStripMenuItem.Size = new System.Drawing.Size(180, 26);
            this.allToolStripMenuItem.Text = "All";
            this.allToolStripMenuItem.Click += new System.EventHandler(this.allToolStripMenuItem_Click);
            // 
            // adviserToolStripMenuItem
            // 
            this.adviserToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.createAdviserToolStripMenuItem,
            this.allAdviserToolStripMenuItem});
            this.adviserToolStripMenuItem.Name = "adviserToolStripMenuItem";
            this.adviserToolStripMenuItem.Size = new System.Drawing.Size(182, 26);
            this.adviserToolStripMenuItem.Text = "Faculty";
            // 
            // createAdviserToolStripMenuItem
            // 
            this.createAdviserToolStripMenuItem.Name = "createAdviserToolStripMenuItem";
            this.createAdviserToolStripMenuItem.Size = new System.Drawing.Size(176, 26);
            this.createAdviserToolStripMenuItem.Text = "Create Faculty";
            this.createAdviserToolStripMenuItem.Click += new System.EventHandler(this.createAdviserToolStripMenuItem_Click);
            // 
            // allAdviserToolStripMenuItem
            // 
            this.allAdviserToolStripMenuItem.Name = "allAdviserToolStripMenuItem";
            this.allAdviserToolStripMenuItem.Size = new System.Drawing.Size(176, 26);
            this.allAdviserToolStripMenuItem.Text = "All Faculty";
            this.allAdviserToolStripMenuItem.Click += new System.EventHandler(this.allAdviserToolStripMenuItem_Click);
            // 
            // bookToolStripMenuItem1
            // 
            this.bookToolStripMenuItem1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.addBookToolStripMenuItem,
            this.allBookToolStripMenuItem});
            this.bookToolStripMenuItem1.Name = "bookToolStripMenuItem1";
            this.bookToolStripMenuItem1.Size = new System.Drawing.Size(182, 26);
            this.bookToolStripMenuItem1.Text = "Book";
            // 
            // addBookToolStripMenuItem
            // 
            this.addBookToolStripMenuItem.Name = "addBookToolStripMenuItem";
            this.addBookToolStripMenuItem.Size = new System.Drawing.Size(150, 26);
            this.addBookToolStripMenuItem.Text = "Add Book";
            this.addBookToolStripMenuItem.Click += new System.EventHandler(this.addBookToolStripMenuItem_Click);
            // 
            // allBookToolStripMenuItem
            // 
            this.allBookToolStripMenuItem.Name = "allBookToolStripMenuItem";
            this.allBookToolStripMenuItem.Size = new System.Drawing.Size(150, 26);
            this.allBookToolStripMenuItem.Text = "All Book";
            this.allBookToolStripMenuItem.Click += new System.EventHandler(this.allBookToolStripMenuItem_Click);
            // 
            // bookCategoryToolStripMenuItem
            // 
            this.bookCategoryToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.addBookCategoryToolStripMenuItem,
            this.bookCategoryToolStripMenuItem1});
            this.bookCategoryToolStripMenuItem.Name = "bookCategoryToolStripMenuItem";
            this.bookCategoryToolStripMenuItem.Size = new System.Drawing.Size(182, 26);
            this.bookCategoryToolStripMenuItem.Text = "Book Category";
            // 
            // addBookCategoryToolStripMenuItem
            // 
            this.addBookCategoryToolStripMenuItem.Name = "addBookCategoryToolStripMenuItem";
            this.addBookCategoryToolStripMenuItem.Size = new System.Drawing.Size(214, 26);
            this.addBookCategoryToolStripMenuItem.Text = "Add Book Category";
            // 
            // bookCategoryToolStripMenuItem1
            // 
            this.bookCategoryToolStripMenuItem1.Name = "bookCategoryToolStripMenuItem1";
            this.bookCategoryToolStripMenuItem1.Size = new System.Drawing.Size(214, 26);
            this.bookCategoryToolStripMenuItem1.Text = "Book Category";
            this.bookCategoryToolStripMenuItem1.Click += new System.EventHandler(this.bookCategoryToolStripMenuItem1_Click);
            // 
            // bookAuthorToolStripMenuItem
            // 
            this.bookAuthorToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.addBookAuthorToolStripMenuItem,
            this.allBookAuthorToolStripMenuItem});
            this.bookAuthorToolStripMenuItem.Name = "bookAuthorToolStripMenuItem";
            this.bookAuthorToolStripMenuItem.Size = new System.Drawing.Size(182, 26);
            this.bookAuthorToolStripMenuItem.Text = "Book Author";
            // 
            // addBookAuthorToolStripMenuItem
            // 
            this.addBookAuthorToolStripMenuItem.Name = "addBookAuthorToolStripMenuItem";
            this.addBookAuthorToolStripMenuItem.Size = new System.Drawing.Size(199, 26);
            this.addBookAuthorToolStripMenuItem.Text = "Add Book Author";
            // 
            // allBookAuthorToolStripMenuItem
            // 
            this.allBookAuthorToolStripMenuItem.Name = "allBookAuthorToolStripMenuItem";
            this.allBookAuthorToolStripMenuItem.Size = new System.Drawing.Size(199, 26);
            this.allBookAuthorToolStripMenuItem.Text = "All Book Author";
            this.allBookAuthorToolStripMenuItem.Click += new System.EventHandler(this.allBookAuthorToolStripMenuItem_Click);
            // 
            // reportsToolStripMenuItem
            // 
            this.reportsToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.borrowReportToolStripMenuItem,
            this.returnReportToolStripMenuItem,
            this.attendanceLogReportToolStripMenuItem,
            this.allBookListToolStripMenuItem,
            this.overDueBookReportToolStripMenuItem});
            this.reportsToolStripMenuItem.Name = "reportsToolStripMenuItem";
            this.reportsToolStripMenuItem.Size = new System.Drawing.Size(72, 24);
            this.reportsToolStripMenuItem.Text = "Reports";
            // 
            // borrowReportToolStripMenuItem
            // 
            this.borrowReportToolStripMenuItem.Name = "borrowReportToolStripMenuItem";
            this.borrowReportToolStripMenuItem.Size = new System.Drawing.Size(238, 26);
            this.borrowReportToolStripMenuItem.Text = "Borrow Report";
            this.borrowReportToolStripMenuItem.Click += new System.EventHandler(this.borrowReportToolStripMenuItem_Click);
            // 
            // returnReportToolStripMenuItem
            // 
            this.returnReportToolStripMenuItem.Name = "returnReportToolStripMenuItem";
            this.returnReportToolStripMenuItem.Size = new System.Drawing.Size(238, 26);
            this.returnReportToolStripMenuItem.Text = "Return Report";
            this.returnReportToolStripMenuItem.Click += new System.EventHandler(this.returnReportToolStripMenuItem_Click);
            // 
            // attendanceLogReportToolStripMenuItem
            // 
            this.attendanceLogReportToolStripMenuItem.Name = "attendanceLogReportToolStripMenuItem";
            this.attendanceLogReportToolStripMenuItem.Size = new System.Drawing.Size(238, 26);
            this.attendanceLogReportToolStripMenuItem.Text = "Attendance Log Report";
            this.attendanceLogReportToolStripMenuItem.Click += new System.EventHandler(this.attendanceLogReportToolStripMenuItem_Click);
            // 
            // allBookListToolStripMenuItem
            // 
            this.allBookListToolStripMenuItem.Name = "allBookListToolStripMenuItem";
            this.allBookListToolStripMenuItem.Size = new System.Drawing.Size(238, 26);
            this.allBookListToolStripMenuItem.Text = "All Book List";
            this.allBookListToolStripMenuItem.Click += new System.EventHandler(this.allBookListToolStripMenuItem_Click);
            // 
            // bookToolStripMenuItem
            // 
            this.bookToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.bookSearchToolStripMenuItem,
            this.bookRegistrationToolStripMenuItem});
            this.bookToolStripMenuItem.Name = "bookToolStripMenuItem";
            this.bookToolStripMenuItem.Size = new System.Drawing.Size(55, 24);
            this.bookToolStripMenuItem.Text = "Book";
            // 
            // bookSearchToolStripMenuItem
            // 
            this.bookSearchToolStripMenuItem.Name = "bookSearchToolStripMenuItem";
            this.bookSearchToolStripMenuItem.Size = new System.Drawing.Size(202, 26);
            this.bookSearchToolStripMenuItem.Text = "Book Search";
            this.bookSearchToolStripMenuItem.Click += new System.EventHandler(this.bookSearchToolStripMenuItem_Click);
            // 
            // bookRegistrationToolStripMenuItem
            // 
            this.bookRegistrationToolStripMenuItem.Name = "bookRegistrationToolStripMenuItem";
            this.bookRegistrationToolStripMenuItem.Size = new System.Drawing.Size(202, 26);
            this.bookRegistrationToolStripMenuItem.Text = "Book Registration";
            this.bookRegistrationToolStripMenuItem.Click += new System.EventHandler(this.bookRegistrationToolStripMenuItem_Click);
            // 
            // transactionToolStripMenuItem
            // 
            this.transactionToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.borrowToolStripMenuItem,
            this.returnToolStripMenuItem,
            this.allTransactionToolStripMenuItem});
            this.transactionToolStripMenuItem.Name = "transactionToolStripMenuItem";
            this.transactionToolStripMenuItem.Size = new System.Drawing.Size(96, 24);
            this.transactionToolStripMenuItem.Text = "Transaction";
            // 
            // borrowToolStripMenuItem
            // 
            this.borrowToolStripMenuItem.Name = "borrowToolStripMenuItem";
            this.borrowToolStripMenuItem.Size = new System.Drawing.Size(181, 26);
            this.borrowToolStripMenuItem.Text = "Borrow";
            this.borrowToolStripMenuItem.Click += new System.EventHandler(this.borrowToolStripMenuItem_Click);
            // 
            // returnToolStripMenuItem
            // 
            this.returnToolStripMenuItem.Name = "returnToolStripMenuItem";
            this.returnToolStripMenuItem.Size = new System.Drawing.Size(181, 26);
            this.returnToolStripMenuItem.Text = "Return";
            this.returnToolStripMenuItem.Click += new System.EventHandler(this.returnToolStripMenuItem_Click);
            // 
            // allTransactionToolStripMenuItem
            // 
            this.allTransactionToolStripMenuItem.Name = "allTransactionToolStripMenuItem";
            this.allTransactionToolStripMenuItem.Size = new System.Drawing.Size(181, 26);
            this.allTransactionToolStripMenuItem.Text = "All Transaction";
            this.allTransactionToolStripMenuItem.Click += new System.EventHandler(this.allTransactionToolStripMenuItem_Click);
            // 
            // panelMainArea
            // 
            this.panelMainArea.AutoSize = true;
            this.panelMainArea.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panelMainArea.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelMainArea.Location = new System.Drawing.Point(0, 28);
            this.panelMainArea.Name = "panelMainArea";
            this.panelMainArea.Size = new System.Drawing.Size(864, 679);
            this.panelMainArea.TabIndex = 1;
            // 
            // overDueBookReportToolStripMenuItem
            // 
            this.overDueBookReportToolStripMenuItem.Name = "overDueBookReportToolStripMenuItem";
            this.overDueBookReportToolStripMenuItem.Size = new System.Drawing.Size(238, 26);
            this.overDueBookReportToolStripMenuItem.Text = "Over Due Book Report";
            this.overDueBookReportToolStripMenuItem.Click += new System.EventHandler(this.overDueBookReportToolStripMenuItem_Click);
            // 
            // AdminForm
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.ClientSize = new System.Drawing.Size(864, 707);
            this.Controls.Add(this.panelMainArea);
            this.Controls.Add(this.menuStrip1);
            this.Font = new System.Drawing.Font("Segoe UI", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this.menuStrip1;
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "AdminForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Library Management System";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.AdminForm_FormClosing);
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.AdminForm_FormClosed);
            this.Load += new System.EventHandler(this.AdminForm_Load);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem systemToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem logoutToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem listToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem reportsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem bookToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem transactionToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem borrowToolStripMenuItem;
        private System.Windows.Forms.Panel panelMainArea;
        private System.Windows.Forms.ToolStripMenuItem bookSearchToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem usersToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem administratorToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem createToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem viewAllToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem librarianToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem createToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem viewAllToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem studentToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem createToolStripMenuItem2;
        private System.Windows.Forms.ToolStripMenuItem viewAllToolStripMenuItem2;
        private System.Windows.Forms.ToolStripMenuItem sectionToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem addToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem allToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem adviserToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem createAdviserToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem allAdviserToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem bookToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem addBookToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem allBookToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem bookCategoryToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem addBookCategoryToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem bookCategoryToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem bookAuthorToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem addBookAuthorToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem allBookAuthorToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem bookRegistrationToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem systemConfigurationToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem databaseConnectionToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem fileDirectoryToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem returnToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem utilitiesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem calculatorToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem dashboardToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem attendanceToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem borrowReportToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem returnReportToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem attendanceLogReportToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem allTransactionToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem allBookListToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem attendanceMonitorToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem overDueBookReportToolStripMenuItem;
    }
}