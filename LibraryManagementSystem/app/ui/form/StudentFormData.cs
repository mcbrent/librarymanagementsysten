﻿using LibraryManagementSystem.app.db;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LibraryManagementSystem.app.ui.form
{
    public partial class StudentFormData : Form
    {
        private String previousStudentNo = "";
        private Boolean isValidate = true;
        private Boolean isEdit = false;
        public bool IsEdit { get => isEdit; set => isEdit = value; }
        public string FirstName { get => textBoxFirstName.Text.ToString(); set => textBoxFirstName.Text = value; }
        public string MiddleName { get => textBoxMiddleName.Text.ToString(); set => textBoxMiddleName.Text = value; }
        public string LastName { get => textBoxLastName.Text.ToString(); set => textBoxLastName.Text = value; }
        public DateTime Birthdate { get => dateTimePickerBirthdate.Value; set => dateTimePickerBirthdate.Value = value; }
        public string Gender {
            get
            {
                String gender = "";
                if(radioButtonMale.Checked)
                {
                    gender = "male";
                }
                if (radioButtonFemale.Checked)
                {
                    gender = "female";
                }
                return gender;
            }                
            set
            {
                if(value == "male")
                {
                    radioButtonMale.Checked = true;
                }
                if (value == "female")
                {
                    radioButtonFemale.Checked = true;
                }
            }
        }
        public string StudentNo { get => textBoxStudentNumber.Text.ToString();
            set {
                previousStudentNo = value;
                textBoxStudentNumber.Text = value;
            }
        }
        public string Address { get => textBoxAddress.Text.ToString(); set => textBoxAddress.Text = value; }
        public string Section { get => comboBoxSection.SelectedValue.ToString(); set => comboBoxSection.SelectedValue = value; }
        public string Adviser { get => comboBoxAdviser.SelectedValue.ToString(); set => comboBoxAdviser.SelectedValue = value; }
        public string FatherName { get => textBoxFatherName.Text.ToString(); set => textBoxFatherName.Text = value; }
        public string MotherName { get => textBoxMotherName.Text.ToString(); set => textBoxMotherName.Text = value; }
        public string MobileNumber { get => textBoxMobileNumber.Text.ToString(); set => textBoxMobileNumber.Text = value; }
        public string TelephoneNumber { get => textBoxTelephoneNumber.Text.ToString(); set => textBoxTelephoneNumber.Text = value; }
        public string EContactName { get => textBoxEContactName.Text.ToString(); set => textBoxEContactName.Text = value; }
        public string ERelation { get => textBoxERelation.Text.ToString(); set => textBoxERelation.Text = value; }
        public string EContactNumber { get => textBoxEContactNumber.Text.ToString(); set => textBoxEContactNumber.Text = value; }
        public string EAddress { get => textBoxEAddress.Text.ToString(); set => textBoxEAddress.Text = value; }
        public string IDPicture { get => pictureBoxIDPicture.ImageLocation;
            set {
                if(File.Exists(value))
                {
                    pictureBoxIDPicture.Load(value);
                }
                else
                {
                    //Load Default image on network
                    String profileDir = "\\";
                    DatabaseConnection dc = new DatabaseConnection();
                    Dictionary<string, string> dic = new Dictionary<string, string>();
                    dic.Add("settings_key", "FILE_PATH");
                    String query = @"SELECT * FROM tbl_settings WHERE 	settings_key = @settings_key";
                    DataTable dt = dc.Select(query, dic);
                    string targetPath = dt.Rows[0]["settings_value"].ToString() + profileDir;
                    pictureBoxIDPicture.Load(Path.Combine(targetPath, "user.png"));
                }
            }
        }

        
        public StudentFormData()
        {
            InitializeComponent();
            DatabaseConnection dc = new DatabaseConnection();
            comboBoxSection.DataSource = dc.Select("Select * FROM tbl_section");
            comboBoxSection.DisplayMember = "section_name";
            comboBoxSection.ValueMember = "section_id";

            comboBoxAdviser.DataSource = dc.Select("Select * FROM tbl_adviser");
            comboBoxAdviser.DisplayMember = "adviser_name";
            comboBoxAdviser.ValueMember = "adviser_id";
        }

        private void buttonImageLocation_Click(object sender, EventArgs e)
        {
            OpenFileDialog ofd = new OpenFileDialog();
            ofd.Filter = "Image files (*.jpg, *.jpeg, *.jpe, *.jfif, *.png) | *.jpg; *.jpeg; *.jpe; *.jfif; *.png";
            DialogResult dr = ofd.ShowDialog();
            if(dr == DialogResult.OK)
            {
                textBoxImageLocation.Text = ofd.FileName;
                pictureBoxIDPicture.Load(textBoxImageLocation.Text);
            }
        }

        private void StudentFormData_FormClosing(object sender, FormClosingEventArgs e)
        {
            if(e.CloseReason == CloseReason.UserClosing)
            {
                isValidate = false;
            }
                
            if (isValidate)
            {
                e.Cancel = !validateForm();
            }            
        }
        private Boolean validateForm()
        {
            Boolean IsValid = true;
            String errorMessage = "";
            if(textBoxStudentNumber.Text.Trim() == "")
            {
                errorMessage = "Student Number is Empty";
                IsValid = false;
            }
            if (!CheckDuplicationOfStudentNumber())
            {
                errorMessage = "Student Number cannot be duplicated";
                IsValid = false;
            }
            if (textBoxFirstName.Text.Trim() == "")
            {
                errorMessage = "First Name must not be Empty";
                IsValid = false;
            }
            if (textBoxLastName.Text.Trim() == "")
            {
                errorMessage = "Last Name must not be Empty";
                IsValid = false;
            }
            if (!IsValid)
            {
                MessageBox.Show(this, errorMessage, "Failed to Save", MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1);
            }

            return IsValid;
        }

        private Boolean CheckDuplicationOfStudentNumber()
        {
            bool isValid = false;

            if (StudentNo != previousStudentNo)
            {
                //A change in student no
                DatabaseConnection dc = new DatabaseConnection();
                Dictionary<string, string> dic = new Dictionary<string, string>();
                dic.Add("@student_id", textBoxStudentNumber.Text.Trim());
                DataTable dt = dc.Select("Select * FROM tbl_student WHERE student_id = @student_id ",dic);
                if (dt.Rows.Count > 0)
                {
                    if (isEdit)
                    {
                        isValid = false;
                    }
                    else
                    {
                        //Safe

                        isValid = true;
                    }
                }
                else
                {
                    isValid = true;
                }
               
            }
            else
            {
                isValid = true;
            }
            
            return isValid;
        }

        private void buttonCancel_Click(object sender, EventArgs e)
        {
            isValidate = false;
        }

        private void textBoxStudentNumber_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = !char.IsDigit(e.KeyChar) && !char.IsControl(e.KeyChar);
        }

        private void buttonSave_Click(object sender, EventArgs e)
        {
            isValidate = true;
        }
    }
}
