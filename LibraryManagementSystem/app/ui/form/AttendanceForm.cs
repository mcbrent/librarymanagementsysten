﻿using LibraryManagementSystem.app.db;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Net;
using System.Net.Sockets;
using System.IO;

namespace LibraryManagementSystem.app.ui.form
{
    public partial class AttendanceForm : Form
    {
        private TcpClient client;
        public StreamWriter STW;
        public string recieve;
        public String TextToSend;

        private int duration_upon_last_login = 60; //1 minutes
        public AttendanceForm()
        {
            InitializeComponent();   
        }        

        private void textBoxScan_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                if(textBoxScan.Text.Trim().Length > 0)
                {                    
                    DatabaseConnection db = new DatabaseConnection();
                    Dictionary<string, string> dic = new Dictionary<string, string>();
                    dic.Add("@student_id", textBoxScan.Text);
                    String query = @"SELECT * 
                                FROM `tbl_student`
                                INNER JOIN `tbl_section`
                                ON `student_section_id` = `section_id`
                                WHERE student_id = @student_id ";
                    DataTable dt = db.Select(query, dic);
                    if (dt.Rows.Count > 0)
                    {
                        String al_id = SetLogin(dt);
                        TextToSend = al_id;
                        backgroundWorkerProcessor.RunWorkerAsync();
                        labelFailedToRegister.Visible = false;
                        textBoxScan.Text = "";
                    }
                    else
                    {
                        groupBoxUserInformation.Visible = false;
                        //MessageBox.Show(this, "Invalid Student User Access", "Login Denied", MessageBoxButtons.OK, MessageBoxIcon.Stop, MessageBoxDefaultButton.Button1);
                        labelFailedToRegister.Visible = true;
                    }
                    
                }
                
                //_login_type = 2;
                //proceedLogin();
            }
        }
        private String SetLogin(DataTable dtStudent)
        {
            DatabaseConnection db = new DatabaseConnection();
            Int64 al_id = LogAttendance(dtStudent.Rows[0]["student_id"].ToString());
            al_id.ToString();
            groupBoxUserInformation.Visible = true;

            Dictionary<string, string> dicAttendance = new Dictionary<string, string>();
            dicAttendance.Add("@al_id", al_id.ToString());
            String queryAttendance = @"SELECT * 
                                FROM `tbl_attendance_log`
                                WHERE al_id = @al_id ";
            DataTable dtAttendance = db.Select(queryAttendance, dicAttendance);
            labelTimeIn.Text = DateTime.Parse(dtAttendance.Rows[0]["al_entry_date"].ToString()).ToString("MM/dd/yyyy hh:mm:ss tt");

            if (dtAttendance.Rows[0]["al_exit_date"].ToString() == "")
            {
                labelTimeOut.Text = "";
            }
            else
            {
                labelTimeOut.Text = DateTime.Parse(dtAttendance.Rows[0]["al_exit_date"].ToString()).ToString("MM/dd/yyyy hh:mm:ss tt");
            }

            Dictionary<string, string> dicFile = new Dictionary<string, string>();
            dicFile.Add("@fm_id", dtStudent.Rows[0]["student_fm_id"].ToString());
            String queryFile = @"SELECT * 
                                FROM `tbl_file_manager`
                                WHERE fm_id = @fm_id ";
            DataTable dtFile = db.Select(queryFile, dicFile);
            
            String queryImgDir = @"SELECT * 
                                FROM `tbl_settings`
                                WHERE settings_key = 'FILE_PATH' ";
            DataTable dtImgDir = db.Select(queryImgDir);


            labelStudentFullName.Text = dtStudent.Rows[0]["student_lastname"].ToString() 
                                        + ", " + dtStudent.Rows[0]["student_firstname"].ToString()
                                        + " " + dtStudent.Rows[0]["student_middlename"].ToString();
            labelSection.Text = dtStudent.Rows[0]["section_name"].ToString();
            pictureBoxIDPicture.Load(dtImgDir.Rows[0]["settings_value"].ToString() + "\\student\\" + dtFile.Rows[0]["fm_file_name"].ToString());

            groupBoxUserInformation.Visible = true;
            return al_id.ToString();
        }

        private void backgroundWorkerProcessor_DoWork(object sender, DoWorkEventArgs e)
        {
            if (client.Connected)
            {
                STW.WriteLine(TextToSend);
            }
            else
            {
                //MessageBox.Show("Sending Failed");
            }
        }

        private void AttendaceForm_Load(object sender, EventArgs e)
        {
            client = new TcpClient();
            IPEndPoint IpEnd = new IPEndPoint(IPAddress.Parse(Properties.Settings.Default.DB_server), int.Parse(Properties.Settings.Default.Server_port));

            try
            {
                client.Connect(IpEnd);
                if (client.Connected)
                {
                    STW = new StreamWriter(client.GetStream());
                    STW.AutoFlush = true;
                    //backgroundWorkerProcessor.WorkerSupportsCancellation = true;

                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message.ToString());
            }
        }

        private void timerClock_Tick(object sender, EventArgs e)
        {
            labelTime.Text = DateTime.Now.ToString("dddd - MM/dd/yyyy hh:mm:ss tt");
            if (duration_upon_last_login == 0)
            {
                
                groupBoxUserInformation.Visible = false;
            }
            duration_upon_last_login--;
        }
        private Int64 LogAttendance(String studentNo)
        {
            Int64 al_id = 0;
            DatabaseConnection db = new DatabaseConnection();
            Dictionary<string, string> dicAttendanceRec = new Dictionary<string, string>();
            dicAttendanceRec.Add("@al_student_id", studentNo);
            String queryAttendanceRec = @"SELECT * 
                                FROM `tbl_attendance_log`
                                WHERE al_student_id = @al_student_id AND DATE(`al_entry_date`) = CURDATE() AND al_exit_date IS NULL ";
            DataTable dtAttendanceRec = db.Select(queryAttendanceRec, dicAttendanceRec);
            if(dtAttendanceRec.Rows.Count > 0)
            {
                al_id = int.Parse(dtAttendanceRec.Rows[0]["al_id"].ToString());
                Dictionary<string, string> dicAttendance = new Dictionary<string, string>();
                dicAttendance.Add("@al_id", dtAttendanceRec.Rows[0]["al_id"].ToString());
                String queryAttendance = @"UPDATE tbl_attendance_log 
                                        SET al_exit_date =NOW()
                                        WHERE al_id = @al_id ";
                db.Update(queryAttendance, dicAttendance);
            }
            else
            {
                Dictionary<string, string> dicAttendance = new Dictionary<string, string>();
                dicAttendance.Add("@al_student_id", studentNo);
                String queryAttendance = @"INSERT INTO `tbl_attendance_log`
                                    (al_student_id,
                                    al_entry_date)
                                    VALUES
                                    (@al_student_id,
                                    NOW())";
                al_id =db.Insert(queryAttendance, dicAttendance);
            }
            return al_id;
            
        }

        private void groupBoxUserInformation_VisibleChanged(object sender, EventArgs e)
        {
            if(groupBoxUserInformation.Visible)
            {
                duration_upon_last_login = 60;
            }
        }

        private void AttendanceForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            //this.Hide();
            //e.Cancel = true;
        }

        private void textBoxScan_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = !char.IsDigit(e.KeyChar) && !char.IsControl(e.KeyChar);
        }
    }
}
