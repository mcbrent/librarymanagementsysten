﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LibraryManagementSystem.app.ui.form
{
    public partial class LibrarianFormData : Form
    {
        private Boolean isValidate = true;
        public string Librarian { get => textBoxLibrarian.Text.Trim(); set => textBoxLibrarian.Text = value; }
        public string Username { get => textBoxUsername.Text.Trim(); set => textBoxUsername.Text = value; }
        public string Password { get => textBoxPassword.Text.Trim(); set => textBoxPassword.Text = value; }
        public string Type { get => _type; set => _type = value; }

        private string _type = "add";

        public LibrarianFormData()
        {
            InitializeComponent();
            if(_type == "edit")
            {
                labelEnterPassword.Visible = true;
            }
        }

        private Boolean validateForm()
        {
            Boolean IsValid = true;
            String errorMessage = "";
            if (textBoxUsername.Text.Trim() == "")
            {
                errorMessage = "Username is Empty";
                IsValid = false;
            }
            if (textBoxLibrarian.Text.Trim() == "")
            {
                errorMessage = "Librarian is Empty";
                IsValid = false;
            }
            if (_type == "add")
            {
                if (textBoxPassword.Text.Trim() == "")
                {
                    errorMessage = "Password is Empty";
                    IsValid = false;
                }
            }
            if (!IsValid)
            {
                MessageBox.Show(this, errorMessage, "Failed to Save", MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1);
            }
            return IsValid;
        }

        private void LibrarianFormData_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (e.CloseReason == CloseReason.UserClosing)
            {
                isValidate = false;
            }

            if (isValidate)
            {
                e.Cancel = !validateForm();
            }
        }

        private void buttonSave_Click(object sender, EventArgs e)
        {
            isValidate = true;
        }

        private void buttonCancel_Click(object sender, EventArgs e)
        {
            isValidate = false;
        }
    }
}
