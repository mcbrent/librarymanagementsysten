﻿using LibraryManagementSystem.app.db;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LibraryManagementSystem.app.ui.form
{
    public partial class LoginForm : Form
    {
        private String _current_login = "0";
        /*
            0 = Admin
            1 = Librarian
            2 = Student
        */
        private int _login_type = 0;
        public string Current_login { get => _current_login; }
        public int Login_type { get => _login_type; set => _login_type = value; }
        private Boolean isLogin = false;
        public LoginForm()
        {
            InitializeComponent();
            isLogin = false;
        }
        private void proceedLogin()
        {

            DatabaseConnection db = new DatabaseConnection();
            Dictionary<string, string> dic = new Dictionary<string, string>();
            if (_login_type == 0)
            {
                dic.Add("@user_name", textBoxUsername.Text);
                dic.Add("@user_password", lib.Encryption.CreateSHA256(textBoxPassword.Text.ToString()));
                String query = "SELECT * FROM `tbl_user` WHERE user_name = @user_name AND user_password = @user_password AND user_type IN ('1', '2') ";
                DataTable dt = db.Select(query, dic);
                if (dt.Rows.Count > 0)
                {
                    _current_login = dt.Rows[0]["user_id"].ToString();

                    _login_type = Int32.Parse(dt.Rows[0]["user_type"].ToString()) - 1;

                    lib.Singleton.Current_user = _current_login;
                    isLogin = true;
                    this.Close();
                }
                else
                {
                    MessageBox.Show(this, "Invalid User Access", "Login Denied", MessageBoxButtons.OK, MessageBoxIcon.Stop, MessageBoxDefaultButton.Button1);
                }
            }

            //BookSearchForm bsf = new BookSearchForm();
            //bsf.Show();
        }

        private void buttonLogin_Click(object sender, EventArgs e)
        {
            proceedLogin();
        }

        private void buttonSettings_Click(object sender, EventArgs e)
        {
            DatabaseSettingsFormData dsfd = new DatabaseSettingsFormData();
            dsfd.ShowDialog();
        }

        private void textBoxUsername_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                proceedLogin();
            }
        }

        private void textBoxPassword_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                proceedLogin();
            }
        }

        private void LoginForm_FormClosed(object sender, FormClosedEventArgs e)
        {
            if(!isLogin)
            {
                Application.Exit();
            }            
        }

        private void buttonBookSearch_Click(object sender, EventArgs e)
        {
            isLogin = true;
            this.Close();
            BookSearchForm bsf = new BookSearchForm();
            bsf.Show();
        }
    }
}
