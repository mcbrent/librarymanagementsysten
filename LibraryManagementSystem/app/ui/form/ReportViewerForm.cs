﻿using LibraryManagementSystem.app.db;
using Microsoft.Reporting.WinForms;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Script.Serialization;
using System.Windows.Forms;

namespace LibraryManagementSystem.app.ui.form
{
    public partial class ReportViewerForm : Form
    {
        private DataTable _reportDataTable;
        public DataTable ReportDataTable
        {
            set
            {
                _reportDataTable = value;
            }
        }

        private DataTable _reportDataTable2;
        public DataTable ReportDataTable2
        {
            set
            {
                _reportDataTable2 = value;
            }
        }

        private Dictionary<string, string> _dic_reportParameters;
        public Dictionary<string, string> DicReportParameters
        {
            set
            {
                _dic_reportParameters = value;
            }
        }

        private String _reportFile = "";
        public string ReportFile
        {
            get
            {
                return _reportFile;
            }

            set
            {
                _reportFile = value;
            }
        }
        public ReportViewerForm()
        {
            InitializeComponent();            
        }
        private void setReport(String reportPath, String DefaultFileName, IDictionary<string, string> parameters, DataTable dt, DataTable dt2 = null)
        {
            reportViewerMain.LocalReport.ReportEmbeddedResource = reportPath;
            reportViewerMain.LocalReport.DisplayName = DefaultFileName;
            reportViewerMain.LocalReport.EnableExternalImages = true;
            ReportParameter[] rParams = new ReportParameter[parameters.Count];
            int i = 0;
            foreach (KeyValuePair<string, string> entry in parameters)
            {
                rParams[i++] = new ReportParameter(entry.Key, entry.Value);
            }

            try
            {
                reportViewerMain.LocalReport.SetParameters(rParams);
            }
            catch (Exception ex)
            {
                //MessageBox.Show(ex.Message);
            }

            ReportDataSource rpts = new ReportDataSource("DataSetDummy", dt);
            reportViewerMain.LocalReport.DataSources.Clear();
            reportViewerMain.LocalReport.DataSources.Add(rpts);

            if (dt2 != null)
            {
                reportViewerMain.LocalReport.DataSources.Add(new ReportDataSource("DataSetDummy2", dt2));
            }

            //Set automatic to print layout
            reportViewerMain.SetDisplayMode(DisplayMode.PrintLayout);

            reportViewerMain.RefreshReport();
        }

        private void ReportViewerForm_Load(object sender, EventArgs e)
        {
            setReport(_reportFile, "List", _dic_reportParameters, _reportDataTable, _reportDataTable2);
        }
    }
}
