﻿using LibraryManagementSystem.app.db;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LibraryManagementSystem.app.ui.form
{
    public partial class BookCategoryFormData : Form
    {
        public string CategoryName { get => textBoxCategory.Text.ToString(); set => textBoxCategory.Text = value; }
        public BookCategoryFormData()
        {
            InitializeComponent();
        }

        
    }
}
