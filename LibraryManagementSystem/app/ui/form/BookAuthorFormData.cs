﻿using LibraryManagementSystem.app.db;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LibraryManagementSystem.app.ui.form
{
    public partial class BookAuthorFormData : Form
    {
        public string AuthorName { get => textBoxAuthor.Text.ToString(); set => textBoxAuthor.Text = value; }
        public BookAuthorFormData()
        {
            InitializeComponent();
        }

        
    }
}
