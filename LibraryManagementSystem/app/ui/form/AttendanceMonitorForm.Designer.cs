﻿namespace LibraryManagementSystem.app.ui.form
{
    partial class AttendanceMonitorForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(AttendanceMonitorForm));
            this.panel1 = new System.Windows.Forms.Panel();
            this.groupBoxUserInformation = new System.Windows.Forms.GroupBox();
            this.labelTimeOut = new System.Windows.Forms.Label();
            this.labelTimeIn = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.labelSection = new System.Windows.Forms.Label();
            this.labelStudentFullName = new System.Windows.Forms.Label();
            this.groupBoxMonitorLogin = new System.Windows.Forms.GroupBox();
            this.labelNotification = new System.Windows.Forms.Label();
            this.panelMonitorLogin = new System.Windows.Forms.Panel();
            this.dataGridViewAttendanceLog = new System.Windows.Forms.DataGridView();
            this.backgroundWorkerServer = new System.ComponentModel.BackgroundWorker();
            this.buttonReloadLast = new System.Windows.Forms.Button();
            this.pictureBoxIDPicture = new System.Windows.Forms.PictureBox();
            this.panel1.SuspendLayout();
            this.groupBoxUserInformation.SuspendLayout();
            this.groupBoxMonitorLogin.SuspendLayout();
            this.panelMonitorLogin.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewAttendanceLog)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxIDPicture)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.AutoScroll = true;
            this.panel1.BackColor = System.Drawing.Color.Navy;
            this.panel1.Controls.Add(this.buttonReloadLast);
            this.panel1.Controls.Add(this.groupBoxUserInformation);
            this.panel1.Controls.Add(this.groupBoxMonitorLogin);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.ForeColor = System.Drawing.Color.White;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(926, 579);
            this.panel1.TabIndex = 5;
            // 
            // groupBoxUserInformation
            // 
            this.groupBoxUserInformation.Controls.Add(this.labelTimeOut);
            this.groupBoxUserInformation.Controls.Add(this.labelTimeIn);
            this.groupBoxUserInformation.Controls.Add(this.label2);
            this.groupBoxUserInformation.Controls.Add(this.label1);
            this.groupBoxUserInformation.Controls.Add(this.pictureBoxIDPicture);
            this.groupBoxUserInformation.Controls.Add(this.labelSection);
            this.groupBoxUserInformation.Controls.Add(this.labelStudentFullName);
            this.groupBoxUserInformation.ForeColor = System.Drawing.Color.White;
            this.groupBoxUserInformation.Location = new System.Drawing.Point(12, 12);
            this.groupBoxUserInformation.Name = "groupBoxUserInformation";
            this.groupBoxUserInformation.Size = new System.Drawing.Size(902, 171);
            this.groupBoxUserInformation.TabIndex = 4;
            this.groupBoxUserInformation.TabStop = false;
            this.groupBoxUserInformation.Text = "User Information";
            this.groupBoxUserInformation.Visible = false;
            // 
            // labelTimeOut
            // 
            this.labelTimeOut.AutoSize = true;
            this.labelTimeOut.Location = new System.Drawing.Point(112, 126);
            this.labelTimeOut.Name = "labelTimeOut";
            this.labelTimeOut.Size = new System.Drawing.Size(0, 23);
            this.labelTimeOut.TabIndex = 7;
            // 
            // labelTimeIn
            // 
            this.labelTimeIn.AutoSize = true;
            this.labelTimeIn.Location = new System.Drawing.Point(112, 101);
            this.labelTimeIn.Name = "labelTimeIn";
            this.labelTimeIn.Size = new System.Drawing.Size(0, 23);
            this.labelTimeIn.TabIndex = 6;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(16, 126);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(90, 23);
            this.label2.TabIndex = 5;
            this.label2.Text = "Time Out: ";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(30, 101);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(76, 23);
            this.label1.TabIndex = 4;
            this.label1.Text = "Time In: ";
            // 
            // labelSection
            // 
            this.labelSection.AutoSize = true;
            this.labelSection.Location = new System.Drawing.Point(16, 54);
            this.labelSection.Name = "labelSection";
            this.labelSection.Size = new System.Drawing.Size(66, 23);
            this.labelSection.TabIndex = 2;
            this.labelSection.Text = "Section";
            // 
            // labelStudentFullName
            // 
            this.labelStudentFullName.AutoSize = true;
            this.labelStudentFullName.Location = new System.Drawing.Point(16, 31);
            this.labelStudentFullName.Name = "labelStudentFullName";
            this.labelStudentFullName.Size = new System.Drawing.Size(151, 23);
            this.labelStudentFullName.TabIndex = 1;
            this.labelStudentFullName.Text = "Student Full Name";
            // 
            // groupBoxMonitorLogin
            // 
            this.groupBoxMonitorLogin.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBoxMonitorLogin.Controls.Add(this.labelNotification);
            this.groupBoxMonitorLogin.Controls.Add(this.panelMonitorLogin);
            this.groupBoxMonitorLogin.ForeColor = System.Drawing.Color.White;
            this.groupBoxMonitorLogin.Location = new System.Drawing.Point(12, 222);
            this.groupBoxMonitorLogin.Name = "groupBoxMonitorLogin";
            this.groupBoxMonitorLogin.Size = new System.Drawing.Size(902, 347);
            this.groupBoxMonitorLogin.TabIndex = 3;
            this.groupBoxMonitorLogin.TabStop = false;
            this.groupBoxMonitorLogin.Text = "Monitor Login";
            // 
            // labelNotification
            // 
            this.labelNotification.AutoSize = true;
            this.labelNotification.Location = new System.Drawing.Point(600, -9);
            this.labelNotification.Name = "labelNotification";
            this.labelNotification.Size = new System.Drawing.Size(44, 23);
            this.labelNotification.TabIndex = 5;
            this.labelNotification.Text = "false";
            this.labelNotification.Visible = false;
            this.labelNotification.TextChanged += new System.EventHandler(this.labelNotification_TextChanged);
            // 
            // panelMonitorLogin
            // 
            this.panelMonitorLogin.AutoScroll = true;
            this.panelMonitorLogin.Controls.Add(this.dataGridViewAttendanceLog);
            this.panelMonitorLogin.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelMonitorLogin.ForeColor = System.Drawing.Color.Black;
            this.panelMonitorLogin.Location = new System.Drawing.Point(3, 26);
            this.panelMonitorLogin.Name = "panelMonitorLogin";
            this.panelMonitorLogin.Size = new System.Drawing.Size(896, 318);
            this.panelMonitorLogin.TabIndex = 0;
            // 
            // dataGridViewAttendanceLog
            // 
            this.dataGridViewAttendanceLog.AllowUserToAddRows = false;
            this.dataGridViewAttendanceLog.AllowUserToDeleteRows = false;
            this.dataGridViewAttendanceLog.AllowUserToResizeColumns = false;
            this.dataGridViewAttendanceLog.AllowUserToResizeRows = false;
            this.dataGridViewAttendanceLog.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dataGridViewAttendanceLog.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.dataGridViewAttendanceLog.BackgroundColor = System.Drawing.Color.Navy;
            this.dataGridViewAttendanceLog.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.dataGridViewAttendanceLog.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewAttendanceLog.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridViewAttendanceLog.Location = new System.Drawing.Point(0, 0);
            this.dataGridViewAttendanceLog.MultiSelect = false;
            this.dataGridViewAttendanceLog.Name = "dataGridViewAttendanceLog";
            this.dataGridViewAttendanceLog.ReadOnly = true;
            this.dataGridViewAttendanceLog.RowHeadersVisible = false;
            this.dataGridViewAttendanceLog.RowTemplate.Height = 24;
            this.dataGridViewAttendanceLog.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridViewAttendanceLog.Size = new System.Drawing.Size(896, 318);
            this.dataGridViewAttendanceLog.TabIndex = 4;
            // 
            // backgroundWorkerServer
            // 
            this.backgroundWorkerServer.WorkerSupportsCancellation = true;
            this.backgroundWorkerServer.DoWork += new System.ComponentModel.DoWorkEventHandler(this.backgroundWorkerServer_DoWork);
            // 
            // buttonReloadLast
            // 
            this.buttonReloadLast.BackColor = System.Drawing.Color.RoyalBlue;
            this.buttonReloadLast.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonReloadLast.Image = global::LibraryManagementSystem.Properties.Resources.Synchronize_white_26px;
            this.buttonReloadLast.Location = new System.Drawing.Point(799, 191);
            this.buttonReloadLast.Name = "buttonReloadLast";
            this.buttonReloadLast.Size = new System.Drawing.Size(115, 38);
            this.buttonReloadLast.TabIndex = 5;
            this.buttonReloadLast.Text = "Retrieve";
            this.buttonReloadLast.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.buttonReloadLast.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.buttonReloadLast.UseVisualStyleBackColor = false;
            this.buttonReloadLast.Click += new System.EventHandler(this.buttonReloadLast_Click);
            // 
            // pictureBoxIDPicture
            // 
            this.pictureBoxIDPicture.BackColor = System.Drawing.Color.White;
            this.pictureBoxIDPicture.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.pictureBoxIDPicture.Location = new System.Drawing.Point(757, 29);
            this.pictureBoxIDPicture.Name = "pictureBoxIDPicture";
            this.pictureBoxIDPicture.Size = new System.Drawing.Size(130, 130);
            this.pictureBoxIDPicture.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBoxIDPicture.TabIndex = 3;
            this.pictureBoxIDPicture.TabStop = false;
            // 
            // AttendanceMonitorForm
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(926, 579);
            this.Controls.Add(this.panel1);
            this.Font = new System.Drawing.Font("Segoe UI", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "AttendanceMonitorForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Attendance Monitoring";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.AttendanceMonitorForm_FormClosing);
            this.Load += new System.EventHandler(this.AttendanceMonitorForm_Load);
            this.panel1.ResumeLayout(false);
            this.groupBoxUserInformation.ResumeLayout(false);
            this.groupBoxUserInformation.PerformLayout();
            this.groupBoxMonitorLogin.ResumeLayout(false);
            this.groupBoxMonitorLogin.PerformLayout();
            this.panelMonitorLogin.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewAttendanceLog)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxIDPicture)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.GroupBox groupBoxMonitorLogin;
        private System.Windows.Forms.Panel panelMonitorLogin;
        private System.Windows.Forms.DataGridView dataGridViewAttendanceLog;
        private System.Windows.Forms.GroupBox groupBoxUserInformation;
        private System.Windows.Forms.Label labelTimeOut;
        private System.Windows.Forms.Label labelTimeIn;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.PictureBox pictureBoxIDPicture;
        private System.Windows.Forms.Label labelSection;
        private System.Windows.Forms.Label labelStudentFullName;
        private System.Windows.Forms.Label labelNotification;
        private System.ComponentModel.BackgroundWorker backgroundWorkerServer;
        private System.Windows.Forms.Button buttonReloadLast;
    }
}