﻿using LibraryManagementSystem.app.db;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LibraryManagementSystem.app.ui.form
{
    public partial class DatabaseSettingsFormData : Form
    {
        public string DB_server { get => textBoxServer.Text.ToString(); set => textBoxServer.Text = value; }
        public string DB_name { get => textBoxDatabaseName.Text.ToString(); set => textBoxDatabaseName.Text = value; }
        public string DB_user { get => textBoxUserId.Text.ToString(); set => textBoxUserId.Text = value; }
        public string DB_password { get => textBoxPassword.Text.ToString(); set => textBoxPassword.Text = value; }
        public string Socket_port { get => textBoxSocketPort.Text.ToString(); set => textBoxSocketPort.Text = value; }
        public DatabaseSettingsFormData()
        {
            InitializeComponent();
            DB_server = Properties.Settings.Default.DB_server;
            DB_name = Properties.Settings.Default.DB_name;
            DB_user = Properties.Settings.Default.DB_user;
            DB_password = Properties.Settings.Default.DB_pass;
            Socket_port = Properties.Settings.Default.Server_port;
        }

        private void buttonSave_Click(object sender, EventArgs e)
        {
            DatabaseConnection dc = new DatabaseConnection(false);
            if(dc.TestConnection(DB_server, DB_name, DB_user, DB_password))
            {
                Properties.Settings.Default.DB_server = DB_server;
                Properties.Settings.Default.DB_name = DB_name;
                Properties.Settings.Default.DB_user = DB_user;
                Properties.Settings.Default.DB_pass = DB_password;
                Properties.Settings.Default.Server_port = Socket_port;
                Properties.Settings.Default.Save();
                this.Close();
            }
            else
            {
                MessageBox.Show(this, "Database Connection Failed", "Failure to Connect to Database", MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1);
            }

            
        }
    }
}
