﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using LibraryManagementSystem.app.db;
using LibraryManagementSystem.app.lib;

namespace LibraryManagementSystem.app.ui.form
{
    public partial class BookSearchForm : Form
    {
        private String preventShiftingUC = "";
        public string PreventShiftingUC { get => preventShiftingUC; set => preventShiftingUC = value; }

        private Boolean _overpassFormClose = false;
        public bool OverpassFormClose { get => _overpassFormClose; set => _overpassFormClose = value; }
        

        private UserControl _current = null;

        

        public BookSearchForm()
        {
            InitializeComponent();
        }


        public void setContent(UserControl userControl)
        {
            if (preventShiftingUC == "")
            {
                panelMainArea.Controls.Clear();
                _current = userControl;
                _current.Dock = DockStyle.Fill;
                panelMainArea.Controls.Add(userControl);
            }
            else
            {
                MessageBox.Show(this, preventShiftingUC, "Library Management System", MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1);
            }
            
        }
       

        private void AdminForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            DialogResult dr = MessageBox.Show(this, "Are you sure you like to exit the application", "Library Management System", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if (dr == DialogResult.Yes)
            {
                e.Cancel = false;
            }
            else
            {
                e.Cancel = true;
            }
        }

        private void AdminForm_FormClosed(object sender, FormClosedEventArgs e)
        {
            Application.Exit();
        }

        private void AdminForm_Load(object sender, EventArgs e)
        {
            setContent(new uc.BookListUserControl());
        }        
        
    }
}