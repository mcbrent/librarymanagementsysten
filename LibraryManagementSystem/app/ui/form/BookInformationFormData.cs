﻿using LibraryManagementSystem.app.db;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Script.Serialization;
using System.Windows.Forms;

namespace LibraryManagementSystem.app.ui.form
{
    public partial class BookInformationFormData : Form
    {
        private Boolean isValidate = true;
        private string _scannedAuthor = "";
        public String ScannedAuthor { get => _scannedAuthor; }

        public string BookISBN { get => textBoxISBN.Text.ToString(); set => textBoxISBN.Text = value; }
        public string BookTitle { get => textBoxTitle.Text.ToString(); set => textBoxTitle.Text = value; }
        public string BookAuthor { get => comboBoxAuthor.SelectedValue.ToString(); set => comboBoxAuthor.SelectedValue = value; }
        public string BookCategory { get => comboBoxCategory.SelectedValue.ToString(); set => comboBoxCategory.SelectedValue = value; }
        private DatabaseConnection dc;
        public BookInformationFormData()
        {
            InitializeComponent();
            dc = new DatabaseConnection();
            DataTable dtAuthor = dc.Select("Select * FROM tbl_author");
            DataRow drDefault = dtAuthor.NewRow();
            drDefault["author_id"] = -1;
            drDefault["author_name"] = "--Select Author--";
            dtAuthor.Rows.InsertAt(drDefault, 0);
            comboBoxAuthor.DataSource = dtAuthor;
            comboBoxAuthor.DisplayMember = "author_name";
            comboBoxAuthor.ValueMember = "author_id";

            DataTable dtCategory = dc.Select("Select * FROM tbl_category");
            comboBoxCategory.DataSource = dtCategory;
            comboBoxCategory.DisplayMember = "category_name";
            comboBoxCategory.ValueMember = "category_id";
        }

        private void processSearchedISBN()
        {
            String filter = textBoxISBN.Text.Trim();
            lib.BookLookUp bl = new lib.BookLookUp();
            String data = bl.search(filter, "isbn");
            DataTable dt = null;

            dt = this.convertFromJSON(data);
            if (dt.Rows.Count > 0)
            {
                textBoxTitle.Text = dt.Rows[0]["Title"].ToString();
                labelBookTitle.Text = dt.Rows[0]["Title"].ToString();
                labelBookAuthor.Text = dt.Rows[0]["Author"].ToString();
                _scannedAuthor = dt.Rows[0]["Author"].ToString();                

                Dictionary<string, string> dic = new Dictionary<string, string>();
                dic.Add("@author_name", '%' + _scannedAuthor + '%');
                DataTable dtAuthor = dc.Select("Select * FROM tbl_author WHERE author_name LIKE @author_name", dic);
                if(dtAuthor.Rows.Count > 0)
                {                    
                    comboBoxAuthor.SelectedValue = dtAuthor.Rows[0]["author_id"].ToString();
                }
                else
                {
                    comboBoxAuthor.SelectedValue = -1;
                }

                labelPublishedYear.Text = dt.Rows[0]["Publish Year"].ToString();
            }
        }

        private void textBoxISBN_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter && checkBoxUseScanner.Checked)
            {
                //enter key is down
                if (lib.BookLookUp.CheckForInternetConnection())
                {
                    processSearchedISBN();
                }
            }
        }
        private DataTable convertFromJSON(String jsonResult)
        {
            JavaScriptSerializer serializer = new JavaScriptSerializer();


            DataTable dt = new DataTable();

            var formattedData = serializer.Deserialize<Dictionary<string, object>>(jsonResult);
            dt.Columns.Add("Title");
            dt.Columns.Add("Author");
            dt.Columns.Add("ISBN");
            dt.Columns.Add("Publish Year");

            for (int i = 0; i < formattedData.Count; i++)
            {
                DataRow dr = dt.NewRow();
                //formattedData[i + ""];
                Dictionary<string, object> row = ((Dictionary<string, object>)formattedData.ElementAt(i).Value);
                Dictionary<string, object> layer2 = ((Dictionary<string, object>)row["details"]);
                Console.WriteLine(layer2);
                //Console.WriteLine((Dictionary<string, object>)layer1.ElementAt(4).Value);
                //var layer1 = serializer.Deserialize<Dictionary<string, object>>(formattedData.ElementAt(i).Value.ToString());
                dr["Title"] = layer2["title"];
                dr["Author"] = (layer2.ContainsKey("by_statement")) ? layer2["by_statement"] : "";
                dr["ISBN"] = row["bib_key"];
                dr["Publish Year"] = layer2["publish_date"];
                dt.Rows.Add(dr);
            }
            


            return dt;

        }

        private void checkBoxUseScanner_CheckedChanged(object sender, EventArgs e)
        {
            textBoxISBN.Focus();
            //Select most like then set selected
        }

        private void linkLabelAddAuthor_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            BookAuthorFormData add = new BookAuthorFormData();
            add.AuthorName = _scannedAuthor;
            if (DialogResult.OK == add.ShowDialog())
            {
                _scannedAuthor = add.AuthorName;

                DataTable dtAuthor = (DataTable)comboBoxAuthor.DataSource;
                DataRow drDefault = dtAuthor.NewRow();
                drDefault["author_id"] = 0;
                drDefault["author_name"] = "--Newly Added--";
                dtAuthor.Rows.InsertAt(drDefault, dtAuthor.Rows.Count);                
                comboBoxAuthor.DataSource = dtAuthor;
                comboBoxAuthor.DisplayMember = "author_name";
                comboBoxAuthor.ValueMember = "author_id";
                comboBoxAuthor.SelectedValue = 0;

            }
        }

        private void BookInformationFormData_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (e.CloseReason == CloseReason.UserClosing)
            {
                isValidate = false;
            } 
            if (isValidate)
            {
                e.Cancel = !validateForm();
            }
        }

        private Boolean validateForm()
        {
            Boolean IsValid = true;
            String errorMessage = "";
            if (comboBoxAuthor.SelectedValue.ToString() == "-1")
            {
                errorMessage = "Author is Empty";
                IsValid = false;
            }
            if (textBoxTitle.Text.Trim() == "")
            {
                errorMessage = "Book Title is Empty";
                IsValid = false;
            }
            if (textBoxTitle.Text.Trim() == "")
            {
                errorMessage = "Book Title is Empty";
                IsValid = false;
            }
            if (!IsValid)
            {
                MessageBox.Show(this, errorMessage, "Failed to Save", MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1);
            }
            return IsValid;
        }

        private void buttonSave_Click(object sender, EventArgs e)
        {
            isValidate = true;
        }

        private void buttonCancel_Click(object sender, EventArgs e)
        {
            isValidate = false;
        }
    }
}
