﻿namespace LibraryManagementSystem.app.ui.form
{
    partial class AttendanceForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(AttendanceForm));
            this.panel1 = new System.Windows.Forms.Panel();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.labelFailedToRegister = new System.Windows.Forms.Label();
            this.labelTime = new System.Windows.Forms.Label();
            this.labelScanID = new System.Windows.Forms.Label();
            this.groupBoxUserInformation = new System.Windows.Forms.GroupBox();
            this.labelTimeOut = new System.Windows.Forms.Label();
            this.labelTimeIn = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.pictureBoxIDPicture = new System.Windows.Forms.PictureBox();
            this.labelSection = new System.Windows.Forms.Label();
            this.labelStudentFullName = new System.Windows.Forms.Label();
            this.textBoxScan = new System.Windows.Forms.TextBox();
            this.backgroundWorkerProcessor = new System.ComponentModel.BackgroundWorker();
            this.timerClock = new System.Windows.Forms.Timer(this.components);
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.groupBoxUserInformation.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxIDPicture)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.AutoScroll = true;
            this.panel1.BackColor = System.Drawing.Color.White;
            this.panel1.Controls.Add(this.pictureBox1);
            this.panel1.Controls.Add(this.labelFailedToRegister);
            this.panel1.Controls.Add(this.labelTime);
            this.panel1.Controls.Add(this.labelScanID);
            this.panel1.Controls.Add(this.groupBoxUserInformation);
            this.panel1.Controls.Add(this.textBoxScan);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.ForeColor = System.Drawing.Color.Black;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1088, 726);
            this.panel1.TabIndex = 5;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.pictureBox1.Image = global::LibraryManagementSystem.Properties.Resources.logo;
            this.pictureBox1.Location = new System.Drawing.Point(11, 149);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(1085, 145);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox1.TabIndex = 5;
            this.pictureBox1.TabStop = false;
            // 
            // labelFailedToRegister
            // 
            this.labelFailedToRegister.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.labelFailedToRegister.ForeColor = System.Drawing.Color.Red;
            this.labelFailedToRegister.Location = new System.Drawing.Point(171, 638);
            this.labelFailedToRegister.Name = "labelFailedToRegister";
            this.labelFailedToRegister.Size = new System.Drawing.Size(714, 23);
            this.labelFailedToRegister.TabIndex = 4;
            this.labelFailedToRegister.Text = "Failed!";
            this.labelFailedToRegister.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.labelFailedToRegister.Visible = false;
            // 
            // labelTime
            // 
            this.labelTime.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.labelTime.Location = new System.Drawing.Point(11, 23);
            this.labelTime.Name = "labelTime";
            this.labelTime.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.labelTime.Size = new System.Drawing.Size(1084, 25);
            this.labelTime.TabIndex = 3;
            this.labelTime.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // labelScanID
            // 
            this.labelScanID.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.labelScanID.Location = new System.Drawing.Point(168, 579);
            this.labelScanID.Name = "labelScanID";
            this.labelScanID.Size = new System.Drawing.Size(714, 23);
            this.labelScanID.TabIndex = 2;
            this.labelScanID.Text = " Scan ID Here";
            this.labelScanID.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // groupBoxUserInformation
            // 
            this.groupBoxUserInformation.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBoxUserInformation.Controls.Add(this.labelTimeOut);
            this.groupBoxUserInformation.Controls.Add(this.labelTimeIn);
            this.groupBoxUserInformation.Controls.Add(this.label2);
            this.groupBoxUserInformation.Controls.Add(this.label1);
            this.groupBoxUserInformation.Controls.Add(this.pictureBoxIDPicture);
            this.groupBoxUserInformation.Controls.Add(this.labelSection);
            this.groupBoxUserInformation.Controls.Add(this.labelStudentFullName);
            this.groupBoxUserInformation.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBoxUserInformation.Location = new System.Drawing.Point(16, 309);
            this.groupBoxUserInformation.Name = "groupBoxUserInformation";
            this.groupBoxUserInformation.Size = new System.Drawing.Size(1056, 264);
            this.groupBoxUserInformation.TabIndex = 1;
            this.groupBoxUserInformation.TabStop = false;
            this.groupBoxUserInformation.Text = "User Information";
            this.groupBoxUserInformation.Visible = false;
            this.groupBoxUserInformation.VisibleChanged += new System.EventHandler(this.groupBoxUserInformation_VisibleChanged);
            // 
            // labelTimeOut
            // 
            this.labelTimeOut.AutoSize = true;
            this.labelTimeOut.Font = new System.Drawing.Font("Segoe UI", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelTimeOut.Location = new System.Drawing.Point(180, 188);
            this.labelTimeOut.Name = "labelTimeOut";
            this.labelTimeOut.Size = new System.Drawing.Size(0, 41);
            this.labelTimeOut.TabIndex = 7;
            // 
            // labelTimeIn
            // 
            this.labelTimeIn.AutoSize = true;
            this.labelTimeIn.Font = new System.Drawing.Font("Segoe UI", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelTimeIn.Location = new System.Drawing.Point(180, 139);
            this.labelTimeIn.Name = "labelTimeIn";
            this.labelTimeIn.Size = new System.Drawing.Size(0, 41);
            this.labelTimeIn.TabIndex = 6;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Segoe UI", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(16, 188);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(156, 41);
            this.label2.TabIndex = 5;
            this.label2.Text = "Time Out: ";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Segoe UI", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(30, 139);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(131, 41);
            this.label1.TabIndex = 4;
            this.label1.Text = "Time In: ";
            // 
            // pictureBoxIDPicture
            // 
            this.pictureBoxIDPicture.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.pictureBoxIDPicture.BackColor = System.Drawing.Color.White;
            this.pictureBoxIDPicture.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.pictureBoxIDPicture.Location = new System.Drawing.Point(758, 27);
            this.pictureBoxIDPicture.Name = "pictureBoxIDPicture";
            this.pictureBoxIDPicture.Size = new System.Drawing.Size(210, 210);
            this.pictureBoxIDPicture.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBoxIDPicture.TabIndex = 3;
            this.pictureBoxIDPicture.TabStop = false;
            // 
            // labelSection
            // 
            this.labelSection.AutoSize = true;
            this.labelSection.Font = new System.Drawing.Font("Segoe UI", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelSection.Location = new System.Drawing.Point(16, 74);
            this.labelSection.Name = "labelSection";
            this.labelSection.Size = new System.Drawing.Size(116, 41);
            this.labelSection.TabIndex = 2;
            this.labelSection.Text = "Section";
            // 
            // labelStudentFullName
            // 
            this.labelStudentFullName.AutoSize = true;
            this.labelStudentFullName.Font = new System.Drawing.Font("Segoe UI", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelStudentFullName.Location = new System.Drawing.Point(16, 31);
            this.labelStudentFullName.Name = "labelStudentFullName";
            this.labelStudentFullName.Size = new System.Drawing.Size(262, 41);
            this.labelStudentFullName.TabIndex = 1;
            this.labelStudentFullName.Text = "Student Full Name";
            // 
            // textBoxScan
            // 
            this.textBoxScan.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textBoxScan.Location = new System.Drawing.Point(171, 605);
            this.textBoxScan.Name = "textBoxScan";
            this.textBoxScan.Size = new System.Drawing.Size(714, 30);
            this.textBoxScan.TabIndex = 0;
            this.textBoxScan.KeyDown += new System.Windows.Forms.KeyEventHandler(this.textBoxScan_KeyDown);
            this.textBoxScan.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textBoxScan_KeyPress);
            // 
            // backgroundWorkerProcessor
            // 
            this.backgroundWorkerProcessor.DoWork += new System.ComponentModel.DoWorkEventHandler(this.backgroundWorkerProcessor_DoWork);
            // 
            // timerClock
            // 
            this.timerClock.Enabled = true;
            this.timerClock.Interval = 1000;
            this.timerClock.Tick += new System.EventHandler(this.timerClock_Tick);
            // 
            // AttendanceForm
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(1088, 726);
            this.Controls.Add(this.panel1);
            this.Font = new System.Drawing.Font("Segoe UI", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimumSize = new System.Drawing.Size(1106, 773);
            this.Name = "AttendanceForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Student Attendance Log";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.AttendanceForm_FormClosing);
            this.Load += new System.EventHandler(this.AttendaceForm_Load);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.groupBoxUserInformation.ResumeLayout(false);
            this.groupBoxUserInformation.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxIDPicture)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.TextBox textBoxScan;
        private System.Windows.Forms.GroupBox groupBoxUserInformation;
        private System.Windows.Forms.Label labelScanID;
        private System.ComponentModel.BackgroundWorker backgroundWorkerProcessor;
        private System.Windows.Forms.Label labelTime;
        private System.Windows.Forms.Timer timerClock;
        private System.Windows.Forms.Label labelStudentFullName;
        private System.Windows.Forms.Label labelSection;
        private System.Windows.Forms.PictureBox pictureBoxIDPicture;
        private System.Windows.Forms.Label labelTimeOut;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label labelFailedToRegister;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label labelTimeIn;
        private System.Windows.Forms.Label label1;
    }
}