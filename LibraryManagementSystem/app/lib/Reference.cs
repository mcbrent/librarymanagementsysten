﻿using LibraryManagementSystem.app.db;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LibraryManagementSystem.app.lib
{
    class Reference
    {
        public String getTransactionSeries(String type, Boolean isConsume = false)
        {
            String TransActionID = "";
            int current = 1;
            //T-2018-10-15-00000
            String seriesMask = "T-";
            DatabaseConnection dc = new DatabaseConnection();
            Dictionary<string, string> dicSeries = new Dictionary<string, string>();
            dicSeries.Add("@series_type", type);
            String querySeries = @"SELECT *  
                                FROM tbl_series 
                                WHERE series_date = CURDATE() AND series_type = @series_type ";
            DataTable dtSeries = dc.Select(querySeries, dicSeries);

            if(dtSeries.Rows.Count > 0)
            {
                //There is already a series
                current = Int32.Parse(dtSeries.Rows[0]["series_current"].ToString()) + 1;
                TransActionID = maskSeries(current.ToString());
            }
            else
            {
                String insertQuerySeries = @"INSERT INTO `tbl_series`
                                            (`series_type`, `series_current`, `series_date`) 
                                            VALUES 
                                            ('" + type + "',0,CURDATE()) ";
                dc.Insert(insertQuerySeries, new Dictionary<string, string>());                
                TransActionID = maskSeries(current.ToString());
            }

            if(isConsume)
            {
                Dictionary<string, string> dicNewSeries = new Dictionary<string, string>();
                dicNewSeries.Add("@series_type", type);
                dicNewSeries.Add("@series_current", current.ToString());

                String updateQuerySeries = @"UPDATE `tbl_series` SET `series_current` = @series_current
                                            WHERE series_type = @series_type AND series_date = CURDATE()";
                dc.Update(updateQuerySeries, dicNewSeries);
            }
            DataTable dtCurrent = dc.Select("SELECT CURDATE() as 'current_date'");
            DateTime dateToday = DateTime.Parse(dtCurrent.Rows[0]["current_date"].ToString());

            return "T-" + dateToday.ToString("yyyyMMdd") + "-" + TransActionID;
           

        }



        private String maskSeries(String value)
        {
            while(value.Length < 5)
            {
                value = "0" + value;
            }
            return value;

        }
      
    }
}
