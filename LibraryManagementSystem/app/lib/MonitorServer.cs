﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LibraryManagementSystem.app.lib
{
    class MonitorServer
    {
        private static TcpListener listener;

        public static TcpListener Listener { get => listener; }

        public void initializeServer()
        {
            if(listener == null)
            {
                if (Properties.Settings.Default.DB_server.ToString() == GetLocalIPAddress())
                {
                    try
                    {
                        listener = new TcpListener(IPAddress.Any, int.Parse(Properties.Settings.Default.Server_port));
                        listener.Start();
                    }
                    catch(Exception ex)
                    {
                        listener = null;
                        MessageBox.Show(ex.Message, "Error On Server Port", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1);
                    }
                    
                    
                    //Running On Server

                }
            }
            
        }

        private string GetLocalIPAddress()
        {
            var host = Dns.GetHostEntry(Dns.GetHostName());
            foreach (var ip in host.AddressList)
            {
                if (ip.AddressFamily == AddressFamily.InterNetwork)
                {
                    return ip.ToString();
                }
            }
            throw new Exception("No network adapters with an IPv4 address in the system!");
        }
    }
}
