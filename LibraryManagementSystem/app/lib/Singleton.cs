﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LibraryManagementSystem.app.lib
{
    static class Singleton
    {
        private static String _current_user = "0";

        public static String Current_user { get => _current_user; set => _current_user = value; }
    }
}
