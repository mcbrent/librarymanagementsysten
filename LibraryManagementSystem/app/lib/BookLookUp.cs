﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace LibraryManagementSystem.app.lib
{
    class BookLookUp
    {

        public static bool CheckForInternetConnection()
        {
            try
            {
                using (var client = new WebClient())
                using (client.OpenRead("http://clients3.google.com/generate_204"))
                {
                    return true;
                }
            }
            catch
            {
                return false;
            }
        }

        private String primaryUrl = "http://openlibrary.org/";
        public String search(string filter, string type)
        {
            String curl = primaryUrl + "";
            switch (type)
            {
                case "isbn":
                    curl += "api/books?bibkeys=ISBN:" + filter + "&jscmd=details&format=json";
                break;
                case "title":
                    curl += "search.json?title=" + filter + "&jscmd=details&format=json";
                break;
                case "author":
                    curl += "search.json?author=" + filter + "&jscmd=details&format=json";
                break;
            }
            return this.getResult(curl);
            //string url = primaryUrl + "search.json?q=" + filter + "&page=1";


            //Console.WriteLine(data); //yes, the json string is correct
            //uri of the service


        }

        private String getResult(String curl)
        {
            String result = "";

            Uri address = new Uri(curl);
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(address);
            ServicePointManager.ServerCertificateValidationCallback = delegate
            {
                return true; //always trust the presented cerificate
            };
            request.Method = "get";
            request.ContentType = "application/x-www-form-urlencoded";
            string response = null;
            try
            {
                using (HttpWebResponse resp = request.GetResponse() as HttpWebResponse)
                {
                    var reader = new StreamReader(resp.GetResponseStream(), Encoding.UTF8);
                    response = reader.ReadToEnd();
                }
                result = response;
            }
            catch (Exception ex)
            {
                
            }
            return result;
        }

       
    }
}
