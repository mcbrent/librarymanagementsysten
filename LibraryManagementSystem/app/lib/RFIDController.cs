﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NDEF;
using Acs.Readers.Pcsc;
using Acs.Pcsc;
using Acs.SmartCard.Reader.Pcsc.Nfc;
using System.Text.RegularExpressions;
using Acs;
using System.Windows.Forms;

namespace LibraryManagementSystem.app.lib
{
    class RFIDController
    {
        private String Mask = "FF FF FF FF FF FF";
        MifareClassic _mifareClassic;
        ReaderFunctions _readerFunctions;

        byte[] _mifareTransportKey = new byte[] { 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF };
        byte[] _mifareTransportAccessBits = new byte[] { 0xFF, 0x07, 0x80, 0x69 };

        byte[] _ndefPublicKeyAMadSector = new byte[] { 0xA0, 0xA1, 0xA2, 0xA3, 0xA4, 0xA5 };
        byte[] _ndefPublicKeyANfcForumSector = new byte[] { 0xD3, 0xF7, 0xD3, 0xF7, 0xD3, 0xF7 };

        byte[] _ndefSectorTrailerMadSector = new byte[] { 0x78, 0x77, 0x88, 0xC1 };
        byte[] _ndefSectorTrailerNfcForumSector = new byte[] { 0x7F, 0x07, 0x88, 0x40 };


        String _currentReader = "";
        String _cardName = "";
        String _msg = "";
        String _ndefEncodedMessage = "";
        public Boolean Connect()
        {
            Boolean isGood = false;
            try
            {
                string[] readerList;

                _readerFunctions = new ReaderFunctions();

                //Register to event OnReceivedCommand
                _readerFunctions.OnReceivedCommand += new TransmitApduDelegate(readerFucntions_OnReceivedCommand);

                //Register to event OnSendCommand
                _readerFunctions.OnSendCommand += new TransmitApduDelegate(readerFucntions_OnSendCommand);

                //Get all smart card reader connected to computer
                readerList = _readerFunctions.getReaderList();
                if(readerList.Length > 0)
                {
                    _currentReader = readerList[0];
                    isGood = true;
                }

                //ComboBoxReaderList.Items.Clear();
                //ComboBoxReaderList.Items.AddRange(readerList);

                //if (ComboBoxReaderList.Items.Count > 0)
                //    ComboBoxReaderList.SelectedIndex = 0;

                //addMsgToLog("\nInitialize success");

                //ButtonConnect.Enabled = true;
            }
            catch (PcscException pcscException)
            {
                isGood = false;
                MessageBox.Show("Make sure RFID Scanner is attached", "RFID Scanner not detected", MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1);
                //addTitleToLog(false, "[" + pcscException.errorCode.ToString() + "] "
                //  + pcscException.Message);

                //showErrorMessage("[" + pcscException.errorCode.ToString() + "] "
                //    + pcscException.Message);
            }
            catch (Exception generalException)
            {
                isGood = false;
                MessageBox.Show("Make sure RFID Scanner is attached", "RFID Scanner not detected", MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1);
                //addMsgToLog(generalException.Message);
                //showErrorMessage(generalException.Message);
            }
            return isGood;
        }
        public Boolean CheckCard()
        {
            Boolean isFound = false;
            byte[] atr = null;
            CardSelector cardSelector;

            try
            {

                _readerFunctions.connect(_currentReader);

                //Initialize Mifare classic and CardSelector class
                _mifareClassic = new MifareClassic(_readerFunctions.pcscConnection);
                cardSelector = new CardSelector();

                _readerFunctions.getStatusChange(ref atr);
                cardSelector.pcscReader = _readerFunctions;

                _cardName = cardSelector.readCardType(atr, (byte)atr.Length);

                if (_cardName != "Mifare Standard 1K" && _cardName != "Mifare Standard 4K")
                {
                    MessageBox.Show("Card not supported.\r\nPlease present Mifare Classic card.");
                    return false;
                }
                isFound = true;
                //addMsgToLog("Chip Type: " + _cardName);
                //addMsgToLog("");

                //enableControls(true);

            }
            catch (PcscException pcscException)
            {
                //addTitleToLog(false, "[" + pcscException.errorCode.ToString() + "] "
                //    + pcscException.Message);

                //showErrorMessage("[" + pcscException.errorCode.ToString() + "] "
                //    + pcscException.Message);
            }
            catch (Exception generalException)
            {
                //addMsgToLog(generalException.Message);
                //showErrorMessage(generalException.Message);
            }
            return isFound;
        }
        public void Write(String message)
        {
            resetCard();
            encodeToNdefMessage(message);
            writeToCard();
        }
        public String Read()
        {
            _msg = "";
            readFromCard();
            return _msg;
        }

        public String generateKey()
        {
            return CreateMD5(Guid.NewGuid().ToString("N"));
        }
        private string CreateMD5(string input)
        {
            // Use input string to calculate MD5 hash
            using (System.Security.Cryptography.MD5 md5 = System.Security.Cryptography.MD5.Create())
            {
                byte[] inputBytes = System.Text.Encoding.ASCII.GetBytes(input);
                byte[] hashBytes = md5.ComputeHash(inputBytes);

                // Convert the byte array to hexadecimal string
                StringBuilder sb = new StringBuilder();
                for (int i = 0; i < hashBytes.Length; i++)
                {
                    sb.Append(hashBytes[i].ToString("X2"));
                }
                return sb.ToString();
            }
        }




        void readFromCard()
        {
            byte numberOfSectors;
            byte currentSector = 1;
            byte physicalBlock = 0;
            byte[] buffer = new byte[0];
            byte[] tmpArray;
            int endIndex = 0;
            int temporaryLen;
            int temporaryPayLoadLen;
            string payLoadTypeNameStr = "";
            string[] vCard;
            string[] vCardInfo;
            string[] delimeter = new string[] { "\r\n" };
            const int DELAY = 2000;
            NdefMessage ndefMessage;
            NdefMessage temporaryNdefMessage;
            List<NdefRecord> ndefRecords;
            NdefRecord temporaryNdefRecord;
            const int MAD_SECTOR2_ADDRESS = 16;
            const int MIFARE_4K_SECTOR_WITH_4_BLOCKS = 31;
            const int MIFARE_4K_EXTENDED_USER_DATA_COUNT = 15;
            const int MIFARE_4K_START_WITH_15_BLOCKS = 128;
            const int MIFARE_4K_START_SECTOR_WITH_15_BLOCKS = 32;
            const int THREE_BYTE_MESSAGE_TAG = 4;
            const int TWO_BYTE_MESSAGE_TAG = 2;
            const int BLOCK_COUNT = 4;
            const int BLOCK_LENGTH = 16;

            try
            {
                //Load NFC Forum Sector Secret Key B to device's memory slot number 1
                //addTitleToLog(false, "Load MAD Sector Public Key");
                if (!_readerFunctions.loadAuthKey(_ndefPublicKeyAMadSector, 0x00))
                {
                    MessageBox.Show("Load Key Failed");
                    return;
                }

                //Load Mifare Transport Configuration Key (Default Key) to device's memory slot number 2
                //addTitleToLog(false, "Load NFC Forum Sector Public Key");
                if (!_readerFunctions.loadAuthKey(_ndefPublicKeyANfcForumSector, 0x01))
                {
                    MessageBox.Show("Load Key Failed");
                    return;
                }

                //RichTextBoxEncodedNdefMessage.Text = "";

                //Read MAD Sector 1
                //Authenticate using the MAD public key
                //That is previously loaded on the device
                //addTitleToLog(false, "Authenticate MAD1 using MAD public key");
                if (!_readerFunctions.authenticate(0x01, KEYTYPES.ACR122_KEYTYPE_A, 0x00))
                    throw new Exception("Invalid Card");

                //addTitleToLog(false, "Read MAD1 block 1");
                tmpArray = _mifareClassic.readBinary(0x01, 0x10);
                buffer = Helper.appendArrays(buffer, tmpArray);

                //addTitleToLog(false, "Read MAD1 block 2");
                tmpArray = _mifareClassic.readBinary(0x02, 0x10);
                buffer = Helper.appendArrays(buffer, tmpArray);


                if (_cardName == "Mifare Standard 4K")
                {
                    //Read MAD Sector 2

                    //Authenticate using the MAD public key
                    //That is previously loaded on the device
                    //addTitleToLog(false, "Authenticate MAD2 using MAD public key");
                    _readerFunctions.authenticate(0x40, KEYTYPES.ACR122_KEYTYPE_A, 0x00);

                    //addTitleToLog(false, "Read MAD2 block 64");
                    tmpArray = _mifareClassic.readBinary(0x40, 0x10);
                    buffer = Helper.appendArrays(buffer, tmpArray);

                    //addTitleToLog(false, "Read MAD2 block 65");
                    tmpArray = _mifareClassic.readBinary(0x41, 0x10);
                    buffer = Helper.appendArrays(buffer, tmpArray);

                    //addTitleToLog(false, "Read MAD2 block 66");
                    tmpArray = _mifareClassic.readBinary(0x42, 0x10);
                    buffer = Helper.appendArrays(buffer, tmpArray);
                }

                numberOfSectors = 0;

                for (int i = 0; i < buffer.Length; i += 2)
                {
                    if (Helper.byteArrayIsEqual(new byte[] { buffer[i], buffer[i + 1] }, new byte[] { 0x03, 0xE1 }))
                    {
                        numberOfSectors++;
                    }
                }

                buffer = new byte[0];
                currentSector = 1;

                //delay for ACR122U reader
                if (_currentReader.Contains("ACR122"))
                    System.Threading.Thread.Sleep(DELAY);

                for (int i = 0; i < numberOfSectors; i++)
                {
                    //Skip MAD Sector 2
                    if (currentSector == MAD_SECTOR2_ADDRESS)
                    {
                        currentSector++;
                    }

                    if (currentSector <= MIFARE_4K_SECTOR_WITH_4_BLOCKS)
                    {
                        physicalBlock = (byte)(currentSector * BLOCK_COUNT);
                        _readerFunctions.authenticate(physicalBlock, KEYTYPES.ACR122_KEYTYPE_A, 0x01);

                        for (int j = 0; j < (BLOCK_COUNT - 1); j++)
                        {
                            tmpArray = _mifareClassic.readBinary((byte)(physicalBlock + j), 0x10);
                            buffer = Helper.appendArrays(buffer, tmpArray);
                        }
                    }
                    else
                    {
                        physicalBlock = (byte)(MIFARE_4K_START_WITH_15_BLOCKS + ((currentSector - MIFARE_4K_START_SECTOR_WITH_15_BLOCKS) * BLOCK_LENGTH));
                        _readerFunctions.authenticate(physicalBlock, KEYTYPES.ACR122_KEYTYPE_A, 0x01);

                        for (int j = 0; j < (MIFARE_4K_EXTENDED_USER_DATA_COUNT); j++)
                        {
                            tmpArray = _mifareClassic.readBinary((byte)(physicalBlock + j), 0x10);
                            buffer = Helper.appendArrays(buffer, tmpArray);
                        }
                    }

                    currentSector++;
                }

                //Check if NDEF Message TAG is present
                if (buffer[0] != (byte)(NfcForum.TAGS.NDEF_MESSAGE_TLV))
                    throw new Exception("NDEF Message TAG is not found");

                //Find the Terminator TLV
                for (endIndex = 0; endIndex < buffer.Length; endIndex++)
                {
                    if (buffer[endIndex] == 0xFE)
                        break;

                    if ((endIndex + 1) >= buffer.Length)
                        throw new Exception("Invalid Card.\r\nTerminator Tag is not found.");
                }

                Array.Resize(ref buffer, endIndex);

                //Remove NDEF Message Tag
                //Check if it uses one byte or 3 byte format for the length
                if (buffer[1] == 0xFF)
                {
                    //Uses 3 bytes format
                    tmpArray = new byte[buffer.Length - THREE_BYTE_MESSAGE_TAG];
                    Array.Copy(buffer, THREE_BYTE_MESSAGE_TAG, tmpArray, 0, tmpArray.Length);
                }
                else
                {
                    tmpArray = new byte[buffer.Length - TWO_BYTE_MESSAGE_TAG];
                    Array.Copy(buffer, TWO_BYTE_MESSAGE_TAG, tmpArray, 0, tmpArray.Length);
                }


                //RichTextBoxEncodedNdefMessage.Text = Helper.byteAsString(tmpArray, false);

                ndefMessage = NdefRecord.getNestedNdefMessage(0, tmpArray);

                if (ndefMessage.getNumberOfRecords() < 0)
                {
                    //showErrorMessage("No record to display");
                    return;
                }

                ndefRecords = ndefMessage.getRecords();

                payLoadTypeNameStr = ndefRecords[0].recordType.typeName;

                if (payLoadTypeNameStr == "Sp")
                {
                    //temporaryNdefMessage = ndefRecords[0].getNestedNdefMessage();

                    //if (temporaryNdefMessage.getNumberOfRecords() < 1)
                    //    throw new Exception("Invalid Smart Poster NDEF Message");

                    ////Get URI Record (Mandatory)
                    //temporaryNdefRecord = temporaryNdefMessage.getRecord(new NdefRecordType(TYPE_NAME_FORMAT.NFC_WELL_KNOWN_TYPE, "U"));
                    //if (temporaryNdefRecord == null)
                    //{
                    //    //showErrorMessage("Mandatory URI Record is not found");
                    //    return;
                    //}

                    //ComboBoxSmartPosterPrefixURI.SelectedIndex = temporaryNdefRecord.payLoad[0];
                    //TextBoxSmartPosterURI.Text = ASCIIEncoding.ASCII.GetString(temporaryNdefRecord.payLoad.Skip(1).Take(temporaryNdefRecord.payLoad.Length - 1).ToArray());

                    ////Get Action Record
                    //ComboBoxSmartPosterAction.SelectedIndex = 0;
                    //temporaryNdefRecord = temporaryNdefMessage.getRecord(new NdefRecordType(TYPE_NAME_FORMAT.NFC_WELL_KNOWN_TYPE, "act"));
                    //if (temporaryNdefRecord != null)
                    //{
                    //    switch ((NDEF.ACTION_RECORD)temporaryNdefRecord.payLoad[0])
                    //    {
                    //        case ACTION_RECORD.DO_THE_ACTION:
                    //            {
                    //                ComboBoxSmartPosterAction.SelectedIndex = 1;
                    //                break;
                    //            }
                    //        case ACTION_RECORD.SAVE_FOR_LATER:
                    //            {
                    //                ComboBoxSmartPosterAction.SelectedIndex = 2;
                    //                break;
                    //            }
                    //        case ACTION_RECORD.OPEN_FOR_EDITING:
                    //            {
                    //                ComboBoxSmartPosterAction.SelectedIndex = 3;
                    //                break;
                    //            }
                    //        default:
                    //            {
                    //                ComboBoxSmartPosterAction.SelectedIndex = 0;
                    //                break;
                    //            }
                    //    }
                    //}
                    //else
                    //{
                    //    ComboBoxSmartPosterAction.SelectedIndex = 0;
                    //}

                    ////Get Text Record
                    //temporaryNdefRecord = temporaryNdefMessage.getRecord(new NdefRecordType(TYPE_NAME_FORMAT.NFC_WELL_KNOWN_TYPE, "T"));
                    //TextBoxSmartPosterTitle.Text = "";
                    //if (temporaryNdefRecord != null)
                    //{
                    //    temporaryLen = temporaryNdefRecord.payLoad[0] & 0x1F;
                    //    ComboBoxSmartPosterTitleLanguage.Text = ASCIIEncoding.ASCII.GetString(temporaryNdefRecord.payLoad.Skip(1).Take(temporaryLen).ToArray());

                    //    temporaryPayLoadLen = temporaryNdefRecord.payLoad.Length - (temporaryLen - 1);
                    //    TextBoxSmartPosterTitle.Text = ASCIIEncoding.ASCII.GetString(temporaryNdefRecord.payLoad.Skip(temporaryLen + 1).Take(temporaryPayLoadLen).ToArray());

                    //}

                    //MessageBox.Show("Smart Poster Message Found", "Information");
                    //TabControlMain.SelectedIndex = 0;

                }
                else if (payLoadTypeNameStr == "T")
                {
                    //Get Text Record
                    temporaryNdefRecord = ndefRecords[0];

                    //if ((temporaryNdefRecord.payLoad[0] & 0x80) != 0x00)
                    //    RadioButtonUtf16.Checked = true;
                    //else
                    //    RadioButtonUtf8.Checked = true;

                    temporaryLen = temporaryNdefRecord.payLoad[0] & 0x1F;
                    //ComboBoxTextLanguage.Text = ASCIIEncoding.ASCII.GetString(temporaryNdefRecord.payLoad.Skip(1).Take(temporaryLen).ToArray());

                    temporaryPayLoadLen = temporaryNdefRecord.payLoad.Length - (temporaryLen - 1);
                    _msg = ASCIIEncoding.ASCII.GetString(temporaryNdefRecord.payLoad.Skip(temporaryLen + 1).Take(temporaryPayLoadLen).ToArray());

                    //MessageBox.Show("Text Message Found", "Information");
                    //TabControlMain.SelectedIndex = 2;

                }
                else if (payLoadTypeNameStr == "U")
                {
                    //temporaryNdefRecord = ndefRecords[0];
                    //TextBoxUriUri.Text = ASCIIEncoding.ASCII.GetString(temporaryNdefRecord.payLoad.Skip(1).Take(temporaryNdefRecord.payLoad.Length - 1).ToArray());

                    //MessageBox.Show("URI Message Found", "Information");
                    //TabControlMain.SelectedIndex = 1;
                }
                else if (payLoadTypeNameStr == "text/x-vCard")
                {
                    //temporaryNdefRecord = ndefRecords[0];
                    //vCard = ASCIIEncoding.ASCII.GetString(temporaryNdefRecord.payLoad).Split(delimeter, StringSplitOptions.None);
                    //vCardInfo = vCard[2].Split(new char[] { ';' });
                    //TextBoxLastName.Text = vCardInfo[0].Substring(2, vCardInfo[0].Length - 2);
                    //TextBoxGivenName.Text = vCardInfo[1];
                    //TextBoxMiddleName.Text = vCardInfo[2];

                    //MessageBox.Show("vCard Message Found", "Information");
                    //TabControlMain.SelectedIndex = 3;
                }
                else
                {
                    MessageBox.Show("Unsupported payload type", "Information");
                }
            }
            catch (CardException cardException)
            {
                //addTitleToLog(false, "[" + Helper.byteAsString(cardException.statusWord, true) +
                //    "] " + cardException.Message);

                //showErrorMessage(cardException.Message);
            }
            catch (PcscException pcscException)
            {
                //addTitleToLog(false, "[" + pcscException.errorCode.ToString() + "] "
                //    + pcscException.Message);

                //showErrorMessage("[" + pcscException.errorCode.ToString() + "] "
                //    + pcscException.Message);
            }
            catch (Exception generalException)
            {
                //addMsgToLog(generalException.Message);
                //showErrorMessage(generalException.Message);
            }
        }

        void resetCard()
        {
            byte[] madKeyB, nfcKeyB;
            byte[] transportValue = new byte[] { 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0x07, 0x80, 0x69, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF };
            byte numberOfSectors = 15;
            byte sectorTrailer = 0;
            const int KEY_LENGTH = 6;
            const int BLOCK_DATA_LENGTH = 16;
            const int MIFARE_1K_SECTOR_COUNT = 15;
            const int MIFARE_4K_SECTOR_COUNT = 39;
            const int MIFARE_4K_SECTOR_WITH_4_BLOCKS = 31;
            const int MIFARE_4K_EXTENDED_USER_DATA_COUNT = 15;
            const int MIFARE_USER_DATA_PER_SECTOR = 3;
            const int MIFARE_4K_START_WITH_15_BLOCKS = 128;
            const int MIFARE_4K_START_SECTOR_WITH_15_BLOCKS = 32;
            const int BLOCK_COUNT = 4;
            const int MAD_SECTORS_COUNT = 2;
            //const int DELAY = 2000;

            try
            {

                madKeyB = getBytes(Mask.Trim(), ' ');

                nfcKeyB = getBytes(Mask.Trim(), ' ');




                //Load NFC Forum Sector Secret Key B to device's memory slot number 1
                if (!_readerFunctions.loadAuthKey(madKeyB, 0x00))
                {
                    //showErrorMessage("Load Key Failed");
                    return;
                }

                //addTitleToLog(false, "Load Default Key to slot 2");
                //Load Mifare Transport Configuration Key (Default Key) to device's memory slot number 2
                if (!_readerFunctions.loadAuthKey(_mifareTransportKey, 0x01))
                {
                    //showErrorMessage("Load Key Failed");
                    return;
                }

                //addTitleToLog(false, "Reset MAD Sector 1");

                for (int i = 0; i < MAD_SECTORS_COUNT; i++)
                {
                    //Authenticate using MAD Sector Secret Key B
                    //addTitleToLog(false, "Authenticate using MAD Sector Key B");
                    if (i == 0)
                    {
                        if (!_readerFunctions.authenticate(0x03, KEYTYPES.ACR122_KEYTYPE_B, 0x00))
                            continue;
                    }
                    else
                    {
                        //If failed, assume that the sector is using the transport configuration keys
                        //FF FF FF FF FF FF
                        if (!_readerFunctions.authenticate(0x03, KEYTYPES.ACR122_KEYTYPE_A, 0x01))
                            continue;
                    }

                    if (_mifareClassic.updateBinary(0x03, transportValue, 0x10) == 0)
                        break;
                }

                if (_cardName == "Mifare Standard 4K")
                {
                    //addTitleToLog(false, "Reset MAD Sector 2");

                    for (int i = 0; i < MAD_SECTORS_COUNT; i++)
                    {
                        //Authenticate using MAD Sector Secret Key B
                        if (i == 0)
                        {
                            if (!_readerFunctions.authenticate(0x63, KEYTYPES.ACR122_KEYTYPE_B, 0x00))
                                continue;
                        }
                        else
                        {
                            //If failed, assume that the sector is using the transport configuration keys
                            //FF FF FF FF FF FF
                            if (!_readerFunctions.authenticate(0x63, KEYTYPES.ACR122_KEYTYPE_A, 0x01))
                                continue;
                        }

                        if (_mifareClassic.updateBinary(0x63, transportValue, 0x10) == 0)
                            break;
                    }
                }

                if (_cardName == "Mifare Standard 4K")
                    numberOfSectors = MIFARE_4K_SECTOR_COUNT; // 39 sectors
                else if (_cardName == "Mifare Standard 1K")
                    numberOfSectors = MIFARE_1K_SECTOR_COUNT; // 15 sectors

                for (int i = 1; i <= numberOfSectors; i++)
                {
                    if (i <= MIFARE_4K_SECTOR_WITH_4_BLOCKS)
                        sectorTrailer = (byte)((i * BLOCK_COUNT) + MIFARE_USER_DATA_PER_SECTOR);
                    else
                        sectorTrailer = (byte)(MIFARE_4K_START_WITH_15_BLOCKS + ((i - MIFARE_4K_START_SECTOR_WITH_15_BLOCKS) * BLOCK_DATA_LENGTH) + MIFARE_4K_EXTENDED_USER_DATA_COUNT);

                    //addTitleToLog(false, "Reset Sector: " + i.ToString());

                    if (!_readerFunctions.authenticate(sectorTrailer, KEYTYPES.ACR122_KEYTYPE_B, 0x00))
                        continue;

                    _mifareClassic.updateBinary(sectorTrailer, transportValue, 0x10);
                }

                //delay for ACR122U reader
                //System.Threading.Thread.Sleep(DELAY);
            }
            catch (CardException cardException)
            {
                //addTitleToLog(false, "[" + Helper.byteAsString(cardException.statusWord, true) + "] " + cardException.Message);

                //showErrorMessage(cardException.Message);
            }
            catch (PcscException pcscException)
            {
                //addTitleToLog(false, "[" + pcscException.errorCode.ToString() + "] " + pcscException.Message);

                //showErrorMessage("[" + pcscException.errorCode.ToString() + "] " + pcscException.Message);
            }
            catch (Exception generalException)
            {
                //addMsgToLog(generalException.Message);
                //showErrorMessage(generalException.Message);
            }
        }

        void writeToCard()
        {
            byte[] madSector = new byte[0];
            byte[] dataToWrite = new byte[0];
            byte[] madKeyB, nfcKeyB;
            byte[] arrLength;
            byte[] buffer;
            byte numberOfNfcForumSector = 0;
            byte numberOfBlocksNeeded = 0;
            byte lastSectorAuthenticated = 0;
            byte physicalBlockToWrite = 0x04;
            byte sectorTrailer = 0x03;
            byte[] tmpArray;
            const int KEY_LENGTH = 6;
            const int ACCESS_BITS_LENGTH = 4;
            const int DATA_LENGTH = 255;
            const int MIFARE_WITH_4_BLOCKS_SECTOR_COUNT = 32;
            const int MAD_SECTOR2_LENGTH = 48;
            const int BLOCK_DATA_LENGTH = 16;
            const int MIFARE_WITH_4_BLOCKS_TOTAL_COUNT = 127;
            const int MIFARE_4K_TOTAL_SECTOR = 38;
            const int MIFARE_1K_TOTAL_SECTOR = 35;
            const int MAD2_SECTOR_TRAILER_BLOCK = 67;
            const int MIFARE_4K_EXTENDED_USER_DATA_COUNT = 15;
            const int MIFARE_USER_DATA_COUNT_PER_SECTOR = 3;
            const int AIDS_COUNT = 15;
            const int MAD2_SECTOR_START = 64;
            const int MAD2_SECTOR_LAST = 68;
            const int BLOCK_COUNT = 4;
            const int COPY_LENGTH = 2;
            const int DELAY = 2000;
            const double BLOCK_LENGTH = 16.00;
            const double MIFARE_4K_EXTENDED_USER_DATA_COUNT_DOUBLE = 15.00;
            const double MIFARE_USER_BLOCK = 3.00;

            int dataLength;
            int lenToCopy;

            try
            {

                dataToWrite = Helper.getBytes(_ndefEncodedMessage);

                madKeyB = getBytes(Mask, ' ');


                nfcKeyB = getBytes(Mask, ' ');

                //addTitleToLog(false, "Load Authentication Key");
                _readerFunctions.loadAuthKey(_mifareTransportKey, 0x00);

                //delay for ACR122U reader
                if (_currentReader.Contains("ACR122"))
                    System.Threading.Thread.Sleep(DELAY);

                //Form the data to be written to the card
                buffer = new byte[0];
                if (dataToWrite.Length < DATA_LENGTH)
                {
                    //If data is less than or equal to 254 (0xFE) use one byte format
                    //Refere to NXP Type MF1K/4K Tag Operation document
                    //http://www.nxp.com/
                    buffer = Helper.appendArrays(buffer, new byte[] { (byte)NfcForum.TAGS.NDEF_MESSAGE_TLV, (byte)dataToWrite.Length });
                }
                else
                {
                    //If data is greater than 254 (0xFE) use 3 bytes format
                    dataLength = dataToWrite.Length;

                    //In 3 bytes format the last 2 bytes is for 00FE - FFFF                    
                    dataLength -= (DATA_LENGTH - 1);

                    arrLength = new byte[3];

                    //Indicates that two more bytes folow
                    arrLength[0] = 0xFF;
                    arrLength[1] = (byte)((dataLength & 0xFF00) >> 8);
                    arrLength[2] = (byte)(dataLength & 0xFF);

                    //NDEF Message TAG
                    buffer = Helper.appendArrays(buffer, (byte)NfcForum.TAGS.NDEF_MESSAGE_TLV);

                    //length
                    buffer = Helper.appendArrays(buffer, arrLength);

                }

                buffer = Helper.appendArrays(buffer, dataToWrite);

                //Append Terminator TLV
                buffer = Helper.appendArrays(buffer, (byte)NfcForum.TAGS.TERMINATOR_TLV);

                //Write the data to Mifare Card
                //Start from NFC Form Sector 1 Block 0

                //addTitleToLog(false, "Write NDEF Message To Card");

                //Get mumber of blocks needed
                // MAD sector blocks not included
                numberOfBlocksNeeded = (byte)((double)Math.Ceiling(buffer.Length / BLOCK_LENGTH));
                physicalBlockToWrite = 0x04; //Start to write at sector 1 block 1 (Physical Block 0x04)


                for (int indx = 0; indx < numberOfBlocksNeeded; indx++, physicalBlockToWrite++)
                {
                    tmpArray = new byte[BLOCK_DATA_LENGTH];
                    lenToCopy = BLOCK_DATA_LENGTH;

                    //Check if remaining data to be copied is less than 16
                    if ((buffer.Length - (indx * BLOCK_DATA_LENGTH)) > BLOCK_DATA_LENGTH)
                        lenToCopy = BLOCK_DATA_LENGTH;
                    else
                        lenToCopy = buffer.Length - (indx * BLOCK_DATA_LENGTH);

                    Array.Copy(buffer, (indx * BLOCK_DATA_LENGTH), tmpArray, 0, lenToCopy);


                    //Check if sector trailer
                    //If sector trailer skip
                    if (physicalBlockToWrite > MIFARE_WITH_4_BLOCKS_TOTAL_COUNT)
                    {
                        if (((physicalBlockToWrite - MIFARE_4K_EXTENDED_USER_DATA_COUNT) % (MIFARE_4K_EXTENDED_USER_DATA_COUNT + 1)) == 0)
                            physicalBlockToWrite++;
                    }
                    else
                    {
                        if (((physicalBlockToWrite - MIFARE_USER_DATA_COUNT_PER_SECTOR) % BLOCK_COUNT) == 0)
                            physicalBlockToWrite++;

                        //Check if MAD Sector 2 (Sector 16)
                        //if MAD Sector skip
                        if (physicalBlockToWrite >= MAD2_SECTOR_START && physicalBlockToWrite <= MAD2_SECTOR_LAST)
                        {
                            //addMsgToLog("Skip MAD Sector : " + (physicalBlockToWrite / BLOCK_COUNT).ToString());
                            physicalBlockToWrite = MAD2_SECTOR_LAST + 1; //Move to physical block 68 (Sector 17))
                        }
                    }

                    if (physicalBlockToWrite > MIFARE_WITH_4_BLOCKS_TOTAL_COUNT)
                    {
                        if (lastSectorAuthenticated != (physicalBlockToWrite / BLOCK_DATA_LENGTH))
                        {
                            //addTitleToLog(false, "Authenticate Sector: " + (((physicalBlockToWrite - MIFARE_WITH_4_BLOCKS_TOTAL_COUNT) / BLOCK_DATA_LENGTH) + MIFARE_WITH_4_BLOCKS_SECTOR_COUNT).ToString());
                            if (!_readerFunctions.authenticate(physicalBlockToWrite, KEYTYPES.ACR122_KEYTYPE_A, 0x00))
                                throw new Exception("Unable to authenticate in sector " + (physicalBlockToWrite / BLOCK_DATA_LENGTH).ToString());

                            lastSectorAuthenticated = (byte)(physicalBlockToWrite / BLOCK_DATA_LENGTH);
                        }
                    }
                    else
                    {

                        if (lastSectorAuthenticated != (physicalBlockToWrite / BLOCK_COUNT))
                        {
                            //addTitleToLog(false, "Authenticate Sector: " + (physicalBlockToWrite / BLOCK_COUNT).ToString());
                            if (!_readerFunctions.authenticate(physicalBlockToWrite, KEYTYPES.ACR122_KEYTYPE_A, 0x00))
                                throw new Exception("Unable to authenticate in sector " + (physicalBlockToWrite / BLOCK_COUNT).ToString());

                            lastSectorAuthenticated = (byte)(physicalBlockToWrite / BLOCK_COUNT);
                        }
                    }


                    //addTitleToLog(false, "Write in block " + physicalBlockToWrite.ToString());

                    _mifareClassic.updateBinary(physicalBlockToWrite, tmpArray, 0x10);

                }

                //Initialize MAD Sector
                //If number of sector blocks needed is greater than 127 (31 sectors)
                //32nd sector have 15 user blocks (0 - 31 sector have 3 user blocks)
                if (numberOfBlocksNeeded > MIFARE_WITH_4_BLOCKS_TOTAL_COUNT)
                {
                    numberOfNfcForumSector = (MIFARE_WITH_4_BLOCKS_SECTOR_COUNT - 1);
                    numberOfNfcForumSector += (byte)(Math.Ceiling((numberOfBlocksNeeded - MIFARE_WITH_4_BLOCKS_TOTAL_COUNT) / MIFARE_4K_EXTENDED_USER_DATA_COUNT_DOUBLE));
                }
                else
                {
                    numberOfNfcForumSector = (byte)(Math.Ceiling(numberOfBlocksNeeded / MIFARE_USER_BLOCK));
                }

                madSector = new byte[MIFARE_WITH_4_BLOCKS_SECTOR_COUNT];
                for (int i = 0; i < numberOfNfcForumSector; i++)
                {
                    Array.Copy(new byte[] { 0x03, 0xE1 }, 0, madSector, ((i * COPY_LENGTH) + COPY_LENGTH), COPY_LENGTH);

                    //Mad Sector 1 contains 15 AIDs (0-14 = 15)
                    //the rest of the AIDs will be written in MAD Sector 2
                    if (i == (AIDS_COUNT - 1))
                        break;
                }

                //Initialize MAD Sector 1

                //addTitleToLog(false, "Initialize MAD Sector 1");
                //addTitleToLog(false, "Authenticate MAD Sector 1");

                //Write AIDs to MAD Sector 1
                _readerFunctions.authenticate(0x01, KEYTYPES.ACR122_KEYTYPE_A, 0x00);

                //Write the first 16 bytes (first 7 AIDs) to block 1 of MAD Sector 1 (Sector 0)
                //addTitleToLog(false, "Write first 7 AIDs to block 1 of MAD1");
                _mifareClassic.updateBinary(0x01, madSector.Take(BLOCK_DATA_LENGTH).ToArray(), 0x10);

                //Write next 16 bytes (next 8 AIDs) to block 2 of MAD Sector 1 (Sector 0)
                //addTitleToLog(false, "Write next 8 AIDs to block 2 of MAD1");
                _mifareClassic.updateBinary(0x02, madSector.Skip(BLOCK_DATA_LENGTH).Take(BLOCK_DATA_LENGTH).ToArray(), 0x10);

                //Initialize MAD Sector 2

                //If number of sectors needed (AIDs) is greater than 15 then write the remaining
                //AIDs in the MAD Sector 2 (Sector 16 of the Mifare Card)
                //MAD Sector 2 can store up to 23 AIDs
                if (numberOfNfcForumSector >= (MIFARE_1K_TOTAL_SECTOR + 1) && numberOfNfcForumSector <= MIFARE_4K_TOTAL_SECTOR)
                {
                    //addTitleToLog(false, "Initialize MAD Sector 2");

                    madSector = new byte[MAD_SECTOR2_LENGTH];

                    for (int i = AIDS_COUNT; i < numberOfNfcForumSector; i++)
                        Array.Copy(new byte[] { 0x03, 0xE1 }, 0, madSector, ((i - AIDS_COUNT) * COPY_LENGTH), COPY_LENGTH);


                    //Write AIDs to MAD Sector 2 (Sector 16 of the Mifare Card)
                    //addTitleToLog(false, "Authenticate MAD Sector 2");
                    _readerFunctions.authenticate(0x40, KEYTYPES.ACR122_KEYTYPE_A, 0x00);

                    //Write the first 16 bytes (first 7 AIDs) to block 0 of MAD Sector 2 (Sector 16 of the Mifare Card)
                    //addTitleToLog(false, "Write first 7 AIDs to block 0 of MAD2");
                    _mifareClassic.updateBinary(0x40, madSector.Take(BLOCK_DATA_LENGTH).ToArray(), 0x10);

                    //Write next 16 bytes (next 8 AIDs) to block 1 of MAD Sector 2
                    //addTitleToLog(false, "Write next 8 AIDs to block 1 of MAD2");
                    _mifareClassic.updateBinary(0x41, madSector.Skip(BLOCK_DATA_LENGTH).Take(BLOCK_DATA_LENGTH).ToArray(), 0x10);

                    //Write next 16 bytes (next 8 AIDs) to block 1 of MAD Sector 2
                    //addTitleToLog(false, "Write next 8 AIDs to block 2 of MAD2");
                    _mifareClassic.updateBinary(0x42, madSector.Skip(BLOCK_DATA_LENGTH * 2).Take(BLOCK_DATA_LENGTH).ToArray(), 0x10);
                }

                //addTitleToLog(false, "Change Sector Keys");

                //Update Sector Keys and Access Bits                
                buffer = new byte[BLOCK_DATA_LENGTH];

                //Key A
                Array.Copy(_ndefPublicKeyANfcForumSector, 0, buffer, 0, KEY_LENGTH);

                //Access Bits
                Array.Copy(_ndefSectorTrailerNfcForumSector, 0, buffer, KEY_LENGTH, ACCESS_BITS_LENGTH);

                //Key B
                Array.Copy(nfcKeyB, 0, buffer, KEY_LENGTH + ACCESS_BITS_LENGTH, KEY_LENGTH);

                //Update NFC Forum Sector Keys
                //Start from sector 1
                sectorTrailer = 0x03;
                for (int i = 1; i <= numberOfNfcForumSector; i++)
                {
                    if (i <= (MIFARE_WITH_4_BLOCKS_SECTOR_COUNT - 1))
                        sectorTrailer += BLOCK_COUNT;
                    else
                        sectorTrailer += (MIFARE_4K_EXTENDED_USER_DATA_COUNT + 1);

                    //Check if MAD Sector 2 (Sector 16, Trailer Block = Phycal Block 67)
                    //Skip if MAD Sector 2 trailer
                    if (sectorTrailer == MAD2_SECTOR_TRAILER_BLOCK)
                    {
                        //Move to next sector trailer
                        sectorTrailer += BLOCK_COUNT;
                    }

                    //if (sectorTrailer < MAD2_SECTOR_TRAILER_BLOCK)
                        //addTitleToLog(false, "Update Trailer Block of NFC Forum Sector: " + i.ToString());
                    //else
                        //addTitleToLog(false, "Update Trailer Block of NFC Forum Sector: " + (i + 1).ToString());

                    _readerFunctions.authenticate(sectorTrailer, KEYTYPES.ACR122_KEYTYPE_A, 0x00);

                    _mifareClassic.updateBinary(sectorTrailer, buffer, 0x10);
                }

                //Update MAD Sector 1 Keys
                //addTitleToLog(false, "Update MAD Sector 1 Trailer");

                _readerFunctions.authenticate(0x03, KEYTYPES.ACR122_KEYTYPE_A, 0x00);

                buffer = new byte[BLOCK_DATA_LENGTH];

                //Key A
                Array.Copy(_ndefPublicKeyAMadSector, 0, buffer, 0, KEY_LENGTH);

                //Access Bits
                Array.Copy(_ndefSectorTrailerMadSector, 0, buffer, KEY_LENGTH, ACCESS_BITS_LENGTH);

                //Key B
                Array.Copy(madKeyB, 0, buffer, KEY_LENGTH + ACCESS_BITS_LENGTH, KEY_LENGTH);

                _mifareClassic.updateBinary(0x03, buffer, 0x10);

                //If MAD Sector is greater than 15
                //Write the remaining AIDs in the MAD Sector 2
                if (numberOfNfcForumSector > AIDS_COUNT)
                {
                    //Update MAD Sector 2 Keys
                    //addTitleToLog(false, "Update MAD Sector 2 Trailer Block");

                    _readerFunctions.authenticate(0x43, KEYTYPES.ACR122_KEYTYPE_A, 0x00);

                    buffer = new byte[BLOCK_DATA_LENGTH];

                    //Key A
                    Array.Copy(_ndefPublicKeyAMadSector, 0, buffer, 0, KEY_LENGTH);

                    //Access Bits
                    Array.Copy(_ndefSectorTrailerMadSector, 0, buffer, KEY_LENGTH, ACCESS_BITS_LENGTH);

                    //Key B
                    Array.Copy(madKeyB, 0, buffer, KEY_LENGTH + ACCESS_BITS_LENGTH, KEY_LENGTH);


                    _mifareClassic.updateBinary(0x43, buffer, 0x10);
                }
            }
            catch (CardException cardException)
            {
                //addTitleToLog(false, "[" + Helper.byteAsString(cardException.statusWord, true) +
                //    "] " + cardException.Message);

                //showErrorMessage(cardException.Message);
            }
            catch (PcscException pcscException)
            {
                //addTitleToLog(false, "[" + pcscException.errorCode.ToString() + "] "
                //    + pcscException.Message);

                //showErrorMessage("[" + pcscException.errorCode.ToString() + "] "
                //    + pcscException.Message);
            }
            catch (Exception generalException)
            {
                //addMsgToLog(generalException.Message);
                //showErrorMessage(generalException.Message);
            }
        }

        void encodeToNdefMessage(String textMessage)
        {
            String language = "en";
            NdefMessage ndefMessage;
            NdefRecord tmpRecord;
            byte statusByte = 0;
            byte[] buffer;


            //Length of the Language (ISO/IANA language code)
            //ex. "en-US"
            statusByte |= (byte)2;

            tmpRecord = new NdefRecord(new NdefRecordType(TYPE_NAME_FORMAT.NFC_WELL_KNOWN_TYPE, "T"));
            tmpRecord.messageBegin = true;
            tmpRecord.messageEnd = true;
            tmpRecord.appendPayload(textMessage);

            tmpRecord.payLoad = new byte[] { statusByte };
            tmpRecord.payLoad = Helper.appendArrays(tmpRecord.payLoad, ASCIIEncoding.ASCII.GetBytes(language));
            tmpRecord.payLoad = Helper.appendArrays(tmpRecord.payLoad, ASCIIEncoding.ASCII.GetBytes(textMessage));

            ndefMessage = new NdefMessage();
            ndefMessage.appendRecord(tmpRecord);

            buffer = ndefMessage.toByteArray();

            _ndefEncodedMessage = Helper.byteAsString(buffer, false);

        }

        byte[] getBytes(string stringBytes, char delimeter)
        {
            int counter = 0;
            byte tmpByte;
            string[] arrayString = stringBytes.Split(delimeter);
            byte[] bytesResult = new byte[arrayString.Length];

            foreach (string str in arrayString)
            {
                if (byte.TryParse(str, System.Globalization.NumberStyles.HexNumber, null, out tmpByte))
                {
                    bytesResult[counter] = tmpByte;
                    counter++;
                }
                else
                {
                    return null;
                }
            }

            return bytesResult;
        }
        //Needed Event
        void readerFucntions_OnSendCommand(object sender, TransmitApduEventArg e)
        {
            Console.WriteLine("<< ", e.data, e.data.Length);
        }

        void readerFucntions_OnReceivedCommand(object sender, TransmitApduEventArg e)
        {
            Console.WriteLine(">> ", e.data, e.data.Length);
        }
    }
}
