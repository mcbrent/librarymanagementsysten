﻿using LibraryManagementSystem.app.ui.form;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LibraryManagementSystem.app.lib
{
    class Initiator
    {
        LoginForm loginForm;
        static AdminForm adminForm;
        static StudentForm studentForm;
        static AttendanceForm attendanceForm;
        static LibrarianForm librarianForm;

        public static AdminForm AdminForm { get => adminForm;  }
        public static LibrarianForm LibrarianForm { get => librarianForm; }
        public static StudentForm StudentForm { get => studentForm; }

        public Initiator()
        {            
            loginForm = new LoginForm();
            if (adminForm == null)
            {
                adminForm = new AdminForm();
            }
            if (studentForm == null)
            {
                studentForm = new StudentForm();
            }
            if (librarianForm == null)
            {
                librarianForm = new LibrarianForm();
            }
            //if (attendanceForm == null)
            //{
            //    attendanceForm = new AttendanceForm();
            //}

        }
        public void ShowLogin()
        {      
            DialogResult dr = loginForm.ShowDialog();
            if (loginForm.Current_login != "0")
            {
                loginForm.Close();
                if (loginForm.Login_type == 0)
                {
                    MonitorServer ms = new MonitorServer();
                    ms.initializeServer();
                    ShowAdmin();
                }
                else if (loginForm.Login_type == 1)
                {
                    ShowLibrarian();
                }
                else if (loginForm.Login_type == 2)
                {
                    ShowStudent();
                }
                
                
            }
        }
        public void ShowAdmin()
        {
            AdminForm.Show();
            
            
            
        }
        public void ShowLibrarian()
        {
            librarianForm.Show();

        }
        public void ShowStudent()
        {
            studentForm.Show();

        }
        public void ShowAttendance()
        {
            attendanceForm = new AttendanceForm();
            attendanceForm.Show();
        }
    }
}
