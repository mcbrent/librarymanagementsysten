﻿/*===========================================================================================
 * 
 *  Copyright (C)   : Advanced Card System Ltd
 * 
 *  File            : Acr1281UC1.cs
 * 
 *  Description     : Contains Methods and Properties related to ACR1281U-C1 device's functionality
 * 
 *  Author          : Arturo Salvamante
 *  
 *  Date            : October 19, 2011
 * 
 *  Revision Traile : [Author] / [Date if modification] / [Details of Modifications done] 
 * 
 * =========================================================================================*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Acs;
using Acs.Readers.Pcsc;

namespace Acs
{
    public enum CHIP_TYPE
    {
        UNKNOWN = 0,
        MIFARE_1K = 1,
        MIFARE_4K = 2,
        MIFARE_ULTRALIGHT = 3,
    }

    public enum KEY_STRUCTURE
    {
        VOLATILE = 0x00,
        NON_VOLATILE = 0x20
    }

    public enum KEYTYPES
    {
        ACR122_KEYTYPE_A = 96,
        ACR122_KEYTYPE_B = 97,
    }

    public class ReaderFunctions : PcscReader
    {
        public byte[] getCardSerialNumber()
        {
            byte[] cardSerial;

            apduCommand = new Apdu();
            apduCommand.setCommand(new byte[] {  0xFF,      //Intruction Class
                                                 0xCA,      //Intruction Code
                                                 0x00,      //Parameter 1
                                                 0x00,      //Parameter 2
                                                 0x00 });   //Parameter 3
            apduCommand.lengthExpected = 20;
            sendCommand();

            if (apduCommand.statusWord[0] != 0x90)
                return null;

            cardSerial = new byte[apduCommand.response.Length];
            Array.Copy(apduCommand.response, cardSerial, cardSerial.Length);

            return cardSerial;

        }

        public byte[] getAnswerToSelect()
        {
            apduCommand = new Apdu();
            apduCommand.setCommand(new byte[] {  0xFF, 
                                                 0xCA, 
                                                 0x01, 
                                                 0x00, 
                                                 0x00 });

            apduCommand.lengthExpected = 50;

            sendCommand();

            if (!apduCommand.statusWordEqualTo(new byte[] { 0x90, 0x00 }))
                throw new CardException("Unable to get Answer to Select (ATS)", apduCommand.statusWord);

            return apduCommand.response;
        }

        public bool loadAuthKey(byte[] key, byte keyNumber)
        {

            if (keyNumber > 0x01)
                throw new Exception("Key number is invalid");

            if (key.Length != 6)
                throw new Exception("Invalid key length");


            apduCommand = new Apdu();
            apduCommand.setCommand(new byte[] { 0xFF, 0x82, 0x00, keyNumber, 0x06 });
            apduCommand.data = key;

            sendCommand();

            if (!apduCommand.statusWordEqualTo(new byte[] { 0x90, 0x00 }))
                return false;

            return true;
        }

        public bool authenticate(byte blockNum, KEYTYPES keyType, byte KeyNum)
        {

            if (KeyNum < 0x00 && KeyNum > 0x20)
                throw new Exception("Key number is invalid");

            apduCommand = new Apdu();
            apduCommand.setCommand(new byte[] { 0xFF, 0x86, 0x00, 0x00, 0x05 });
            apduCommand.data = new byte[] { 0x01, 0x00, (byte)blockNum, (byte)keyType, KeyNum };

            sendCommand();

            if (!apduCommand.statusWordEqualTo(new byte[] { 0x90, 0x00 }))
                return false;

            return true;
        }
    }
}
