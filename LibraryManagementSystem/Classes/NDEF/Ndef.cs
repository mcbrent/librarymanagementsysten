﻿/*===========================================================================================
 * 
 *  Author          : Arturo Salvamante
 * 
 *  File            : NDEF.cs
 * 
 *  Copyright (C)   : Advanced Card System Ltd
 * 
 *  Description     : Contains Methods and Properties for NDEF
 * 
 *  Date            : June 03, 2011
 * 
 *  Revision Traile : [Author] / [Date if modification] / [Details of Modifications done]
 * 
 * 
 * =========================================================================================*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Acs;

namespace NDEF
{
    public enum TYPE_NAME_FORMAT
    {
        EMPTY = 0x00,
        NFC_WELL_KNOWN_TYPE = 0x01,
        MEDIA_TYPE = 0x02,
        ABSOLUTE_URI = 0x03,
        NFC_FORM_EXTERNAL_TYPE = 0x04,
        UNKNOWN = 0x05
    }

    public enum ACTION_RECORD
    {
        DO_THE_ACTION = 0x00,
        SAVE_FOR_LATER = 0x01,
        OPEN_FOR_EDITING = 0x02
    }

    public enum URI_IDENTIFIER
    {
        NONE = 0x00,
        HTTP_WWW = 0x01,
        HTTPS_WWW = 0x02,
        HTTP = 0x03,
        HTTPS = 0x04,
        TEL = 0x05,
        MAIL_TO = 0x06,
        FTP_ANONYMOUS = 0x07,
        FTP_FTP = 0x08,
        FTPS = 0x09,
        SFTP = 0x0A,
        SMB = 0x0B,
        NFS = 0x0C,
        FTP = 0x0D,
        DAV = 0x0E,
        NEWS = 0x0F,
        TELNET = 0x10,
        IMAP = 0x11,
        RTSP = 0x12,
        URN = 0x13,
        POP = 0x14,
        SIP = 0x15,
        SIPS = 0x16,
        TFTP = 0x17,
        BTSPP = 0x18,
        BTL2CAP = 0x19,
        BTGOEP = 0x1A,
        TCPOBEX = 0x1B,
        IRDAOBEX = 0x1C,
        FILE = 0x1D,
        URNEPCID = 0x1E,
        URNEPCTAG = 0x1F,
        URNEPCRAW = 0x21,
        URNEPC = 0x22,
        URNNFC = 0x23
    }

     public class NdefMessage
    {
        List<NdefRecord> _ndefRecords = new List<NdefRecord>();

        public void appendRecord(NdefRecord record)
        {
            _ndefRecords.Add(record);
        }

        public void insertRecord(int index, NdefRecord record)
        {
            _ndefRecords.Insert(index, record);
        }

        public List<NdefRecord> getRecords()
        {
            return _ndefRecords;
        }

        public NdefRecord getRecord(NdefRecordType recordType)
        {
            foreach (NdefRecord record in _ndefRecords)
            {
                if (record.recordType.typeName == recordType.typeName &&
                   record.recordType.typeNameFormat == recordType.typeNameFormat)
                {
                    return record;
                }
            }

            return null;
        }

        public int getNumberOfRecords()
        {
            return _ndefRecords.Count;
        }

        public byte[] toByteArray()
        {
            byte[] buffer = new byte[0];
            byte[] tmpArray;
            int indx = 0;

            for (int i = 0; i < _ndefRecords.Count; i++)
            {
                indx = buffer.Length;

                if (i == 0)
                    _ndefRecords[i].messageBegin = true;

                if ((i + 1) == _ndefRecords.Count)
                    _ndefRecords[i].messageEnd = true;
                    

                tmpArray = _ndefRecords[i].encodeToNdef();

                //Resize destination array to acoomodate new record
                Array.Resize(ref buffer, buffer.Length + tmpArray.Length);
                
                //Copy new  ndef record to byte array
                Array.Copy(tmpArray, 0, buffer, indx, tmpArray.Length);
            }

            return buffer;
        }        
    }

    public class NdefRecordType
    {
        string _typeName;
        TYPE_NAME_FORMAT _typeNameFormat;

        public NdefRecordType(TYPE_NAME_FORMAT format, string name)
        {
            _typeNameFormat = format;
            _typeName = name;
        }
        
        public TYPE_NAME_FORMAT typeNameFormat
        {
            get { return _typeNameFormat; }
            set { _typeNameFormat = value; }
        }
        
        public string typeName
        {
            get { return _typeName; }
            set 
            {
                if (value.Trim().Length > 255)
                    throw new Exception("Pay load type is too long");

                _typeName = value.Trim();
            }
        }
    }

    public class NdefRecord
    {
        private NdefRecordType _recordType;
        private bool _messageBegin;
        private bool _messageEnd;
        private bool _isTerminatingRecordChunk;        
        private byte[] _payLoad;
        private byte[] _messageID = new byte[0];
        private const int PAYLOAD_MAX_LENGTH = 256;

        public NdefRecord(NdefRecordType recordType)
        {
            _messageBegin = false;
            _messageEnd = false;
            _isTerminatingRecordChunk = true;
            _recordType = recordType;
        }
        
        public bool messageBegin
        {
            get { return _messageBegin; }
            set { _messageBegin = value; }
        }
        
        public bool messageEnd
        {
            get { return _messageEnd; }
            set { _messageEnd = value; }
        }

        public bool isTerminatingRecordChunk
        {
            get { return _isTerminatingRecordChunk; }
            set { _isTerminatingRecordChunk = value; }
        }
        
        public bool isShortRecord
        {
            get
            {
                if (payLoad.Length < PAYLOAD_MAX_LENGTH) 
                    return true;
                else
                    return false;
            }
        }
        
        public NdefRecordType recordType
        {
            get { return _recordType; }
            set { _recordType = value; }
        }        

        public byte[] payLoad
        {
            get { return _payLoad; }
            set { _payLoad = value; }
        }

        public string payLoadStr
        {
            get { return ASCIIEncoding.ASCII.GetString(_payLoad); }
            set { _payLoad = ASCIIEncoding.ASCII.GetBytes(value);
            }
        }
        
        public byte[] messageID
        {
            get { return _messageID; }
            set { _messageID = value; }
        }
        
        public byte[] encodeToNdef()
        {
            byte ndefHeader = 0;
            byte[] ndef = new byte[0];

            //Set NDEF Header
            if (messageBegin)
                ndefHeader |= 0x80;

            if (messageEnd)
                ndefHeader |= 0x40;

            if (!isTerminatingRecordChunk)
                ndefHeader |= 0x20;

            if (payLoad.Length < PAYLOAD_MAX_LENGTH)
                ndefHeader |= 0x10;

            if (messageID.Length > 0)
                ndefHeader |= 0x08;

            ndefHeader |= (byte)recordType.typeNameFormat;

            ndef = Helper.appendArrays(ndef, ndefHeader);

            //Set Payload Type Length
            ndef = Helper.appendArrays(ndef, (byte)recordType.typeName.Length);

            //Set Payload Length
            if (payLoad.Length < PAYLOAD_MAX_LENGTH)
                ndef = Helper.appendArrays(ndef, (byte)payLoad.Length);
            else
                ndef = Helper.appendArrays(ndef, Helper.intToByte(payLoad.Length));

            //Set Message ID Length
            if (messageID != null && messageID.Length > 0)
                ndef = Helper.appendArrays(ndef, (byte)messageID.Length);

            //Set Payload Type
            ndef = Helper.appendArrays(ndef, ASCIIEncoding.ASCII.GetBytes(recordType.typeName.Trim()));

            //Set Message ID
            if (messageID != null && messageID.Length > 0)
                ndef = Helper.appendArrays(ndef, messageID);

            //Set Payload
            ndef =  Helper.appendArrays(ndef, payLoad);
            
            return ndef;
        }
        
        public void appendPayload(byte[] payLoad)
        {
            int indx = 0;

            if (_payLoad == null)
                _payLoad = new byte[0];

            indx = _payLoad.Length;

            Array.Resize(ref _payLoad, _payLoad.Length + payLoad.Length);

            Array.Copy(payLoad, 0, _payLoad, indx, payLoad.Length);
        }

        public void appendPayload(string payLoad)
        {
            int indx = 0;
            byte[] buffer;

            if (payLoad == "")
                return;

            if (_payLoad == null)
                _payLoad = new byte[0];

            indx = _payLoad.Length;

            buffer = ASCIIEncoding.ASCII.GetBytes(payLoad);

            Array.Resize(ref _payLoad, _payLoad.Length + buffer.Length);

            Array.Copy(buffer, 0, _payLoad, indx, buffer.Length);
        }

        public void appendPayload(byte payLoad)
        {
            appendPayload(new byte[] { payLoad });
        }

        public NdefMessage getNestedNdefMessage()
        {
            if (payLoad == null)
                throw new Exception("Payload is not yet been set");

            return getNestedNdefMessage(0, payLoad);
        }

        public static NdefMessage getNestedNdefMessage(int index, byte[] data)
        {
            NdefMessage ndefMessage = new NdefMessage();
            NdefRecord ndefRecord;
            byte typeNameFormat = 0x00;
            byte typeNameLength = 0x00;
            byte idLength = 0x00;

            int payloadLength = 0x00;
            int currentIndex = 0;

            string typeName = "";

            bool isMessageBeginSet = false;
            bool isMessageEndSet = false;
            bool isIdLengthPresent = false;
            bool isChunkFlagPresent = false;
            bool isShortRecordSet = false;


            if (data.Length <= index)
                throw new Exception("Invalid index");

            for (; index < data.Length;)
            {
                currentIndex = index;

                //Get Type Name Format
                //e.g. NFC Well Know Type = 0x01
                typeNameFormat = (byte)(data[currentIndex] & 0x07);

                if (typeNameFormat != 0x01 && typeNameFormat != 0x02)
                    throw new Exception("Type Name Format " + ((TYPE_NAME_FORMAT)(typeNameFormat)).ToString() + " is not supported.");
                
                //Check if Message Begin (Bit 7) is set 
                if ((data[currentIndex] & 0x80) != 0x00)
                    isMessageBeginSet = true;
                else
                    isMessageBeginSet = false;

                //Check if Message End (Bit 6) is set
                if ((data[currentIndex] & 0x40) != 0x00)
                    isMessageEndSet = true;
                else
                    isMessageEndSet = false;

                //Check if Chunk Flag (Bit 5) is set
                if ((data[currentIndex] & 0x20) != 0x00)
                    isChunkFlagPresent = true;
                else
                    isChunkFlagPresent = false;

                //Check if Short Record (bit 4) is set
                if ((data[currentIndex] & 0x10) != 0x00)
                    isShortRecordSet = true;
                else
                    isShortRecordSet = false;

                //Check if ID length is set
                if ((data[currentIndex] & 0x08) != 0x00)
                    isIdLengthPresent = true;
                else
                    isIdLengthPresent = false;

                currentIndex += 1;

                //get the type length
                //Refer for Short Record section of NFC Data Exchange Format
                //Technical Specification for more information
                typeNameLength = data[currentIndex];
                currentIndex += 1;

                if (isShortRecordSet)
                {
                    //For Short Record payload, length is 1 byte
                    //Get Payload Length    
                    payloadLength = data[currentIndex];
                    currentIndex += 1;
                }
                else
                {
                    //For Non Short Record payload, length is 4 bytes
                    payloadLength = Helper.byteToInt(data.Skip(currentIndex).Take(4).ToArray());
                    currentIndex += 4;
                }

                if (isIdLengthPresent)
                {
                    //+1 to get ID Length
                    idLength = data[currentIndex];
                    currentIndex += 1;
                }

                //+1 to get Payload Type Name
                //Payload Type Name offset = currentIndex to currentIndex  + Type Name Length
                typeName = ASCIIEncoding.ASCII.GetString(data.Skip(currentIndex).Take(typeNameLength).ToArray());
                currentIndex += typeNameLength;
                
                //Initialize new ndef record object
                ndefRecord = new NdefRecord(new NdefRecordType((TYPE_NAME_FORMAT)typeNameFormat, typeName));
                ndefRecord.messageBegin = isMessageBeginSet;
                ndefRecord.messageEnd = isMessageEndSet;
                ndefRecord.isTerminatingRecordChunk = !isChunkFlagPresent;

                //If ID Length is present get record ID
                if (isIdLengthPresent)
                {
                    ndefRecord.messageID = data.Skip(currentIndex).Take(idLength).ToArray();
                    currentIndex += idLength;
                }

                ndefRecord.payLoad = data.Skip(currentIndex).Take(payloadLength).ToArray();
                currentIndex += payloadLength;
                index = currentIndex;

                ndefMessage.appendRecord(ndefRecord);
            }

            return ndefMessage;
        }
    }

    public class Ndef
    {
        /// <summary>
        /// Encode the following paramter for NDEF SmartPoster
        /// </summary>
        /// <param name="titleLanguage"></param>
        /// <param name="title"></param>
        /// <param name="uriPrefix"></param>
        /// <param name="uri"></param>
        /// <param name="action"></param>
        /// <returns></returns>
        public static byte[] encodeSmartPoster(string titleLanguage, string title, URI_IDENTIFIER uriPrefix, string uri, ACTION_RECORD? action)
        {
            byte[] buffer = new byte[0];
            bool useUtf8 = true;
            byte statusByte = 0;

            NdefRecord tmpRecord;
            NdefMessage ndefMessage = new NdefMessage();

            //Encode Title
            if (title.Trim() != "")
            {
                //If UTF 8 set bit 7 to 0
                //If UTF 16 set bit 7 to 1
                if(!useUtf8)
                    statusByte = 0x80;
                   
                //Length of the Language (ISO/IANA language code)
                //ex. "en-US"
                statusByte |= (byte)titleLanguage.Length;

                tmpRecord = new NdefRecord(new NdefRecordType(TYPE_NAME_FORMAT.NFC_WELL_KNOWN_TYPE, "T"));
                tmpRecord.messageBegin = true;
                tmpRecord.appendPayload(statusByte);
                tmpRecord.appendPayload(titleLanguage);
                tmpRecord.appendPayload(title);

                ndefMessage.appendRecord(tmpRecord);
            }

            
            //Encode Action            
            if (action != null && action.HasValue)
            {
                tmpRecord = new NdefRecord(new NdefRecordType(TYPE_NAME_FORMAT.NFC_WELL_KNOWN_TYPE, "act"));
                tmpRecord.appendPayload((byte)action);
                ndefMessage.appendRecord(tmpRecord);
            }


            //Encode URI
            tmpRecord = new NdefRecord(new NdefRecordType(TYPE_NAME_FORMAT.NFC_WELL_KNOWN_TYPE, "U"));

            
            //URI Prefix
            //See URI Record Type Definition 
            //http://www.nfc-forum.org
            tmpRecord.appendPayload((byte)uriPrefix);
            tmpRecord.appendPayload(uri);

            ndefMessage.appendRecord(tmpRecord);

            buffer = ndefMessage.toByteArray();

            //Smart Poster Header
            tmpRecord = new NdefRecord(new NdefRecordType(TYPE_NAME_FORMAT.NFC_WELL_KNOWN_TYPE, "Sp"));
            tmpRecord.messageBegin = true;
            tmpRecord.messageEnd = true;
            tmpRecord.appendPayload(buffer);

            buffer = tmpRecord.encodeToNdef();

            //Create NDEF Message for SmartPoster
            ndefMessage = new NdefMessage();
            ndefMessage.appendRecord(tmpRecord);

            //Encode NDEF Message
            buffer = ndefMessage.toByteArray();

            return buffer;            
        }

        public static string getURIIdentifierCode(URI_IDENTIFIER code)
        {
            //For more information please refer to
            // URI Record Type Definition Technical Specification
            switch (code)
            {
                case URI_IDENTIFIER.HTTP: return "http://";
                case URI_IDENTIFIER.HTTPS: return "https://";
                case URI_IDENTIFIER.HTTP_WWW: return "http://www.";
                case URI_IDENTIFIER.HTTPS_WWW: return "https://www.";
                case URI_IDENTIFIER.TEL: return "tel:";
                case URI_IDENTIFIER.MAIL_TO: return "mailto:";
                case URI_IDENTIFIER.FTP: return "ftp://";
                case URI_IDENTIFIER.NONE: return "";
                default: return "Unknown";
            }
        }
    }
}
