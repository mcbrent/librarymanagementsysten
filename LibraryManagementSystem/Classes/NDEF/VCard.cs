﻿/*===========================================================================================
 * 
 *  Author          : Arturo Salvamante
 * 
 *  File            : VCard.cs
 * 
 *  Copyright (C)   : Advanced Card System Ltd
 * 
 *  Description     : Contains Methods and Properties related vCard v3.0 (Media Type RFC 2426) 
 * 
 *  Date            : June 28, 2011
 * 
 *  Revision Traile : [Author] / [Date if modification] / [Details of Modifications done]
 * 
 * 
 * =========================================================================================*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Drawing;
using System.Drawing.Imaging;

namespace Acs.SmartCard.Reader.Pcsc.Nfc
{
    public enum ADDRESS_TYPE
    {
        DOMESTIC = 0x01,
        INTERNATIONAL = 0x02,
        POSTAL = 0x04,
        PARCEL = 0x08,
        HOME = 0x10,
        WORK = 0x20,
        PREFERRED = 0x40
    }

    public enum TELEPHONE_NUMBER_TYPE
    {
        /// <summary>
        /// home telephone number
        /// </summary>
        HOME = 0x01,

        /// <summary>
        /// telephone number has voice messaging support
        /// </summary>
        MSG = 0x02,

        /// <summary>
        /// Work telephone number
        /// </summary>
        WORK = 0x03,

        /// <summary>
        /// preferred-use telephone number
        /// </summary>
        PREFERRED = 0x04,

        /// <summary>
        /// voice telephone number
        /// </summary>
        VOICE = 0x05,

        /// <summary>
        /// facsimile telephone number
        /// </summary>
        FAX = 0x06,

        /// <summary>
        /// cellular telephone number
        /// </summary>
        CELL = 0x07,

        /// <summary>
        /// video conferencing telephone number
        /// </summary>
        VIDEO = 0x08,

        /// <summary>
        /// paging device telephone number
        /// </summary>
        PAGER = 0x09,

        /// <summary>
        /// bulletin board system telephone number
        /// </summary>
        BBS = 0x0A,

        /// <summary>
        /// MODEM connected telephone number
        /// </summary>
        MODEM = 0x0B,

        /// <summary>
        /// car-phone telephone number
        /// </summary>
        CAR = 0x0C,

        /// <summary>
        /// ISDN service telephone number
        /// </summary>
        ISDN = 0x0D,

        /// <summary>
        /// personal communication services 
        /// </summary>
        PCS = 0x0E
    }

    public enum EMAIL_TYPE
    {
        INTERNET = 0x01,
        X400 = 0x02,
        PREFERRED = 0x04
    }

    public class IdentificationType
    {
        const string SEPERATOR = ";";

        private string _formattedName = "";
        private string _additionalName = "";
        private string _familyName = "";
        private string _nickName = "";
        private string _honorificPrefix = "";
        private string _honorificSuffix = "";
        private string _givenName = "";
        private DateTime? _birthdate = null;

        public string formattedName
        {
            get { return _formattedName; }
            set { _formattedName = value; }
        }
        
        public string givenName
        {
            get { return _givenName; }
            set { _givenName = value; }
        }
        
        public string additionalName
        {
            get { return _additionalName; }
            set { _additionalName = value; }
        }
        
        public string familyName
        {
            get { return _familyName; }
            set { _familyName = value; }
        }
        
        public string nickName
        {
            get { return _nickName; }
            set { _nickName = value; }
        }
        
        public string honorificPrefix
        {
            get { return _honorificPrefix; }
            set { _honorificPrefix = value; }
        }
        
        public string honorificSuffix
        {
            get { return _honorificSuffix; }
            set { _honorificSuffix = value; }
        }
        
        public DateTime birthDate
        {
            get { return _birthdate.Value; }
            set { _birthdate = value; }
        }

        public string getFormattedString()
        {
            string fString = "";
            string tempString = "";

            //FORMATTED NAME
            if (formattedName.Trim() != "")
            {
                tempString = VCard.escapeString(formattedName);

                fString += "FN:" + tempString;
            }

            //NAME
            //Family Name, Given Name, Additional Names, Honorific Prefixes, and Honorific Suffixes.
            //The text components are separated by the SEMI-COLON character (ASCII decimal 59). Individual text
            //components can include multiple text values (e.g., multiple Additional Names) separated by the COMMA character (ASCII decimal 44).            
            //fString += "\r\nN:";

            fString += "N:";

            //Family name
            fString += familyName;

            //Given name
            fString += ";" + givenName;

            //Additional name
            fString += ";" + additionalName;

            return fString;
        }

        public void loadData(byte[] data)
        {
            loadData(ASCIIEncoding.ASCII.GetString(data));
        }

        public void loadData(string dataAsString)
        {
            string[] vProperties = dataAsString.Split(new string[] { "\r\n" }, StringSplitOptions.RemoveEmptyEntries);
            string tempString = "";

            foreach (string property in vProperties)
            {
                tempString = property;

                tempString.TrimEnd();
                tempString.TrimStart();
                tempString.Trim(new char[] { '\0', '\n', '\r' });

                //Check if formatted Name
                if(property.Substring(0,2).ToLower() == "fn")
                {
                    formattedName = property.Substring(3);
                    continue;
                }

                //Check if Name
                if (property.Substring(0, 2).ToLower() == "n")
                {
                    formattedName = property.Substring(2);
                    continue;
                }
            }
        }
    }

    public class OrganizationalType
    {
        private string _title = "";
        private string _organizationName = "";
        private string _role;
        private string _agent;
        private List<string> _organizatinalUnit = new List<string>();

        /// <summary>
        /// To specify the job title, functional position or function of the object the vCard represents
        /// Ex. TITLE:Director\, Research and Development
        /// </summary>
        public string title
        {
            get { return _title; }
            set { _title = value; }
        }
        
        /// <summary>
        /// The type is based on the X.520 Organization Name
        /// and Organization Unit attributes. The type value is a structured type
        /// consisting of the organization name, followed by one or more levels
        /// of organizational unit names.
        /// 
        /// Ex. ORG:ABC\, Inc.;North American Division;Marketing
        /// </summary>        
        public string organizationName
        {
            get { return _organizationName; }
            set { _organizationName = value; }
        }

        /// <summary>
        /// To specify information concerning the role, occupation, or business category of the object the vCard represents.
        /// Ex. ROLE:Programmer
        /// </summary>
        public string role
        {
            get { return _role; }
            set { _role = value; }
        }

        /// <summary>
        ///  To specify information about another person who will
        ///  act on behalf of the individual or resource associated with the vCard.
        ///  
        ///  Type value: The default is a single vcard value. It can also be reset
        ///  to either a single text or uri value. The text value can be used to
        ///  specify textual information. The uri value can be used to specify
        ///  information outside of this MIME entity.
        /// </summary>
        public string agent
        {
            get { return _agent; }
            set { _agent = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public List<string> organizationalUnit
        {
            get { return _organizatinalUnit; }
            set { _organizatinalUnit = value; }
        }

        /// <summary>
        /// Add Division
        /// </summary>
        /// <param name="unitName"></param>
        public void addOrganizationalUnit(string unitName)
        {
            _organizatinalUnit.Add(unitName);
        }

        public string getFormattedString()
        {
            string fString = "";

            //TITLE
            fString = "TITLE:" + VCard.escapeString(title);

            //ROLE
            fString += "\r\nROLE:" + VCard.escapeString(role);

            //ORGANIZATION
            fString += "\r\nORG:" + VCard.escapeString(organizationName);

            //Divisions/Organizational Unit
            for (int i = 0; i < organizationalUnit.Count; i++)
                fString += ";" + VCard.escapeString(organizationalUnit[i]);

            if (agent.Trim() != "")
            {
                //AGENT - Currently Single Text/URI is only supported. Single vCard will be supported later
                fString += "\r\nAGENT;VALUE=text:" + agent;
            }

            return fString;
        }

    }

    public class AddressingType
    {
        private string _poBoxNumber = "";
        private string _extendedAddress = "";
        private string _streetAddress = "";
        private string _locality = "";
        private string _region = "";
        private string _postalCode = "";
        private string _country = "";
        private string _label = "";

        private List<ADDRESS_TYPE> _labelType = new List<ADDRESS_TYPE>();
        private List<ADDRESS_TYPE> _addressType = new List<ADDRESS_TYPE>();

        public string poBoxNumber
        {
            get { return _poBoxNumber; }
            set { _poBoxNumber = value; }
        }

        public string extendedAddress
        {
            get { return _extendedAddress; }
            set { _extendedAddress = value; }
        }

        public string streetAddress
        {
            get { return _streetAddress; }
            set { _streetAddress = value; }
        }

        public string locality
        {
            get { return _locality; }
            set { _locality = value; }
        }

        public string region
        {
            get { return _region; }
            set { _region = value; }
        }

        public string postalCode
        {
            get { return _postalCode; }
            set { _postalCode = value; }
        }

        public string country
        {
            get { return _country; }
            set { _country = value; }
        }

        /// <summary>
        /// To specify the formatted text corresponding to delivery address of the object the vCard represents.
        /// 
        /// </summary>
        public string label
        {
            get { return _label; }
            set { _label = value; }
        }

        /// <summary>
        /// The TYPE parameter values can include "dom" to indicate a domestic delivery address; "intl" to indicate an
        /// international delivery address; "postal" to indicate a postal delivery address; "parcel" to indicate a parcel delivery address;
        /// "home" to indicate a delivery address for a residence; "work" to indicate delivery address for a place of work; and "pref" to indicate
        /// the preferred delivery address when more than one address is specified.
        /// 
        /// Value: AddressType 
        /// You can ORed AddressType to specify mutiple value
        /// </summary>
        public List<ADDRESS_TYPE> labelType
        {
            get { return _labelType; }
            set { _labelType = value; }
        }

        /// <summary>
        /// The TYPE parameter values can include "dom" to indicate a domestic delivery address; "intl" to indicate an
        /// international delivery address; "postal" to indicate a postal delivery address; "parcel" to indicate a parcel delivery address;
        /// "home" to indicate a delivery address for a residence; "work" to indicate delivery address for a place of work; and "pref" to indicate
        /// the preferred delivery address when more than one address is specified.
        /// 
        /// Value: AddressType 
        /// You can ORed AddressType to specify mutiple value
        /// </summary>
        public List<ADDRESS_TYPE> addressType
        {
            get { return _addressType; }
            set { _addressType = value; }
        }

        public void addLabelType(ADDRESS_TYPE addType)
        {
            _labelType.Add(addType);
        }

        public void addAddressType(ADDRESS_TYPE addType)
        {
            _addressType.Add(addType);
        }

        public string getFormattedString()
        {
            const string SEPERATOR = ";";
            string fString = "";


            //ADDRESS
            fString = "ADR";

            //PO Box Number
            fString += poBoxNumber;

            //Extended Address
            fString += SEPERATOR + VCard.escapeString(extendedAddress);

            //Street Address
            fString += SEPERATOR + VCard.escapeString(streetAddress);

            //locality
            fString += SEPERATOR + VCard.escapeString(locality);

            //Region
            fString += SEPERATOR + VCard.escapeString(region);

            //Postal Code
            fString += SEPERATOR + postalCode;

            //Country
            fString += SEPERATOR + VCard.escapeString(country);

            //LABEL
            if (label != "")
            {
                fString += "\r\nLABEL;";

                //Address type
                if (labelType.Count > 0)
                {
                    fString += "TYPE=";

                    for (int i = 0; i < labelType.Count; i++)
                    {
                        fString += getAddressTypeAsString(labelType[i]);

                        if ((i + 1) < labelType.Count)
                            fString += ",";
                    }

                    fString += ":";
                }

                //Label 
                fString += VCard.escapeString(label);
            }

            return fString;

        }

        private string getAddressTypeAsString(ADDRESS_TYPE addType)
        {
            switch (addType)
            {
                case ADDRESS_TYPE.DOMESTIC: return "dom";
                case ADDRESS_TYPE.HOME: return "home";
                case ADDRESS_TYPE.INTERNATIONAL: return "intl";
                case ADDRESS_TYPE.PARCEL: return "parcel";
                case ADDRESS_TYPE.POSTAL: return "postal";
                case ADDRESS_TYPE.PREFERRED: return "pref";
                case ADDRESS_TYPE.WORK: return "work";
                default: return "home";
            }
        }
    }

    public class TelecommunicationType
    {
        private string _telephoneNumber = "";
        private string _emailAddress = "";
        private string _mailerName = "";
        private List<TELEPHONE_NUMBER_TYPE> _telephoneNumberType = new List<TELEPHONE_NUMBER_TYPE>();
        private List<EMAIL_TYPE> _emailType = new List<EMAIL_TYPE>();

        public TelecommunicationType()
        {
            _telephoneNumberType.Add(TELEPHONE_NUMBER_TYPE.VOICE);
            _emailType.Add(EMAIL_TYPE.INTERNET);
        }

        public string telephoneNumber
        {
            get { return _telephoneNumber; }
            set { _telephoneNumber = value; }
        }

        public string emailAddress
        {
            get { return _emailAddress; }
            set { _emailAddress = value; }
        }

        public string mailerName
        {
            get { return _mailerName; }
            set { _mailerName = value; }
        }

        public List<EMAIL_TYPE> emailType
        {
            get { return _emailType; }
            set { _emailType = value; }
        }

        public List<TELEPHONE_NUMBER_TYPE> telephoneNumberType
        {
            get { return _telephoneNumberType; }
            set { _telephoneNumberType = value; }
        }

        public void addEmailType(EMAIL_TYPE emailType)
        {
            if (_emailType.Contains(emailType))
                return;

            _emailType.Add(emailType);
        }

        public void addTelephoneNumberType(TELEPHONE_NUMBER_TYPE telType)
        {
            if (_telephoneNumberType.Contains(telType))
                return;

            _telephoneNumberType.Add(telType);
        }

        public string getFormattedString()
        {
            string fString = "";

            #region TELEPHONE

            if (telephoneNumber.Trim() != "")
            {
                fString += "TEL;";

                //Telephone Number type

                fString += "TYPE=";

                for (int i = 0; i < telephoneNumberType.Count; i++)
                {
                    fString += getTelphoneNumberTypeAsString(telephoneNumberType[i]);

                    if ((i + 1) < telephoneNumberType.Count)
                        fString += ",";
                }

                fString += ":";

                //Telephone Number
                fString += telephoneNumber;
            }

            #endregion

            #region EMAIL

            if (emailAddress.Trim() != "")
            {

                fString += "\nEMAIL;";

                //Email Type
                fString += "TYPE=";

                for (int i = 0; i < emailType.Count; i++)
                {
                    fString += getEmailTypeAsString(emailType[i]);

                    if ((i + 1) < emailType.Count)
                        fString += ",";
                }

                fString += ":";

                //Email Address
                fString += emailAddress;
            }

            #endregion

            #region MAILER

            if (mailerName != "")
            {
                fString += "\nMAILER:" + mailerName;
            }

            #endregion

            return fString;
        }

        private string getTelphoneNumberTypeAsString(TELEPHONE_NUMBER_TYPE telphoneNumberType)
        {
            switch (telphoneNumberType)
            {
                case TELEPHONE_NUMBER_TYPE.BBS: return "bss";
                case TELEPHONE_NUMBER_TYPE.CAR: return "car";
                case TELEPHONE_NUMBER_TYPE.CELL: return "cell";
                case TELEPHONE_NUMBER_TYPE.FAX: return "fax";
                case TELEPHONE_NUMBER_TYPE.HOME: return "home";
                case TELEPHONE_NUMBER_TYPE.ISDN: return "isdn";
                case TELEPHONE_NUMBER_TYPE.MODEM: return "modem";
                case TELEPHONE_NUMBER_TYPE.MSG: return "msg";
                case TELEPHONE_NUMBER_TYPE.PAGER: return "pager";
                case TELEPHONE_NUMBER_TYPE.PCS: return "pcs";
                case TELEPHONE_NUMBER_TYPE.PREFERRED: return "pref";
                case TELEPHONE_NUMBER_TYPE.VIDEO: return "video";
                case TELEPHONE_NUMBER_TYPE.VOICE: return "voice";
                case TELEPHONE_NUMBER_TYPE.WORK: return "work";
                default: return "voice";
            }
        }

        private string getEmailTypeAsString(EMAIL_TYPE emailType)
        {
            switch (emailType)
            {
                case EMAIL_TYPE.INTERNET: return "internet";
                case EMAIL_TYPE.PREFERRED: return "pref";
                case EMAIL_TYPE.X400: return "x400";
                default: return "internet";
            }
        }
    }

    public class ExplanatoryType
    {
        private string _url;
        string fString = "";

        public string url
        {
            get { return _url; }
            set { _url = value; }
        }

        public string getFormattedString()
        {            
            //URL
            fString = "URL:" + url;

            return fString;
        }
    }

    class VCard
    {
        //private string firstname

        public static string escapeString(string text)
        {
            return escapeString(text, new char[] { ',', ';' });
        }

        public static string escapeString(string text, char[] characters)
        {
            string parsedString = "";
            string tempString = "";

            if (text == "")
                return text;

            if (characters == null || characters.Length < 1)
                return text;

            parsedString = text;

            for (int c = 0; c < characters.Length; c++)
            {
                tempString = parsedString;

                for (int indx = 0; indx < tempString.Length;)
                {
                    if (tempString[indx] == characters[c])
                    {
                        tempString = tempString.Insert(indx, @"\");
                        indx += 2;
                    }
                    else
                    {
                        indx++;
                    }
                }

                parsedString = tempString;
            }

            return parsedString;
        }
    }
}
